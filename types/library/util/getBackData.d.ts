/**
 * 获取回调数据
 *
 * @param {*} dataFn 数据方法 (默认返回dataFn,如果)
 * @param {Array} arr 参数列表
 * @param {Boolean} isDefalut 如果传入的不是Promise对象是否把内空加到default属性上
 * @param {*} context this上下文
 * @return {Promise}
 */
export default function getBackData(dataFn: any, arr?: any, isDefalut?: boolean, context?: any): Promise<any>;
