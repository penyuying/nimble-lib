import { IKeyValue } from '../helpers/IKeyValue';
/**
 * 拷贝当前对象已有的属性
 *
 * @export
 * @param {Object} target 默认对象
 * @param {Object} rest 被拷贝的对象
 * @returns {Object} target
 */
declare function copyProper(isUndefined: boolean, ...args: IKeyValue[]): any;
declare function copyProper(...args: IKeyValue[]): any;
export default copyProper;
