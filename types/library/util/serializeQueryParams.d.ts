import { IKeyValue } from '../helpers/IKeyValue';
/**
 * 参数序列化(返回不？或&)
 *
 * @param {Object} params 参数对象
 * @param {String} perFix 前缀
 * @returns {String} 序列化完的url参数
 */
export default function serializeQueryParams(params: IKeyValue, perFix?: string): string;
