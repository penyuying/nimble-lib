import { IKeyValue } from './../helpers/IKeyValue';
/**
 * 从URL中获取参数
 * @param {String} key 参数名（不指定则返回所有参数组成的对象）
 * @param {String} url 链接
 * @param {Boolean} noToText 不转换参数
 * @returns {Object | type} 返回的参数
 */
export default function getParams(key?: string, url?: string, noToText?: boolean): IKeyValue | string;
