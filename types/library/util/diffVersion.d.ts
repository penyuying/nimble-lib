/**
 * 比较两个版本
 *
 * @export
 * @param {String} nowVer 当前版本
 * @param {String} ver 比较的版本
 * @returns {-1|0|1} 返回-1：当前版本小;0：和当前版本相等;1：当前版本大
 */
export default function diffVersion(nowVer: string | number, ver: string | number): number;
