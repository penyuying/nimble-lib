/**
 * 将用"-"分隔和小驼峰转成大驼峰
 *
 * @export
 * @param {string} text 需要转换的文本
 * @returns {string}
 */
export default function toPascal(text: string, splitStr?: string): string;
