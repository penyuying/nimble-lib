/**
 * 获取父级有可能出现滚动条的元素
 * @param { HTMLElement } el 当前元素
 * @return {HTMLElement|window}
 */
export default function scrollParent(el: HTMLElement): any;
