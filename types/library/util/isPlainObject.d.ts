/**
 * 判断是否为纯对象
 *
 * @param {*} obj
 * @returns
 */
export default function isPlainObject(obj: any): boolean;
