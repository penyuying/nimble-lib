/**
 * 获取文件扩展名
 *
 * @export
 * @param {*} path 文件路径
 */
export default function getFileExt(path: string): string;
