import { IEvent } from '../helpers/IEvent';
/**
 * 初始化数据对象
 *
 * @export
 * @param {*} type 事件类型
 * @return {Event}
 */
export default function initEvent(type: string): IEvent;
