/**
 * 设置Cookie
 *
 * @param {string} key 设置的Cookie名称
 * @param {string} value 设置的Cookie值
 * @param {number|{
 *   time?: Number,
 *   domain?: string
 * }} options 设置Cookie失效时间或domain
 * @memberof StorageClient
 */
export default function setCookie(key: string, value: any, options?: number | {
    time?: number;
    domain?: string;
}): void;
