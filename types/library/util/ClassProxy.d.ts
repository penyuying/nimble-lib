export declare class ClassProxy {
    _watchNames: any[];
    constructor();
    /**
     * 调用代理监听方法
     * @param {*} obj 代理对象
     * @param {*} proxyObj 代理的源对像
     * @private
     */
    private _handlerWatch;
    /**
     * 遍历方法名
     * @param {*} proxyNames 代理的方法名称列表
     * @param {*} cb 回调
     * @return {Array<string>}
     * @private
     */
    private _eachProxyNames;
    /**
     * 代理方法钩子
     *
     * @param {*} obj 代理对象
     * @param {*} getObjCb 获取对象的回调
     * @param {*} proxyNames 代理的方法名称列表
     * @param {*} proxyWatchNames 代理的监听方法名称列表
     * @memberof classProxy
     * @public
     */
    proxyHook(obj: object, getObjCb: () => Promise<object>, proxyNames: string[] | string, proxyWatchNames: string[] | string): void;
    /**
     * 代理监听方法
     * @param {*} obj 代理对象
     * @param {*} proxyNames 代理的方法名称列表
     * @private
     */
    private proxyWatch;
    /**
     * 初始化代理
     *
     * @param {*} loadCb 加载源对象的回调
     * @param {*} fnName 代理方法名
     * @returns {Function}
     * @memberof classProxy
     * @public
     */
    initProxy(loadCb: () => Promise<object>, fnName: string): (...args: any[]) => Promise<{}>;
}
