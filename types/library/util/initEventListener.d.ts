/**
 * 监听和取消监听事件
 * @param {HtmlElement} el 事件监听dom元素
 * @param {String} types 注册的事件名称
 * @param {Function} func 事件回调方法
 * @param {add|remove} flag 事件方法
 * @param {Object|Boolean} capture 事件注册参数
 */
export default function initEventListener(el: HTMLElement, types: string | string[], func: Function, flag?: 'add' | 'remove', capture?: any): void;
