import { IStorageLocal } from '../../modules/storageClient/interface/storage';
export declare class LStorage implements IStorageLocal {
    setLocal(key: string, value: any): void;
    getLocal<T>(key: string): T;
    removeLocal(key: string): void;
    clearLocal(): void;
}
declare const _default: LStorage;
export default _default;
