/**
 * 派自定义DOM事件
 *
 * @param {HTMLElement|Window|Document} target 事件元素
 * @param {String} eventType 事件类型
 * @param {Object} options 事件选项
 * @memberof EventModule
 * @export
 */
export default function dispatchEvent(target: any, eventType: string, options: any): void;
