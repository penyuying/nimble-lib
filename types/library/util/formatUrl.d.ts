/**
 * 格式化url
 *
 * @param {string} [url] url
 * @returns {String}
 */
export default function formatUrl(url?: string): string;
