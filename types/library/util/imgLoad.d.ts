/**
 * 图片加载完后回调后的返回数据结构
 */
interface IImgLoadBack {
    /**
     * 加载完成的图片高度
     */
    naturalHeight: number;
    /**
     * 加载完成的图片宽度
     */
    naturalWidth: number;
    /**
     * 加载完成的图片地址
     */
    src: string;
}
/**
 * 图片加载
 *
 * @export
 * @param {String} src 图片地址
 * @param {Function} startCb 开始前的回调(返回true则不加载)
 * @returns {Promise}
 */
export default function imgLoad(src: string, startCb: (image: HTMLImageElement, src: string) => boolean | void): Promise<IImgLoadBack>;
export { IImgLoadBack };
