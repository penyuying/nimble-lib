declare module 'fetch-jsonp' {
    /**
     * jsonp的可选参数
     * @param {Number} timeout 超时时间
     * @param {String} jsonpCallback 回调方法
     */
    export interface IOptions {
        timeout?: number;
        jsonpCallback?: string;
    }
    export default function(url: RequestInfo, options?: IOptions): Promise<Response>;
}
