import { IKeyValue } from '../helpers/IKeyValue';
/**
 * 过滤字符串前后空格
 * @param {any} data 需要过滤的数据
 * @return {any}
 */
export default function stringTirm(data: IKeyValue | string | Array<IKeyValue | string>): string | IKeyValue<any> | (string | IKeyValue<any>)[];
