/// <reference path="../../../src/library/util/system.d.ts" />
import { IDevice } from './browser/interface/browser';
import { IKeyValue } from '../helpers/IKeyValue';
/**
 * 过滤原生设备调用
 *
 * @export
 * @param {*} options 选项
 * @param {*} device 设备信息
 * @return {Boolean}
 */
export default function nativeDeviceFilter(options: IKeyValue, device?: IDevice, type?: string): boolean;
