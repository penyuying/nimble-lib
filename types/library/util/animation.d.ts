/**
 * 动画
 * @param {Function} cb 回调
 * @param {Number} time 间隔时长
 * @param {Number} duration 总时长
 * @param {Number} end 结束位置
 */
export default function animation(cb: (time: Number) => void, time: number, duration: number, end: number): void;
