import { IDevice } from './interface/browser';
/**
 * 硬件设备
 *
 * @returns {IBrowserDevice} IBrowserDevice
 * @memberof BrowserService
 */
declare const deviceInfo: IDevice;
export default deviceInfo;
