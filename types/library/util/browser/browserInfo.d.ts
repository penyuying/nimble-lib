/// <reference path="../../../../src/library/util/browser/interface/miniprogram.d.ts" />
import { IBrowser } from './interface/browser';
/**
 * 浏览器内核版本
 *
 * @returns {IBrowserVersions} IBrowserVersions
 * @memberof BrowserService
 */
declare const browserInfo: IBrowser;
export default browserInfo;
