/**
 * 给css3样式加前缀
 *
 * @export
 * @param {any} type 事件名称
 * @param {Boolean} isEvent 是否为事件
 * @param {Element} [el = window] 事件元素
 * @returns {String} 加过前缀的样式
 */
export default function prefixEvent(type: string, isEvent?: boolean, el?: any): string;
