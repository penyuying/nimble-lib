/**
 * 获取字符串长度(中文为两个字节)
 *
 * @param {string} [str=''] 默认为空
 * @returns {number}
 * @memberof StorageCore
 */
export default function getBLen(str?: string): number;
