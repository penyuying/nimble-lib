/**
 * 监听元素渲染
 *
 * @param {HTMLElement} el 元素
 * @param {Function} callback 完成后的回调
 * @param {Object} options 完成后的回调
 * @param {Number} options.count 次数
 * @param {Number} options.time 每次的间隔时间
 */
export default function onRender(el: HTMLElement, callback: Function, options: {
    count: number;
    time: number;
}): void;
