/// <reference path="../../../src/library/util/system.d.ts" />
/**
 * 获取同盾BlackBox
 *
 * @export
 * @param {*} opts 选项
 * @returns {String}
 */
export default function getBlackBox(opts: {
    uuid: string;
    partner: string;
    appName: string;
}): any;
