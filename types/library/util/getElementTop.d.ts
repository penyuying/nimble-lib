/**
 * 获取元素距离顶边距离
 *
 * @export
 * @param {Element} elem 元素
 * @returns {Number} 顶部距离
 */
export default function getElementTop(elem: any): any;
