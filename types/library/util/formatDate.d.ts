/**
 * 格式化时间
 *
 * @param {Date} _date 时间对象
 * @param {string} [fmt='yyyy-MM-dd hh:mm:ss'] 时间的格式
 * @returns {string}
 */
export default function formatDate(_date: Date | string | number, fmt?: string): string;
