/**
 * 获取统一的回调的名称
 * @param {String} preFix 前缀
 * @returns {String}
 */
export default function genGlobalBackName(preFix: string): string;
