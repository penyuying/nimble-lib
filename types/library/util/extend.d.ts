/**
 * 拷贝对象
 *
 * @export
 * @param {Object} target 默认对象(如果为boolean的时候flase为浅拷贝，true为深拷贝，默认为深拷贝)
 * @param {Object} args 被拷贝的对象
 * @returns {Object} target
 */
declare function extend<T extends (object | null), U extends (object | null), V extends (object | null), W extends (object | null)>(target: boolean, source: T, source1: U, source2: V, source3: W): T & U & V & W;
declare function extend<T extends (object | null), U extends (object | null), V extends (object | null)>(target: boolean, source: T, source1: U, source2: V): T & U & V;
declare function extend<T extends (object | null), U extends (object | null)>(target: boolean, source: T, source1: U): T & U;
declare function extend<T extends (object | null)>(target: boolean, ...args: T[]): T;
declare function extend<T extends (object | null), U extends (object | null), V extends (object | null), W extends (object | null)>(source: T, source1: U, source2: V, source3: W): T & U & V & W;
declare function extend<T extends (object | null), U extends (object | null), V extends (object | null)>(source: T, source1: U, source2: V): T & U & V;
declare function extend<T extends (object | null), U extends (object | null)>(source: T, source1: U): T & U;
declare function extend<T extends (object | null)>(target: boolean, source: T): T;
declare function extend<T extends (object | null)>(...args: T[]): T;
export default extend;
