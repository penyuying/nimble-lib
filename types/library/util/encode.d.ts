/**
 * 编码
 *
 * @export
 * @param {*} str 编码的字符串
 * @returns {String}
 */
export default function encode(str: string): string;
