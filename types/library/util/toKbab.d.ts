/**
 * 将驼峰转成用"-"分隔
 *
 * @export
 * @param {string} text 需要转换的文本
 * @param {string} splitStr 分隔符
 * @returns {string}
 */
export default function toKbab(text: string, splitStr?: string): string;
