/**
 * 获取当前元素所在的滚动条元素
 * @param {Element} el 当前元素
 * @returns {Element}
 */
export default function getScrollEl(el: HTMLElement): any;
