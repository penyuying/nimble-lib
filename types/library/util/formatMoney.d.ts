/**
 * 格式化价格
 * @export
 * @param {number} value 金额
 * @param {string|number} args 保留小数位数
 * @returns {string} 格式化后的价格
 */
export default function formatMoney(value: number | string, args: number | string, prefix?: string): string;
