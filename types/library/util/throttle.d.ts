/**
 * 回调节流、防抖
 *
 * @export
 * @param {*} action 回调
 * @param {*} delay 等待的时间
 * @param {*} context this上下文
 * @param {Boolean} iselapsed 是否节流
 * @returns {Function}
 */
export default function throttle(action: (...args: any[]) => void, delay: number, context?: any, iselapsed?: boolean): (...args: any[]) => void;
