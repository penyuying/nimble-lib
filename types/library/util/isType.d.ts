/**
 * 判断类型
 *
 * @export
 * @param {*} obj
 * @param {string} str
 * @returns
 */
export default function isType(obj: any, str: string): boolean;
