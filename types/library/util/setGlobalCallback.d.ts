/**
 * 设置全局回调
 *
 * @param {*} fnName 回调的名称
 * @param {*} fn 回调的方法
 * @param {*} flag 当前注册全局方法的维一标识
 * @param {Boolean} [once = true] 是否只调用一次
 * @param {Object} [global = window] 需要挂载的对象
 */
export default function setGlobalCallback(fnName: string, fn: Function, flag?: any, once?: boolean, global?: object): void;
