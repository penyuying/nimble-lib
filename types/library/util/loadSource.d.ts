import { IKeyValue } from './../helpers/IKeyValue';
/**
 * 加载资源
 *
 * @export
 * @param {String} src 资源地址
 * @param {String} attr 添加的属性
 * @param {String} type 资源类型
 * @returns {Promise}
 */
export default function loadSource(src: string, attr?: 'js' | 'css' | string | IKeyValue, type?: 'js' | 'css' | string): Promise<Event>;
