/**
 * 判断浏览器是否支持设置passive事件监听器
 * @returns {Boolean} 返回浏览器是否支持passive事件监听器的flag
 */
declare const isPassive: boolean;
export default isPassive;
