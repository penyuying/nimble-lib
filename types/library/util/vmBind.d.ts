/**
 * 双向绑定值的装饰器
 */
export default function VmBind(options?: {
    get?: Function;
    set?: Function;
}): Function;
