/**
 * 给css3样式加前缀
 *
 * @export
 * @param {any} style 样式名称
 * @param {Boolean} isFix 是否返回前缀
 * @returns {String} 加过前缀的样式
 */
export default function prefixStyle(style: string, isFix?: boolean): string;
