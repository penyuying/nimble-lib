/**
 * 浅比较2个对象的值是否相等
 * @param {Object} defObj 比较对象
 * @param {Object} obj 被比较对象
 * @param {Array<string>?} keys 需要比较的key列表
 * @returns {Boolean} 对象比较结果
 */
export default function diffData(defObj: any, obj: any, keys?: string[]): boolean;
