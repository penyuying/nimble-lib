/**
 * 生成uuid
 *
 * @return {String} UUID
 * @export
 */
export default function generateUUID(): string;
