/**
 * 将用"-"分隔转成小驼峰
 *
 * @export
 * @param {string} text 需要转换的文本
 * @returns {string}
 */
export default function toCamel(text: string, splitStr?: string): string;
