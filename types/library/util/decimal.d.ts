/**
 * 格式化数字小数位
 * @export
 * @param {number} value 需要格式化的数字
 * @param {(string|number)} [args] 格式(如：1.2-3)
 * @returns {string}
 */
export default function decimal(value: number | string, args: number | string): string;
