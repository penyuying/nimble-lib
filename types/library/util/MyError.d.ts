/**
 * 自定义错误
 */
export default class MyError extends Error {
    constructor(message: string, options?: object);
}
