/**
 * 创建转换成dom元素
 *
 * @param {String} html html文本
 * @param {Object} data 数据对象
 * @returns {Node} DOM节点/元素
 */
export default function genDom(html: string): DocumentFragment;
