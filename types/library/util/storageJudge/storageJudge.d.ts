/**
 * 判断浏览器对Storage的支持
 *
 * @export
 * @class Browser
 */
export declare class StorageJudge {
    /**
     * 是否支持SessionStorage
     * @returns {Boolean} true|false
     * @memberof BrowserStorage
     */
    isSessionStorage(): Boolean;
    /**
     * 是否支持LocalStorage
     * @returns {Boolean} true|false
     * @memberof BrowserStorage
     */
    isLocalStorage(): Boolean;
}
declare const storageJudge: StorageJudge;
export { storageJudge };
