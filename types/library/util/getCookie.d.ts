/**
 * 获取cookie
 *
 * @export
 * @param {string} key cookie的key
 * @returns {(string | null)}
 */
export default function getCookie(key: string): string | null;
