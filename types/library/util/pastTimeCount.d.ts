/**
 * 计算两个时间之前的相差时间
 *
 * @param {String} startTime 开始时间
 * @param {String} endTime 结束时间
 * @return {Number} 返回用掉的时间
 */
export default function pastTimeCount(startTime: any, endTime: any): number;
