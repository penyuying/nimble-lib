import { IKeyValue } from '../../IKeyValue';
export interface IRouterLiveCofig {
    /**
     * 页面名称
     */
    pageName?: string;
    /**
     * 是否不使用路由
     */
    notRouter?: boolean;
    /**
     * url参数的key
     */
    queryKey?: string;
    /**
     * 标题
     */
    title?: string;
    /**
     * 状态数据
     */
    state?: IKeyValue;
    /**
     * 后退页数
     */
    goBack?: number;
    /**
     * 过滤放入的参数方法
     */
    getUrlParams?(params: IRouterLiveCofig): any;
}
