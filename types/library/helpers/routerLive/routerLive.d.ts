import { BaseAbstract } from '../../basic/base.abstrat';
import { IRouterLiveCofig } from './interface/routerLiveConfig';
export declare class RouterLive extends BaseAbstract<IRouterLiveCofig> {
    defaultOption: IRouterLiveCofig;
    constructor(options: IRouterLiveCofig);
    /**
     * 跳转页面
     * @param {Object} params url参数
     * @param {String} url url参数
     *
     * @return {String}
     */
    toPage(params: IRouterLiveCofig, url?: string): string | null | undefined;
    /**
     * 回退页面
     *
     * @param {Object} params url参数
     * @return {Promise}
     * @memberof CarBase
     */
    goBackPage(params: IRouterLiveCofig): Promise<string | null>;
}
export { IRouterLiveCofig };
