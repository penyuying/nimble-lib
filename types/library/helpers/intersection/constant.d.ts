import { IItersectionDefaultConfig } from './interface/intersectionConfig';
/**
 * 默认配置参数
 */
export declare let INTERSECTION_DEFAULT_CONFIG: IItersectionDefaultConfig;
/**
 * Intersection事件类型
 */
export declare enum INTERSECTION_EVENT_TYPE {
    UNOBSERVE = "unobserve"
}
