import { IItersectionConfig, IItersectionDefaultConfig, IScrollElementItem } from './interface/intersectionConfig';
import { ListenerQueue } from '../listenerQueue/ListenerQueue';
/**
 * Scroll模式加载
 *
 * @export
 * @class ScrollIntersection
 * @extends {LazyBase}
 */
export declare class ScrollIntersection extends ListenerQueue<IItersectionConfig, IScrollElementItem> {
    /**
     * 默认数据
     */
    protected defaultOption: IItersectionDefaultConfig;
    /**
     * 滚动条元素列表
     */
    protected scrollList: IScrollElementItem[];
    /**
     * Creates an instance of ScrollIntersection.
     * @param {Object} options 选项
     * @param {Boolean} [options.intersectionPattern=hasIntersectionObserver] 是否为IntersectionObserver模式
     * @param {String|Array<String>} [options.listenEvents='scroll'] 是否为IntersectionObserver模式
     * @memberof ScrollIntersection
     */
    constructor(options: IItersectionConfig);
    /**
     * 初始化
     *
     * @private
     * @memberof ScrollIntersection
     */
    private _init;
    /**
     * 设置视图区域大小
     * @return {Object}
     */
    private _setViewSize;
    /**
     * 执行队列
     */
    private _listenerHandler;
    /**
     * 事件执行队列
     */
    private _eventListenerHandler;
    /**
     * 加载图片
     * @param {Object} opts 选项
     */
    private _loadHandler;
    /**
     * 注册或解除dom元素的事件监听器
     * @param {HtmlElement} el 注册/解除事件的监听器的元素
     * @param {Boolean} isAdd 解除/注册元素的事件监听器标志
     */
    private _initListener;
    /**
     * 添加滚动条节点列表
     *
     * @param {HTMLElement} el 当前img元素
     * @param {IListener} listener 当前项
     * @memberof ImgLazy
     */
    private _addScrollList;
    /**
     * 移除滚动节点列表
     * @param {IListener} listener 当前的img元素的监听器
     * @memberof ImgLazy
     */
    private _removeScrollList;
}
