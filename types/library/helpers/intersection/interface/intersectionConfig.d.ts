import { IViewRegionConfig } from '../../viewRegion/interface/viewRegionConfig';
/**
 * 滚动交叉区域选项
 */
export interface IItersectionConfig extends IViewRegionConfig {
    /**
     * 默认监听的事件
     */
    listenEvents?: IListenEvent[] | IListenEvent;
    /**
     * 执行队列的等待时间(防抖)
     */
    throttleWait?: number;
    /**
     * IntersectionObserver模式移出视图是否执行回调
     */
    isShiftOut?: boolean;
    /**
     * IntersectionObserver参数
     */
    observerOptions?: {
        /**
         *  边距
         */
        rootMargin?: string;
        threshold: 0 | 1;
    };
}
/**
 * 默认配置
 */
export interface IItersectionDefaultConfig extends IItersectionConfig {
    /**
     * 默认监听的事件
     */
    listenEvents: IListenEvent[] | IListenEvent;
    /**
     * 执行队列的等待时间(防抖)
     */
    throttleWait: number;
    /**
     * IntersectionObserver模式移出视图是否执行回调
     */
    isShiftOut: boolean;
    /**
     * 加载屏视口宽度
     */
    viewWidth: number;
    /**
     * 加载屏视口高度
     */
    viewHeight: number;
    /**
     * 预加载(屏)
     */
    preLoad: number;
    /**
     * IntersectionObserver参数
     */
    observerOptions: {
        /**
         * 边距
         */
        rootMargin?: string;
        threshold: 0 | 1;
    };
    /**
     * 底部距离顶部的最小距离（底部超过此距离才可加载）
     */
    preLoadTop: number;
}
/**
 * scroll模式时兼听的事件
 */
export declare type IListenEvent = 'scroll' | 'wheel' | 'mousewheel' | 'resize' | 'animationend' | 'transitionend' | 'touchmove';
/**
 * Scroll模式Scroll的元素数据
 *
 * @export
 * @interface IScrollElementItem
 */
export interface IScrollElementItem {
    /**
     * 元素
     */
    el: HTMLElement;
    /**
     * 子元素的数量
     */
    childrenCount: number;
}
