import { ListenerQueue } from '../listenerQueue/ListenerQueue';
import { IItersectionConfig, IItersectionDefaultConfig } from './interface/intersectionConfig';
/**
 * IntersectionObserver模式加载
 *
 * @export
 * @class Intersection
 * @extends {Intersection}
 */
export declare class Intersection extends ListenerQueue<IItersectionConfig, null> {
    /**
     * 默认数据
     */
    protected defaultOption: IItersectionDefaultConfig;
    /**
     * IntersectionObserver对象
     */
    protected _observer: null;
    /**
     * Creates an instance of Intersection.
     * @param {Object} options 选项
     * @param {Number} [options.viewWidth=window.innerHeight] 加载屏视口宽度
     * @param {Number} [options.viewHeight=window.innerWidth] 加载屏视口高度
     * @param {Number} [options.preLoad=1] 预加载(屏)
     * @param {Boolean} [options.intersectionPattern=hasIntersectionObserver] 是否为IntersectionObserver模式
     * @param {Object} [options.observerOptions={threshold: 0,rootMargin: '${y}px ${x}px ${y}px ${x}px'}] IntersectionObserver的选项
     * @memberof Intersection
     */
    constructor(options: IItersectionConfig);
    private _init;
    /**
     * 初始化 IntersectionObserver
     * @return {IntersectionObserver}
     */
    private _initIntersectionObserver;
    /**
     * IntersectionObserver事件回调
     * @param {Array} entries 监听的列表
     */
    private _loadObserver;
}
