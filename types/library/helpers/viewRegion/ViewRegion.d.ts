import { BaseAbstract } from '../../basic/base.abstrat';
import { IViewRegionConfig } from './interface/viewRegionConfig';
/**
 * 计算视图区域
 *
 * @export
 * @class ViewRegion
 * @extends {DefaultOption}
 */
export declare class ViewRegion<T extends object = IViewRegionConfig> extends BaseAbstract<T | IViewRegionConfig> {
    /**
     * 默认数据
     *
     * @memberof ViewRegion
     */
    protected defaultOption: IViewRegionConfig;
    /**
     * 当前元素
     *
     * @type {(Element|null)}
     * @memberof ViewRegion
     */
    content: Element | null;
    /**
     * 最后getBoundingClientRect的时间
     *
     * @type {number}
     * @memberof ViewRegion
     */
    private _lastRun;
    /**
     * 上次getBoundingClientRect的数据
     *
     * @type {*}
     * @memberof ViewRegion
     */
    private _rect;
    /**
     * Creates an instance of ViewRegion.
     * @param {*} el 观察的元素
     * @param {Object} options 选项
     * @param {Number} [options.viewWidth=window.innerWidth] 加载屏视口宽度
     * @param {Number} [options.viewHeight=window.innerHeight] 加载屏视口高度
     * @param {Number} [options.preLoad=1] 预加载(屏)
     * @param {Number} [options.preLoadTop=0] 底部距离顶部的最小距离
     * @memberof ViewRegion
     */
    constructor(el: Element, options: IViewRegionConfig);
    /**
     * 获取元素的位置
     * @param {Boolean} isReset 是否重新获取
     * @returns {Object}
     * @memberof ViewRegion
     */
    getRect(isReset: boolean): any;
    /**
     * 判断是否在加载的区域
     *
     * @param {Object} opts 选项
     * @return {Boolean}
     * @memberof ViewRegion
     */
    checkInView(opts?: IViewRegionConfig): boolean;
}
