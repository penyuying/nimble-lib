import { IKeyValue } from './IKeyValue';
/**
 * 挂载服务
 *
 * @export
 * @param {any} Vue vue
 * @param {any} source 对象源
 * @returns {strig} 返回对象的name名
 */
export default function mountService(Vue: Function, source: IKeyValue): string;
