import { IKeyValue } from './IKeyValue';
export declare class EventModule {
    protected _events: IKeyValue;
    /**
     * 派自定义DOM事件
     *
     * @param {HTMLElement} target 事件元素
     * @param {String} eventType 事件类型
     * @param {Object} options 事件选项
     * @memberof EventModule
     */
    $dispatchEvent(target: HTMLElement, eventType: string, options: any): void;
    /**
     * 监听自定义事件
     *
     * @param {String} type 事件类型
     * @param {Function} fn 回调
     * @param {this} [context=this] this指向
     * @memberof EventModule
     */
    $on(type: string, fn: Function, context?: this): void;
    /**
     * 绑定后执行一次就移除的事件
     *
     * @param {String} type 事件类型
     * @param {Function} fn 回调
     * @param {this} [context=this] this指向
     * @memberof EventModule
     */
    $once(type: string, fn: Function, context?: this): void;
    /**
     * 解绑事件
     *
     * @param {String} type 事件类型
     * @param {Function} fn 回调
     * @memberof EventModule
     */
    $off(type: string, fn: Function): void;
    /**
     * 派自定义事件
     *
     * @param {String} type 事件类型
     * @memberof EventModule
     */
    $emit(type: string, ...args: any[]): void;
}
