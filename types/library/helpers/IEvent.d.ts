/**
 * 键值对
 *
 * @export
 * @interface IKeyValue
 * @template T
 */
export interface IEvent extends Event {
    /**
     * 标识是否为自定义事件
     */
    _constructed?: boolean;
}
