import { LISTENER_STATE, EVENT_TYPE } from './constant';
import { BaseAbstract } from '../../basic/base.abstrat';
import { IListener, IListenerQueueStateType, IListenerQueueState } from './interface/listenerQueue';
/**
 * 懒加载基础队列
 *
 * @export
 * @class ListenerQueue
 * @extends {DefaultOption}
 */
export declare class ListenerQueue<T extends (object | null), S, U = IListenerQueueStateType> extends BaseAbstract<T> {
    listenerQueue: IListener<S, U>[];
    /**
     * Creates an instance of ListenerQueue.
     * @param {Object} options 选项
     * @memberof ListenerQueue
     */
    constructor(options: T);
    /**
     * 添加加载项到队列
     * @param {Object|Function} listener listener或回调
     * @return {Object}
     */
    addQueue(listener: IListener<S, U> | (() => IListener<S, U>)): {
        listener: IListener<S, U> | (() => IListener<S, U>);
        isUpdata: boolean;
    };
    /**
     * 更新队列加载项
     * @param {HTMLElement} content 当前img元素
     * @param {Object} data 选项
     * @param {Object|Function} genListener 生成listener的回调
     * @return {Object}
     */
    updateQueue(content: any, data: any, genListener: () => IListener<S, U>): {
        listener: IListener<S, U> | (() => IListener<S, U>);
        isUpdata: boolean;
    } | {
        listener: IListener<S, any>;
        isUpdata: boolean;
    };
    /**
     * 清理队列(已加载完成项)
     *
     * @memberof ImgLazy
     */
    clearQueue(): void;
    /**
     * 移除单项listener
     *
     * @memberof ImgLazy
     */
    removeItem(index: number, item?: IListener<S, U>): void;
    /**
     * 获取listener
     *
     * @param {HTMLElement} el 当前元素
     * @returns {Object}
     * @memberof ListenerQueue
     */
    getListener(el: HTMLElement): {
        listener: IListener<S, U>;
        index: number;
    } | undefined;
}
export { IListener, LISTENER_STATE, EVENT_TYPE, IListenerQueueStateType, IListenerQueueState };
