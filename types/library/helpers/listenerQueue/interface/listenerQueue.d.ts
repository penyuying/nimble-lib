/**
 * IListener对象
 *
 * @export
 * @interface IListener
 */
export interface IListener<S, T = IListenerQueueStateType> {
    /**
     * 内容
     */
    content: any;
    /**
     * 状态
     */
    state: T;
    /**
     * 更新方法
     */
    update?: (content: any, data: any) => void;
    /**
     * 加载
     * @param {any} options 选项
     * @param {boolean} refresh 队列刷新
     * @memberof IListener
     */
    display?: (options?: any, refresh?: boolean) => void;
    /**
     * 移出视图
     */
    shiftOut?: () => void;
    /**
     * 判断是否在视口区域加载
     * @param {any} options 选项
     * @param {boolean} refresh 队列刷新
     * @memberof IListener
     */
    viewDisplay?: (options?: any, refresh?: boolean) => boolean;
    /**
     * 销毁
     */
    destroyed?: () => void;
    /**
     * 是否删除
     */
    isDel?: boolean;
    /**
     * 滚动条元素数据
     */
    _$scroll?: S;
    [key: string]: any;
}
/**
 * 状态（init:初始化；update: 更新）
 */
export declare type IListenerQueueStateType = 'init' | 'update';
/**
 * 状态类型
 */
export interface IListenerQueueState<T = IListenerQueueStateType> {
    /**
     * 状态
     */
    [key: string]: T;
}
