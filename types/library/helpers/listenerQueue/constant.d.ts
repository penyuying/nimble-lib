import { IListenerQueueState } from './interface/listenerQueue';
/**
 * 图片加载状态
 */
export declare const LISTENER_STATE: IListenerQueueState;
/**
 * 事件类型
 *
 * @export
 * @enum {number}
 */
export declare enum EVENT_TYPE {
    /**
     * Listener推入队列事件
     */
    PUSH = "pushListener",
    /**
     * 更新Listener事件
     */
    UPDATE = "updateListener",
    /**
     * 移除Listener事件
     */
    REMOVE = "removeListener",
    /**
     * 清理队列
     */
    CLEAR = "clearQueue"
}
