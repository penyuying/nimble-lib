export interface IFactoryUse<T, R, P> {
    (options: T): R;
    use?: (preset: P) => void;
    /**
     * use的参数列表
     */
    _useArgs?: Array<Array<P>>;
}
