import { IBaseInterface } from './base.interface';
import { EventModule } from '../helpers/EventModule';
export declare class BaseAbstract<T extends (object | null)> extends EventModule implements IBaseInterface {
    /**
     * 单例实例化
     */
    static _singleton: any;
    /**
     * 名称
     */
    name: string;
    /**
     * 默认选项
     */
    protected defaultOption: T;
    /**
     * 构造函数
     */
    constructor(options?: T);
    /**
     * 单例实例化
     *
     * @static
     * @param {{[key: string]: any}} [options] 实例化参数
     * @returns
     * @memberof BaseAbstract
     */
    static singleton<U>(options?: any): U;
    /**
     * 实例化
     *
     * @static
     * @param {{[key: string]: any}} [options] 实例化参数
     * @returns
     * @memberof BaseAbstract
     */
    static instance<U>(options?: any): U;
    /**
     * 安装
     *
     * @param {Vue} Vue Vue
     * @param {Object} options 选项
     * @memberof Service
     * @returns {Object}
     */
    static install<U>(Vue: any, options?: any): U;
    /**
     * 安装
     *
     * @param {Vue} Vue Vue
     * @param {Object} options 选项
     * @memberof Service
     * @returns {Object}
     */
    install(Vue: Function, options?: any): this;
    /**
     * 设置默认参数
     *
     * @protected
     * @param {Boolean|T} [target]
     * @param {T} [options]
     * @memberof BaseAbstract
     */
    protected setDefaultOptions<U extends T>(target: Boolean, options?: U): void;
    protected setDefaultOptions<U extends T>(target?: U): void;
    /**
     * 获取参数
     *
     * @protected
     * @param {...T[]} args
     * @memberof BaseAbstract
     */
    protected getOptions(target: Boolean, ...args: T[]): T;
    protected getOptions(...args: T[]): T;
    /**
     * 异步加载的时候预加载
     *
     * @returns {Promise}
     * @memberof BaseAbstract
     */
    preload(): Promise<{}>;
}
