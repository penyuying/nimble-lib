import aeAnimationFactory, { AeAnimation, IAeAnimationConfig, ILottieConfig, ILottieFile, ILottieOtherFile, ILottieFileJsons } from './aeAnimation.imp';
export default aeAnimationFactory;
export { AeAnimation, IAeAnimationConfig, ILottieConfig, ILottieFile, ILottieOtherFile, ILottieFileJsons };
