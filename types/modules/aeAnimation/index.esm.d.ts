import { IAeAnimationConfig, ILottieConfig, ILottieFile, ILottieOtherFile, ILottieFileJsons, IPreset, AeAnimation } from './aeAnimation.imp';
import { IFactoryUse } from '../../library/helpers/IFactoryUse';
/**
 * 实例化工厂方法
 *
 * @export
 * @param {IAeAnimationConfig} options 配置选项
 * @returns {AeAnimation}
 */
declare let fn: IFactoryUse<IAeAnimationConfig, AeAnimation, IPreset<ILottieConfig>>;
export default fn;
export { IAeAnimationConfig, ILottieConfig, ILottieFile, ILottieOtherFile, ILottieFileJsons };
