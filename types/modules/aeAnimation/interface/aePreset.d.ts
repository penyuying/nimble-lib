import { IKeyValue } from '../../../library/helpers/IKeyValue';
/**
 * zip文件数据
 */
export interface ILottieFile {
    /**
     * json文件内容
     */
    json: ILottieFileJsons[];
    /**
     * 图片路径列表
     */
    files: {
        [key: string]: ILottieOtherFile;
    };
}
/**
 * 其它文件数据
 *
 * @export
 * @interface ILottieOtherFile
 */
export interface ILottieOtherFile {
    /**
     * url
     */
    url: string;
    /**
     * 文件相对路径
     */
    path: string;
}
/**
 * jsons文件数据
 *
 * @export
 * @interface ILottieFileJsons
 */
export interface ILottieFileJsons {
    /**
     * url
     */
    url: string | IKeyValue;
    /**
     * 文件相对路径
     */
    path: string;
}
/**
 * 文件解析器
 *
 * @export
 * @interface IPreset
 */
export interface IPreset<T> {
    /**
     * 文件后缀名
     */
    ext: string | Array<string>;
    /**
     * 文件解析方法
     */
    preset: IPresetFn<T>;
}
/**
 * 文件解析方法
 *
 * @export
 * @interface IPresetFn
 */
export interface IPresetFn<T> {
    (path: string, options: T, ext: string): Promise<ILottieFile>;
}
