/// <reference path="../../../src/modules/aeAnimation/interface/ae.d.ts" />
import { IAeAnimationConfig, ILottieConfig, IAeAnimation } from './interface/aeAnimationConfig';
import { AeAnimationPreset } from './aeAnimation.preset';
import { ILottieFile, ILottieOtherFile, ILottieFileJsons, IPreset } from './interface/aePreset';
import { IFactoryUse } from '../../library/helpers/IFactoryUse';
/**
 * AE动画
 * @class AeAnimation
 */
export declare class AeAnimation extends AeAnimationPreset<IAeAnimationConfig> implements IAeAnimation {
    name: string;
    protected defaultOption: IAeAnimationConfig;
    constructor(options: IAeAnimationConfig);
    /**
     * 初始化ae动画
     * @param {string} src 动画文件地址
     * @param {HTMLElement|ILottieConfig} options 选项或el
     * @returns {Promise}
     * @memberof AeAnimation
     */
    load(src: string, options: HTMLElement | ILottieConfig): Promise<{}>;
    /**
     * 渲染AE动画到dom元素中
     * @param {Object} options 动画数据
     * @return {Promise}
     */
    aeJson(options: ILottieConfig): Promise<boolean>;
    /**
     * 过滤参数
     * @param {string} src 动画文件地址
     * @param {HTMLElement|ILottieConfig} options 选项或el
     * @return {Promise}
     */
    private _filterData;
    /**
     * 过滤json数据
     * @param {String} jsonData json字符串
     * @param {Object} files 文件列表
     * @returns {Promise}
     * @memberof AeAnimation
     */
    private _filtersJson;
    /**
     * 缓存数据
     * @param {*} key 缓存的key
     * @param {*} cb 数据的回调
     * @param {*} isReset 是否重置
     * @returns {Promise}
     * @memberof AeAnimation
     */
    private _cacheData;
    /**
     * 解析参数
     * @param {string} src 动画文件地址
     * @param {HTMLElement|ILottieConfig} options 选项或el
     * @returns {Object}
     * @memberof AeAnimation
     */
    private _parseOptions;
}
/**
 * 实例化工厂方法
 * @export
 * @param {IAeAnimationConfig} options 配置选项
 * @returns {AeAnimation}
 */
declare let aeAnimationFactory: IFactoryUse<IAeAnimationConfig, AeAnimation, IPreset<ILottieConfig>>;
export default aeAnimationFactory;
export { IAeAnimationConfig, ILottieConfig, ILottieFile, ILottieOtherFile, ILottieFileJsons, IPreset };
