/// <reference path="../../../src/modules/aeAnimation/interface/ae.d.ts" />
import { ILottieFile, IPreset } from './interface/aePreset';
import { BaseAbstract } from '../../library/basic/base.abstrat';
import { ILottieConfig } from './interface/aeAnimationConfig';
/**
 * AE动画Preset解析器
 * @class AeAnimationPreset
 */
export declare class AeAnimationPreset<T extends object> extends BaseAbstract<T> {
    static _presets: IPreset<ILottieConfig>[];
    constructor(options: T);
    /**
     * 扩展preset
     *
     * @static
     * @param {IPreset} preset 文件解析器
     * @returns
     * @memberof AeAnimationPreset
     */
    static use(preset: IPreset<ILottieConfig>): void;
    /**
     * 解析其它文件
     * @static
     * @param {string} path 文件路径
     * @param {string} ext 文件后缀名
     * @returns {Promise<ILottieFile>}
     * @memberof AeAnimation
     */
    static preset(path: string, options: ILottieConfig, ext: string): Promise<ILottieFile>;
}
