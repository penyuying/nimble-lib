import { IUserInfo } from '../interface/userConfig';
/**
 * 过滤用户信息数据
 *
 * @export
 * @param {*} userInfo 用户信息数据
 * @returns {Object}
 */
export default function filterUserInfoData(userInfo?: any): IUserInfo;
