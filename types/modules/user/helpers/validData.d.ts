import { IValidDataType } from '../interface/userConfig';
/**
 * 验证数据是否符合正则匹配
 * @param {IValidDataType} type 验证的类型
 * @param {String|Number|Undefined|Null} data 需要验证的数据
 * @returns {Boolean} 匹配结果
 * @memberof UserBase
 */
export default function validData(type: IValidDataType, data?: string | number | null): string;
