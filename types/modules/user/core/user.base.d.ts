import { IKeyValue } from '../../../library/helpers/IKeyValue';
import { BaseAbstract } from '../../../library/basic/base.abstrat';
import { IUserConfig, IUserInfo, IUserDataType, IValidDataType, IAppApiType } from '../interface/userConfig';
/**
 * 用户模块基础类
 *
 * @export
 * @class UserBase
 * @extends {DefaultOption}
 */
export declare class UserBase extends BaseAbstract<IUserConfig> {
    /**
     * 用户信息
     *
     * @protected
     * @type {(IUserInfo|null)}
     * @memberof UserBase
     */
    protected _userInfo: IUserInfo | null;
    /**
     * 当前用户信息(拿来作比较用的)
     *
     * @private
     * @memberof UserBase
     */
    private _currentUserInfo;
    /**
     * 登录状态
     *
     * @protected
     * @type {(boolean|null)}
     * @memberof UserBase
     */
    protected _loginState: boolean | null;
    /**
     * 当前选项
     *
     * @protected
     * @memberof UserBase
     */
    protected currentOption: IUserConfig;
    /**
     * 默认选项
     *
     * @protected
     * @memberof UserBase
     */
    protected defaultOption: IUserConfig;
    /**
     * 用户模块基础类
     * @param {IUserConfig} options 默认参数
     * @memberof UserBase
     */
    constructor(options: IUserConfig);
    /**
     * 重设置初始化配置
     *
     * @protected
     * @param {IUserConfig} [options]
     * @memberof UserBase
     */
    protected _resetInit(options?: IUserConfig): IUserConfig;
    /**
     * 重置数据
     * @memberof UserBase
     */
    protected _resetLogin(): void;
    /**
     * 获取数据
     *
     * @param {*} type 类型
     * @param {*} param 参数
     * @param {*} headers 请求头
     * @returns {Promise}
     * @memberof CarCore
     */
    protected _getData(type: IUserDataType, param?: IKeyValue, headers?: IKeyValue): Promise<any>;
    /**
     * 过滤传入的用户信息
     *
     * @param {*} userInfo 用户信息
     * @returns {Promise}
     * @memberof UserBase
     */
    protected _userInfoFilter(userInfo?: IKeyValue | null): Promise<IUserInfo>;
    /**
     * 用户信息获取完成
     *
     * @param {*} userInfo 车型数据
     * @returns {Promise}
     * @memberof UserCore
     */
    protected _successfilter(userInfo: IKeyValue | null): Promise<IUserInfo>;
    /**
     * 设置用户信息
     *
     * @param {*} userInfo 用户信息
     * @param {*} isNotLocal 是否不存储本地
     * @returns {Promise}
     * @memberof UserCore
     */
    protected _setUserInfo(userInfo: IUserInfo | null, isNotLocal?: boolean): Promise<IUserInfo>;
    /**
     * 调用原生
     * @param {String} type 原生接口名称
     * @param {Object} params 接口请求参数
     * @returns {Promise<Object|String>} 结果数据
     */
    protected _toNative(type: IAppApiType, params?: IKeyValue): Promise<any>;
    /**
     * 是否调用原生
     *
     * @param {String} type 调用类型
     * @return {Boolean}
     * @memberof UserBase
     */
    protected _isToNative(type: IAppApiType): any;
    /**
     * 验证数据是否符合正则匹配
     * @param {String|Number|Undefined|Null} data 需要验证的数据
     * @param {String} type 验证的类型
     * @returns {Boolean} 匹配结果
     * @memberof UserBase
     */
    validData(type: IValidDataType, data?: string | number | null): string;
    /**
     * 设置登录状态
     * @param {Boolean} state 登录状态
     */
    setLoginState(state: boolean): void;
    /**
     * 提示消息
     *
     * @param {*} msgCont 消息内容
     * @param {*} param 类型|参数
     * @param {toast|alert} param.type 类型
     * @returns {Promise}
     * @memberof CarBase
     */
    message(msgCont: string, param: {
        type: 'toast' | 'alert';
    }): Promise<any>;
    /**
     * 获取登录uuid
     *
     * @memberof UserBase
     */
    getUUID(): any;
    /**
     * 加载同盾apiJs
     * @return {Promise}
     */
    getBlackBox(): Promise<string>;
}
export { IUserConfig, IUserInfo, IUserDataType, IValidDataType, IAppApiType };
