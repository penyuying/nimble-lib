import { IKeyValue } from '../../../library/helpers/IKeyValue';
import { UserBase, IUserConfig, IUserInfo, IUserDataType, IValidDataType, IAppApiType } from './user.base';
import { ILoginConfig } from '../interface/userConfig';
/**
 * 用户模块核心
 *
 * @export
 * @class User
 * @extends {UserBase}
 */
export declare class UserCore extends UserBase {
    /**
     * 记录上次获取时间
     *
     * @protected
     * @memberof UserCore
     */
    protected _lastRun: number;
    /**
     * 登录的Promise
     *
     * @private
     * @type {(Promise<any>|null)}
     * @memberof UserCore
     */
    private _loginPromise;
    /**
     * 缓存获取用户信息接口
     *
     * @private
     * @type {(null|Promise<IUserInfo>)}
     * @memberof UserCore
     */
    private _getUserInfoPromise;
    /**
     * 缓存是否正在请求接口查询是否
     *
     * @private
     * @type {(null|Promise<boolean>)}
     * @memberof UserCore
     */
    private _getIsLoginPromise;
    /**
     * 用户模块核心
     * @param {IUserConfig} options 默认参数
     * @memberof UserCore
     */
    constructor(options: IUserConfig);
    /**
     * 获取是否登录
     *
     * @returns {Promise} 用户登录状态Promise
     * @memberof UserCore
     */
    protected _getIsLogin(): Promise<boolean>;
    /**
     * 设置用户登录Cookie
     * @param {String} usersession 用户session信息
     * @returns {Promise} 用户设置cookie的promise
     * @memberof UserCore
     */
    protected _setLoginCookie(usersession: string): Promise<any>;
    /**
     * 获取本地用户信息
     *
     * @returns {Promise}
     */
    protected _getLocalData(): Promise<IUserInfo | null>;
    /**
     * 获取用户信息结束
     * @param {Object} userInfo 用户信息
     * @param {Boolean} isApp 是否从app取的用户信息
     * @returns {Promise}
     * @memberof UserCore
     */
    protected _getUserInfoEnd(userInfo?: IKeyValue | null, isApp?: boolean): Promise<IUserInfo>;
    /**
     * 获取用户信息
     * @param {Object} options 选项
     * @param {IUserInfo} userInfo 用户信息
     * @memberof UserCore
     * @returns {Promise} 用户信息promise
     */
    protected _getUserInfo(options?: IUserConfig, userInfo?: {
        usersession?: string;
    } | null): Promise<IUserInfo>;
    /**
     * 去原生登录页
     *
     * @returns {Promise} 登录页promise
     */
    protected _toNativeLogin(): Promise<IUserInfo | null>;
    /**
     * 唤起h5登录页
     * @returns {Promise} 登录页promise
     */
    protected _toH5Login(options?: ILoginConfig): Promise<any>;
    /**
     * 登录
     * @param {Object} params 登录参数
     * @param {String} params.phone 手机号
     * @param {String} params.channel 设备标识
     * @param {String} params.code 验证码
     * @return {Promise<IUserInfo>}
     * @memberof User
     */
    signIn(params: {
        phone: string;
        code: string;
        event_id?: string;
    }): Promise<IUserInfo>;
    /**
     * 登录成功
     * @param {IKeyValue} userinfo 登录后接口返回的用户信息
     * @return {Promise<IUserInfo>}
     */
    protected _h5LoginSuccess(userinfo: IKeyValue): Promise<IUserInfo>;
    /**
     * 获取登录验证码
     *
     * @param {Object} params 请求参数
     * @param {String} params.phone 手机号
     * @return {Promise}
     * @memberof UserCore
     */
    getCode(params: {
        phone: string;
    }): Promise<{}>;
    /**
     * 唤起登录页
     * @returns {Promise} 唤起登录页的promise
     * @memberof User
     */
    login(options?: ILoginConfig): Promise<IUserInfo | null>;
    /**
     * 退出app登录
     */
    _appLogout(): Promise<{}>;
    /**
     * 退出登录
     * @param {Object} options 配置参数
     * @param {Boolean} options.forceClearCookie 是否强制使cookie信息失效
     * @returns {Promise} 退出登录promise对象
     */
    _logout(options?: IUserConfig): Promise<{}>;
}
export { UserBase, IUserConfig, IUserInfo, IUserDataType, IValidDataType, IAppApiType };
