import { UserCore, UserBase, IUserConfig, IUserInfo, IUserDataType, IValidDataType, IAppApiType } from './user.core';
/**
 * 用户模块核心
 *
 * @export
 * @class User
 * @extends {UserBase}
 */
export declare class UserThirdparty extends UserCore {
    /**
     * 登录的授权code
     */
    private _accessCode;
    /**
     * 微信授权Promise
     */
    private _wxAuthAccessPromise;
    /**
     * 微信登录授权失败
     */
    isAccessFailure: boolean;
    /**
     * 用户第三方授权登录模块
     * @param {IUserConfig} options 默认参数
     * @memberof UserCore
     */
    constructor(options: IUserConfig);
    /**
     * 授权code登录
     *
     * @param {string} code 授权Code
     * @returns
     * @memberof UserThirdparty
     */
    private _getAuthCode;
    /**
     * 获授权登录数据
     * @param {IUserConfig} options 选项
     */
    protected _getAccess(options?: IUserConfig): Promise<IUserInfo | null>;
    /**
     * 重置url
     */
    private _resetUrl;
    /**
     * 微信授权
     * @param {IUserConfig} options 选项
     */
    private _wxAuthorize;
    /**
     * 绑定并登录
     * @param {Object} params 登录参数
     * @param {String} params.phone 手机号
     * @param {String} params.channel 设备标识
     * @param {String} params.code 验证码
     * @return {Promise<IUserInfo>}
     */
    weChatBind(params: {
        phone: string;
        code: string;
    }): Promise<IUserInfo>;
    /**
     * 授权登录
     *
     * @memberof UserThirdparty
     */
    wxAuthorizeUserInfo(): void;
    /**
     * 静默登录
     *
     * @memberof UserThirdparty
     */
    wxAuthorize(): void;
}
export { UserCore, UserBase, IUserConfig, IUserInfo, IUserDataType, IValidDataType, IAppApiType };
