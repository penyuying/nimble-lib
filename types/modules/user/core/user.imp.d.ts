import { UserMiniProgram, UserThirdparty, UserCore, UserBase, IUserConfig, IUserInfo, IUserDataType, IValidDataType, IAppApiType } from './user.miniProgram';
/**
 * 用户模块
 *
 * @export
 * @class User
 * @extends {UserMiniProgram}
 */
export declare class User extends UserMiniProgram {
    name: string;
    /**
     * 用户模块
     * @param {IUserConfig} options 默认参数
     * @memberof User
     */
    constructor(options: IUserConfig);
    /**
     * 退出登录
     * @param {Object} options 配置参数
     * @param {Boolean} options.forceClearCookie 是否强制使cookie信息失效
     * @returns {Promise} 退出登录promise对象
     */
    logout(options?: IUserConfig): Promise<{}>;
    /**
     * 判断用户是否已经登录
     * @param {Object} options 配置参数
     * @param {Boolean} options.isReset 是否强制重置用户登录状态信息
     * @returns {Promise} 用户登录状态Promise
     * @memberof UserCore
     */
    isLogin(options?: IUserConfig): Promise<{}>;
    /**
     * 用户取消登录
     * @memberof User
     * @exports
     */
    loginCancel(): void;
    /**
     * 获取用户信息
     * @param {Boolean} force 是否强制登录
     * @param {Object} options 配置参数
     * @param {Boolean} options.force 是否强制唤起登录页
     * @param {Boolean} options.isReset 是否强制重置登录状态
     * @returns {Promise<Object>} 返回用户信息promise
     * @memberof User
     * @exports
     */
    getUserInfo(force: boolean | IUserConfig, options: IUserConfig): Promise<IUserInfo | null>;
    /**
     * 设置登录状态和设置或清除localstorage(兼容老的)
     * @param {Object|Undefined} userInfo 用户信息（当type＝add时，需传userInfo，否则可以不传）
     * @returns {Promise}
     */
    setUserInfo(userInfo: IUserInfo | null): Promise<{}>;
    /**
     * 绑定和手机号登录
     * @param {Object} params 登录参数
     * @param {String} params.phone 手机号
     * @param {String} params.channel 设备标识
     * @param {String} params.code 验证码
     * @return {Promise<IUserInfo>}
     */
    signInAndBind(params: {
        phone: string;
        code: string;
        event_id?: string;
    }): Promise<IUserInfo>;
}
/**
 * 实例化User
 *
 * @export
 * @param {*} args 参数
 * @returns {User}
 */
export default function userFactory(options: IUserConfig): User;
export { UserMiniProgram, UserThirdparty, UserCore, UserBase, IUserConfig, IUserInfo, IUserDataType, IValidDataType, IAppApiType };
