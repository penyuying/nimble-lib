import { UserThirdparty, UserCore, UserBase, IUserConfig, IUserInfo, IUserDataType, IValidDataType, IAppApiType } from './user.thirdparty';
/**
 * 小程序用户登录模块
 *
 * @export
 * @class UserMiniProgram
 * @extends {UserThirdparty}
 */
export declare class UserMiniProgram extends UserThirdparty {
    name: string;
    /**
     * 小程序登录参数
     */
    private _miniProgramLoginParams;
    /**
     * 小程序登录的Promise
     */
    private _miniProgramLoginPromise?;
    /**
     * 用户模块
     * @param {IUserConfig} options 默认参数
     * @memberof User
     */
    constructor(options: IUserConfig);
    /**
     * 获授权登录数据
     * @param {IUserConfig} options 选项
     */
    protected _miniProgramLogin(options?: IUserConfig): Promise<IUserInfo | null>;
    /**
     * 获取微信小程序登录信息
     * @param {IUserConfig} options 选项
     */
    private _getAuthInfo;
}
export { UserThirdparty, UserCore, UserBase, IUserConfig, IUserInfo, IUserDataType, IValidDataType, IAppApiType };
