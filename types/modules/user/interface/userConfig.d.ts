import { IStorageLocal } from '../../storageClient/interface/storage';
import { IDevice } from '../../../library/util/browser/interface/browser';
import { IKeyValue } from '../../../library/helpers/IKeyValue';
/**
 * 默认配置参数
 *
 * @export
 * @interface IUserConfig
 */
export interface IUserConfig extends IWxAuthorize, ILoginConfig {
    /**
     * 设置COOKIE时的domain
     */
    domain?: string;
    /**
     * 是否启用获取微信小程序登录信息
     */
    isMiniProgram?: boolean;
    /**
     * 同盾的partnerCode
     *
     * @type {string}
     * @memberof IUserConfig
     */
    partner?: string;
    /**
     * 同盾的app名称
     *
     * @type {string}
     * @memberof IUserConfig
     */
    appName?: string;
    /**
     * 标识
     *
     * @type {string}
     * @memberof IUserConfig
     */
    channel?: string;
    /**
     * 登录事件ID
     *
     * @type {string}
     * @memberof IUserConfig
     */
    event_id?: string;
    /**
     * 本地存储
     *
     * @type {IStorageLocal}
     * @memberof IUserConfig
     */
    storage?: IStorageLocal;
    /**
     * 硬件信息
     *
     * @type {IDevice}
     * @memberof IUserConfig
     */
    device?: IDevice;
    /**
     * 如果用户没有登录是否强制唤起登录页
     *
     * @type {boolean}
     * @memberof IUserConfig
     */
    force?: boolean;
    /**
     * 默认头像
     *
     * @type {string}
     * @memberof IUserConfig
     */
    userHeadImg?: string;
    /**
     * 是否强制重置
     *
     * @type {boolean}
     * @memberof IUserConfig
     */
    isReset?: boolean;
    /**
     * 是否和原生交互
     *
     * @type {boolean}
     * @memberof IUserConfig
     */
    isNative?: boolean;
    /**
     * 缓存登录状态时间
     *
     * @type {number}
     * @memberof IUserConfig
     */
    timeout?: number;
    /**
     * 是否强制使cookie信息失效
     *
     * @type {boolean}
     * @memberof IUserConfig
     */
    forceClearCookie?: boolean;
    /**
     * 需要比较的key
     *
     * @type {string[]}
     * @memberof IUserConfig
     */
    diffLoginKeys?: string[];
    /**
     * 调用app的方法
     *
     * @param {IAppApiType} type
     * @param {*} params
     * @returns {Promise<any>}
     * @memberof IUserConfig
     */
    actionWithNative?(type: IAppApiType, params: any): Promise<any>;
    /**
     * 获取接口数据的方法
     *
     * @param {IUserDataType} type 接口类型
     * @param {IKeyValue} [params] 参数
     * @param {IKeyValue} [headers] 请求header
     * @returns {Promise<any>}
     * @memberof IUserConfig
     */
    getUserData?(type: IUserDataType, params?: IKeyValue, headers?: IKeyValue): Promise<any>;
    /**
     * 验证输入
     *
     * @param {IValidDataType} type 验证类型
     * @param {(string|number|null)} [data] 数据
     * @returns {string}
     * @memberof IUserConfig
     */
    validData?(type: IValidDataType, data?: string | number | null): string;
    /**
     * 获取同盾设备信息
     *
     * @param {{
     *         uuid: string; // uuid
     *         partner: string; // 同盾的partnerCode
     *         appName: string; // 同盾的app名称
     *     }} opts 参数
     * @returns {Promise<string>}
     * @memberof IUserConfig
     */
    getBlackBox?(opts: {
        uuid: string;
        partner: string;
        appName: string;
    }): Promise<string>;
    /**
     * 比较数据的方法
     *
     * @param {*} defObj 比较的数据1
     * @param {*} obj 比较的数据2
     * @param {string[]} [keys] 需要比较的key列表
     * @returns {boolean}
     * @memberof IUserConfig
     */
    diffData?(defObj: any, obj: any, keys?: string[]): boolean;
    /**
     * 完成时过滤用户信息
     *
     * @param {IKeyValue} userInfo 用户信息
     * @returns {IUserInfo}
     * @memberof IUserConfig
     */
    successfilter?(userInfo: IKeyValue): IUserInfo;
    /**
     * 过滤用户信息
     *
     * @param {IKeyValue} userInfo
     * @returns {IUserInfo}
     * @memberof IUserConfig
     */
    filterUserInfoData?(userInfo: IKeyValue): IUserInfo;
    /**
     * 登录的唯一标识
     *
     * @returns {string}
     * @memberof IUserConfig
     */
    generateUUID?(): string;
    /**
     * 消息提示的回调
     *
     * @param {string} msgCont 消息内容
     * @param {{type?: 'confirm'}} [param] 选项
     * @memberof IUserConfig
     */
    message?(msgCont: string, param?: {
        type?: 'confirm';
    }): void;
    /**
     * 验证是否调用app
     *
     * @param {IUserConfig} options 选项
     * @param {IAppApiType} type app的api类型
     * @returns {boolean}
     * @memberof IUserConfig
     */
    isToNativeFilter?(options: IUserConfig, type: IAppApiType): boolean;
    /**
     * 获取wxjssdk
     */
    getWxSdk?(): any;
}
/**
 * 微信授权登录参数
 *
 * @export
 * @interface IThirdparty
 */
export interface IWxAuthorize {
    /**
     * 微信appid
     */
    appid?: string;
    /**
     * 授权完返回的页面
     */
    returnUrl?: string;
    /**
     * 授权作用域
     */
    scope?: string;
}
/**
 * 用户信息
 *
 * @export
 * @interface IUserInfo
 */
export interface IUserInfo {
    /**
     * 是否登录
     *
     * @type {boolean}
     * @memberof IUserInfo
     */
    islogin: boolean;
    /**
     * 用户id
     *
     * @type {string}
     * @memberof IUserInfo
     */
    userid?: string;
    /**
     * 手机号
     *
     * @type {string}
     * @memberof IUserInfo
     */
    phone?: string;
    /**
     * 用户名
     *
     * @type {string}
     * @memberof IUserInfo
     */
    name?: string;
    /**
     * 昵称
     *
     * @type {string}
     * @memberof IUserInfo
     */
    nickname?: string;
    /**
     * 用户session
     *
     * @type {string}
     * @memberof IUserInfo
     */
    usersession?: string;
    /**
     * 用户头像
     *
     * @type {string}
     * @memberof IUserInfo
     */
    headUrl?: string;
    /**
     * 获取数据的渠道
     *
     * @type {('app'|'h5'|'lightApp')}
     * @memberof IUserInfo
     */
    _localDataType?: 'app' | 'h5' | 'lightApp';
    /**
     * 登录的渠道
     *
     * @type {('app'|'h5'|'lightApp')}
     * @memberof IUserInfo
     */
    _loginType?: 'app' | 'h5' | 'lightApp';
    /**
     * 操作状态（cancel：取消）
     *
     * @type {'cancel'}
     * @memberof IUserInfo
     */
    _handleType?: 'cancel';
    /**
     * 是否要新用户库设置的用户信息
     */
    _newUser?: boolean;
}
export interface ILoginConfig {
    popup?: IKeyValue;
}
/**
 * 调用app的api(获取用户信息|唤起登录页|验证登录)
 */
export declare type IAppApiType = 'getUserInfo' | 'forceLogin' | 'validateLogin';
/**
 * 验证的类型（手机号｜验证码）
 */
export declare type IValidDataType = 'vPhone' | 'vCode';
/**
 * 获取数据（获取会员信息(用户头像)｜用户是否登录｜获取会员等级｜退出时失效Cookie｜
 * 用户登录签名｜设置用户登录Cookie｜获取验证码|同盾获取设备指纹|授权code登录|授权登录失败绑定登录）
 * 退出登录
 */
export declare type IUserDataType = 'GetMemberMallUserInfo' | 'IsLogin' | 'GetUserInfo' | 'LogoutCookie' | 'SignIn' | 'SetLoginCookie' | 'GetIdentityCode' | 'GetBlackBox' | 'authLogin' | 'AuthLoginBingding' | 'getAuthInfo' | 'SignOut';
/**
 * 事件类型（完成时候的事件｜定向页面|用户登录事件|用户取消登录事件|获取完用户信息|退出登录事件）
 * wx绑定登录|wx授权登录完成|获取微信小程序登录信息
 */
export declare type IEventType = 'success' | 'redirect' | 'userLogin' | 'userCancel' | 'getUserInfo' | 'logout' | 'weBindSuccess' | 'wxAuthSuccess' | 'miniProgramAuthInfo';
/**
 * 页面类型(登录页|途虎用户协议页面)
 */
export declare type IPageType = 'login' | 'agreement';
/**
 * 小程序登录参数
 */
export interface IMiniProgramUrlParams {
    token: string;
    sign: string;
    timestap: string;
    source: string;
    needlogin: 0 | 1;
}
/**
 * 小程序登录参数
 */
export interface IMiniProgramLoginParams {
    token: string;
    sign: string;
    timeStap: string;
    source: string;
    needLogin: 0 | 1;
}
