import { IKeyValue } from '../../library/helpers/IKeyValue';
import { IUserConfig, IAppApiType, IUserDataType, IEventType, IPageType } from './interface/userConfig';
export declare const DIFF_KEYS: string[];
/**
 * 默认配置参数
 * @type {IUserConfig}
 */
export declare const DEFAULT_CONFIG: IUserConfig;
/**
 * 调用app的api
 */
export declare const APP_API_TYPE: IKeyValue<IAppApiType>;
/**
 * 获取接口数据的类型
 */
export declare const USER_DATA_TYPE: IKeyValue<IUserDataType>;
/**
 * 事件类型
 */
export declare const EVENT_TYPE: IKeyValue<IEventType>;
/**
 * 本地存储的key
 */
export declare enum STORAGE_KEY {
    /**
     * 用户登录的uuid
     */
    LOGIN_UUID = "_login_uuid",
    /**
     * 用户信息的key
     */
    USER_STORAGE_KEY = "_tuhu_user_key",
    /**
     * 用户信息的key
     */
    USER_COOKIE_KEY = "__user_cookie_key_"
}
/**
 * 页面类型
 */
export declare const PAGE_TYPE: IKeyValue<IPageType>;
