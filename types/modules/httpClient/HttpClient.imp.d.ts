import 'rxjs/add/operator/publishReplay';
import { Observable } from 'rxjs/Observable';
import { HttpClientCore } from './HttpClient.core';
import { IHttpClientConfig, IRequestParams, IHttpClientPlugin } from './interface/httpClientConfig';
import { IFactoryUse } from '../../library/helpers/IFactoryUse';
export declare class HttpClient<T = any, H = any, R = any> extends HttpClientCore<T, H, R> {
    name: string;
    constructor(options: IHttpClientConfig<T, H>);
    /**
     * 发送请求
     * @param {*} url api url
     * @param {*} option 选项
     * @returns {Observable<object>}
     */
    getData<D = R>(url: string, option: IRequestParams<T, H>): Observable<D>;
    /**
     * get请求
     * @param {*} url api url
     * @param {*} option 选项
     * @returns {Observable<object>}
     */
    get<D = R>(url: string, option: IRequestParams<T, H>): Observable<D>;
    /**
     * post请求
     * @param {*} url api url
     * @param {*} option 选项
     * @returns {Observable<object>}
     */
    post<D = R>(url: string, option: IRequestParams<T, H>): Observable<D>;
    /**
     * formData方式post请求
     * @param {*} url api url
     * @param {*} option 选项
     * @returns {Observable<object>}
     */
    formData<D = R>(url: string, option: IRequestParams<T, H>): Observable<D>;
    /**
     * jsonp请求
     * @param {*} url api url
     * @param {*} option 选项
     * @returns {Observable<object>}
     */
    jsonp<D = R>(url: string, option: IRequestParams<T, H>): Observable<D>;
}
/**
 * 实例化HttpClient
 *
 * @export
 * @param {*} args 参数
 * @returns {HttpClient}
 */
declare const httpClientFactory: IFactoryUse<IHttpClientConfig<any, any>, HttpClient, IHttpClientPlugin<any, any>>;
export default httpClientFactory;
export { IHttpClientConfig, IRequestParams };
