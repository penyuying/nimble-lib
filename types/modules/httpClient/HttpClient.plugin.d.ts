import { Observable } from 'rxjs/Observable';
import { IKeyValue } from '../../library/helpers/IKeyValue';
import { BaseAbstract } from '../../library/basic/base.abstrat';
import { IHttpClientPlugin, IHttpClientConfig } from './interface/httpClientConfig';
export declare class HttpClientPlugin<O extends object | null> extends BaseAbstract<O> {
    /**
     * 插件列表
     *
     * @static
     * @type {IHttpClientPlugin[]}
     * @memberof HttpClientCore
     */
    static _pluginList: IHttpClientPlugin[];
    constructor(options: O);
    /**
     * 挂载插件
     */
    static use(plugin: IHttpClientPlugin): void;
    /**
     * 执行插件操作
     *
     * @static
     * @param {(Function|Observable<any>)} data 数据Observable/回调
     * @param {string} service 当前请求的
     * @param {IKeyValue} [options] 请求参数
     * @param {IHttpClientConfig} [config] 默认配置
     * @returns {Observable}
     * @memberof HttpClientPlugin
     */
    static handlerPlugin(data: Function | Observable<any>, service: string, options?: IKeyValue, config?: IHttpClientConfig): Observable<any>;
}
