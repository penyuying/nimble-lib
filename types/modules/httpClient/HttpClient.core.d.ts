import { AxiosStatic } from 'axios';
import 'rxjs/add/operator/publishReplay';
import { Observable } from 'rxjs/Observable';
import { IHttpClientConfig, IRequestParams } from './interface/httpClientConfig';
import { HttpClientPlugin } from './HttpClient.plugin';
import { IKeyValue } from '../../library/helpers/IKeyValue';
export declare class HttpClientCore<T = any, H extends IKeyValue = any, R = any> extends HttpClientPlugin<IHttpClientConfig<T, H>> {
    protected defaultOption: IHttpClientConfig<T, H>;
    axios: AxiosStatic;
    private _axios;
    private _jsonp;
    constructor(options: IHttpClientConfig<T, H>);
    /**
     * 打包请求参数
     * @param {*} service api url
     * @param {Object} options 请求选项
     * @param {Object} extOpts 追加人请求选项
     * @return {Promise}
     */
    private _packOptions;
    /**
     * 包装请求header头信息
     * @param {Object} options 选项
     * @return {Object|undefined}
     */
    private _packHeader;
    /**
     * 包装请求body体信息
     * @param {Object} options 选项
     * @return {Object|undefined}
     */
    private _packBody;
    /**
     * 解包返回的数据
     *
     * @param {*} back 返回的数据
     * @returns {Promise}
     * @memberof HttpClientCore
     */
    private _unPackData;
    /**
     * 设置数据缓存
     *
     * @private
     * @param {*} obs 请求的Observable
     * @param {string} url 接口名称
     * @param {IRequestParams} options 请求参数
     * @returns {Observable}
     * @memberof HttpClientCore
     */
    private _handlerPlugin;
    /**
     * 出错后尝试调用
     *
     * @private
     * @param {number} attempt 请求出错后尝试次数
     * @param {Error} err 错误信息
     * @param {string} service 接口服务地址
     * @param {IRequestParams} options 请求参数
     * @param {(options: IRequestParams) => void} cb 成功的回调
     * @param {(err: any) => void} errCb 出错的回调
     * @memberof HttpClientCore
     */
    private _attemptFilter;
    /**
     * 发送请求
     * @param {Object} opts 请求选项
     */
    private _send;
    /**
     * 发送请求
     * @param {*} url api url
     * @param {*} options 选项
     * @returns {Observable<object>}
     */
    send<D = R>(url: string, options: IRequestParams<T, H>): Observable<D>;
    /**
     * 获取api链接
     * @param {*} key api服务器key
     * @param {*} service 服务名称
     * @returns {string}
     */
    getApiUrl(key: string, service: string): string;
}
