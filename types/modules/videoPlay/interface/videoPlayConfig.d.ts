import { IKeyValue } from './../../../library/helpers/IKeyValue';
import { IItersectionConfig } from '../../../library/helpers/intersection/interface/intersectionConfig';
import { IViewRegionConfig } from '../../../library/helpers/viewRegion/interface/viewRegionConfig';
import { VIDEO_LISTENER_EVENT_TYPE } from '../constant';
import { IListenerQueueStateType } from '../../../library/helpers/listenerQueue/ListenerQueue';
import { IDevice } from '../../../library/util/browser/interface/browser';
export interface IVideoPlayConfig extends IItersectionConfig, IVideoListenerConfig {
    /**
     * 视频节点
     *
     * @type {HTMLVideoElement}
     * @memberof IVideoPlayConfig
     */
    videoEl?: HTMLVideoElement;
    /**
     * IntersectionObserver模式
     *
     * @type {boolean}
     * @memberof IVideoPlayConfig
     */
    intersectionPattern?: boolean;
}
export interface IVideoListenerConfig extends IViewRegionConfig {
    /**
     * 是否自动播放
     *
     * @type {boolean}
     * @memberof IVideoListenerConfig
     */
    isAutoplay?: boolean;
    /**
     * 设备信息
     */
    device?: IDevice;
    /**
     * video元素的属性
     */
    videoAttr?: IKeyValue;
    /**
     * 视频节点
     */
    videoEl?: HTMLVideoElement;
}
/**
 * 图片渲染的配置参数
 *
 * @export
 * @interface IStateEvent
 */
export interface IStateEvent {
    /**
     * 观察对象
     *
     * @type {*}
     * @memberof IStateEvent
     */
    listener?: any;
    /**
     * 类型
     *
     * @type {VIDEO_LISTENER_EVENT_TYPE}
     * @memberof IStateEvent
     */
    type?: VIDEO_LISTENER_EVENT_TYPE;
    /**
     * 选项
     *
     * @type {IVideoPlayConfig}
     * @memberof IStateEvent
     */
    options?: IVideoPlayConfig;
    /**
     * 元素
     *
     * @type {HTMLElement}
     * @memberof IStateEvent
     */
    el?: HTMLElement;
    /**
     * 图片数据
     *
     * @type {*}
     * @memberof IStateEvent
     */
    data?: any;
}
/**
 * 观察对象状态
 */
export declare type IVideoStateType = IListenerQueueStateType | 'play' | 'pause' | 'refresh' | 'destroyed' | 'display' | 'shiftOut';
