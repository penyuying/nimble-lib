import { IVideoPlayConfig, IVideoListenerConfig, IVideoStateType } from './interface/videoPlayConfig';
import { IListenerQueueState } from '../../library/helpers/listenerQueue/ListenerQueue';
/**
 * VideoListener默认配置
 * @type {IVideoListenerConfig}
 */
export declare const VIDEO_LISTENER_DEFAULT_CONFIG: IVideoListenerConfig;
/**
 * videoPlay默认配置
 * @type {IVideoPlayConfig}
 */
export declare const DEFAULT_CONFIG: IVideoPlayConfig;
/**
 * status事件类型
 * @type {IListenerQueueState}
 */
export declare const VIDEO_STATE: IListenerQueueState<IVideoStateType>;
/**
 * ImgListener 事件
 *
 * @export
 * @enum {VIDEO_LISTENER_EVENT_TYPE}
 */
export declare enum VIDEO_LISTENER_EVENT_TYPE {
    /**
     * 状态事件
     */
    STATUS = "status"
}
