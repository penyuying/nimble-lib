import { ViewRegion } from '../../library/helpers/viewRegion/ViewRegion';
import { IListener } from '../../library/helpers/listenerQueue/ListenerQueue';
import { IScrollElementItem } from '../../library/helpers/intersection/interface/intersectionConfig';
import { IVideoListenerConfig, IVideoStateType } from './interface/videoPlayConfig';
/**
 * 图片加载队列项
 *
 * @export
 * @class Listener
 * @extends {ViewRegion}
 */
export declare class VideoListener extends ViewRegion<IVideoListenerConfig> implements IListener<IScrollElementItem | null, IVideoStateType> {
    defaultOption: IVideoListenerConfig;
    /**
     * 是否删除
     *
     * @memberof VideoListener
     */
    isDel: boolean;
    /**
     * 状态
     *
     * @memberof VideoListener
     */
    state: IVideoStateType;
    /**
     * 视频节点
     *
     * @memberof VideoListener
     */
    videoEl?: HTMLVideoElement;
    /**
     * 当前对应的滚动条数据
     *
     * @memberof VideoListener
     */
    _$scroll: null;
    /**
     * 老的src
     *
     * @memberof VideoListener
     */
    oldSrc?: string | null;
    /**
     * Creates an instance of VideoListener.
     * @param {HTMLElement} el 元素
     * @param {Object} options 选项
     * @memberof VideoListener
     */
    constructor(el: HTMLElement, options: IVideoListenerConfig);
    /**
     * 设置video自定义属性
     * @param {Object} options 选项
     */
    private setVideoAttr;
    /**
     * 加载
     *
     * @param {Object} opts 选项
     * @param {Boolean} refresh 刷新
     *
     * @returns {Listener}
     * @memberof Listener
     */
    display(opts?: IVideoListenerConfig, refresh?: boolean): this;
    /**
     * 移出视图
     */
    shiftOut(): void;
    /**
     * 当前在图中的时候加载
     * @param {Object} opts 选项
     * @param {Boolean} refresh 刷新
     *
     * @memberof Listener
     */
    viewDisplay(opts?: IVideoListenerConfig, refresh?: boolean): boolean;
    /**
     * 更新src
     *
     * @param {*} src 更新的src
     * @param {*} options 配置参数
     * @returns {Boolean} 是否有更新
     * @memberof Listener
     */
    update(el: HTMLElement, options: IVideoListenerConfig): void;
    /**
     * 销毁
     */
    destroyed(): void;
    /**
     * 设置状态、事件
     *
     * @param {*} state 状态
     * @param {*} evt 事件对像
     * @memberof Listener
     */
    private _stateHandler;
    /**
     * 初始化事件
     */
    private _init;
}
