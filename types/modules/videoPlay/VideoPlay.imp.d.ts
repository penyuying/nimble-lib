import { BaseAbstract } from '../../library/basic/base.abstrat';
import { IListener } from '../../library/helpers/listenerQueue/ListenerQueue';
import { IVideoPlayConfig, IVideoStateType } from './interface/videoPlayConfig';
import { VideoListener } from './VideoListener';
/**
 * 视频播放
 * @export
 * @class VideoPlay
 * @extends {IntersectionPattern}
 */
export declare class VideoPlay extends BaseAbstract<IVideoPlayConfig> {
    name: string;
    /**
     * 默认数据
     *
     * @protected
     * @memberof VideoPlay
     */
    protected defaultOption: IVideoPlayConfig;
    /**
     * 锁定状态（处理x5内核安卓的兼容）
     *
     * @private
     * @type {boolean}
     * @memberof VideoPlay
     */
    private _videoLocked;
    private _intersection;
    /**
     * 当前播放的视频元素
     *
     * @private
     * @type {(IListener<IScrollElementItem|null, IVideoStateType>)}
     * @memberof VideoPlay
     */
    private _playListener?;
    /**
     * 显示的元素
     *
     * @private
     * @type {(IListener<IScrollElementItem|null, IVideoStateType>[])}
     * @memberof VideoPlay
     */
    private _displayList;
    /**
     * 当前显示的元素
     *
     * @private
     * @type {(IListener<IScrollElementItem|null, IVideoStateType>|null)}
     * @memberof VideoPlay
     */
    private _upDisplay;
    /**
     * 是否自动播放
     *
     * @private
     * @type {Boolean}
     * @memberof VideoPlay
     */
    private _isAutoplay;
    /**
     * 播放调用节流
     *
     * @private
     * @type {Function}
     * @memberof VideoPlay
     */
    private _playThrottle;
    /**
     * Creates an instance of VideoPlay.
     * @param {Object} options 数据选项
     * @memberof VideoPlay
     */
    constructor(options: IVideoPlayConfig);
    private _autoPlay;
    /**
     * 初始化
     * @private
     * @memberof VideoPlay
     */
    private _init;
    /**
     * 刷新display
     */
    refreshDisplay(): void;
    /**
     * 添加数据
     * @param {HTMLImageElement} el 当前图片节点
     * @param {Object} options 数据选项
     * @return {Object}
     */
    push(el: HTMLVideoElement, options: IVideoPlayConfig): {
        listener: IListener<any, IVideoStateType> | (() => IListener<any, IVideoStateType>);
        isUpdata: boolean;
    } | {
        listener: IListener<any, any>;
        isUpdata: boolean;
    };
    /**
     * 更新url
     * @param {HTMLImageElement} el 当前图片节点
     * @param {Object} options 数据选项
     * @memberof VideoPlay
     * @return {Object}
     */
    update(el: HTMLElement, options: IVideoPlayConfig): {
        listener: IListener<any, IVideoStateType> | (() => IListener<any, IVideoStateType>);
        isUpdata: boolean;
    } | {
        listener: IListener<any, any>;
        isUpdata: boolean;
    };
    /**
     * 移除节点Listener
     * @param {HTMLElement} el 当前图片节点
     */
    remove(el: HTMLElement): void;
    /**
     * 播放
     * @param {HTMLVideoElement} video 视频节点
     */
    play(video: HTMLVideoElement): void;
    /**
     * 暂停播放
     * @param {HTMLVideoElement} playVideo 当前需要播放的视频
     */
    pause(playVideo?: HTMLVideoElement): void;
    /**
     * 重新设置浮动显示项
     */
    resetDisplayItem(): void;
    /**
     * 根据el获取Listener
     * @param {HTMLElement} el 元素
     */
    getItme(el: HTMLElement): {
        listener: IListener<any, IVideoStateType>;
        index: number;
    } | undefined;
    /**
     * 加入和更新队列
     * @param {HTMLImageElement} el 元素
     * @param {Object} options 选项
     * @param {Boolean} isUpdata 是否为更新
     * @returns {ImgListener}
     * @memberof VideoPlay
     */
    private _queueHandler;
    /**
     * 初始化VideoListener
     * @param {HTMLImageElement} el 元素
     * @param {Object} options 选项
     */
    private _initListener;
    /**
     * 设置显示的列表
     */
    private _setDisplayList;
}
/**
 * 实例化工厂方法
 * @export
 * @param {IVideoPlayConfig} options 配置选项
 * @returns {VideoPlay}
 */
export default function (options: IVideoPlayConfig): VideoPlay;
export { IVideoPlayConfig, VideoListener, IVideoStateType };
