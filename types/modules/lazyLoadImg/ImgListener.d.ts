import { ViewRegion } from '../../library/helpers/viewRegion/ViewRegion';
import { IListener } from '../../library/helpers/listenerQueue/ListenerQueue';
import { IImgListenerConfig, ILoadStateType } from './interface/lazyLoadImgConfig';
import { IScrollElementItem } from '../../library/helpers/intersection/interface/intersectionConfig';
/**
 * 图片加载队列项
 *
 * @export
 * @class Listener
 * @extends {ViewRegion}
 */
export declare class ImgListener extends ViewRegion<IImgListenerConfig> implements IListener<IScrollElementItem | null, ILoadStateType> {
    /**
     * 默认数据
     *
     * @protected
     * @memberof ImgListener
     */
    protected defaultOption: IImgListenerConfig;
    /**
     * 是否删除
     *
     * @memberof ImgListener
     */
    isDel: boolean;
    /**
     * 状态Wait;
     *
     * @memberof ImgListener
     */
    state: ILoadStateType;
    /**
     * 当前对应的滚动条数据
     *
     * @memberof ImgListener
     */
    _$scroll: null;
    /**
     * 老的src
     *
     * @memberof ImgListener
     */
    oldSrc?: string | null;
    /**
     * Creates an instance of ImgListener.
     * @param {HTMLElement} el 元素
     * @param {Object} options 选项
     * @param {Function?} options.loading 图片url
     * @param {String} options.src 图片url
     * @param {String?} options.loadingSrc 加载时的图片地址
     * @param {String?} options.errorSrc 出错后的图片地址
     * @param {Function?} options.loadData 加载数据的回调
     * @param {Function?} [options.attempt=1] 加载出错后尝试加载次数
     * @memberof ImgListener
     */
    constructor(el: HTMLElement, options: IImgListenerConfig);
    /**
     * 加载
     *
     * @returns {Listener}
     * @memberof Listener
     */
    display(): this;
    /**
     * 当前在图中的时候加载
     * @param {Object} opts 选项
     *
     * @memberof Listener
     */
    viewDisplay(opts?: IImgListenerConfig): boolean;
    /**
     * 更新src
     *
     * @param {*} src 更新的src
     * @param {*} options 配置参数
     * @returns {Boolean} 是否有更新
     * @memberof Listener
     */
    update(el: HTMLElement, options: IImgListenerConfig): false | undefined;
    /**
     * 销毁
     */
    destroyed(): void;
    /**
     * 设置状态、事件
     *
     * @param {*} state 状态
     * @param {*} evt 事件对像
     * @memberof Listener
     */
    private _stateHandler;
    /**
     * 初始化事件
     */
    private _init;
}
