import { IItersectionConfig } from '../../../library/helpers/intersection/interface/intersectionConfig';
import { IImgLoadBack } from '../../../library/util/imgLoad';
import { IKeyValue } from '../../../library/helpers/IKeyValue';
import { IViewRegionConfig } from '../../../library/helpers/viewRegion/interface/viewRegionConfig';
import { IListenerQueueStateType } from '../../../library/helpers/listenerQueue/ListenerQueue';
export interface ILazyLoadImgConfig extends IImgListenerConfig, IItersectionConfig {
    /**
     * 图片url过滤器
     */
    imgFilter?: (opts: ILazyLoadImgConfig) => ILazyLoadImgConfig;
    /**
     * IntersectionObserver模式
     */
    intersectionPattern?: boolean;
    /**
     * 渲染图片的回调
     */
    imgRenderer?: (evt: ILoadEvent) => void;
}
/**
 * 图片渲染的配置参数
 *
 * @export
 * @interface ILoadEvent
 */
export interface ILoadEvent {
    /**
     * 加载状态
     */
    type?: ILoadStateType;
    /**
     * 图片渲染的选项
     */
    options?: IImgRendererOptions;
    /**
     * 当前img元素
     */
    el?: HTMLElement;
    /**
     * 原src
     */
    oldSrc?: string | null;
    /**
     * 图片数据
     */
    data?: IImgLoadBack;
}
/**
 * 图片渲染的选项
 *
 * @export
 * @interface IImgRendererOptions
 */
export interface IImgRendererOptions extends ILoadOptions {
    /**
     * 是否只裁剪图片
     */
    onlyCut?: boolean;
    /**
     * 图片宽度
     */
    width?: number;
    /**
     * 图片高度
     */
    height?: number;
    /**
     * 包裹的class
     */
    lazyWrapCla?: string | null;
    /**
     * 当前加载项的class
     */
    lazyCla?: string | null;
    /**
     * src属性名称
     */
    srcAttr?: string;
    /**
     * 是否设置图片style
     */
    isSetStyle?: boolean;
    /**
     * 加载时是否加包裹元素
     */
    isWrap?: boolean;
}
export interface ILoadOptions {
    /**
     * 加载的src
     */
    src?: string;
    /**
     * 加载时的图片地址
     */
    loadingSrc?: string | null;
    /**
     * 出错后的图片地址
     */
    errorSrc?: string | null;
}
export interface IImgListenerConfig extends IImgRendererOptions, IViewRegionConfig {
    /**
     * 加载时的回调
     */
    loading?: (evt: any) => void;
    /**
     * 加载出错后尝试加载次数
     */
    attempt?: number;
    /**
     * 尝试时的过滤器回调方法接收三个参数(src: 当前的src, count: 第几次尝试, options: 初始化时的选项)=> (Promise|src)
     */
    attemptFilter?: (src: string, attempt: number, opts: ILazyLoadImgConfig) => false | Promise<string | false>;
    /**
     * 加载数据的回调
     */
    loadData?: (opts: ILoadOptions, stateHandler: IStateHandler, attemptFilter?: IAttemptFilter) => void;
}
/**
 * 处理状态
 *
 * @interface IStateHandler
 */
export interface IStateHandler {
    (state: ILoadStateType, evt?: IKeyValue): void;
}
/**
 * 出错后是否继续尝试的回调
 *
 * @interface IAttemptFilter
 */
export interface IAttemptFilter {
    (src: string, attempt: number): Promise<string | false> | false;
}
/**
 * 加载状态(init:初始化；update:更新；loaded:加载中；reload:重新加载；loadEnd:加载结束；loadError:加载出错；destroyed:销毁)
 */
export declare type ILoadStateType = IListenerQueueStateType | 'loadStart' | 'loaded' | 'reload' | 'loadEnd' | 'loadError' | 'destroyed';
