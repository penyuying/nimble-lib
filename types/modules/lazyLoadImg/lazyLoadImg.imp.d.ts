import { BaseAbstract } from '../../library/basic/base.abstrat';
import { ILazyLoadImgConfig, ILoadEvent, ILoadStateType } from './interface/lazyLoadImgConfig';
/**
 * 图片懒加载
 *
 * @export
 * @class LazyLoadImg
 * @extends {IntersectionPattern}
 */
export declare class LazyLoadImg extends BaseAbstract<ILazyLoadImgConfig> {
    /**
     * 默认数据
     */
    protected defaultOption: ILazyLoadImgConfig;
    name: string;
    private _intersection?;
    /**
     * Creates an instance of LazyLoadImg.
     * @param {Object} options 数据选项
     * @param {String} options.loadingSrc 加载时的图片地址
     * @param {String?} options.errorSrc 出错后的图片地址
     * @param {Function?} options.imgFilter 图片url过滤器 数据选项
     * @param {Function?} options.imgRenderer 渲染图片的回调
     * @memberof LazyLoadImg
     */
    constructor(options: ILazyLoadImgConfig);
    /**
     * 初始化
     *
     * @private
     * @memberof LazyLoadImg
     */
    private _init;
    /**
     * 添加数据
     * @param {HTMLImageElement} el 当前图片节点
     * @param {Object} options 数据选项
     * @param {String} options.loadingSrc 加载时的图片地址
     * @param {String?} options.errorSrc 出错后的图片地址
     * @param {String?} options.imgFilter 图片url过滤器 数据选项
     * @memberof LazyLoadImg
     * @return {Object}
     */
    push(el: HTMLElement, options: ILazyLoadImgConfig): {
        listener: import("../../library/helpers/listenerQueue/ListenerQueue").IListener<any, ILoadStateType> | (() => import("../../library/helpers/listenerQueue/ListenerQueue").IListener<any, ILoadStateType>);
        isUpdata: boolean;
    } | {
        listener: import("../../library/helpers/listenerQueue/ListenerQueue").IListener<any, any>;
        isUpdata: boolean;
    } | undefined;
    /**
     * 更新url
     *
     * @param {HTMLImageElement} el 当前图片节点
     * @param {Object} options 数据选项
     * @param {String} options.loadingSrc 加载时的图片地址
     * @param {String?} options.errorSrc 出错后的图片地址
     * @param {String?} options.imgFilter 图片url过滤器 数据选项
     * @memberof LazyLoadImg
     * @return {Object}
     */
    update(el: HTMLElement, options: ILazyLoadImgConfig): {
        listener: import("../../library/helpers/listenerQueue/ListenerQueue").IListener<any, ILoadStateType> | (() => import("../../library/helpers/listenerQueue/ListenerQueue").IListener<any, ILoadStateType>);
        isUpdata: boolean;
    } | {
        listener: import("../../library/helpers/listenerQueue/ListenerQueue").IListener<any, any>;
        isUpdata: boolean;
    } | undefined;
    /**
     * 移除节点Listener
     * @param {HTMLElement} el 当前图片节点
     */
    remove(el: HTMLElement): void;
    /**
     * 加入和更新队列
     *
     * @param {HTMLImageElement} el 元素
     * @param {Object} options 选项
     * @param {Boolean} isUpdata 是否为更新
     * @returns {ImgListener}
     * @memberof LazyLoadImg
     */
    private _queueHandler;
    /**
     * 图片url格式化
     * @param {Object} options 选项
     * @return {Promise}
     */
    private _urlFormatter;
}
/**
 * 实例化工厂方法
 *
 * @export
 * @param {ILazyLoadImgConfig} options 配置选项
 * @returns {LazyLoadImg}
 */
export default function (options: ILazyLoadImgConfig): LazyLoadImg;
export { ILazyLoadImgConfig, ILoadEvent };
