import { ILoadOptions, IStateHandler, IAttemptFilter } from '../interface/lazyLoadImgConfig';
/**
 * 加载图片
 *
 * @param {Object} options 选项
 * @param {Listener} setState Listener
 * @param {Function} attemptFilter 尝试的时候的过滤器
 *
 */
export default function loadData(options: ILoadOptions, setState: IStateHandler, attemptFilter?: IAttemptFilter): void;
