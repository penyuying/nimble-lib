import { ILoadEvent } from '../interface/lazyLoadImgConfig';
/**
 * 图片渲染
 *
 * @param {Object} data 当前加载的数据
 * @param {LOAD_STATE} data.type 事件类型
 * @param {Object} data.options 选项数据
 * @param {String} data.options.src 加载时的图片地址
 * @param {String} data.options.loadingSrc 加载时的图片地址
 * @param {Boolean?} data.options.isSetStyle 是否设置图片style
 * @param {String?} data.options.isWrap 加载时是否加包裹元素
 * @param {String?} data.options.lazyWrapCla 包裹的class
 * @param {String?} data.options.lazyCla 当前加载项的class
 * @param {Number} data.options.width 宽度
 * @param {Number} data.options.height 高度
 * @param {HTMLImageElement} data.el 元素
 * @param {Object?} data.data 事件对象
 * @memberof ImgLazy
 */
export default function imgRenderer(data: ILoadEvent): void;
