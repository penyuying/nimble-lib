import { ILazyLoadImgConfig, IImgListenerConfig, ILoadStateType } from './interface/lazyLoadImgConfig';
import { IListenerQueueState } from '../../library/helpers/listenerQueue/interface/listenerQueue';
/**
 * ImgListener默认配置
 */
export declare const IMG_LISTENER_DEFAULT_CONFIG: IImgListenerConfig;
export declare const DEFAULT_CONFIG: ILazyLoadImgConfig;
/**
 * ImgListener 事件
 *
 * @export
 * @enum {number}
 */
export declare enum IMG_LISTENER_EVENT_TYPE {
    LOADING = "loading"
}
export declare const IMT_LISTENER_STATE: IListenerQueueState<ILoadStateType>;
