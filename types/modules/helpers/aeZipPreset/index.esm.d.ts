import { IAeAnimationConfig } from '../../aeAnimation/aeAnimation.imp';
/**
 * 实例化工厂方法
 *
 * @export
 * @param {IAeAnimationConfig} options 配置选项
 * @returns {AeAnimation}
 */
export default function (options: IAeAnimationConfig): any;
