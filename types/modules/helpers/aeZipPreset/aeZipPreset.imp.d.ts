/// <reference path="../../../../src/modules/helpers/aeZipPreset/interface/zip.d.ts" />
import { BaseAbstract } from '../../../library/basic/base.abstrat';
import { ILottieFile } from '../../aeAnimation/aeAnimation.imp';
export declare class AeZipPreset<T> extends BaseAbstract<any> {
    /**
     * 解析的方法后缀名
     *
     * @type {'zip'}
     * @memberof AeZipPreset
     */
    ext: string;
    preset(path: string, options: T, ext: string): Promise<ILottieFile>;
    /**
     * 解包zip文件
     *
     * @param {*} fileCb 获取文件的回调
     * @returns {Promise}
     * @memberof AeAnimation
     */
    private _unpackZip;
    /**
     * 加载zip文件
     *
     * @param {String} url zip文件地址
     * @returns {Promise}
     * @memberof AeAnimation
     */
    private _loadZip;
}
/**
 * 实例化工厂方法
 *
 * @export
 * @param {any} options 配置选项
 * @returns {AeZipPreset}
 */
export default function <T>(options: T): AeZipPreset<T>;
