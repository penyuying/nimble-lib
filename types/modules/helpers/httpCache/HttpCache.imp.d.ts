import 'rxjs/add/operator/publishReplay';
import { Observable } from 'rxjs/Observable';
import { BaseAbstract } from '../../../library/basic/base.abstrat';
import { IHttpCacheConfig } from './interface/httpCacheConfig';
import { IHttpClientPlugin, IRequestParams } from '../../httpClient/interface/httpClientConfig';
export declare class HttpCache extends BaseAbstract<IHttpCacheConfig> implements IHttpClientPlugin {
    protected defaultOption: IHttpCacheConfig;
    constructor(options?: IHttpCacheConfig);
    /**
     * httpClient执行
     * @param {*} cb 数据或回调方法
     * @param {*} options 参数选项
     * @memberof HttpCache
     */
    handler(cb: () => any, url: string, options: IRequestParams): Observable<any>;
    /**
     * 获取缓存参数
     * @param {String} url 请求地址
     * @param {Object} options 请求选项
     * @returns {Object|undefined}
     * @memberof HttpClient
     */
    private _getCacheOption;
    /**
     * 缓存请求数据
     * @param {*} cb 请求的回调
     * @param {object} options 缓存选项
     * @param {Function} filtersCacheData 过滤缓存数据
     * @returns {Observable<object>}
     * @memberof HttpCache
     */
    private _cacheData;
    /**
     * 获取接口数据
     * @private
     * @param {() => any} cb 获取数据的回调
     * @param {IHttpCacheParams} options 选项
     * @param {Observer<any>} observer Observer
     * @memberof HttpCache
     */
    private _getData;
    /**
     * 获取内存+本地份数据的内存数据
     * @param {*} options 缓存选项
     * @returns {object}
     * @memberof HttpCache
     */
    private _getNotNeedSendData;
    /**
     * 获取缓存数据
     * @param {object} options 缓存选项
     * @returns {object}
     * @memberof HttpCache
     */
    private _getCacheData;
    /**
     * 设置缓存数据
     * @param {object} options 缓存选项
     * @param {any} data 缓存的数据
     * @memberof HttpCache
     */
    private _setCacheData;
}
/**
 * 实例化工厂方法
 * @export
 * @param {any} options 配置选项
 * @returns {HttpCache}
 */
export default function httpCacheFactory(options?: IHttpCacheConfig): HttpCache;
