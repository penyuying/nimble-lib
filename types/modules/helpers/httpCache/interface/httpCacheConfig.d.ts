import { IStorageLocal, IStorageSession, IStorageMemory } from '../../../storageClient/interface/storage';
/**
 * http缓存配置
 */
export interface IHttpCacheConfig extends IHttpCacheParams {
    /**
     * 本地存储
     */
    storage?: IStorageMemory & IStorageSession & IStorageLocal;
}
/**
 * 参数
 */
export interface IHttpCacheParams {
    /**
     * 缓存用的key
     */
    cacheKey?: string;
    /**
     * 缓存用的类型不区分大小写
     */
    cacheType?: ICacheType;
    /**
     * 过滤器
     */
    cacheFilter?: any | ((back: any) => any);
    /**
     * 超时时间
     */
    timeout?: number;
    /**
     * 缓存里取的是否需要发请求
     */
    needSend?: boolean;
}
/**
 * 基础类型简写
 */
export declare type ICacheTypeBase = '' | 'cl' | 'cm' | 'cs' | 'l' | 's' | 'm' | 'gm';
/**
 * 类型
 */
export declare type ICacheType = ICacheTypeBase | 'local' | 'session' | 'memory' | 'ms' | 'memorysession' | 'ml' | 'memorylocal' | 'cleanlocal' | 'cleansession' | 'cleanmemory';
