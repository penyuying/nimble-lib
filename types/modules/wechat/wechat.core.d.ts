import { IWeChatInterface, IWeChatContructor } from './interface/wechat';
import { IWeChatInitConfig, ISign, ISignDataConfig, IShareOptions, IShareBack } from './interface/config';
import { BaseAbstract } from '../../library/basic/base.abstrat';
export declare abstract class WeChatCore extends BaseAbstract<IWeChatInitConfig> implements IWeChatInterface<IWeChatInitConfig, ICheckJsApiBackParams> {
    private wxConfigData?;
    /**
     * 正在发送的请求
     *
     * @private
     * @type {Promise<ICheckJsApiBackParams>}
     * @memberof WeChatCore
     */
    private _tempConfigPromise?;
    /**
     * 初始化时候的url
     */
    private _initUrl?;
    /**
     * 默认参数
     *
     * @protected
     * @type {IWeChatInitConfig}
     * @memberof WeChatCore
     */
    protected defaultOption: IWeChatInitConfig;
    /**
     * constructor
     *
     * @param {IWeChatInitConfig} options
     * @memberof WeChatCore
     */
    constructor(config: IWeChatInitConfig);
    /**
     * init wx config
     *
     * @returns {Promise<ICheckJsApiBackParams>}
     * @memberof WeChatCore
     */
    private wxConfig;
    /**
     * get sign
     *
     * @param {ISignDataConfig} config
     * @returns {Promise<ISign>}
     * @memberof WeChatCore
     */
    private getSign;
    /**
     * init config
     *
     * @private
     * @param {IWxConfig} [options]
     * @param {boolean} [isReset=false]
     * @memberof WeChatCore
     */
    protected initConfig(isReset?: boolean): Promise<ICheckJsApiBackParams>;
    /**
     * set wx share
     *
     * @param {IShareOptions} options 分享选项
     * @param {jsApi|jsApi[]} type jsApi 分享的jsapi
     * @param {IShareBack} resolve resolve 分享完成的回调
     */
    protected setShare(options: IShareOptions, type: jsApi | jsApi[], resolve?: IShareBack): void;
    /**
     * 调用微信api
     *
     * @param {(jsApi| jsApi[])} apis 微信api或微信api列表
     * @param {object} [wxApiParam] api需要的参数
     * @param {boolean} [isReset=false] 是否重新初始化config
     * @memberof WeChatCore
     */
    handlerWxApi(apis: jsApi | jsApi[], wxApiParam?: object | ((type: jsApi) => object), isReset?: boolean): Promise<void>;
    /**
     * 分享
     *
     * @param {IShareOptions} options 分享参数
     * @param {IShareBack} resolve 分享完成后的回调
     * @returns
     * @memberof Weixin
     */
    abstract share(options: IShareOptions, resolve: IShareBack): void;
}
export { ISign, IWeChatInitConfig, ISignDataConfig, IShareOptions, IShareBack, IWeChatContructor };
