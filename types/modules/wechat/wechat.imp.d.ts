import { ISign, IWeChatInitConfig, ISignDataConfig, IShareOptions, IShareBack, WeChatCore, IWeChatContructor } from './wechat.core';
/**
 * 微信分享类
 *
 * @export
 * @class WeChat
 * @extends {WeChatCore}
 */
export declare class WeChat extends WeChatCore {
    name: string;
    /**
     * 配置参数
     *
     * @param {IWeChatInitConfig} options 初始化默认参数
     * @memberof WeixinCore
     */
    constructor(config: IWeChatInitConfig);
    /**
     * 分享
     *
     * @param {IShareOptions} options 分享参数
     * @param {IShareBack} resolve 分享完成后的回调
     * @returns
     * @memberof Weixin
     */
    share(options: IShareOptions, resolve?: IShareBack): void;
}
/**
 * 实例化工厂方法
 *
 * @export
 * @param {IWeChatInitConfig} options 配置选项
 * @returns {WeChat}
 */
export default function (options: IWeChatInitConfig): WeChat;
export { ISign, IWeChatInitConfig, ISignDataConfig, IShareOptions, IShareBack, IWeChatContructor };
