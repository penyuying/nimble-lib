/// <reference path="../../../src/modules/wechat/wechat.d.ts" />
import { ISign, IWeChatInitConfig, ISignDataConfig, IShareOptions, IShareBack, IWeChatContructor, WeChat } from './wechat.imp';
/**
 * 实例化工厂方法
 *
 * @export
 * @param {IAeAnimationConfig} options 配置选项
 * @returns {AeAnimation}
 */
export default function (options: IWeChatInitConfig): WeChat;
export { ISign, IWeChatInitConfig, ISignDataConfig, IShareOptions, IShareBack, IWeChatContructor };
