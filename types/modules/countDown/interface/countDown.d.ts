import { IBaseInterface } from '../../../library/basic/base.interface';
/**
 * wechat constructor
 */
export interface ICountDown extends IBaseInterface {
    /**
     * 停止倒计时
     */
    stop(): void;
}
