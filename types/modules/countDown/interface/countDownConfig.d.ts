import { IBaseInterface } from '../../../library/basic/base.interface';
/**
 * 倒计时时间的对象参数
 *
 * @export
 * @interface ICountDown
 */
export interface ICountDownInitConfig extends IBaseInterface {
    /**
     * 开始时间
     */
    startTime?: string | number | Date;
    /**
     * 结束时间
     */
    endTime?: string | number | Date;
    /**
     * 计数时间（毫秒）
     */
    timeCount?: number;
    /**
     * 间隔时间计数（毫秒）
     */
    interval?: number;
    /**
     * 时间格式类型(ms:毫秒；s:秒；m：分；h:时；d:天；M:月；y年)
     */
    formatType?: string;
    /**
     * 最小值
     */
    minNum?: number;
    /**
     * 小于最小值的时候返回值的内容
     */
    minText?: any;
    /**
     * 时间变动的回调(接收一个参数为：计算好的时间毫秒数);
     */
    callback?: ITimeCountCallback;
}
/**
 * 时间变动的回调(接收一个参数为：计算好的时间毫秒数);
 */
export interface ITimeCountCallback {
    (timeCount: number): void;
}
