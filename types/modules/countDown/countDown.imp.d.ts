import { ICountDownInitConfig } from './interface/countDownConfig';
import { BaseAbstract } from '../../library/basic/base.abstrat';
import { ICountDown } from './interface/countDown';
export declare class CountDown extends BaseAbstract<ICountDownInitConfig> implements ICountDown {
    name: string;
    /**
     * 倒计时对象
     *
     * @private
     * @type {NodeJS.Timer}
     * @memberof CountDown
     */
    private _interval;
    protected defaultOption: ICountDownInitConfig;
    constructor(_options?: ICountDownInitConfig);
    /**
     * 计算到关闭时间的剩余时间
     *
     * @export
     * @param {String} startTime 开始时间
     * @param {String} currentTime 当前时间
     * @param {Number} timeCount 计数时间（毫秒）
     * @param {Number} interval 间隔时间计数（毫秒）
     * @param {String} formatType 时间格式类型(ms:毫秒；s:秒；m：分；h:时；d:天；M:月；y年)
     * @param {Function} callback 时间变动的回调(接收一个参数为：计算好的时间毫秒数)
     * @param {String|Number} key 当前时间的插件的
     */
    private timeCount;
    /**
     * 格式化时间
     *
     * @param {Number} _time 毫秒
     * @param {String} formatType 时间格式类型(ms:毫秒；s:秒；m：分；h:时；d:天；M:月；y年)
     * @return {String} 返回格式化好的时间
     */
    private _formatDate;
    /**
     * 停止定时
     *
     * @memberof CountDown
     */
    stop(): void;
}
/**
 * 实例化工厂方法
 *
 * @export
 * @param {IWeChatInitConfig} options 配置选项
 * @returns {WeChat}
 */
export default function (options: ICountDownInitConfig): CountDown;
