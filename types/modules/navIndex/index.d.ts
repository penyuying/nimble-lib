import navIndexFactory, { INavIndexConfig, IOffsetItem, IElementItem, IIndexRegion } from './navIndex.imp';
export default navIndexFactory;
export { INavIndexConfig, IOffsetItem, IElementItem, IIndexRegion };
