import { BaseAbstract } from '../../library/basic/base.abstrat';
import { INavIndexConfig, IOffsetItem, IElementItem, IIndexRegion } from './interface/navIndexConfig';
export declare class NavIndex extends BaseAbstract<INavIndexConfig> {
    name: string;
    /**
     * 最大高度
     */
    private _maxHeight;
    /**
     * 最小高度
     *
     * @private
     * @type {number}
     * @memberof NavIndex
     */
    private _minHeight;
    private _offsetList;
    /**
     * 当前元素元素的父节点是否为fixed定位
     *
     * @private
     * @type {boolean}
     * @memberof NavIndex
     */
    private _isFixed?;
    constructor(options?: INavIndexConfig);
    /**
     * 刷新数据
     * @param {Array} navItems navItem组件或元素列表
     */
    refresh(navItems?: IElementItem[]): void;
    /**
     * 根据indexa获取item位置数据
     * @param {Number} index 索引
     * @param {Array} navItems navItem组件或元素列表
     * @returns {Object}
     */
    getItem(index: number, navItems?: IElementItem[]): IOffsetItem;
    /**
     * 获取滚动条距离顶部的位置
     * @param {Array} navItems navItem组件或元素列表
     * @returns {Number}
     */
    getScrollTop(navItems?: IElementItem[]): {
        scrollTop: number;
        el: any;
        isFixed: boolean;
    };
    /**
     * 获取索引
     * @param {Number} current 当前位置
     * @param {Array} navItems navItem组件或元素列表
     * @return {Number}
     */
    getIndex(current: number, navItems?: IElementItem[]): number;
    /**
     * 初始化偏移量列表
     * @param {Array} navItems navItem组件或元素列表
     * @param {Array} isReset 重置偏移量列表
     * @returns {Array}
     */
    private _initListOffset;
    /**
     * 计算偏移量
     * @param {Array} navItems navItem组件或元素列表
     * @returns {Array}
     */
    private _getListOffset;
    /**
     * 获取当前索引的范围
     *
     * @param {Number} minHeight 最小的item高度
     * @param {Number} maxHeight 最高的item高度
     * @param {Number} current 当前位置
     * @returns {Object}
     */
    private _getRegion;
    /**
     * 比较数据
     * @param {Number} startIndex 开始索引
     * @param {Number} endIndex 结束索引
     * @param {Number} current 当前位置
     * @param {Array} list 数据列表
     * @returns {Number|null}
     */
    private _calcIndex;
}
/**
 * 原生交互
 *
 * @export
 * @param {*} options 选项
 * @return {Object}
 */
export default function navIndexFactory(options: INavIndexConfig): NavIndex;
export { INavIndexConfig, IOffsetItem, IElementItem, IIndexRegion };
