/**
 * Intersection事件类型
 */
export declare enum INDEX_LIST_EVENT_TYPE {
    /**
     * item项移动事件
     */
    ITEM_MOVE = "itemmove"
}
