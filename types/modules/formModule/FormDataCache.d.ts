import { IFormCacheKey, IFormCacheItem, ICacheKeyConfigKey, ICacheKeyConfig } from './interface/FormDataCacheConfig';
/**
 * 缓存FormModule数据
 *
 * @class FormDataCache
 */
export declare class FormDataCache {
    /**
     * 缓存的数据map
     */
    __data: object | object[] | undefined;
    /**
     * 当前数据是否为数组
     *
     * @param {(object|object[])} data
     * @returns {(boolean|undefined)}
     * @memberof FormDataCache
     */
    private _isArray;
    /**
     * 设置map列表
     * @param {Object[]} dataItem 当前缓存里的数据
     * @param {Object[]} list formArray
     * @param {IFormCacheKey} listItemKey 列表项取key的字段名或字段名列表
     * @returns {object}
     */
    private _setList;
    /**
     * 设置map项
     * @param {Object} dataItem 当前缓存里的数据项
     * @param {Object} groupData formgroup
     * @param {IFormCacheItem} listItemKey 列表项取key的字段名或字段名列表
     * @returns {Array<Object>}
     */
    private _setItem;
    /**
     * 获取列表map
     * @param {Object[]} dataItem 当前缓存里的数据
     * @param {Object[]} list formArray
     * @param {IFormCacheKey} listItemKey 列表项取key的字段名或字段名列表
     * @returns {Array<Object>}
     */
    private _getList;
    /**
     * 获取列表map
     * @param {Object} dataItem 当前缓存里的数据项
     * @param {Object} groupData formGroup的value
     * @param {IFormCacheKey} listItemKey 列表项取key的字段名或字段名列表
     * @returns {Array<Object>}
     */
    private _getItem;
    /**
     * 获取formArray对应的key
     *
     * @param {IFormCacheKey} listItemKey
     * @param {number} index
     * @returns
     * @memberof FormDataCache
     */
    private _getKeyItem;
    /**
     * 获取列表map
     * @param {Object} listItem 列表项
     * @param {Object|string} keyItem 列表项取key的字段名(string的话为keyName))
     * @returns {string[]}
     */
    private _getKey;
    /**
     * 设置缓存
     *
     * @param {(object|object[])} data formGroup或formArray的value
     * @param {IFormCacheKey} listItemKey 列表项取key的字段名或字段名列表
     * @memberof FormDataCache
     */
    setCache(data: object | object[], listItemKey: IFormCacheKey): void;
    /**
     * 获取缓存的数据
     *
     * @param {(object|object[])} data formGroup或formArray的value
     * @param {(IFormCacheKey|IFormCacheKey[])} listItemKey 列表项取key的字段名或字段名列表
     * @returns {(object|object[]|undefined)}
     * @memberof FormDataCache
     */
    getCache(data: object | object[], listItemKey: IFormCacheKey | IFormCacheKey[]): object | object[] | undefined;
    /**
     * 增加项
     * @param {number} index 增加项对应的索引号
     * @param {object} dataItem 增加项对应的索引号
     */
    addItem(index: number, dataItem?: object): object[] | undefined;
    /**
     * 移除项
     * @param {number} index 移除项对应的索引号
     * @returns {Object} 返回整个项的map
     */
    removeItem(index: number): object[] | undefined;
    /**
     * 清除缓存
     *
     * @memberof FormDataCache
     */
    clear(): void;
}
export { IFormCacheKey, IFormCacheItem, ICacheKeyConfigKey, ICacheKeyConfig };
