import { IGroupOptions } from './interface/formGroupConfig';
import { FormControl } from './formControl';
import { IFormInterface } from './interface/form';
import { FormGroupCore } from './formGroup.core';
import { IFormFilter } from './interface/formConfig';
import { IKeyValue } from '../../library/helpers/IKeyValue';
import { FormArray } from './formArray';
/**
 * 表单对像
 *
 * @export
 * @class FormGroup
 */
export declare class FormGroup<T = any> extends FormGroupCore<IGroupOptions<FormControl> | IGroupOptions<FormControl>[], T> implements IFormInterface<T> {
    parent: FormGroup<T> | FormArray<T[]> | null;
    formGroup: FormGroup<T> | null;
    formArray: FormArray<T[]> | null;
    /**
     * 是否点过保存
     *
     * @type {boolean}
     * @memberof FormGroupCore
     */
    isSave: boolean;
    /**
     * 表单的验证结果
     *
     * @type {*}
     * @memberof FormGroup
     */
    valid: any;
    /**
     * 最后的验证结果是否有错误
     */
    isValid: boolean;
    /**
     * 表单的内容数据
     *
     * @type {(T|null)}
     * @memberof FormGroup
     */
    value: T | null;
    controls: IGroupOptions<FormControl> | IGroupOptions<FormControl>[];
    constructor(groupData: IGroupOptions<FormControl> | IGroupOptions<FormControl>[]);
    private setControls;
    /**
     * 设置FormGroup的isSave的值
     */
    protected setGroupsIsSave(groups: any, isSave: boolean): boolean;
    /**
     * 获取FormControl
     *
     * @param {string} key Controls的key名称
     * @returns
     * @memberof FormGroup
     */
    get(key: string | number): FormControl | FormGroup | null;
    /**
     * 验证数据
     *
     * @memberof FormGroup
     */
    getValid(): any;
    /**
     * 获取值
     *
     * @memberof FormGroup
     */
    getValue(): T;
    /**
     * 设置表单数据
     *
     * @param {IKeyValue} defaultData 数据
     * @param {IFormFilter} [callback] 数据过滤的回调
     * @returns {*}
     * @memberof FormGroup
     */
    setValue(defaultData: IKeyValue, callback?: IFormFilter): any;
    /**
     * 添加/设置项
     *
     * @param {(FormControl|FormGroupCore<any, any>)} item
     * @param {(string|number)} [key]
     * @returns {any} 返回false为修改失败
     * @memberof FormGroup
     */
    setItem(item: FormControl | FormGroupCore<any, any>, key?: string | number): boolean | T;
    /**
     * 移除项
     * @param {(string|number)} key 移除项的key
     * @returns {any} 返回false为移除失败
     * @memberof FormGroup
     */
    removeItem(key: string | number): boolean | T;
    /**
     * 合并FormGroup
     * @param {FormGroup} formGroup 需要合并的FormGroup
     * @memberof FormGroup
     */
    merge(formGroup: FormGroup): this;
    /**
     * 清空内空
     * @memberof FormGroup
     */
    clear(): void;
}
export { IGroupOptions };
