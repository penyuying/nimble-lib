import { IFormInterface } from './interface/form';
import { FormControl } from './formControl';
import { FormGroup } from './formGroup';
import { FormGroupCore } from './formGroup.core';
import { IKeyValue } from '../../library/helpers/IKeyValue';
import { IFormFilter } from './interface/formConfig';
export declare class FormArray<T = any[]> extends FormGroupCore<FormGroup[], T> implements IFormInterface<T> {
    parent: FormGroup<any> | FormArray<any[]> | null;
    formGroup: FormGroup<any> | null;
    /**
     * 表单的验证结果
     *
     * @type {*}
     * @memberof FormArray
     */
    valid: any;
    /**
     * 最后的验证结果是否有错误
     */
    isValid: boolean;
    /**
     * 表单的内容数据
     *
     * @type {(T|null)}
     * @memberof FormArray
     */
    value: T | null;
    /**
     * 是否点过保存
     *
     * @type {boolean}
     * @memberof FormGroupCore
     */
    isSave: boolean;
    /**
     * 表单列表
     *
     * @type {FormGroup[]}
     * @memberof FormArray
     */
    groups: FormGroup[];
    constructor(groupData: FormGroup[]);
    /**
     * 设置FormGroup的isSave的值
     */
    protected setGroupsIsSave(groups: any, isSave: boolean): boolean;
    /**
     * 获取FormControl
     *
     * @param {number} index 列表的索引
     * @returns
     * @memberof FormArray
     */
    get(index: string | number): any;
    /**
     * 验证数据
     *
     * @memberof FormArray
     */
    getValid(): any;
    /**
     * 获取值
     *
     * @memberof FormArray
     */
    getValue(): T;
    /**
     * 设置表单列表数据
     *
     * @param {IKeyValue[]} defaultData 数据
     * @param {IFormFilter} [callback] 数据过滤的回调
     * @returns {*}
     * @memberof FormArray
     */
    setValue(defaultData: IKeyValue[], callback?: IFormFilter): any;
    /**
     * 添加/设置项
     *
     * @param {(FormControl|FormGroupCore<any, any>)} item
     * @param {(string|number)} [key]
     * @returns {boolean} 返回false为修改失败
     * @memberof FormArray
     */
    setItem(item: FormControl | FormGroupCore<any, any>, key?: string | number): boolean;
    /**
     * 移除项
     *
     * @param {(number)} key 移除项的key
     * @returns {boolean} 返回false为移除失败
     * @memberof FormArray
     */
    removeItem(key: number): any | false;
    /**
     * 合并FormArray
     *
     * @param {FormGroup} formGroup 需要合并的FormGroup
     * @memberof FormArray
     */
    merge(formArray: FormArray): this;
    /**
     * 清空内空
     *
     * @memberof FormArray
     */
    clear(): void;
}
