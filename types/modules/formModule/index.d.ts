import { FormGroup, IGroupOptions, FormControl, IControlsOptions, IFormControlDataMap, IFormFilter, IFormValidErrorBack, IInitControlData, IFormControlOptions, FormArray, IFormInterface, IFormModuleInterface, FormDataCache, IFormCacheKey, IFormCacheItem, ICacheKeyConfigKey, ICacheKeyConfig } from './formModule.imp';
import { IValidetorParams, IValidFunction, IValidetorArrayParams, IValidateInitConfig, IRules, IValidOptions, IValidetorData, IValidateInterface } from './validate';
export default function FormFactory(config?: {
    validate?: IValidateInitConfig;
}): import("./formModule.imp").FormModule;
export { FormGroup, IGroupOptions, FormControl, IControlsOptions, IFormControlDataMap, IFormFilter, IFormValidErrorBack, IInitControlData, IFormControlOptions, FormArray, IFormInterface, IFormModuleInterface, IValidetorParams, IValidFunction, IValidetorArrayParams, IValidateInitConfig, IRules, IValidOptions, IValidetorData, IValidateInterface, FormDataCache, IFormCacheKey, IFormCacheItem, ICacheKeyConfigKey, ICacheKeyConfig };
