/**
 * 缓存数据的配置
 *
 * @export
 * @interface IFormCacheKey
 */
export declare type IFormCacheKey = IFormCacheItem | IFormCacheItem[];
export declare type IFormCacheItem = string | ICacheKeyConfigKey | ICacheKeyConfig;
/**
 * 缓存数据的配置项
 *
 * @export
 * @interface ICacheKeyConfigKey
 */
export interface ICacheKeyConfigKey {
    /**
     * 获取缓存数据对象值的key名
     */
    keyName: undefined;
    /**
     * 缓存的key名
     */
    key: string;
}
/**
 * 缓存数据的配置项
 *
 * @export
 * @interface ICacheKeyConfig
 */
export interface ICacheKeyConfig {
    /**
     * 获取缓存数据对象值的key名
     */
    keyName: string | string[];
    /**
     * 缓存的key名
     */
    key: undefined;
}
