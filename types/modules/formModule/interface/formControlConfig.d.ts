import { IValidFunction, IValidetorData } from '../validate';
/**
 * 生成表单项的参数
 *
 * @export
 * @interface IControlsOptions
 */
export interface IControlsOptions {
    /**
     * 校验流量时间毫秒
     */
    validFlowTime?: number;
    /**
     * 异步验证的等待信息
     */
    asyncWaitInfo?: IValidetorData | null;
    /**
     * 异步出错信息
     */
    asyncErrInfo?: IValidetorData | null;
    /**
     * 表单默认值
     */
    defaultValue?: any;
    /**
     * 验证方法列表
     */
    valid?: IValidFunction | IValidFunction[];
}
