import { IKeyValue } from '../../../library/helpers/IKeyValue';
import { IBaseInterface } from '../../../library/basic/base.interface';
import { FormControl } from '../formControl';
import { FormGroup } from '../formGroup';
/**
 * 验证初始化参数
 */
export interface IValidateInitConfig extends IBaseInterface {
    /**
     * 正则规则
     */
    rules?: IKeyValue<IRules>;
    /**
     * 是否可为空
     */
    required?: boolean;
    /**
     * 不能为空的校验错误信息
     */
    requiredErr?: string;
    /**
     * 最小长度
     */
    minLength?: number;
    /**
     * 最小长度错误信息
     */
    minLengthErr?: string;
    /**
     * 最大长度
     */
    maxLength?: number;
    /**
     * 最大长度错误信息
     */
    maxLengthErr?: string;
    /**
     * 最小字节数
     */
    minSize?: number;
    /**
     * 最小字节数错误信息
     */
    minSizeErr?: string;
    /**
     * 最大字节数
     */
    maxSize?: number;
    /**
     * 最大字节数错误信息
     */
    maxSizeErr?: string;
    /**
     * 是否选择
     */
    isChecked?: boolean;
}
/**
 * 验证规则的正则和提示信息
 */
export interface IRules {
    /**
     * 正则
     */
    reg: RegExp;
    /**
     * 提示消息
     */
    infoTxt: string;
}
declare type IFormat = 'vMobile' | 'vPassword' | 'vEmail';
/**
 * 验证参数
 *
 * @export
 * @interface IValideOptions
 */
export interface IValidOptions extends IValidateInitConfig {
    /**
     * 正则格式类型
     */
    format?: IFormat | IFormat[] | string | string[];
    /**
     * 正则格式错误信息
     */
    formatErr?: string;
}
/**
 * 验证单个control
 *
 * @export
 * @interface IValidetorParams
 */
export interface IValidetorParams extends IValidOptions {
    /**
     * 字段名称
     */
    name: string;
}
/**
 * 表单控件列表的验证参数
 *
 * @export
 * @interface IValidetorArrayParams
 */
export interface IValidetorArrayParams {
    /**
     * 字段名称
     */
    name: string;
    /**
     * 必填的字段名
     */
    minRequiredName?: string;
    /**
     * 最小必填数量(-1表示全部输入)
     */
    minRequiredAmount?: number;
    /**
     * 最小必填数量错误信息
     */
    minRequiredAmountErr?: string;
}
/**
 * 验证函数
 *
 * @export
 * @interface IValidFunction
 */
export interface IValidFunction {
    (params: FormControl): boolean | IValidetorData | Promise<IValidetorData | null>;
}
/**
 * 组验证函数
 *
 * @export
 * @interface IValidFunction
 */
export interface IGroupValidFunction {
    (params: FormGroup): boolean | IValidetorData;
}
/**
 * 验证结果
 *
 * @export
 * @interface IValidetorData
 */
export interface IValidetorData {
    /**
     * 错误信息
     */
    errInfo: IValidetorErrInfo;
}
/**
 * 错误信息
 *
 * @export
 * @interface IValidetorErrInfo
 */
export interface IValidetorErrInfo {
    /**
     * 错误描述
     */
    desc: string;
    /**
     * 错误类型
     */
    type: any;
}
export {};
