/**
 * 生成表单模型的参数
 *
 * @export
 * @interface IGroupOptions
 * @template T
 */
export interface IGroupOptions<T> {
    [key: string]: T;
    [index: number]: T;
}
