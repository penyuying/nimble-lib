import { IBaseInterface } from '../../../library/basic/base.interface';
import { FormControl } from '../formControl';
import { FormGroup } from '../formGroup';
import { FormArray } from '../formArray';
import { IFormFilter, IInitControlData, IFormControlDataMap, IFormControlOptions, IFormValidErrorBack } from './formConfig';
import { IKeyValue } from '../../../library/helpers/IKeyValue';
/**
 * form interface function
 */
export interface IFormInterface<T = any, V = any> extends IBaseInterface {
    /**
     * 验证结果
     */
    valid: V | null;
    /**
     * 表单的内容数据
     */
    value: T | null;
    /**
     * 当前的父节点
     */
    parent: FormGroup | FormArray | FormControl | null;
    /**
     * 当前项所在的formGroup
     */
    formGroup: FormGroup | null;
    get(key?: string | number): FormControl | FormGroup | FormArray | null;
    /**
     * 验证结果
     */
    getValid(): any;
    /**
     * 获取数据
     */
    getValue(): T;
    /**
     * 设置数据
     */
    setValue(defaultData: any, callback?: IFormFilter): any;
}
/**
 * FormModule的接口定义
 */
export interface IFormModuleInterface {
    /**
     * 初始化表单列表
     *
     * @param {(IInitControlData[][]|IFormControlDataMap[])} formList 表单基础数据列表
     * @param {IKeyValue[]} [defaultData] 表单默认数据
     * @param {IFormFilter} [callback] 设置每项值前的回调
     * @returns {FormArray}
     * @memberof FormModule
     */
    initFormArray(formList: IInitControlData[][] | IFormControlDataMap[], defaultData?: IKeyValue[], callback?: IFormFilter): FormArray;
    /**
     * 列表数据生成对象表单
     *
     * @param {IInitControlData[]} formList 列表
     * @param {IFormFilter} callback 过滤回调函数
     * @memberof FormModule
     */
    initFormData(formList: IInitControlData[], defaultData?: IKeyValue, callback?: IFormFilter): FormGroup;
    /**
     * 初始化表单数据
     *
     * @param {IQueryMemberInfoData} data
     * @memberof PersonalInfoComponent
     */
    initFormGroup(formData: IFormControlDataMap | IFormControlOptions[], defaultData?: IKeyValue, callback?: IFormFilter): FormGroup;
    /**
     * 验证列表
     *
     * @param {FormGroup} formMd 表单模型
     * @param {IFormValidErrorBack} errorBack 获取值的时候的回调
     * @returns {boolean} false为通过验证，true为不通过验证
     * @memberof FormModule
     */
    validForm(formMd: FormGroup, errorBack: IFormValidErrorBack): boolean;
}
