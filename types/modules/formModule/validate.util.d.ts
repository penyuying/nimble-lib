import { IRules, IValidetorParams } from './interface/validateConfig';
/**
 * 验证字符串字节数
 *
 * @param {IValidetorParams} options 验证选项
 * @param {any} _val 验证的内容
 * @returns {boolean|string}
 */
export declare function verifySize(options: IValidetorParams, _val: string): string | false;
/**
 * 验证字符串长度
 *
 * @param {IValidetorParams} options 验证选项
 * @param {any} _val 验证的内容
 * @returns {boolean|string}
 */
export declare function verifyLength(options: IValidetorParams, _val: string): string | false;
/**
 * 校验空
 *
 * @param {IValidetorParams} options 验证选项
 * @param {any} _val 验证的内容
 * @returns {boolean|string}
 */
export declare function verifyEmpty(options: IValidetorParams, _val: any): string | false;
/**
 * 校验正则格式
 *
 * @param {IRules} _formatData 格式对象
 * @param {(number|string)} _text 需要校验的内容
 * @returns {(boolean|string)} 返回flase为校验通过；其它为错误信息
 */
export declare function validFormat(_formatData: IRules, _text: number | string): boolean | string;
