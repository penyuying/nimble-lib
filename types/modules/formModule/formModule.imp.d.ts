import { IKeyValue } from '../../library/helpers/IKeyValue';
import { Validate } from './validate';
import { FormGroup, IGroupOptions } from './formGroup';
import { FormControl, IControlsOptions } from './formControl';
import { IFormControlDataMap, IFormFilter, IFormValidErrorBack, IInitControlData, IFormControlOptions } from './interface/formConfig';
import { BaseAbstract } from '../../library/basic/base.abstrat';
import { FormArray } from './formArray';
import { IFormInterface, IFormModuleInterface } from './interface/form';
import { FormDataCache, IFormCacheKey, IFormCacheItem, ICacheKeyConfigKey, ICacheKeyConfig } from './FormDataCache';
/**
 * 表单处理
 *
 * @export
 * @class FormModule
 * @extends {SValidService}
 */
export declare class FormModule extends BaseAbstract<null> implements IFormModuleInterface {
    validate: Validate;
    name: string;
    constructor(validate: Validate);
    /**
     * 初始化表单列表
     *
     * @param {(IInitControlData[][]|IFormControlDataMap[])} formList 表单基础数据列表
     * @param {IKeyValue[]} [defaultData] 表单默认数据
     * @param {IFormFilter} [callback] 设置每项值前的回调
     * @returns {FormArray}
     * @memberof FormModule
     */
    initFormArray(formList: IInitControlData[][] | IFormControlDataMap[], defaultData?: IKeyValue[], callback?: IFormFilter): FormArray;
    /**
     * 列表数据生成对象表单
     *
     * @param {IInitControlData[]} formList 列表
     * @param {IFormFilter} callback 过滤回调函数
     * @memberof FormModule
     */
    initFormData(formList: IInitControlData[], defaultData?: IKeyValue, callback?: IFormFilter): FormGroup;
    /**
     * 初始化表单数据
     *
     * @param {IQueryMemberInfoData} data 控件map生成表单
     * @memberof PersonalInfoComponent
     */
    initFormGroup(formData: IFormControlDataMap | IFormControlOptions[], defaultData?: IKeyValue, callback?: IFormFilter): FormGroup;
    /**
     * 初始化表单控件
     *
     * @param {IFormControlOptions} controlsData 控件选项数据
     * @returns {FormControl}
     * @memberof FormModule
     */
    initFormControl(controlsData: IFormControlOptions): FormControl;
    /**
     * 验证列表
     *
     * @param {FormGroup|FormArray} formMd 表单模型
     * @param {IFormValidErrorBack} errorBack 取得第一项错误数据的回调
     * @returns {boolean} 验证通过返回false
     * @memberof FormModule
     */
    validForm(formMd: FormGroup, errorBack: IFormValidErrorBack): boolean;
    /**
     * 初始化数据缓存
     *
     * @returns
     * @memberof FormModule
     */
    initFormCache(): FormDataCache;
    /**
     * 设置校验方法
     *
     * @private
     * @param {IFormControlOptions} _conItem 表单项
     * @returns {IValidFunction[]}
     * @memberof PersonalInfoComponent
     */
    private setGroupValid;
    /**
     * 设置校验方法
     *
     * @private
     * @param {IFormControlOptions} _conItem 表单项
     * @returns {IValidFunction[]}
     * @memberof PersonalInfoComponent
     */
    private setValid;
    /**
     * 验证表单项
     *
     * @private
     * @param {Array<any>|Object}} controls 表单控件错误对象
     * @param {IFormValidErrorBack} errorBack 取得第一项错误数据的回调
     * @returns {boolean}  取得第一项错误数据的回调
     * @memberof FormModule
     */
    private validControls;
}
/**
 * factory方法
 *
 * @export
 * @param {Object} env 全局参数
 * @param {Http} http Http服务
 * @returns {SHttpService} 返回服务的实例
 */
export default function formFactory(validate: Validate): FormModule;
export { FormGroup, IGroupOptions, FormControl, IControlsOptions, IFormControlDataMap, IFormFilter, IFormValidErrorBack, IInitControlData, IFormControlOptions, FormArray, IFormInterface, IFormModuleInterface, FormDataCache, IFormCacheKey, IFormCacheItem, ICacheKeyConfigKey, ICacheKeyConfig };
