import { IValidetorData, Validate } from './validate';
import { IControlsOptions } from './interface/formControlConfig';
import { FormGroup } from './formGroup';
import { BaseAbstract } from '../../library/basic/base.abstrat';
import { IFormInterface } from './interface/form';
/**
 * 表单项
 *
 * @export
 * @class FormControl
 */
export declare class FormControl extends BaseAbstract<IControlsOptions> implements IFormInterface<any, IValidetorData | boolean> {
    private options;
    parent: FormControl | FormGroup<any> | null;
    formGroup: FormGroup<any> | null;
    validate: Validate | undefined;
    protected defaultOption: IControlsOptions;
    /**
     * 是否更新数据（0否1是）
     *
     * @private
     * @type {(0|1)}
     * @memberof FormControl
     */
    private isValidStatus;
    /**
     * 验证是否是否等待中（0否1是）
     *
     * @private
     * @type {(0|1)}
     * @memberof FormControl
     */
    private isWaitStatus;
    /**
     * 设置数据时验证节流的setTimeout
     */
    private _timeout;
    /**
     * 是否失去过焦点
     *
     * @type {boolean}
     * @memberof FormControl
     */
    isBlur: boolean;
    /**
     * 是否获取过焦点
     *
     * @type {boolean}
     * @memberof FormControl
     */
    isFocus: boolean;
    /**
     * 值是否发生过改变
     *
     * @type {boolean}
     * @memberof FormControl
     */
    isChange: boolean;
    /**
     * 表单项的值
     *
     * @type {*}
     * @memberof FormControl
     */
    value: any;
    /**
     * 表单项的验证结果
     *
     * @type {(IValidetorData|null|boolean)}
     * @memberof FormControl
     */
    valid: IValidetorData | null | boolean;
    /**
     * 最后的验证结果是否有错误
     */
    isValid: boolean;
    constructor(options: IControlsOptions);
    /**
     * 停止设置数据时验证节流的setTimeout
     *
     * @memberof FormControl
     */
    _stopTimeout(): void;
    /**
     * 设置值
     *
     * @param {*} val 值
     * @memberof Controls
     */
    setValue(val: any): void;
    /**
     * 获取验证结果
     *
     * @returns {(IValidetorData|null|boolean)}
     * @memberof FormControl
     */
    getValid(): IValidetorData | null | boolean | undefined;
    /**
     * 获取当前FormControl
     *
     * @returns {this}
     * @memberof FormControl
     */
    get(): this;
    /**
     * 获取值
     *
     * @returns
     * @memberof FormControl
     */
    getValue(): any;
}
export { IControlsOptions };
