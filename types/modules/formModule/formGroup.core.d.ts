import { IGroupOptions } from './formGroup';
import { FormControl } from './formControl';
import { BaseAbstract } from '../../library/basic/base.abstrat';
export declare class FormGroupCore<O, T> extends BaseAbstract<null> {
    /**
     * 验证的函数
     *
     * @private
     * @type {(Function|Function[]|undefined)}
     * @memberof FormGroupCore
     */
    private _validOption;
    /**
     * 表单的验证结果
     *
     * @type {*}
     * @memberof FormGroupCore
     */
    valid: any;
    /**
     * 表单的内容数据
     *
     * @type {*}
     * @memberof FormGroupCore
     */
    value: any;
    isSend: boolean;
    constructor(config: any);
    /**
     * 设置验证方法
     *
     * @param {Function[]} validOption 验证方法列表
     * @memberof FormGroup
     */
    _setValidOption(validOption: Function | Function[]): void;
    /**
     * 验证当前FormGroup对象
     */
    protected getThisValid(): null;
    /**
     * 验证数据
     * @param {IGroupOptions<FormControl>｜IGroupOptions<FormGroup>} datas FormControl或FormGroup集合
     * @memberof FormGroup
     */
    protected _getValid(datas: IGroupOptions<FormControl> | IGroupOptions<FormControl>[] | FormGroupCore<O, T>[]): any;
    /**
     * 获取值
     *
     * @protected
     * @param {(IGroupOptions<FormControl>IGroupOptions<FormGroup>)} datas FormControl或FormGroup集合
     * @returns {T}
     * @memberof FormGroupCore
     */
    protected _getValue(datas: IGroupOptions<FormControl> | IGroupOptions<FormControl>[] | FormGroupCore<O, T>[]): T;
    /**
     * 增加或修改项
     *
     * @protected
     * @param {(IGroupOptions<FormControl>|IGroupOptions<FormControl>[]|FormControl[]|FormGroupCore<O, T>[]|any)} datas 当前的列表
     * @param {(FormControl|FormGroupCore<O, T>)} item 增加或修改项的内空
     * @param {(string|number)} [key] 增加或修改项的key
     * @param {(info: {flag: 'itemTypeErr'}) => void} [errCb] 出错的回调
     * @param {(item: any) => any} [filterItem] 是否修改成功
     * @returns {boolean}
     * @memberof FormGroupCore
     */
    protected _setItem(datas: IGroupOptions<FormControl> | IGroupOptions<FormControl>[] | FormControl[] | FormGroupCore<O, T>[] | any, item: FormControl | FormGroupCore<O, T>, key?: string | number, errCb?: (info: {
        flag: 'itemTypeErr';
    }) => void, filterItem?: (item: any) => any): boolean;
    /**
     * 移除项
     *
     * @protected
     * @param {IGroupOptions<FormControl>|IGroupOptions<FormControl>[]|FormGroup[]} datas 数据集
     * @param {(string|number)} key 移除项的key
     * @returns {boolean} 是否删除成功
     * @memberof FormGroupCore
     */
    protected _removeItem(datas: IGroupOptions<FormControl> | IGroupOptions<FormControl>[] | FormGroupCore<O, T>[], key: string | number): boolean;
}
