import { IValidetorParams, IValidFunction, IValidetorArrayParams, IValidateInitConfig, IRules, IValidOptions, IValidetorData, IGroupValidFunction } from './interface/validateConfig';
import { BaseAbstract } from '../../library/basic/base.abstrat';
import { IValidateInterface } from './interface/validate';
/**
 * 验证表单数据
 *
 * @export
 * @class Validate
 * @extends {BaseAbstract<IValidateInitConfig>}
 */
export declare class Validate extends BaseAbstract<IValidateInitConfig> implements IValidateInterface {
    /**
     * 验证的默认选项
     *
     * @protected
     * @type {IValidateInitConfig}
     * @memberof Validate
     */
    protected defaultOption: IValidateInitConfig;
    constructor(config?: IValidateInitConfig);
    /**
     * 获取正则
     *
     * @param {string} key 正则key(如果为空则返回所有)
     * @returns {any} 返回正则
     * @memberof SValidService
     */
    getRegExp(key?: string, options?: IValidateInitConfig): IRules | {
        [key: string]: IRules;
    } | undefined;
    /**
     * 生成校验方法
     *
     * @param {IValidetorParams} options 规则选项
     * @returns {IValidFunction} 校验方法
     * @memberof SValidService
     */
    generateValidetor(options: IValidetorParams): IValidFunction;
    /**
     * 验证列表数据
     *
     * @param {IValidetorArrayParams} options 规则选项
     * @returns {IValidFunction}
     * @memberof Validate
     */
    generateArrayValidetor(options: IValidetorArrayParams): IGroupValidFunction;
    /**
     * 校验正则格式
     *
     * @private
     * @param {(string|Array<string>)} param 格式名称(_reg对象里的key名)
     * @param {(number|string)} text 需要校验的字符串
     * @returns {(boolean | string)} 返回flase为校验通过；其它为错误信息
     * @memberof SValidService
     */
    private _validFormat;
}
/**
 * factory方法
 *
 * @export
 * @returns {Valid} 返回服务的实例
 */
export default function validFactory(options?: IValidateInitConfig): Validate;
export { IValidetorParams, IValidFunction, IValidetorArrayParams, IValidateInitConfig, IRules, IValidOptions, IValidetorData, IValidateInterface };
