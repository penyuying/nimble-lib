import { IKeyValue } from '../../library/helpers/IKeyValue';
import { BaseAbstract } from '../../library/basic/base.abstrat';
import { IImgClipConfig, IImgSrcData, IImgSizeData, ICurImgSize } from './interface/ImgClipConfig';
/**
 * 获取图片大小
 *
 * @export
 */
export declare class ImgClip extends BaseAbstract<IImgClipConfig> {
    name: string;
    protected defaultOption: IImgClipConfig;
    constructor(options: IImgClipConfig);
    /**
     * 获取图片宽高（必填wEl或hEl）
     *
     * @param {String} src 图片地址
     * @param {IImgClipConfig} options 选项参数
     * @memberof ImgClip
     * @return {Object} res
     */
    getSrc(src: string, options: IImgClipConfig): Promise<IImgSrcData | IKeyValue>;
    /**
     * 获取图片宽高（必填wEl或hEl）
     *
     * @param {String} src 图片地址
     * @param {Object} options 选项参数
     * @memberof ImgClip
     * @return {Object} res
     */
    getSize(src: string, options: IImgClipConfig): IImgSizeData | undefined;
    /**
     * 设置src
     *
     * @param {String} url 图片地址
     * @param {number} ratio 图片缩放倍数
     * @param {Object} opts 选项
     * @return {Object}
     */
    private _getSrc;
    /**
     * 获取图片裁剪码
     * @param {IImgSizeData} size 图片大小
     * @param {number} ratio 图片缩放倍数
     * @param {'auto'|'contain'|'cover'} cutter 裁剪模式
     */
    private _getClipCode;
    /**
     * 生成url
     *
     * @param {string} src 图片地址
     * @param {string} clipCode 裁剪码
     * @param {string} extFix 扩展名
     * @returns {string}
     * @memberof ImgClip
     */
    private _genSrc;
    /**
     * 单位转化成像素
     * @param {string|number} size 大小的值
     * @param {IImgClipConfig} options 选项
     * @param {Boolean} isHeight 是否为高度
     * @param {Boolean} noOffset 是否不计算offset
     * @return {Number}
     */
    private _toPx;
    /**
     * 获取图片的大小
     * @param {String} src 图片路径
     * @return {Object} res
     */
    private _getImgSize;
}
/**
 * 实例化工厂方法
 *
 * @export
 * @param {IImgClipConfig} options 配置选项
 * @returns {ImgClip}
 */
export default function (options: IImgClipConfig): ImgClip;
export { IImgClipConfig, IImgSrcData, IImgSizeData, ICurImgSize };
