/**
 * 图片裁剪的参数
 *
 * @export
 * @interface IImgClipConfig
 */
export interface IImgClipConfig {
    /**
     * rem根元素字号大小
     */
    remRoot?: number;
    /**
     * window.innerWidth, // 内容宽度(Number)
     */
    contWidth?: number;
    /**
     * window.innerHeight, // 内容高度(Number)
     */
    contHeight?: number;
    /**
     * 百分比计算时内容高度的偏移量
     */
    contHeightOffset?: number;
    /**
     * 百分比计算时内容宽度的偏移量
     */
    contWidthOffset?: number;
    /**
     * 最大内容宽度(Number)
     */
    maxWidth?: number;
    /**
     * 最大内容高度(Number)
     */
    maxHeight?: number;
    /**
     * 图片宽度
     */
    width?: string | number | HTMLElement;
    /**
     * 图片宽度
     */
    height?: string | number | HTMLElement;
    /**
     * 缩放的倍数
     */
    ratio?: number | Promise<number> | ((arg: IImgClipConfig) => number | Promise<number>);
    /**
     * 裁剪方式
     */
    cutter?: 'auto' | 'contain' | 'cover';
    /**
     * 图片没有缀宽度的时候是否需要强制裁剪
     */
    forceCut?: boolean;
    /**
     * 是否支持webp
     */
    supportWebp?: boolean;
}
/**
 * 图片大小数据
 */
export interface IImgSizeData extends ICurImgSize {
    /**
     * 宽度
     */
    width?: number;
    /**
     * 高度
     */
    height?: number;
}
/**
 * 当前图片大小数据
 */
export interface ICurImgSize {
    /**
     * 原始图片宽度
     */
    imgWidth?: number;
    /**
     * 原始图片高度
     */
    imgHeight?: number;
    /**
     * 宽高度比例
     */
    scale?: number;
}
/**
 * 图片裁剪的路径数据
 */
export interface IImgSrcData extends IImgSizeData {
    /**
     * 图片url(经处理后的裁剪url)
     */
    src: string;
    /**
     * 扩展名
     */
    extFix: string;
    /**
     * 原展名
     */
    oldExtFix: string;
    /**
     * 原url
     */
    oldSrc: string;
    /**
     * 裁剪后的url
     */
    cutterSrc: string;
}
