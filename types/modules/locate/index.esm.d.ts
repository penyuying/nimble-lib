import { ILocateConfig } from './interface/locateConfig';
/**
 * 实例化获取地理位置
 *
 * @export
 * @param {*} options 配置参数
 * @returns {Locate}
 */
export default function locateFactory(options: ILocateConfig): any;
