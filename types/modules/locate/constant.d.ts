import { IKeyValue } from '../../library/helpers/IKeyValue';
import { ILocateConfig, IMapType, ILocateType, IEventType, IAppApiType, ILocateDataType, INavigationType } from './interface/locateConfig';
/**
 * 导航类型
 */
export declare const NAVIGATION_TYPE: IKeyValue<INavigationType>;
/**
 * 默认配置信息
 * @exports
 */
export declare const DEFAULT_CONFIG: ILocateConfig;
/**
 * 第三方地图SDK
 */
export declare enum MAP_SDK {
    BAIDU_API = "https://api.map.baidu.com/api",
    WX_API = "https://apis.map.qq.com/ws/coord/v1/translate",
    GD_FROM_LOCATE = "https://restapi.amap.com/v3/assistant/coordinate/convert"
}
export declare enum MAP_APP_API {
    BAIDU_ANDROID = "baidumap://map/direction",
    BAIDU_IOS = "androidamap://navi",
    BAIDU_H5 = "https://api.map.baidu.com/direction",
    GD_ANDROID = "amapuri://route/plan/",
    GD_IOS = "iosamap://path",
    GD_H5 = "https://uri.amap.com/navigation"
}
/**
 * 地图地理类型
 */
export declare const MAP_TYPE: IKeyValue<IMapType>;
/**
 * 地图地理位置类型
 */
export declare const LOCATE_TYPE: IKeyValue<ILocateType>;
/**
 * 获取接口类型
 * @exports
 */
export declare const LOCATE_DATA_TYPE: IKeyValue<ILocateDataType>;
/**
 * 调用app的api
 */
export declare const APP_API_TYPE: IKeyValue<IAppApiType>;
/**
 * 事件类型
 */
export declare const EVENT_TYPE: IKeyValue<IEventType>;
/**
 * 本地存储
 * @exports
 */
export declare enum STORAGE_KEY {
    /**
     * 存储用户保存的地址信息
     */
    LOCATE_USER_SELECTED = "_locate_user_selected"
}
