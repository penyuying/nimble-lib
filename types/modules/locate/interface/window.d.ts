interface Window {
    /**
     * 百度地图状态
     */
    BMAP_STATUS_SUCCESS: 0;
    /**
     * 百度地图对象
     */
    BMap: any;
}
