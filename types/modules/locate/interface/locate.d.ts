import { ILocateData, ILocateConfig } from './locateConfig';
/**
 * 不同渠道定位对象
 */
export interface ILocatePart {
    /**
     * 获取定位
     *
     * @param {ILocateConfig} options 选项
     * @returns {Promise<ILocateData>}
     * @memberof ILocatePart
     */
    getLocate(options: ILocateConfig): Promise<ILocateData | null>;
    /**
     * 根据IP获取定位
     *
     * @param {ILocateConfig} options 选项
     * @returns {Promise<ILocateData>}
     * @memberof ILocatePart
     */
    getIpLocate(options: ILocateConfig): Promise<ILocateData>;
    /**
     * 导航
     *
     * @param {ILocateConfig} options 选项
     * @param {boolean} isNative 是否调用原生
     * @returns {Promise<any>}
     * @memberof ILocatePart
     */
    navigation(options: ILocateConfig, isNative: boolean): Promise<{
        type: 'success';
    }>;
    /**
     * 获取跳转原生的参数
     *
     * @param {ILocateConfig} options 选项
     * @returns {(Promise<string|null>)}
     * @memberof ILocatePart
     */
    getCheckParam(options: ILocateConfig): Promise<string | null>;
    /**
     * 唤起微信导航
     * @param {Object} options 地理位置信息
     * @returns {Promise}
     */
    showMap(options: ILocateConfig): Promise<{
        type: 'success' | 'cancel';
    }>;
}
