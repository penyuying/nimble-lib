import { ILocateData } from '../interface/locateConfig';
/**
 * 设置默认的数据
 *
 * @export
 * @param {*} data 数据
 * @returns {ILocateData|null}
 */
export default function filterLocateData(data?: ILocateData & {
    Province: string;
    City: string;
    District: string;
}): ILocateData | null;
