import { ILocateConfig, ILocateData } from '../../interface/locateConfig';
import { LocatePart } from './LocatePart.base';
/**
 * api定位
 *
 * @export
 * @class ApiLocate
 * @extends {LocatePart}
 */
export declare class ApiLocate extends LocatePart {
    private _cacheLocate;
    constructor(options: ILocateConfig);
    /**
     * 根据经纬度获取地理位置
     *
     * @param {*} options 选项
     * @returns {Promise}
     * @memberof BdLocate
     */
    getLocate(options: ILocateConfig): Promise<ILocateData>;
    /**
     * 获取数据
     *
     * @param {*} type 类型
     * @param {*} param 参数
     * @returns {Promise}
     * @memberof CarCore
     */
    private _getData;
    /**
     * 转换经纬度（百度转高德）
     * @param {Object} options 经纬度
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @returns {Promise} 返回链接
     */
    private _fromLocate;
    /**
     * 根据百度经纬度获取地理位置
     * @param {Object} options 经纬度
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @returns {Promise} 返回地理位置
     */
    private _getLocate;
}
/**
 * 实例化api定位
 *
 * @export
 * @param {*} options 选项
 * @returns {ApiLocate}
 */
export default function apiLocateFactory(options: ILocateConfig): ApiLocate;
