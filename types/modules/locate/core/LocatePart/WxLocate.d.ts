import { LocatePart } from './LocatePart.base';
import { ILocateConfig, ILocateData } from '../../interface/locateConfig';
/**
 * 微信定位
 *
 * @export
 * @class WxLocate
 * @extends {LocatePart}
 */
export declare class WxLocate extends LocatePart {
    constructor(options: ILocateConfig);
    /**
     * 唤起微信导航
     * @param {Object} options 地理位置信息
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @param {String} options.name 地理位置名
     * @param {String} options.address 地址
     * @returns {Promise}
     */
    showMap(options: ILocateConfig): Promise<{
        type: 'success' | 'cancel';
    }>;
    /**
     * 调用微信api
     *
     * @param {*} options 其它选项
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @returns {Promise}
     * @memberof WxLocate
     */
    private _openLocation;
    /**
     * 调用微信api
     *
     * @param {*} options 其它选项
     * @param {String} options.handlerWxApi 调用微信api的方法
     * @returns {Promise}
     * @memberof WxLocate
     */
    getLocate(options: ILocateConfig): Promise<ILocateData | null>;
    /**
     * 转换经纬度
     * @param {Object} options 配置参数
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @returns {Promise}
     * @memberof WxLocate
     */
    private _fromLocate;
}
/**
 * 实例化微信地图
 *
 * @export
 * @param {*} options 选项
 * @returns {WxLocate}
 */
export default function wxLocateFactory(options: ILocateConfig): WxLocate;
