import { LocatePart } from './LocatePart.base';
import { ILocateConfig } from '../../interface/locateConfig';
/**
 * 高德定位
 *
 * @export
 * @class GdLocate
 * @extends {LocatePart}
 */
export declare class GdLocate extends LocatePart {
    constructor(options: ILocateConfig);
    /**
     * 获取查询是否安装高德地图app的参数
     *
     * @param {*} options 选项
     * @returns {Promise}
     * @memberof GdLocate
     */
    getCheckParam(options: ILocateConfig): Promise<string | null>;
    /**
     * 唤起高德导航
     * @param {Object} options 地理位置信息
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @param {String} options.toLatitude 导航去的纬度
     * @param {String} options.toLongitude 当前经度
     * @param {String} options.city 当前市
     * @param {String} options.province 当前省
     * @param {String} options.name 地理位置名
     * @param {String} options.address 地址
     * @param {Boolean} isNative 是否跳转原生
     * @returns {Promise}
     */
    navigation(options: ILocateConfig, isNative: boolean): Promise<{
        type: 'success';
    }>;
    /**
     * 转换经纬度（百度转高德）
     * @param {Function} options 配置参数
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @param {String} options.toLatitude 导航去的纬度
     * @param {String} options.toLongitude 当前经度
     * @returns {Promise} 返回链接
     */
    private _fromLocate;
    /**
     * 获取链接
     * @param {Function} options 定位信息
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @param {String} options.toLatitude 导航去的纬度
     * @param {String} options.toLongitude 当前经度
     * @param {String} options.city 当前市
     * @param {String} options.province 当前省
     * @param {Boolean} isNative 是否跳转原生
     * @returns {Promise} 返回链接
     */
    private _getLink;
}
/**
 * 实例化高德地图
 *
 * @export
 * @param {*} options 选项
 * @returns {GdLocate}
 */
export default function gdLocateFactory(options: ILocateConfig): GdLocate;
