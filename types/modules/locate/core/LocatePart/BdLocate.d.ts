import { BdLocateBase } from './BdLocate.base';
import { ILocateConfig, ILocateData } from '../../interface/locateConfig';
import { ILocatePart } from '../../interface/locate';
/**
 * 百度定位
 *
 * @export
 * @class BdLocate
 * @extends {BdLocateBase}
 * @implements {ILocatePart}
 */
export declare class BdLocate extends BdLocateBase implements ILocatePart {
    constructor(options: ILocateConfig);
    /**
     * 优先调用浏览器H5定位接口，如果失败会调用IP定位
     * @param {*} options 选项
     * @returns {Promise}
     * @memberof BdLocate
     */
    getLocate(options: ILocateConfig): Promise<ILocateData>;
    /**
     * 优先调用浏览器H5定位接口，如果失败会调用IP定位
     * @param {*} options 选项
     * @returns {Promise}
     * @memberof BdLocate
     */
    getIpLocate(options: ILocateConfig): Promise<ILocateData>;
    /**
     * 唤起百度导航
     * @param {Object} options 地理位置信息
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @param {String} options.toLatitude 导航去的纬度
     * @param {String} options.toLongitude 当前经度
     * @param {String} options.city 当前市
     * @param {String} options.province 当前省
     * @param {String} options.locateToConfig 跳转的配置
     * @param {Boolean} isNative 是否跳转原生
     * @returns {Promise}
     */
    navigation(options: ILocateConfig, isNative: boolean): Promise<{
        type: 'success';
    }>;
    /**
     * 获取查询是否安装百度地图app的参数
     *
     * @param {*} options 选项
     * @returns {Promise}
     * @memberof GdLocate
     */
    getCheckParam(options: ILocateConfig): Promise<string | null>;
    /**
     * 获取链接
     * @param {Function} options 定位信息
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @param {String} options.toLatitude 导航去的纬度
     * @param {String} options.toLongitude 当前经度
     * @param {String} options.city 当前市
     * @param {String} options.province 当前省
     * @param {Boolean} isNative 是否跳转原生
     * @returns {Promise} 返回链接
     */
    private _getLink;
}
/**
 * 实例化百度地图
 *
 * @export
 * @param {*} options 选项
 * @returns {BdLocate}
 */
export default function bdLocateFactory(options: ILocateConfig): BdLocate;
