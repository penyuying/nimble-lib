import { LocateBase, ILocateConfig, IEventType, INavigationItem, IAppApiType, ILocateData, IMapType, ILocateType, INavigationType } from './Locate.base';
export declare class LocateCore extends LocateBase {
    private _isShowNavigation;
    private _getLocatePromise;
    private _isOpenPage;
    constructor(options: ILocateConfig);
    /**
     * 显示导航列表
     *
     * @param {Object} options 地理位置信息
     * @param {Boolean} isNative 是否跳转原生
     * @returns {Promise}
     * @memberof LocateCore
     */
    showNavigation(options: ILocateConfig, isNative?: boolean): Promise<{}>;
    /**
     * 调用Locate方法
     *
     * @param {*} type 类型
     * @param {*} fnName 方法名
     * @param {*} argList 调用方法的参数列表
     * @returns {Promise}
     * @memberof LocateCore
     */
    handlerLocate(type: ILocateType | INavigationType | IMapType, fnName: string, argList: any[]): Promise<any>;
    /**
     * 定位完成
     * @param {*} locate 地理位置数据
     * @param {*} options 选项
     * @param {*} isNotEmit 是否不派发事件
     * @returns {Promise}
     * @memberof CarCroe
     */
    protected _successfilter(locate: ILocateData | null, options: ILocateConfig, isNotEmit?: boolean): Promise<ILocateData | null>;
    /**
     * 设置地理位置信息
     * @param {*} locate 地理位置信息
     * @returns {Promise}
     * @protected
     */
    private _saveLocate;
    /**
     * 获取定位对象
     *
     * @param {NAVIGATION_TYPE|LOCATE_TYPE} type 对象类型
     * @returns {LocateObject}
     * @memberof LocateCore
     */
    private _locateFactory;
    /**
     * 获取本地的地理位置信息
     * @param {Object} options 配置参数
     * @returns {Promise} 返回地理位置信息
     */
    protected _getCurLocate(options: ILocateConfig): Promise<any>;
    /**
     * 获取定位
     *
     * @param {*} options 选项
     * @param {LOCATE_TYPE} type 类型
     * @returns {Promise}
     * @memberof LocateCore
     */
    protected _getLocate(options: ILocateConfig, type?: ILocateType): Promise<ILocateData>;
    /**
     * 显示地图
     * @param {Object} options 地理位置信息
     * @param {*} type 地图类型
     * @returns {Promise}
     * @memberof LocateCore
     */
    protected _showMap(options: ILocateConfig, type: IMapType): Promise<any>;
    /**
     * 唤起百度地图
     * @param {Object} options 地理位置信息
     * @returns {Promise}
     */
    protected _showBdMap(options: ILocateConfig): Promise<any>;
    /**
     * 唤起导航
     *
     * @param {Object} options 地理位置信息
     * @param {NAVIGATION_TYPE} type 类型
     * @param {Boolean} isNative 是否跳转原生
     * @returns {Promise}
     * @memberof LocateCore
     */
    protected _navigation(options: ILocateConfig, type: ILocateType | INavigationType, isNative?: boolean): Promise<{}>;
    /**
     * 切换城市
     * @param {*} locate 当前地理位置
     * @param {*} sLocate 选着的地理位置
     * @param {*} options 可选参数
     * @returns {*}
     */
    protected _switchLocate(locate?: ILocateData | null, sLocate?: ILocateData | null, options?: ILocateConfig): Promise<ILocateData | null>;
}
export { LocateBase, ILocateConfig, IEventType, INavigationItem, IAppApiType, ILocateData, IMapType, ILocateType, INavigationType };
