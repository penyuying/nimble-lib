import { IKeyValue } from '../../../library/helpers/IKeyValue';
import { BaseAbstract } from '../../../library/basic/base.abstrat';
import { ILocateConfig, IEventType, INavigationItem, IAppApiType, ILocateData, IMapType, ILocateType, INavigationType } from '../interface/locateConfig';
export declare class LocateBase extends BaseAbstract<ILocateConfig> {
    /**
     * 默认配置信息
     * @memberof LocateBase
     */
    protected defaultOption: ILocateConfig;
    protected isCheckLocate: null;
    protected _nowLocate?: ILocateData | null;
    protected _cacheLocate?: ILocateData | null;
    constructor(options: ILocateConfig);
    /**
     * 初始化数据
     *
     * @param {*} options 选项
     * @memberof LocateBase
     */
    protected _initData(options: ILocateConfig): void;
    /**
     * 地图事件
     *
     * @param {*} type 事件类型
     * @param {*} evt 事件对象
     * @memberof LocateCore
     */
    mapEvent(type: IEventType, evt: any): void;
    /**
     * 提示消息
     *
     * @param {*} msgCont 消息内容
     * @param {*} param 类型|参数
     * @returns {Promise}
     * @memberof CarBase
     */
    message(msgCont: string, param: IKeyValue): Promise<any>;
    /**
     * 调用原生
     * @param {String} type 原生接口名称
     * @param {Object} params 接口请求参数
     * @returns {Promise<Object|String>} 结果数据
     * @memberof LocateBase
     */
    protected _toNative(type: IAppApiType, params?: any): Promise<any>;
    /**
     * 是否调用原生
     *
     * @param {String} type 调用类型
     * @return {Boolean}
     * @memberof LocateBase
     */
    protected _isToNative(type: IAppApiType): any;
    /**
     * 获取跳转原生导航列表
     *
     * @returns {Promise}
     * @memberof LocateBase
     */
    protected _getNavigationList(): Promise<INavigationItem[]>;
}
export { ILocateConfig, IEventType, INavigationItem, IAppApiType, ILocateData, IMapType, ILocateType, INavigationType };
