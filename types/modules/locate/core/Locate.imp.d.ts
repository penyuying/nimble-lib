import { LocateCore, LocateBase, ILocateConfig, IEventType, INavigationItem, IAppApiType, ILocateData, IMapType, ILocateType, INavigationType } from './Locate.core';
export declare class Locate extends LocateCore {
    name: string;
    private _locatePromise;
    private _cacheTimeout;
    constructor(options: ILocateConfig);
    /**
     * 切换城市
     *
     * @param {*} selectCity 需要切换的城市
     * @param {*} options 选项
     * @returns {Promise}
     * @memberof Locate
     */
    selectCity(selectCity: ILocateData, options: ILocateConfig): Promise<ILocateData | null>;
    /**
     * 唤起百度地图
     * @param {Object} options 地理位置信息
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @param {String} options.name 地理位置名
     * @param {MAP_TYPE} type 类型
     * @returns {Promise}
     */
    showMap(options: ILocateConfig, type: IMapType): Promise<any>;
    /**
     * 获取本地地理位置
     * @param {Object} options 可选参数
     * @returns {Promise}
     * @memberof Locate
     */
    getLocalLocate(options: ILocateConfig): Promise<ILocateData | null>;
    /**
     * 定位（百度和api已有经纬度）
     * @param {Object} options 可选参数
     * @param {Number} options.timeout 获取超时时间
     * @param {LOCATE_TYPE} type 类型
     * @returns {Promise}
     * @memberof Locate
     */
    getCurLocate(options: ILocateConfig, type: ILocateType): Promise<ILocateData | null>;
    /**
     * 定位（百度和api已有经纬度）
     * @param {Object} options 可选参数
     * @param {Boolean} options.isReset 是否强制重新获取定位信息
     * @param {Number} options.timeout 获取超时时间
     * @param {LOCATE_TYPE} type 类型
     * @param {LOCATE_TYPE} isNotCLocate 不取本地的数据
     * @returns {Promise}
     * @memberof Locate
     */
    getLocate(options: ILocateConfig, type: ILocateType, isNotCLocate?: boolean): Promise<ILocateData | null>;
    /**
     * 唤起导航和地图（微信、百度、高德）
     * @param {Object} options 地理位置
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @param {String} options.name 地理位置名
     * @param {String} options.address 地址
     * @param {MAP_TYPE} type 类型
     * @returns {Promise}
     */
    locateTo(options: ILocateConfig, type: IMapType): Promise<any>;
    /**
     * 显示导航列表
     *
     * @param {Object} options 地理位置信息
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @param {String} options.fromLatitude 当前纬度
     * @param {String} options.fromLongitude 当前经度
     * @param {String} options.fromCity 当前经度
     * @param {String} options.fromProvince 当前省
     * @param {Boolean} isNative 是否跳转原生
     * @returns {Promise}
     * @memberof Locate
     */
    nativeMap(options: ILocateConfig, isNative?: boolean): Promise<{}>;
}
/**
 * 实例化工厂方法
 *
 * @export
 * @param {ILocateConfig} options 配置选项
 * @returns {Locate}
 */
export default function (options: ILocateConfig): Locate;
export { LocateCore, LocateBase, ILocateConfig, IEventType, INavigationItem, IAppApiType, ILocateData, IMapType, ILocateType, INavigationType };
