import { IBaseInterface } from '../../../library/basic/base.interface';
import { IStorageHandleOptions, IMemoryStorageHandleOptions } from './config';
export interface IStorageInterface extends IBaseInterface {
}
/**
 * 本地MemoryStorage存储
 */
export interface IStorageMemory {
    /**
     * 设置临时变量
     *
     * @param {String} key 存储的Key名
     * @param {any} value 存储的值
     * @memberof StorageClient
     */
    setMemory(key: string, value: any, options?: IMemoryStorageHandleOptions): void;
    /**
     * 获取临时变量
     *
     * @param {String} key 获取的Key名
     * @param {boolean} isDel 获取后是否删除
     * @returns {any} 返回存储的值
     * @memberof StorageClient
     */
    getMemory<T>(key: string, isDel?: boolean, options?: IMemoryStorageHandleOptions): T;
    /**
     * 移除指定Key的临时变量
     *
     * @param {String} key 移除的Key名
     * @memberof StorageClient
     */
    removeMemory(key: string, options?: IMemoryStorageHandleOptions): void;
    /**
     * 清除缓存的Data
     *
     * @memberof StorageClient
     */
    clearMemory(options?: IMemoryStorageHandleOptions): void;
}
/**
 * 本地SessionStorage存储
 */
export interface IStorageSession {
    /**
     * 设置Session
     *
     * @param {String} key 存储的Key名
     * @param {any} value 存储的值
     * @memberof StorageClient
     */
    setSession(key: string, value: any, options?: IStorageHandleOptions): void;
    /**
     * 获取session
     *
     * @param {String} key 获取的Key名
     * @param {Boolean} isDel 获取后是否删除
     * @returns {T} 返回存储的值
     * @memberof StorageClient
     */
    getSession<T>(key: string, isDel?: boolean): T;
    /**
     * 移除指定Key的Session
     *
     * @param {String} key 移除的Key名
     * @memberof StorageClient
     */
    removeSession(key: string): void;
    /**
     * 清除缓存的Session
     *
     * @memberof StorageClient
     */
    clearSession(): void;
}
/**
 * 本地LocalStorage存储
 */
export interface IStorageLocal {
    /**
     * 设置local存储的值
     *
     * @param {String} key 存储的Key名
     * @param {any} value 存储的Key值
     * @memberof StorageClient
     */
    setLocal(key: string, value: any, options?: IStorageHandleOptions): void;
    /**
     * 获取local存储的值
     *
     * @param {String} key 获取的Key名
     * @param {Boolean} [isDel]
     * @returns {*}
     * @memberof StorageClient
     */
    getLocal<T>(key: string, isDel?: boolean): T;
    /**
     * 移除指定Key的local的值
     *
     * @param {String} key 移除的Key名
     * @memberof StorageClient
     */
    removeLocal(key: string): void;
    /**
     * 清除缓存的Local
     *
     * @memberof StorageClient
     */
    clearLocal(): void;
}
/**
 * 本地Cookie存储
 */
export interface IStorageCookie {
    /**
     * 设置Cookie
     *
     * @param {string} key 设置的Cookie名称
     * @param {string} value 设置的Cookie值
     * @param {number} time 设置Cookie失效时间
     * @memberof StorageClient
     */
    setCookie(key: string, value: string, time?: number): void;
    /**
     * 获取cookie的值
     *
     * @param {string} key 设置的Cookie名称
     * @returns {string|Null} 返回对应Key的值或null
     * @memberof StorageClient
     */
    getCookie(key: string): string | null;
    /**
     * 移除cookie对应key的值
     *
     * @param {string} key 移除的Cookie的名称
     * @memberof StorageClient
     */
    removeCookie(key: string): void;
    /**
     * 清除所有Cookie
     */
    clearCookie(): void;
}
