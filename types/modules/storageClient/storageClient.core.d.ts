import { MemoryStorage } from './memoryStorage';
import { BaseAbstract } from '../../library/basic/base.abstrat';
import { IStorageInterface } from './interface/storage';
import { IStorageInitConfig, IStorageHandleOptions, IMemoryStorageHandleOptions } from './interface/config';
/**
 * 本地存储核心类
 *
 * @export
 * @abstract
 * @class StorageCore
 * @extends {BaseAbstract<any>}
 * @implements {IStorageInterface}
 */
export declare abstract class StorageCore extends BaseAbstract<any> implements IStorageInterface {
    /**
     * 是否支持sessionStorage
     *
     * @static
     * @type {(Boolean|undefined)}
     * @memberof StorageCore
     */
    static isSess: Boolean | undefined;
    /**
     * 是否支持localStorage
     *
     * @static
     * @type {(Boolean|undefined)}
     * @memberof StorageCore
     */
    static isLocal: Boolean | undefined;
    /**
     * 存储缓存Storage对象
     *
     * @static
     * @type {(MemoryStorage|undefined)}
     * @memberof StorageCore
     */
    static _cacheStorage: MemoryStorage | undefined;
    /**
     * 判断浏览是否支持storage
     *
     * @private
     * @type {StorageJudge}
     * @memberof StorageCore
     */
    private storageJudge;
    /**
     * 默认选项
     *
     * @protected
     * @type {IStorageInitConfig}
     * @memberof StorageCore
     */
    protected defaultOption: IStorageInitConfig;
    /**
     * 本地存储核心类
     * @param {IStorageInitConfig} config 默认参数
     * @memberof StorageCore
     */
    constructor(config: IStorageInitConfig);
    /**
     * 获取Storage对象(对Storage做兼容和缓存处理)
     *
     * @private
     * @param {string} key 存储类型的KEY
     * @param {string} isStorageType 存放的类型（s存session；l存local；其它为暂存）
     * @returns {SStorage}
     * @memberof SStorageService 返回HttpStorage对象
     */
    private getStorage;
    /**
     * 更新数据
     *
     * @private
     * @param {(MemoryStorage|Storage)} storage SStorage|Storage类型
     * @param {string} key 缓存的Key名
     * @param {*} value 存储的值
     * @param {string} [isStorageType] 存放的类型（s存session；l存local；其它为暂存）
     * @memberof StorageCore
     */
    private updateStorageItem;
    /**
     * 保存数据
     *
     * @protected
     * @template T
     * @param {T} storage SStorage|Storage类型
     * @param {string} key 缓存的Key名
     * @param {*} value 存储的值
     * @param {string} [isStorageType] 存放的类型（s存session；l存local；其它为暂存）
     * @memberof SStorageCore
     */
    protected setStorageItem(storage: Storage, key: string, value: any, isStorageType?: string, options?: IStorageHandleOptions): void;
    /**
     * 读取StorageItem的值
     *
     * @private
     * @param {(Storage)} storage
     * @param {string} key
     * @param {string} [isStorageType]
     * @returns {(IStorageItem | undefined)}
     * @memberof StorageCore
     */
    private readStorageItem;
    /**
     * 获取数据
     *
     * @protected
     * @param {(Storage)} storage Storage对象
     * @param {string} key 获的Key名
     * @param {boolean} isDel 获取后是否删除
     * @param {string} isStorageType 获取的类型（s取session；l取local；其它为存临时）
     * @returns {any} 返回获取的值
     * @memberof SStorageService
     */
    protected getStorageItem<T>(storage: Storage, key: string, isDel?: boolean, isStorageType?: string): T | any;
    /**
     * 移除对应Key的数据
     *
     * @protected
     * @param {*} storage storage Storage对象
     * @param {string} key 删除的Key名
     * @memberof SStorageService
     */
    protected removeStorageItem(storage: any, key: string): void;
    /**
     * 清空Storage对象
     *
     * @protected
     * @param {*} storage storage Storage对象
     * @memberof SStorageCore
     */
    protected clearStorage(storage: any): void;
    /**
     * 获取sessionStorage对象
     *
     * @protected
     * @returns {sessionStorage} 返回sessionStorage对象
     * @memberof SStorageService
     */
    protected getSessionStorage(): Storage;
    /**
     * 获取localStorage对象
     *
     * @protected
     * @returns {localStorage} 返回localStorage对象
     * @memberof SStorageService
     */
    protected getLocalStorage(): Storage;
    /**
     * 获取MemoryStorage对象
     *
     * @protected
     * @returns {localStorage} 返回localStorage对象
     * @memberof SStorageService
     */
    protected getMemoryStorage(options?: IMemoryStorageHandleOptions): Storage;
}
export { MemoryStorage, IStorageInitConfig, IStorageInterface, IStorageHandleOptions, IMemoryStorageHandleOptions };
