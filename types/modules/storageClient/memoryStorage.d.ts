/**
 * 内存存储
 *
 * @export
 * @class MemoryStorage
 * @implements {Storage}
 */
export declare class MemoryStorage implements Storage {
    private _key;
    /**
     * 缓存数据
     */
    static _paramCache: any;
    /**
     * 缓存数据
     *
     * @protected
     * @memberof MemoryStorage
     */
    protected _paramCache: {};
    /**
     * 已存数据的个数
     *
     * @readonly
     * @memberof MemoryStorage
     */
    readonly length: number;
    /**
     * 内存储存数据
     * @param {*} key Storage的key
     * @memberof MemoryStorage
     */
    constructor(_key: string);
    /**
     * 获取保存的key
     *
     * @param {number} [index=0]
     * @returns
     * @memberof MemoryStorage
     */
    key(index?: number): string | null;
    /**
     * 储存数据
     * @param {String} key 缓存的key名
     * @param {*} value 值
     */
    setItem(key: string, value: any): void;
    /**
     * 获取登录时的报文体
     * @param {String} key 缓存的key名
     * @returns {type}
     */
    getItem(key: string): any | null;
    /**
     * 移除指定Key的数据
     * @param {type} key 缓存的key名
     */
    removeItem(key: string): void;
    /**
     * 清除所有数据
     */
    clear(): void;
}
