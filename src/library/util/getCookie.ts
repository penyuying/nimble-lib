/**
 * 获取cookie
 *
 * @export
 * @param {string} key cookie的key
 * @returns {(string | null)}
 */
export default function getCookie(key: string): string | null {
    let _cookie = document.cookie;
    let arr = _cookie && _cookie.split(/;\s{0,1}/);
    let res = {};
    if (arr && arr.length) {
        for (let i = 0; i < arr.length; i++) {
            const item = (arr[i] || '') + '';
            const itemArr = item.split('=');
            if (itemArr && itemArr.length) {
                res[itemArr[0]] = decodeURIComponent(itemArr[1] || '');
            }
        }
    }
    return key ? res[key] || null : res;
}
