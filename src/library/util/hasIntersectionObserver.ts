const hasIntersectionObserver = (typeof window !== 'undefined') && 'IntersectionObserver' in window;

export default hasIntersectionObserver;
