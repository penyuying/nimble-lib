/**
 * 格式化时间
 *
 * @param {Date} _date 时间对象
 * @param {string} [fmt='yyyy-MM-dd hh:mm:ss'] 时间的格式
 * @returns {string}
 */
export default function formatDate(_date: Date|string|number, fmt?: string): string {
    _date = toDate(_date);
    if (!_date) {
        return '';
    }
    fmt = (fmt || '') + '' || 'yyyy-MM-dd hh:mm:ss';
    let o = {
        'M+': _date.getMonth() + 1, // 月份
        'd+': _date.getDate(), // 日
        'h+': _date.getHours(), // 小时
        'm+': _date.getMinutes(), // 分
        's+': _date.getSeconds(), // 秒
        'q+': Math.floor((_date.getMonth() + 3) / 3), // 季度
        'S': _date.getMilliseconds() // 毫秒
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (_date.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (let k in o) {
        if (new RegExp('(' + k + ')').test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)));
        }
    }
    return fmt;
}
/**
 * 转换时间为Date对象
 *
 * @param {*} dateStr
 * @returns {Date}
 */
function toDate(dateStr: any): Date {
    let res = dateStr;
    let str: any = '';
    if (typeof dateStr === 'string') {
        str = dateStr.replace('-', '/');
    } else if (typeof dateStr === 'number') {
        str = dateStr;
    }
    if (str) {
        res = new Date(str);
    }
    return res;
}
