import scrollParent from './scrollParent';
/**
 * 获取当前元素所在的滚动条元素
 * @param {Element} el 当前元素
 * @returns {Element}
 */
export default function getScrollEl(el: HTMLElement) {
    let _el = scrollParent(el);
    if (_el && Math.abs(_el.offsetHeight - _el.scrollHeight) < 50) {
        if (_el !== window) {
            _el = getScrollEl(_el);
        } else {
            _el = null;
        }
    }
    if (_el === window) {
        let _scrollHeight = document.body.scrollHeight || document.documentElement.scrollHeight;
        if (_scrollHeight <= (window.innerHeight - 5)) {
            _el = null;
        }
    }
    return _el;
}
