import { IKeyValue } from '../helpers/IKeyValue';
import isPlainObject from './isPlainObject';
/**
 * 过滤字符串前后空格
 * @param {any} data 需要过滤的数据
 * @return {any}
 */
export default function stringTirm (data: IKeyValue|string|Array<IKeyValue|string>) {
    if (data) {
        if (typeof data === 'string') {
            data = data.replace(/^\s*|\s*$/g, '');
        } else if (data instanceof Array) {
            data.map(item => {
                return stringTirm(item);
            });
        } else if (isPlainObject(data)) {
            for (const key in data) {
                if (data.hasOwnProperty(key)) {
                    const item = data[key];
                    data[key] = stringTirm(item);
                }
            }
        }
    }
    return data;
}
