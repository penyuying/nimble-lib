/**
 * 计算两个时间之前的相差时间
 *
 * @param {String} startTime 开始时间
 * @param {String} endTime 结束时间
 * @return {Number} 返回用掉的时间
 */
export default function pastTimeCount(startTime: any, endTime: any) {
    if (!startTime || !endTime) {
        return 0;
    }
    if (typeof startTime === 'string') {
        startTime = startTime && (startTime + '').replace(/-/g, '/');
    }
    if (typeof endTime === 'string') {
        endTime = endTime && (endTime + '').replace(/-/g, '/');
    }

    let _startTime: any;
    let _endTime: any;
    if (startTime instanceof Date) {
        _startTime = startTime;
    } else {
        _startTime = new Date(startTime);
    }
    if (endTime instanceof Date) {
        _endTime = endTime;
    } else {
        _endTime = new Date(endTime);
    }

    _startTime = _startTime && _startTime.getTime();
    _endTime = _endTime && _endTime.getTime();
    return ((_endTime - _startTime) > 0 && _endTime - _startTime) || 0;
}
