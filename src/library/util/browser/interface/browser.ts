import { IBaseInterface } from '../../../basic/base.interface';

/**
 * 浏览器内核版本
 *
 * @export
 * @interface IBrowser
 */
export interface IBrowser extends IBaseInterface {
    /**
     * IE内核
     */
    trident: Boolean;
    /**
     * opera内核
     */
    presto: Boolean;
    /**
     * 苹果、谷歌内核
     */
    webKit: Boolean;
    /**
     * 火狐内核
     */
    gecko: Boolean;
    /**
     * 是否为移动终端
     */
    mobile: Boolean;
    /**
     * ios终端
     */
    ios: Boolean;
    /**
     * android终端或者uc浏览器
     */
    android: Boolean;
    /**
     * 是否为iPhone或者QQHD浏览器
     */
    iPhone: Boolean;
    /**
     * 是否iPad
     */
    iPad: Boolean;
    /**
     * 是否是塞班系统
     */
    symbian: Boolean;
    /**
     * 是否Windows Phone
     */
    windowPhone: Boolean;
    /**
     * 是否是iPod
     */
    iPod: Boolean;
    /**
     * 微信
     */
    weixin: Boolean;
    /**
     * app版本
     */
    appVersion: string;
    /**
     * 浏览器版本
     */
    browserVersion: string;
    /**
     * 内核名称
     */
    kernel: string;
    /**
     * 腾讯新闻
     */
    qqnews: Boolean;
    /**
     * 微信小程序
     */
    isMiniProgram: Boolean;
}


/**
 * 浏览器设备
 *
 * @export
 * @interface IDevice
 */
export interface IDevice extends IBaseInterface {
    /**
     * 移动端
     */
    mobile: Boolean;
    /**
     * pc端
     */
    pc: Boolean;
    /**
     * ios
     */
    ios: Boolean;
    /**
     * 微信
     */
    weixin: Boolean;
    /**
     * 微信
     */
    isWeixin: Boolean;
    /**
     * 是否iPad
     */
    iPad: Boolean;
    /**
     * android
     */
    android: Boolean;
    /**
     * 判断是否为iphoneX
     */
    isIphoneX: Boolean;
    /**
     * 是否为app环境
     */
    isApp: Boolean;
    /**
     * 是否为快应用环境
     */
    isLightApp: Boolean;
    /**
     * app版本
     */
    appVersion: string;
    /**
     * 浏览器版本
     */
    browserVersion: string;
    /**
     * 内核名称
     */
    kernel: string;
    /**
     * 腾讯新闻
     */
    isQqnews: Boolean;
    /**
     * 微信小程序
     */
    isMiniProgram: Boolean;
}
