import { IDevice } from './interface/browser';
import browserInfo from './browserInfo';
/**
 * 硬件设备
 *
 * @returns {IBrowserDevice} IBrowserDevice
 * @memberof BrowserService
 */
const deviceInfo: IDevice = (function () {
    let os = browserInfo;
    let ret: IDevice = {
        mobile: false, // 移动端
        pc: false, // pc端
        ios: os.ios || os.iPhone || os.iPad || false, // ios
        weixin: os.weixin || false, // 微信
        isWeixin: os.weixin || false, // 微信
        iPad: os.iPad || false, // iPad
        isApp: false, // 是否为app
        appVersion: os.appVersion, // app版本
        browserVersion: os.browserVersion, // 浏览器版本
        isLightApp: false, // 是否为快应用
        kernel: os.kernel, // 判断内核
        isIphoneX: os.iPhone && (screen.height === 812 && screen.width === 375), // 判断是否为iphoneX
        android: os.android, // android
        isQqnews: os.qqnews, // 腾讯新闻
        isMiniProgram: os.isMiniProgram // 微信小程序
    };

    if (os.mobile || os.ios || os.android || os.iPhone || os.iPad ||
        os.symbian || os.windowPhone || os.iPod || ret.isApp) { // 判断移动端
        ret.mobile = true;
    } else {
        ret.pc = true;
    }

    return ret;
})();

export default deviceInfo;
