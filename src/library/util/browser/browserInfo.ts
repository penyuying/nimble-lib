///<reference path="./interface/miniprogram.d.ts" />
import { IBrowser } from './interface/browser';

/**
 * 浏览器内核版本
 *
 * @returns {IBrowserVersions} IBrowserVersions
 * @memberof BrowserService
 */
const browserInfo: IBrowser = (function() {
    const u = navigator.userAgent;
    return {// 移动终端浏览器版本信息
        trident: u.indexOf('Trident') > -1, // IE内核
        presto: u.indexOf('Presto') > -1, // opera内核
        webKit: u.indexOf('AppleWebKit') > -1, // 苹果、谷歌内核
        gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') === -1, // 火狐内核
        mobile: !!u.match(/AppleWebKit.*Mobile.*/), // 是否为移动终端
        ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), // ios终端
        // android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, // android终端或者uc浏览器
        android: u.indexOf('Android') > -1 || u.indexOf('Adr') > -1, // android终端或者uc浏览器
        iPhone: u.indexOf('iPhone') > -1, // 是否为iPhone或者QQHD浏览器
        iPad: u.indexOf('iPad') > -1, // 是否iPad
        symbian: u.indexOf('SymbianOS') > -1, // 是否是塞班系统
        windowPhone: u.indexOf('Windows Phone') > -1, // 是否Windows Phone
        iPod: u.indexOf('iPod') > -1, // 是否是iPod
        weixin: /MicroMessenger/i.test(u), // 微信
        browserVersion: (u.match(/version\/(.+?)\s/i) || [])[1], // 浏览器版本号
        appVersion: '', // app版本号
        kernel: (/tencenttraveler|qqbrowse/i.test(u) && 'x5') || '', // 判断内核
        qqnews: /qqnews/i.test(u), // 腾讯新闻
        isMiniProgram: window.__wxjs_environment === 'miniprogram' || /miniProgram/i.test(u) // 微信小程序
    };
})();

export default browserInfo;
