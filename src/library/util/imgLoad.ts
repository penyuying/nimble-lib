import callFn from './callFn';

let imgLoadData = {};
/**
 * 图片加载完后回调后的返回数据结构
 */
interface IImgLoadBack {
    /**
     * 加载完成的图片高度
     */
    naturalHeight: number;
    /**
     * 加载完成的图片宽度
     */
    naturalWidth: number;
    /**
     * 加载完成的图片地址
     */
    src: string;
}
/**
 * 图片加载
 *
 * @export
 * @param {String} src 图片地址
 * @param {Function} startCb 开始前的回调(返回true则不加载)
 * @returns {Promise}
 */
export default function imgLoad(src: string, startCb: (image: HTMLImageElement, src: string) => boolean|void): Promise<IImgLoadBack> {
    return new Promise((resolve, reject) => {
        if (!src) {
            reject(new Error('src不能为：' + src));
            return;
        }
        // let isNotLoad = false;
        let image: any = new Image();
        let isNotLoad = callFn(startCb, [image, src]) || false;
        // if (startCb instanceof Function) {
        //     isNotLoad = startCb(image, src) || false;
        // }
        if (imgLoadData[src]) {
            image = null;
            resolve(imgLoadData[src]);
            return;
        }

        image.onload = () => {
            imgLoadData[src] = {
                naturalHeight: image.naturalHeight || image.height,
                naturalWidth: image.naturalWidth || image.width,
                src: image.src
            };
            resolve(imgLoadData[src]);
            image = null;
        };

        image.onerror = (e: Event) => {
            reject(e);
            image = null;
        };
        if (!isNotLoad) {
            image.src = src;
        }
    });
}


export {
    IImgLoadBack
};
