
import generateUUID from './generateUUID';

/**
 * 获取统一的回调的名称
 * @param {String} preFix 前缀
 * @returns {String}
 */
export default function genGlobalBackName(preFix: string) {
    let uuid = generateUUID().replace(/[-—]/g, '');
    let res = ((preFix || '_global_callback_') + '') + uuid;
    if (window[res]) {
        res = genGlobalBackName(preFix);
    }
    return res;
}
