// import Service from './service';
import getBackData from './getBackData';
import callFn from './callFn';

export class ClassProxy {
    _watchNames: any[] = [];
    constructor() {}

    /**
     * 调用代理监听方法
     * @param {*} obj 代理对象
     * @param {*} proxyObj 代理的源对像
     * @private
     */
    private _handlerWatch(obj: object, proxyObj: any) {
        let _that = this;
        let _watchNames = _that._watchNames;
        if (!obj || !proxyObj || !_watchNames || _watchNames.length <= 0) {
            return;
        }
        _watchNames && _watchNames.forEach(fnName => {
            let argList = (obj[fnName] && obj[fnName]._args) || [];
            obj[fnName] = (...args: any[]) => {
                proxyObj[fnName] && getBackData(proxyObj[fnName].bind(proxyObj), args); // .then(resolve, reject);
            };
            if (argList && argList.length > 0) {
                argList.forEach((args: any) => {
                    proxyObj[fnName] && getBackData(proxyObj[fnName].bind(proxyObj), args);
                });
            }
        });
        _that._watchNames = [];
    }

    /**
     * 遍历方法名
     * @param {*} proxyNames 代理的方法名称列表
     * @param {*} cb 回调
     * @return {Array<string>}
     * @private
     */
    private _eachProxyNames(proxyNames: string[]|string, cb: (fnName: string) => void) {
        if (proxyNames && typeof proxyNames === 'string') {
            proxyNames = [proxyNames];
        }
        if (!(proxyNames instanceof Array) && proxyNames.length < 1) {
            console.error(new Error('function name err'));
            return;
        }
        if (!proxyNames) {
            return;
        }
        if (proxyNames instanceof Array) {
            proxyNames.forEach(fnName => {
                callFn(cb, [fnName]);
            });
        }
        return proxyNames;
    }
    /**
     * 代理方法钩子
     *
     * @param {*} obj 代理对象
     * @param {*} getObjCb 获取对象的回调
     * @param {*} proxyNames 代理的方法名称列表
     * @param {*} proxyWatchNames 代理的监听方法名称列表
     * @memberof classProxy
     * @public
     */
    proxyHook(obj: object, getObjCb: () => Promise<object>, proxyNames: string[]|string, proxyWatchNames: string[]|string) {
        let _that = this;

        if (proxyWatchNames) {
            _that.proxyWatch(obj, proxyWatchNames);
        }
        _that._eachProxyNames(proxyNames, (fnName: string) => {
            obj[fnName] = (...args: any[]) => {
                return new Promise((resolve, reject) => {
                    getBackData(getObjCb).then(res => {
                        if (!res) {
                            reject(new Error('load err'));
                            return;
                        }
                        _that._handlerWatch(obj, res);
                        if (res[fnName]) {
                            getBackData(res[fnName].bind(res), args).then(resolve, reject);
                        } else {
                            resolve(undefined);
                        }
                    }, reject);
                });
            };
        });
    }
    /**
     * 代理监听方法
     * @param {*} obj 代理对象
     * @param {*} proxyNames 代理的方法名称列表
     * @private
     */
    private proxyWatch(obj: object, proxyNames: string[]|string) {
        let _that = this;
        let _proxyNames = _that._eachProxyNames(proxyNames, fnName => {
            obj[fnName] = (...args: any[]) => {
                obj[fnName]._args = obj[fnName]._args || [];
                obj[fnName]._args.push(args);
            };
        });
        _that._watchNames = _that._watchNames.concat(_proxyNames || []);
    }
    /**
     * 初始化代理
     *
     * @param {*} loadCb 加载源对象的回调
     * @param {*} fnName 代理方法名
     * @returns {Function}
     * @memberof classProxy
     * @public
     */
    initProxy(loadCb: () => Promise<object>, fnName: string) {
        fnName = (fnName || '') + '';
        return (...args: any[]) => {
            return new Promise((resolve, reject) => {
                getBackData(loadCb).then(back => {
                    let data = (back && back.default) || back;
                    let res;
                    if (!data) {
                        reject(new Error('load err'));
                        return;
                    }
                    res = callFn(fnName && data[fnName] || res, args, data);
                    if (res instanceof Promise) {
                        res.then(resolve, reject);
                    } else {
                        resolve(res || data);
                    }
                });
            });
        };
    }
}
