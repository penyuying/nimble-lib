import isPassive from './isPassive';
/**
 * 监听和取消监听事件
 * @param {HtmlElement} el 事件监听dom元素
 * @param {String} types 注册的事件名称
 * @param {Function} func 事件回调方法
 * @param {add|remove} flag 事件方法
 * @param {Object|Boolean} capture 事件注册参数
 */
export default function initEventListener (el: HTMLElement, types: string|string[], func: Function,
    flag: 'add'|'remove' = 'add', capture: any = false) {
    let prop = capture;
    if (flag !== 'remove') {
        if (!(capture instanceof Object) && isPassive) {
            prop = {
                capture: capture,
                passive: true
            };
        }
    }
    if (types instanceof Array) {
        types.forEach(type => {
            setEvent(type);
        });
    } else {
        setEvent(types);
    }
    /**
     * 设置事件
     *
     * @param {*} type 事件类型
     */
    function setEvent(type: string|string[]) {
        if (type) {
            el && el[flag + 'EventListener'](type, func, prop);
        }
    }
}
