import isType from './isType';

let hasOwn = Object.prototype.hasOwnProperty;
/**
 * 判断是否为纯对象
 *
 * @param {*} obj
 * @returns
 */
export default function isPlainObject(obj: any) {
    if (!isType(obj, 'Object')) {
        return false;
    }
    let proto = Object.getPrototypeOf(obj);

    // 没有原型的对象是纯粹的，Object.create(null) 就在这里返回 true
    if (!proto) {
        return true;
    }

    /**
     * 以下判断通过 new Object 方式创建的对象
     * 判断 proto 是否有 constructor 属性，如果有就让 Ctor 的值为 proto.constructor
     * 如果是 Object 函数创建的对象，Ctor 在这里就等于 Object 构造函数
     */
    let Ctor = hasOwn.call(proto, 'constructor') && proto.constructor;

    // 在这里判断 Ctor 构造函数是不是 Object 构造函数，用于区分自定义构造函数和 Object 构造函数
    return typeof Ctor === 'function' && hasOwn.toString.call(Ctor) === hasOwn.toString.call(Object);
}
