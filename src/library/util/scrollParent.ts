
import getStyle from './getStyle';
/**
 * 获取父级有可能出现滚动条的元素
 * @param { HTMLElement } el 当前元素
 * @return {HTMLElement|window}
 */
export default function scrollParent(el: HTMLElement) {
    if (!(el instanceof HTMLElement)) {
        return window;
    }

    let parent: any = el;

    while (parent) {
        if (parent === document.body || parent === document.documentElement) {
            break;
        }
        if (parent !== el) {
            if (parent._isScroll) {
                return parent;
            }

            if (!parent.parentNode) {
                break;
            }
            if (parent._isScroll !== false) {
                if (/(scroll|auto)/.test(overflow(parent))) {
                    parent._isScroll = true;
                    return parent;
                } else {
                    parent._isScroll = false;
                }
            }
        }


        parent = parent.parentNode;
    }

    return window;
}

/**
 * 获取overflow的值
 *
 * @param {HTMLElement} el 当前元素
 * @returns {String}
 */
function overflow(el: HTMLElement) {
    return getStyle(el, 'overflow') + getStyle(el, 'overflow-y') + getStyle(el, 'overflow-x');
}
