/**
 * 给数字加前缀
 *
 * @export
 * @param {(number|string)} value 内容
 * @param {number} [leng=1] 长度
 * @param {number} isExt 是否为后缀加0
 * @returns
 */
export default function prefixInt(value: number|string, leng: number = 1, isExt?: boolean) {
    let res = value = (value || '0') + '';
    let _res = res;
    if (value.length < leng || isExt && value.length !== leng && leng > -1) {
        let fix = (new Array(leng + 1)).join ('0');
        res = ((!isExt && fix || '') + value + (isExt && fix || ''));
        if (isExt) {
            _res = res.slice(0, leng);
        } else {
            _res = res.slice(-leng);
        }
    }
    return _res;
}
