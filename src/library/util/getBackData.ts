import callFn from './callFn';
import isType from './isType';

/**
 * 获取回调数据
 *
 * @param {*} dataFn 数据方法 (默认返回dataFn,如果)
 * @param {Array} arr 参数列表
 * @param {Boolean} isDefalut 如果传入的不是Promise对象是否把内空加到default属性上
 * @param {*} context this上下文
 * @return {Promise}
 */
export default function getBackData (dataFn: any, arr?: any, isDefalut?: boolean, context?: any): Promise<any> { // 获取callback的数据
    let res = dataFn; // 有可能它不是一个回调，直接是数据
    // if (typeof dataFn !== 'undefined') {
    //     res = dataFn;
    // }
    if (arr && !(arr instanceof Array)) {
        arr = [arr];
    }
    return new Promise((resolve, reject) => {
        res = callFn(dataFn || res, arr, context || null);
        if (res instanceof Promise || isType(res, 'Promise') || res && res.then instanceof Function) {
            res.then(resolve, reject);
        } else {
            if (isDefalut) {
                resolve({
                    default: res
                });
            } else {
                resolve(res);
            }
        }
    });
}
