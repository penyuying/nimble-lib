/**
 * 浅比较2个对象的值是否相等
 * @param {Object} defObj 比较对象
 * @param {Object} obj 被比较对象
 * @param {Array<string>?} keys 需要比较的key列表
 * @returns {Boolean} 对象比较结果
 */
export default function diffData (defObj: any, obj: any, keys?: string[]): boolean {
    let res = false;
    let defType: any = defObj instanceof Object;
    let _type: any = obj instanceof Object;
    if (defObj === obj) {
        return true;
    }
    if (defType && defType === _type) {
        defType = Object.prototype.toString.call(defObj);
        _type = Object.prototype.toString.call(obj);
    }
    // 判断两个对象有一个为空一个不为空或两个有一个类型为Object一个不为object
    if ((!defObj && obj) || (defObj && !obj) || (defType && !_type) || (!defType && _type)) {
        return res;
    }

    // 判断基础值类型
    if (!defType || !_type) {
        if (defObj === obj) {
            res = true;
        }
        return res;
    }

    if (_diffjson(defObj, obj)) {
        res = true;
    } else {
        if (keys && keys.length) {
            res = _diffValue(keys, defObj, obj);
        } else {
            const aProps = Object.keys(defObj);
            const bProps = Object.keys(obj);

            if (aProps.length === bProps.length) {
                res = _diffValue(aProps, defObj, obj);
            }
        }
    }

    return res;
    /**
     * 对比值是否相等
     *
     * @param {*} keyList 对比的key
     * @param {*} _defObj 比较对象
     * @param {*} _obj 被比较对象
     * @returns {Boolean} 对象比较结果
     * @private
     */
    function _diffValue(keyList: string[], _defObj: any, _obj: any): boolean {
        let _res = true;
        for (let i = 0; i < keyList.length; i++) {
            const key = keyList[i];
            if (key !== '__ob__') { // 防止vue里死循环
                _res = diffData(_defObj[key], _obj[key]);
                if (!_res) {
                    break;
                }
            }
        }
        return _res;
    }
    /**
     * 比较两个对象的json字符串
     *
     * @param {*} _defObj 对象1
     * @param {*} _obj 对象2
     * @returns {Boolean}
     * @private
     */
    function _diffjson(_defObj: any, _obj: any): boolean {
        // eslint-disable-next-line no-self-compare
        let _res = false;
        try {
            _res = JSON.stringify(_defObj) === JSON.stringify(_obj);
        } catch (error) {
            _res = false;
        }
        return _res;
    }
}
