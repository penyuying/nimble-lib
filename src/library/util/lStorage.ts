import { IStorageLocal } from '../../modules/storageClient/interface/storage';

let _ls = localStorage;
export class LStorage implements IStorageLocal {
    setLocal (key: string, value: any) {
        if (!key || typeof value === 'undefined') {
            return;
        }
        _ls.setItem(key, JSON.stringify(value));
    }
    getLocal<T> (key: string): T {
        let res: any = _ls.getItem(key);
        if (res) {
            try {
                res = JSON.parse(res);
            } catch (error) {
                res = null;
                _ls.removeItem(key);
            }
        }
        return res;
    }
    removeLocal(key: string): void {
        return _ls.removeItem(key);
    }
    clearLocal() {
        return _ls.clear();
    }
}

export default new LStorage();
