import toCamel from '../util/toCamel';
import toPascal from '../util/toPascal';
import prefixStyle from './prefixStyle';
let transformNames = ['Moz', 'moz', 'Webkit', 'webkit', 'O', 'o', 'MS', 'ms'];
let _eventNameCache = {};
/**
 * 给css3样式加前缀
 *
 * @export
 * @param {any} type 事件名称
 * @param {Boolean} isEvent 是否为事件
 * @param {Element} [el = window] 事件元素
 * @returns {String} 加过前缀的样式
 */
export default function prefixEvent(type: string, isEvent?: boolean, el?: any): string {
    type = ((type || '') + '').replace(/\s+/g, '');
    let _el: any = el || window;
    let _on = isEvent ? 'on' : '';
    if ((_on + type) in _el) {
        return type;
    }
    let _txt = _on ? type : toPascal(type);
    _txt = _txt.replace(/(start$|end$|animation|frame$)/gi, ($0, $1) => {
        return toPascal($1);
    });
    for (let i = 0; i < transformNames.length; i++) {
        const _fix = transformNames[i];
        let _type = _fix + _txt;
        let _type1 = _fix + type;
        if ((_on + _type) in _el) {
            transformNames = [_fix];
            _eventNameCache[type] = _type;
            return _type;
        }
        if ((_on + _type1) in _el) {
            transformNames = [_fix];
            _eventNameCache[type] = _type1;
            return _type1;
        }
    }
    let fix = prefixStyle('transition-property', true);
    if (fix && (_on + fix + _txt) in _el) {
        type = fix + _txt;
    }
    return isEvent ? type : '';
}
