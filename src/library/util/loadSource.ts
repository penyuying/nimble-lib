import { IKeyValue } from './../helpers/IKeyValue';
const cacheMap = {};

/**
 * 加载的数据配置
 * 包括js类型资源
 * css类型资源
 */
const typeMap = {
    js: {
        position: 'body',
        tagName: 'script',
        src: 'src',
        attr: {
            type: 'text/javascript',
            async: 'async'
        }
    },
    css: {
        tagName: 'link',
        src: 'href',
        attr: {
            rel: 'stylesheet'
        }
    }
};
/**
 * 获取文件类型
 *
 * @param {String} src 资源地址
 * @param {String} type 资源类型
 * @returns {String}
 */
function getType(src: string, type?: 'js'|'css'|string) {
    let _src = src.replace(/^\s+|\s+$/, ''); // 去除空格
    let _srcArr = _src && _src.match(/\.([^.]+)$/);
    let _extFix;
    if (_srcArr && _srcArr.length > 1) {
        _extFix = _srcArr.pop();
    }
    if (!_extFix) {
        _extFix = type;
    }
    if (!_extFix) {
        _extFix = 'js';
    }
    return _extFix.toLowerCase();
}
/**
 * 加载资源
 *
 * @export
 * @param {String} src 资源地址
 * @param {String} attr 添加的属性
 * @param {String} type 资源类型
 * @returns {Promise}
 */
export default function loadSource(src: string, attr?: 'js'|'css'|string|IKeyValue, type?: 'js'|'css'|string): Promise<Event> {
    /** 防止二次加载 */
    if (cacheMap[src]) {
        return cacheMap[src];
    }
    if (typeof attr === 'string') {
        type = attr;
        attr = {};
    }
    src = (src || '') + '';
    type = ((type || '') + '').toLowerCase();
    let _type = getType(src, type);

    /** try again */
    let count = 0;
    const load = (resolve: (evt: Event) => void, reject: (err: Error) => void) => {
        let scriptObj = typeMap[_type] || (type && typeMap[type]);
        if (!src || !scriptObj) {
            reject(new Error('不支持(' + src || '  ' + ')的文件类型'));
            return;
        }
        let el = document.createElement(scriptObj.tagName);
        let _attr = Object.assign({}, scriptObj.attr || {}, attr || {});
        if (_attr) {
            for (const key in _attr) {
                if (_attr.hasOwnProperty(key)) {
                    const _val = _attr[key];
                    el.setAttribute(key, _val || key);
                }
            }
        }
        el[scriptObj.src] = src;
        el.onload = (e: Event) => {
            el = null;
            count = 0;
            setTimeout(() => {
                resolve(e);
            }, 1);
        };
        el.onerror = (e: Error) => {
            el.parentNode.removeChild(el);
            if (count > 0) {
                delete cacheMap[src];
                return reject(e);
            }
            count++;
            el = null;
            load(resolve, reject);
        };
        let contType = scriptObj.position || 'head';
        document[contType].appendChild(el);
    };

    cacheMap[src] = new Promise((resolve, reject) => {
        load(resolve, reject);
    });

    return cacheMap[src];
}
