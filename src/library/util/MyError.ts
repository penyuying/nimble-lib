/**
 * 自定义错误
 */
export default class MyError extends Error {
    constructor(message: string, options?: object) {
        super(message);
        Object.assign(this, options);
        this.name = 'MyError';
    }
}
