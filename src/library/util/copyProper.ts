import { IKeyValue } from '../helpers/IKeyValue';

/**
 * 拷贝当前对象已有的属性
 *
 * @export
 * @param {Object} target 默认对象
 * @param {Object} rest 被拷贝的对象
 * @returns {Object} target
 */
function copyProper (isUndefined: boolean, ...args: IKeyValue[]): any;
function copyProper (...args: IKeyValue[]): any;
function copyProper (...args: any[]): any {
    let n = 0;
    let isUndefined = false; // 是否去除保留source中未定义的值
    let res: any;
    let target: any;
    if (args instanceof Array) {
        target =  args.splice(0, 1)[0];
    }
    if (typeof target === 'boolean') {
        isUndefined = target;
        target = args.splice(0, 1)[0];
    }
    if (args && args.length < 1) {
        return typeof target === 'boolean' ? args[1] : target;
    }
    if (isUndefined) {
        if (args[0] instanceof Array) {
            res = [];
        } else if (args[0] instanceof Object) {
            res = {};
        }
    } else {
        res = target;
    }

    for (let i = (0 + n); i < args.length; i++) {
        let source = args[i];
        if (source instanceof Object) {
            for (const key in target) {
                if (target.hasOwnProperty(key)) {
                    if (key in source) {
                        if (!isUndefined || isUndefined && source[key] !== undefined) {
                            res[key] = source[key];
                        }
                    }
                }
            }
        }
    }
    return res;
}

export default copyProper;
