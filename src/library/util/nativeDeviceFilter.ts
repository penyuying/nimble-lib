///<reference path="system.d.ts" />
import { IDevice } from './browser/interface/browser';
import { IKeyValue } from '../helpers/IKeyValue';


/**
 * 过滤原生设备调用
 *
 * @export
 * @param {*} options 选项
 * @param {*} device 设备信息
 * @return {Boolean}
 */
export default function nativeDeviceFilter (options: IKeyValue, device?: IDevice, type?: string) {
    const _options = options;
    const _device: any = device || {};
    let res = _options.isNative && (_device.isApp || _device.isLightApp) && _options.actionWithNative instanceof Function;
    if (res && _device.isLightApp) {
        if (res && (typeof system !== 'undefined') && system instanceof Object) {
            res = true;
        } else {
            res = false;
        }
    }
    return res;
}
