import initEvent from './initEvent';
import extend from './extend';

/**
 * 派自定义DOM事件
 *
 * @param {HTMLElement|Window|Document} target 事件元素
 * @param {String} eventType 事件类型
 * @param {Object} options 事件选项
 * @memberof EventModule
 * @export
 */
export default function dispatchEvent (target: any, eventType: string, options: any) {
    if (target && (target.tagName || target === window || target === document) && eventType && typeof eventType === 'string') {
        let ev = initEvent(eventType);
        ev._constructed = true;
        ev = extend(false, ev, options);
        target.dispatchEvent(ev);
    }
}
