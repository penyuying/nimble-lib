import callFn from './callFn';

/**
 * 动画
 * @param {Function} cb 回调
 * @param {Number} time 间隔时长
 * @param {Number} duration 总时长
 * @param {Number} end 结束位置
 */
export default function animation(cb: (time: Number) => void, time: number, duration: number, end: number) {
    let step = end / (duration / time); // 步长
    _next(duration, 0);
    /**
     * 下一次
     * @param {Number} _duration 总时长
     * @param {Number} start 开始位置
     */
    function _next(_duration: number, start: number) {
        setTimeout(() => {
            callFn(cb, [start]);
            if (((end > 0 && start < end) || (end < 0 && start > end)) && _duration > 0) {
                _next(_duration - time, start + step);
            } else {
                callFn(cb, [end]);
            }
        }, time);
    }
}
