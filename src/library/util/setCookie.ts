import encode from './encode';

/**
 * 设置Cookie
 *
 * @param {string} key 设置的Cookie名称
 * @param {string} value 设置的Cookie值
 * @param {number|{
 *   time?: Number,
 *   domain?: string
 * }} options 设置Cookie失效时间或domain
 * @memberof StorageClient
 */
export default function setCookie(key: string, value: any, options?: number|{
    time?: number,
    domain?: string
}): void {
    let res = [];
    options = options || {};
    if (!(options instanceof Object)) {
        options = {
            time: options
        };
    }
    if (!key) {
        return;
    }
    try {
        if (typeof value === 'undefined') {
            value = '';
        } else if (value instanceof Object) {
            value = JSON.stringify(value);
        } else {
            value = value + '';
        }
        res.push(key + '=' + encode(value));
        if (options.time) {
            let date = new Date();
            date.setTime(date.getTime() + options.time); // (days * 24 * 60 * 60 * 1000)
            res.push('expires=' + date.toUTCString());
        }
        options.domain && res.push('domain=' + options.domain);
        res.push('path=/');
        document.cookie = res.join(';');
    } catch (error) {
    }
}
