/**
 * 快应用的全局对象
 */
interface System {
    [key:string]:any;
    postMessage: any
}

declare var system: System
