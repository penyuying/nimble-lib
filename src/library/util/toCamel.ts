/**
 * 将用"-"分隔转成小驼峰
 *
 * @export
 * @param {string} text 需要转换的文本
 * @returns {string}
 */
export default function toCamel(text: string, splitStr?: string): string {
    let _splitStr  = '(' + (splitStr || '_|-') + ')';
    // 处理连续的大写字母
    text = (text + '').replace(new RegExp(_splitStr + '?(?:(?:[A-Z]{2,}[A-Z]' + _splitStr + '*$)|(?:[A-Z]{2,}(?=[A-Z])))', 'g'), ($1) => {
        let prefix = ((splitStr || '') + '').split('|')[0] || '-';
        $1 = $1.replace(new RegExp(_splitStr), '');
        return prefix + $1.toLowerCase();
    });
    // 去除开头的分隔符
    text = text.replace(new RegExp('(?:^' + _splitStr + '?([a-z]))|(?:' + _splitStr + '$)', 'gi'), ($1, $2, $3) => {
        return ($3 || '').toLowerCase();
    });
    // 去除中间分隔符
    text = text.replace(new RegExp(_splitStr + '([a-z])', 'gi'), ($1, $2, $3) => {
        return ($3 || '').toUpperCase();
    });
    return text;
}
