
///<reference path="./system.d.ts" />
import loadSource from './loadSource';
import callFn from './callFn';

let _opts = {
    partner: 'xxx', // 同盾的partnerCode
    appName: 'xxxx', // 同盾的app名称
    count: 50, // 读取次数
    interval: 50 // 间隔时间
};

let count = 0; // 读取次数
let interval = 0; // 间隔时间
let cacheData = {};

/**
 * 获取同盾BlackBox
 *
 * @export
 * @param {*} opts 选项
 * @returns {String}
 */
export default function getBlackBox(opts: {
    uuid: string; // uuid
    partner: string; // 同盾的partnerCode
    appName: string; // 同盾的app名称
}) {
    let _options = Object.assign({}, _opts, opts);
    let uuid = _options.uuid;
    cacheData[_options.appName + uuid] = cacheData[_options.appName + uuid] || new Promise((resolve, reject) => {
        count = _options.count;
        interval = _options.interval;
        let _fmOpt: any = window._fmOpt = {
            partner: _options.partner,
            appName: _options.appName,
            token: uuid,
            fmb: true
            // imgLoaded: false
        };
        if (Object.defineProperty) {
            watchReadBlackBox(_fmOpt, resolve, reject);
        }
        let cimg = new Image(1, 1);
        cimg.onload = function () {
            _fmOpt.imgLoaded = true;
        };
        cimg.src = 'https://fp.fraudmetrix.cn/fp/clear.png?partnerCode=' + _options.partner +
            '&appName=' + _options.appName + '&tokenId=' + _fmOpt.token;
        loadSource('//static.fraudmetrix.cn/v2/fm.js?ver=0.1&t=' + (new Date().getTime() / 3600000).toFixed(0), {
            type: 'text/javascript',
            async: true
        }, 'js').then(() => {
            if (!Object.defineProperty) {
                timeReadBlackBox(_fmOpt, resolve, reject);
            }
        });
        // let fm: any = document.createElement('script');
        // fm.type = 'text/javascript';
        // fm.async = true;

        // fm.src = (document.location.protocol === 'https:' ? 'https://' : 'http://') +
        // 'static.fraudmetrix.cn/v2/fm.js?ver=0.1&t=' + (new Date().getTime() / 3600000).toFixed(0);
        // fm.onload = function() {
        //     fm = null;
        //     readBlackBox(_fmOpt, resolve, reject);
        // };
        // fm.onerror = function() {
        //     fm.parentNode.removeChild(fm);
        //     fm = null;
        //     cacheData[_options.appName + uuid] = null;
        //     reject(new Error('js not loaded'));
        // };
        // let s: any = document.getElementsByTagName('script')[0];
        // s.parentNode.insertBefore(fm, s);
    });
    return cacheData[_options.appName + uuid];
}

// /**
//  * 获取blackBox
//  *
//  * @param {Object} fmOpt 选项
//  * @param {Function} cb 回调
//  */
// function readBlackBox(fmOpt: any, cb: Function, errCb?: Function) {
//     if (fmOpt && cb instanceof Function) {
//         if (Object.defineProperty) {
//             watchReadBlackBox(fmOpt, cb, errCb);
//         } else {
//             timeReadBlackBox(fmOpt, cb, errCb);
//         }
//     }
// }

let _currentCount = 0;
/**
 * 监听set读取blackBox
 *
 * @param {Object} fmOpt 选项
 * @param {Function} cb 回调
 * @param {Function} errCb 出错的回调
 */
function timeReadBlackBox(fmOpt: any, cb: Function, errCb?: Function) {
    let res = '';
    _currentCount++;
    // if (fmOpt && fmOpt.getinfo instanceof Function) {
    //     res = fmOpt.getinfo();
    // }
    res = callFn(fmOpt && fmOpt.getinfo, [], fmOpt);
    if (res) {
        callFn(cb, [res]);
        // if (cb instanceof Function) {
        //     cb(res);
        // }
    } else {
        if (_currentCount < count) {
            setTimeout(() => {
                timeReadBlackBox(fmOpt, cb, errCb);
            }, interval);
        } else {
            callFn(errCb, [new Error('getBlackBox is timeout')]);
            // if (cb instanceof Function) {
            //     callFn(errCb, [new Error('getBlackBox is timeout')]);
            // }
        }
    }
}
/**
 * 监听get读取blackBox
 *
 * @param {Object} fmOpt 选项
 * @param {Function} cb 回调
 */
function watchReadBlackBox(fmOpt: any, cb: Function, errCb?: Function) {
    if (fmOpt) {
        if (fmOpt.getinfo && fmOpt['_vmob_']) {
            if (fmOpt['_vmob_'] instanceof Function) {
                let _res = callFn(fmOpt['_vmob_'] || '');
                if (_res) {
                    callFn(cb, [_res]);
                    return;
                }
            }
        }
        let _timeout: any;

        if (fmOpt.getinfo) {
            fmOpt['_vmob_'] = fmOpt.getinfo;
            let res = callFn(fmOpt.getinfo, [], fmOpt);
            res && callFn(cb, [res || '']);
        }
        Object.defineProperty(fmOpt, 'getinfo', {
            set(val) {
                let res = '';
                fmOpt['_vmob_'] = val;
                if (typeof _timeout !== 'undefined') {
                    clearTimeout(_timeout);
                }
                _timeout = setTimeout(() => { // 不写定时的话取出的不准确
                    _timeout = undefined;
                    res = callFn(val, [], fmOpt);
                    if (res) {
                        callFn(cb, [res || '']);
                    } else {
                        timeReadBlackBox(fmOpt, cb, errCb);
                    }
                    // if (val instanceof Function) {
                    //     res = val();
                    // }
                    // if (cb instanceof Function) {
                    //     cb(res || '');
                    // }
                }, 20);
            },
            get() {
                return fmOpt['_vmob_'];
            }
        });
    }
}
