import { IBaseInterface } from './base.interface';
import extend from '../util/extend';
import { EventModule } from '../helpers/EventModule';
import mountService from '../helpers/mountService';

export class BaseAbstract<T extends (object|null)> extends EventModule implements IBaseInterface {
    /**
     * 单例实例化
     */
    static _singleton: any;

    /**
     * 名称
     */
    name: string = '';
    /**
     * 默认选项
     */
    protected defaultOption: T = <T>{};

    /**
     * 构造函数
     */
    constructor(options?: T) {
        super();
        this.setDefaultOptions(true, options);
    }
    /**
     * 单例实例化
     *
     * @static
     * @param {{[key: string]: any}} [options] 实例化参数
     * @returns
     * @memberof BaseAbstract
     */
    static singleton<U>(options?: any): U {
        let _that = this;
        let res: any = _that._singleton || _that.instance(options);
        _that._singleton = res;
        return res;
    }
    /**
     * 实例化
     *
     * @static
     * @param {{[key: string]: any}} [options] 实例化参数
     * @returns
     * @memberof BaseAbstract
     */
    static instance<U>(options?: any): U {
        let res: any = new this(options);
        return res;
    }

    /**
     * 安装
     *
     * @param {Vue} Vue Vue
     * @param {Object} options 选项
     * @memberof Service
     * @returns {Object}
     */
    static install<U>(Vue: any, options?: any): U {
        let res: any = this.instance<U>(options);
        if (Vue && Vue.use instanceof Function) {
            Vue.use(res, options);
        }
        return res;
    }
    /**
     * 安装
     *
     * @param {Vue} Vue Vue
     * @param {Object} options 选项
     * @memberof Service
     * @returns {Object}
     */
    public install(Vue: Function, options?: any) {
        let _that = this;
        let name = mountService(Vue, _that);
        _that.setDefaultOptions((options && name && options[name]) || options);
        return _that;
    }

    /**
     * 设置默认参数
     *
     * @protected
     * @param {Boolean|T} [target]
     * @param {T} [options]
     * @memberof BaseAbstract
     */
    protected setDefaultOptions<U extends T>(target: Boolean, options?: U): void;
    protected setDefaultOptions<U extends T>(target?: U): void;
    protected setDefaultOptions<U extends T>(target?: U): void {
        let _that = this;
        let _options: any;
        let _target: Boolean = true;
        if (typeof target === 'boolean') {
            _target = target;
            _options = arguments[1] || {};
        } else {
            _options = target;
        }
        if (_options) {
            _that.defaultOption = extend(_target, _that.defaultOption || {}, _options);
        }
    }

    /**
     * 获取参数
     *
     * @protected
     * @param {...T[]} args
     * @memberof BaseAbstract
     */
    protected getOptions(target: Boolean, ...args: T[]): T;
    protected getOptions(...args: T[]): T;
    protected getOptions(...args: any[]): T {
        let _that = this;
        let _options = [<T>{}, _that.defaultOption || <T>{}];
        let _target = true;
        if (typeof args[0] === 'boolean') {
            _target = args[0];
            args.splice(0, 1);
        }
        if (args && args.length > 0) {
            _options = _options.concat(args);
        }
        return extend(_target, ..._options);
    }
    /**
     * 异步加载的时候预加载
     *
     * @returns {Promise}
     * @memberof BaseAbstract
     */
    public preload() {
        let _that = this;
        return new Promise((resolve, reject) => {
            resolve(_that);
        });
    }
}
