import { IListenerQueueState } from './interface/listenerQueue';

/**
 * 图片加载状态
 */
export const LISTENER_STATE: IListenerQueueState = {
    /**
     * 等待
     */
    INIT: 'init',
    /**
     * 更新图片
     */
    UPDATE: 'update'
    // START = 'loadStart', // 开始
    // LOADED = 'loaded', // 加载中
    // RELOAD = 'reload', // 重新加载
    // LOAD_END = 'loadEnd', // 加载结束
    // LOAD_ERROR = 'loadError', // 加载出错
    // DESTROYED = 'destroyed' // 销毁
};

/**
 * 事件类型
 *
 * @export
 * @enum {number}
 */
export enum EVENT_TYPE {
    /**
     * Listener推入队列事件
     */
    PUSH = 'pushListener',
    /**
     * 更新Listener事件
     */
    UPDATE = 'updateListener',
    /**
     * 移除Listener事件
     */
    REMOVE = 'removeListener',
    /**
     * 清理队列
     */
    CLEAR = 'clearQueue'
}
