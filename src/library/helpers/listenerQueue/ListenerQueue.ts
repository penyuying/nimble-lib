// import extend from '../utils/extend';
import { LISTENER_STATE, EVENT_TYPE } from './constant';
import { BaseAbstract } from '../../basic/base.abstrat';
import callFn from '../../util/callFn';
import { IListener, IListenerQueueStateType, IListenerQueueState } from './interface/listenerQueue';

/**
 * 懒加载基础队列
 *
 * @export
 * @class ListenerQueue
 * @extends {DefaultOption}
 */
export class ListenerQueue<T extends (object|null), S, U = IListenerQueueStateType> extends BaseAbstract<T> {
    public listenerQueue: IListener<S, U>[] = []; // 监听队列
    /**
     * Creates an instance of ListenerQueue.
     * @param {Object} options 选项
     * @memberof ListenerQueue
     */
    constructor(options: T) {
        super(options);
    }

    /**
     * 添加加载项到队列
     * @param {Object|Function} listener listener或回调
     * @return {Object}
     */
    public addQueue(listener: IListener<S, U>|(() => IListener<S, U>)) {
        const _that = this;
        let _listener: IListener<S, U> = callFn(listener, [], listener);
        // if (listener instanceof Function) {
        //     _listener = listener(el, _that.getOptions(_urls));
        // }
        if (listener) {
            _that.listenerQueue.push(_listener);
            // _listener.$on('loading', evt => {
            //     _that.$emit('loading', evt);
            // });

            _that.$emit(EVENT_TYPE.PUSH, _listener);
        }

        // _listener.viewLoad(); // 更新一下数据
        return {
            listener: listener,
            isUpdata: false // 是否为更新数据项
        };
    }
    /**
     * 更新队列加载项
     * @param {HTMLElement} content 当前img元素
     * @param {Object} data 选项
     * @param {Object|Function} genListener 生成listener的回调
     * @return {Object}
     */
    public updateQueue(content: any, data: any, genListener: () => IListener<S, U>) {
        const _that = this;
        let _listenerQueue = _that.listenerQueue;
        let _listener: IListener<S, any>|undefined = _listenerQueue.find((item: IListener<S, U>) => {
            return item && item.content === content;
        });
        if (_listener) {
            let _oldState = _listener.state;
            callFn(_listener.update, [content, data], _listener);
            if (_oldState !== LISTENER_STATE.INIT && _oldState !== LISTENER_STATE.UPDATE) {
                _that.$emit(EVENT_TYPE.UPDATE, _listener);
            }
            return {
                listener: _listener,
                isUpdata: true // 是否为更新数据项
            };
        } else {
            return _that.addQueue(genListener);
        }
    }
    /**
     * 清理队列(已加载完成项)
     *
     * @memberof ImgLazy
     */
    public clearQueue() {
        const _that = this;
        let isClear = false;
        let list = _that.listenerQueue.filter((item: IListener<S, U>, index: number) => {
            if (item.isDel) {
                isClear = true;
                _that.removeItem(index, item);
                // _that.$emit(EVENT_TYPE.REMOVE, item);
                // callFn(item.destroyed, [], item);
            }
            return !item.isDel;
        });
        if (isClear) {
            _that.listenerQueue = list;
            _that.$emit(EVENT_TYPE.CLEAR, list);
        }
    }
    /**
     * 移除单项listener
     *
     * @memberof ImgLazy
     */
    public removeItem(index: number, item?: IListener<S, U>) {
        const _that = this;
        if (!item) {
            let items = _that.listenerQueue.splice(index, 1);
            if (items && items.length) {
                item = items[0];
            }
        }
        _that.$emit(EVENT_TYPE.REMOVE, item);
        callFn(item && item.destroyed, [], item);
    }
    /**
     * 获取listener
     *
     * @param {HTMLElement} el 当前元素
     * @returns {Object}
     * @memberof ListenerQueue
     */
    public getListener(el: HTMLElement): {listener: IListener<S, U>, index: number}|undefined {
        let _that = this;
        let list = _that.listenerQueue;
        if (el && list && list.length) {
            for (let i = 0; i < list.length; i++) {
                const item = list[i];
                if (item && item.content === el) {
                    return {
                        listener: item,
                        index: i
                    };
                }
            }
        }
    }
}

export {
    IListener,
    LISTENER_STATE,
    EVENT_TYPE,
    IListenerQueueStateType,
    IListenerQueueState
};
