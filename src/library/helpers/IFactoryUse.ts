

export interface IFactoryUse<T, R, P> {
    // tslint:disable-next-line:callable-types
    (options: T): R;
    use?: (preset: P) => void;
    /**
     * use的参数列表
     */
    _useArgs?: Array<Array<P>>;
}
