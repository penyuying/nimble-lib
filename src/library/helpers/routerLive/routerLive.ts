import { BaseAbstract } from '../../basic/base.abstrat';
import { IRouterLiveCofig } from './interface/routerLiveConfig';
import getParams from '../../util/getParams';
import getBackData from '../../util/getBackData';

export class RouterLive extends BaseAbstract<IRouterLiveCofig> {
    defaultOption: IRouterLiveCofig = {
        pageName: ''
    };
    constructor(options: IRouterLiveCofig) {
        super(options);
        this.setDefaultOptions(options);
    }

    /**
     * 跳转页面
     * @param {Object} params url参数
     * @param {String} url url参数
     *
     * @return {String}
     */
    toPage(params: IRouterLiveCofig, url?: string) {
        params = this.getOptions(params) || {};
        let queryKey = params.queryKey || '';
        if (params.notRouter) {
            return (params && params.pageName) || null;
        }
        if (queryKey && params.pageName) {
            let reg = new RegExp('_' + queryKey + '=[a-z]+', 'ig');
            let reg1 = new RegExp('\\?_' + queryKey + '=[a-z]+(&)?|&_' + queryKey + '=[a-z]+', 'ig');
            let i = 0;
            url = url || location.href;

            let _dUrl = url; // decodeURIComponent(url);
            let isReplace = reg.test(_dUrl);
            if (isReplace) {
                reg.lastIndex = 0;
                _dUrl = _dUrl.replace(reg, '_' + queryKey + '=' + params.pageName);
            } else if (/[?&]/.test(_dUrl)) {
                _dUrl = _dUrl + '&_' + queryKey + '=' + params.pageName;
            } else {
                _dUrl = _dUrl + '?_' + queryKey + '=' + params.pageName;
            }
            let _dUrlArr = _dUrl.split('?');
            if (i > 0 && _dUrlArr && _dUrlArr.length > 0) {
                _dUrlArr[1] = encodeURIComponent(_dUrlArr[1]);
            }
            let _oldState = window.history.state;
            let _state = Object.assign({}, params.state, { _type: queryKey });
            if (isReplace && _oldState && _oldState._type === queryKey) {
                window.history.replaceState(_state || null, params.title || '', _dUrlArr.join('?'));
            } else {
                if (isReplace && (!_oldState || _oldState._type !== queryKey)) { // 处理原来url参数带有_carPage的
                    window.history.replaceState(_oldState || null, '', _dUrl.replace(reg1, function ($1: string, $2: string) {
                        let res = '';
                        if ($2 === '&') { res = '?'; }
                        return res;
                    }));
                }
                window.history.pushState(_state || null, params.title || '', _dUrlArr.join('?'));
            }
            return params.pageName;
        }
    }
    /**
     * 回退页面
     *
     * @param {Object} params url参数
     * @return {Promise}
     * @memberof CarBase
     */
    goBackPage(params: IRouterLiveCofig): Promise<string|null> {
        params = this.getOptions(params) || {};
        return new Promise((resolve, reject) => {
            if (params.notRouter) {
                resolve((params && params.pageName) || null);
                return;
            }
            let queryKey = params.queryKey || '';
            if (!params[queryKey]) {
                let reg = new RegExp('_' + queryKey + '=[a-z]+', 'i');
                let _dUrl = decodeURIComponent(location.href);
                let _dUrlArr = _dUrl.match(reg);
                if (_dUrlArr && _dUrlArr[0]) {
                    let valArr = ((_dUrlArr[0] || '') + '').split('=')[1];
                    let val = valArr && valArr[1];
                    if (val) {
                        params[queryKey] = val;
                    }
                }
            }
            getBackData(params.getUrlParams || params, params)
                .then((_params: any) => {
                    _params = _params || {};
                    if (_params[queryKey]) {
                        history.go(_params.goBack || -1);
                        resolve(_params[queryKey]);
                    } else if (_params.goBack) {
                        history.go(_params.goBack);
                        resolve(_params[queryKey]);
                    }
                }, reject);
        });
    }
}

export {
    IRouterLiveCofig
};
