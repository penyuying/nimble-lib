
import { IItersectionConfig, IItersectionDefaultConfig, IScrollElementItem } from './interface/intersectionConfig';
import scrollParent from '../../util/scrollParent';
import throttle from '../../util/throttle';
import initEventListener from '../../util/initEventListener';
import { INTERSECTION_DEFAULT_CONFIG } from './constant';
import { ListenerQueue, IListener, EVENT_TYPE } from '../listenerQueue/ListenerQueue';
import callFn from '../../util/callFn';
/**
 * Scroll模式加载
 *
 * @export
 * @class ScrollIntersection
 * @extends {LazyBase}
 */
export class ScrollIntersection extends ListenerQueue<IItersectionConfig, IScrollElementItem> {
    /**
     * 默认数据
     */
    protected defaultOption = INTERSECTION_DEFAULT_CONFIG;
    /**
     * 滚动条元素列表
     */
    protected scrollList: IScrollElementItem[] = [];

    /**
     * Creates an instance of ScrollIntersection.
     * @param {Object} options 选项
     * @param {Boolean} [options.intersectionPattern=hasIntersectionObserver] 是否为IntersectionObserver模式
     * @param {String|Array<String>} [options.listenEvents='scroll'] 是否为IntersectionObserver模式
     * @memberof ScrollIntersection
     */
    constructor(options: IItersectionConfig) {
        super(options);
        let _that = this;
        _that.setDefaultOptions(options);
        _that._setViewSize();
        _that._init();
    }
    /**
     * 初始化
     *
     * @private
     * @memberof ScrollIntersection
     */
    private _init() {
        let _that = this;
        let _options: IItersectionDefaultConfig = _that.defaultOption;
        /**
         * 执行节流加载图片
         */
        _that._listenerHandler = _options.throttleWait && throttle(_that._loadHandler, _options.throttleWait, _that) || _that._loadHandler;
        _that._addScrollList(document.body, null);
        // if (!_options.viewHeight || !_options.viewWidth) {
        // }
        window.addEventListener('resize', () => {
            let opts = _that._setViewSize();
            opts && _that._listenerHandler(opts);
        });
        window.addEventListener('orientationchange', () => {
            throttle(() => {
                let _orientation = window.orientation;
                if ((_orientation > 85 && _orientation < 95) || (_orientation > 175 && _orientation < 185)) {
                    let opts = _that._setViewSize();
                    opts && _that._listenerHandler(opts);
                }
            }, 200, window);
        });
        // 推入项到队列时候的事件
        _that.$on(EVENT_TYPE.PUSH, (item: IListener<IScrollElementItem>) => {
            if (item && item.content) {
                _that._addScrollList(item.content, item);
                _that._listenerHandler();
            }
        });

        // 更新队列时候的事件
        _that.$on(EVENT_TYPE.UPDATE, (item: IListener<IScrollElementItem>) => {
            _that._listenerHandler();
        });

        // 清理队列时候的事件
        _that.$on(EVENT_TYPE.REMOVE, (item: IListener<IScrollElementItem>) => {
            _that._removeScrollList(item);
        });
    }
    /**
     * 设置视图区域大小
     * @return {Object}
     */
    private _setViewSize(): {viewWidth?: number, viewHeight?: number}|undefined {
        let _that = this;
        let _options: IItersectionDefaultConfig = _that.defaultOption;
        // if (!_options.viewHeight || !_options.viewWidth) {
        let _width = window.innerWidth;
        let _height = window.innerHeight;
        let opts: any;
        if (_options.viewHeight !== _height && _options.viewHeight < _height && _height > 10) {
            opts = opts || {};
            opts.viewHeight = _height;
        }
        if (_options.viewWidth !== _width && _options.viewWidth < _width && _width > 10) {
            opts = opts || {};
            opts.viewWidth = _width;
        }
        _that.setDefaultOptions(false, opts);
        return opts;
        // }
    }
    /**
     * 执行队列
     */
    private _listenerHandler(...args: any[]) {}
    /**
     * 事件执行队列
     */
    private _eventListenerHandler() {}
    /**
     * 加载图片
     * @param {Object} opts 选项
     */
    private _loadHandler (opts: any) {
        const _that = this;
        let _refresh: Boolean = true;
        let _options = _that.defaultOption;
        _that.listenerQueue.forEach(listener => {
            let isShow = callFn(listener.viewDisplay, [opts, _refresh], listener);
            let _item: any = listener;
            if (_item && typeof isShow === 'boolean' && _options && _options.isShiftOut) {
                if (!isShow && _item._isShowFlag_) {
                    callFn(listener.shiftOut, [null], listener);
                }
                _item._isShowFlag_ = isShow;
            }
            _refresh = false;
        });

        // 清理已加载完成项
        _that.clearQueue();
    }
    /**
     * 注册或解除dom元素的事件监听器
     * @param {HtmlElement} el 注册/解除事件的监听器的元素
     * @param {Boolean} isAdd 解除/注册元素的事件监听器标志
     */
    private _initListener (el: HTMLElement, isAdd: boolean) {
        const _that = this;
        let _options = _that.defaultOption;
        _that._eventListenerHandler = function() {
            _that._listenerHandler();
        };
        initEventListener(el, _options.listenEvents, _that._eventListenerHandler, isAdd ? 'add' : 'remove');
    }
    /**
     * 添加滚动条节点列表
     *
     * @param {HTMLElement} el 当前img元素
     * @param {IListener} listener 当前项
     * @memberof ImgLazy
     */
    private _addScrollList(el: HTMLElement, listener: IListener<IScrollElementItem>|null) {
        let _that = this;
        let $parent = scrollParent(el);
        if ($parent) {
            let item = _that.scrollList.find(item => item.el === $parent);
            if (!item) {
                item = {
                    el: $parent,
                    childrenCount: 1
                };
                _that.scrollList.push(item);
                _that._initListener($parent, true);
            } else {
                item.childrenCount += 1;
            }
            if (listener) {
                listener._$scroll = item;
            }
        }
    }

    /**
     * 移除滚动节点列表
     * @param {IListener} listener 当前的img元素的监听器
     * @memberof ImgLazy
     */
    private _removeScrollList(listener: IListener<IScrollElementItem>) {
        const _that = this;
        let target = listener._$scroll;
        if (target) {
            target.childrenCount -= 1;
            if (target.childrenCount <= 0) {
                let _el = target.el;
                _that._initListener(_el, false);
                _that.scrollList = _that.scrollList.filter(item => item.el !== _el);
            }
        }
    }
}
