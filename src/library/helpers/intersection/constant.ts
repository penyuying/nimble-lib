import { IItersectionDefaultConfig } from './interface/intersectionConfig';
import { VIEW_REGION_DEFAULT_CONFIG } from '../viewRegion/constant';
import prefixEvent from '../../util/prefixEvent';

/**
 * 默认配置参数
 */
export let INTERSECTION_DEFAULT_CONFIG: IItersectionDefaultConfig = Object.assign<any, any>({
    isShiftOut: false,
    /**
     * 默认监听的事件（['scroll', 'wheel', 'mousewheel', 'resize', 'animationend', 'transitionend', 'touchmove']）
     */
    listenEvents: ['scroll', 'wheel', 'mousewheel', 'resize', prefixEvent('animationend', true),
        prefixEvent('transitionend', true), 'touchmove'],
    /**
     * 执行队列的等待时间(防抖),
     */
    throttleWait: 100,

    /**
     * observer选项
     */
    observerOptions: {
        threshold: 0
    }
}, VIEW_REGION_DEFAULT_CONFIG);

/**
 * Intersection事件类型
 */
export enum INTERSECTION_EVENT_TYPE {
    UNOBSERVE = 'unobserve'
}
