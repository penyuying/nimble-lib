import { IKeyValue } from './IKeyValue';
import toPascal from '../util/toPascal';

/**
 * 挂载服务
 *
 * @export
 * @param {any} Vue vue
 * @param {any} source 对象源
 * @returns {strig} 返回对象的name名
 */
export default function mountService (Vue: Function, source: IKeyValue) {
    let key = '';
    if (Vue && source) {
        if (source.name) {
            // key = (source.name).replace(/^[a-z]/, (text: string) => {
            //     return text.toUpperCase();
            // });
            key = toPascal(source.name);
            let keyName = '$s' + key;
            if (Vue.prototype && !Vue.prototype[keyName]) {
                mapMethod(Vue.prototype, source, keyName);
            }
        } else {
            console.error('Mount is not defined for the image name');
        }
    }
    return key;
}
/**
 * 映射成员
 *
 * @param {any} VuePrototype vue的原型对象
 * @param {any} source 源对象
 * @param {any} keyName 映射到当前对象的名称
 * @param {any} SourceName 对应在源对象的名称
 */
function mapMethod (VuePrototype: IKeyValue, source: IKeyValue, keyName: string) {
    if (VuePrototype && source && keyName) {
        VuePrototype[keyName] = source;
        // let _this = this
        // source._getParent = () => {
        //     return _this
        //   }
        try {
            Object.defineProperty(VuePrototype, keyName, {
                get () {
                    let _that = this;
                    source._getParent = () => {
                        return _that;
                    };
                    return source;
                },
                // writable: false,
                enumerable: false,
                configurable: false
            });
        } catch (error) {

        }
    }
}
