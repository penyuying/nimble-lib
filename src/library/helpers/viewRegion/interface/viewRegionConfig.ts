import { IBaseInterface } from '../../../../library/basic/base.interface';

/**
 * 计算当前视图区域参数
 *
 * @export
 * @interface ICountDown
 */
export interface IViewRegionConfig extends IBaseInterface {
    /**
     * 加载屏视口宽度
     */
    viewWidth?: number;
    /**
     * 加载屏视口高度
     */
    viewHeight?: number;
    /**
     * 预加载(屏)
     */
    preLoad?: number;
    /**
     * 底部距离顶部的最小距离
     */
    preLoadTop?: number;
}

