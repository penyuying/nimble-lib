import { IValidOptions, IValidetorArrayParams, IValidFunction } from '../validate';
import { IBaseInterface } from '../../../library/basic/base.interface';

/**
 * 表单的数据
 *
 * @export
 * @interface IFormData
 */
export interface IFormData {
    [key: string]: any|IFormData[];
}

// /**
//  * 表单需要验证的字段及默认数据
//  *
//  * @export
//  * @interface IInitControlData
//  */
// export interface IInitControlData {
//     [key: string]: any; // 值
// }


/**
 * 表单项数据的map(包含controlName)
 *
 * @export
 * @interface IFormControlDataMap
 */
export interface IFormControlDataMap {
    [controlName: string]: IFormControlOptions|IControlDataItem; // 表单项数据
}

/**
 * 数据过滤器(返回当前项的默认值)
 *
 * @export
 * @interface IFormFilter
 * @template T
 */
export interface IFormFilter {
    // tslint:disable-next-line:callable-types
    (params: IFormControlOptions, val: any): any;
}


/**
 * 验证错误的回调
 *
 * @export
 * @interface IFormValidErrorBack
 */
export interface IFormValidErrorBack {
    // tslint:disable-next-line:callable-types
    (params: string): void;
}

/**
 * 输入项参数
 *
 * @export
 * @interface IControlOptions
 */
export interface IFormControlOptions {
    /**
     * 名称
     */
    nameText?: string;
    /**
     * 是否只读
     */
    readonly?: true;
    /**
     * 表单项类型
     */
    dataType?: 'city'|'date';
    /**
     * 输入框的底纹
     */
    placeholder?: string;
    /**
     * 校验节流时间
     */
    validFlowTime?: number;
    /**
     * 校验数据参数
     */
    valid?: false|IValidOptions|IValidFunction|Array<IValidOptions|IValidFunction>;
    /**
     * 校验数据参数
     */
    groupValid?: false|IValidetorArrayParams;
    /**
     * 是否为选择项
     */
    isChecked?: true;
    /**
     * 输入框类型
     */
    type?: string;
    /**
     * 输入项组的列表
     */
    list?: IControlGroupItem[];
    /**
     * 当前城市名称
     */
    city?: string[];
    /**
     * 行显示的列数
     */
    col?: number;
    /**
     * 默认数据
     */
    defaultValue?: any;
}

export type IValidOptions = { controlName?: string } & IValidOptions;

/**
 * 输入项组项
 *
 * @export
 * @interface IControlGroupItem
 */
export interface IControlGroupItem {
    /**
     * 值
     */
    vaule?: string|number;
    /**
     * 项名
     */
    type?: string;
    /**
     * 项名
     */
    nameText: string;
}

/**
 * 表单项数据（表单需要验证的字段及默认数据）
 *
 * @export
 * @interface IInitControlData
 * @extends {IFormControlOptions}
 */
export type IInitControlData = IControlDataItem|IControlDataList;
/**
 * 表单项数据（表单需要验证的字段及默认数据）
 *
 * @export
 * @interface IInitControlData
 * @extends {IFormControlOptions}
 */
export interface IControlDataItem extends IFormControlOptions, IBaseInterface {
    /**
     * 输入框名称
     */
    controlName: string;
}
/**
 * 表单项数据（表单需要验证的字段及默认数据）
 *
 * @export
 * @interface IInitControlData
 * @extends {IFormControlOptions}
 */
export interface IControlDataList extends IBaseInterface {
    /**
     * 项名
     */
    nameText: string;
    /**
     * 控件列表
     */
    formControl: IInitControlData[];
}
