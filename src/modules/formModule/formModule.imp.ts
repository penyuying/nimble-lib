import { IKeyValue } from '../../library/helpers/IKeyValue';
import { Validate, IValidFunction, IValidetorParams, IValidetorArrayParams, IValidOptions } from './validate';
import { FormGroup, IGroupOptions } from './formGroup';
import { FormControl, IControlsOptions} from './formControl';
import { IFormControlDataMap, IFormFilter, IFormValidErrorBack, IInitControlData, IFormControlOptions } from './interface/formConfig';
import { BaseAbstract } from '../../library/basic/base.abstrat';
import { FormArray } from './formArray';
import { IFormInterface, IFormModuleInterface } from './interface/form';
import { IGroupValidFunction } from './interface/validateConfig';
import { FormDataCache, IFormCacheKey, IFormCacheItem, ICacheKeyConfigKey, ICacheKeyConfig } from './FormDataCache';
import callFn from '../utils/callFn';

/**
 * 表单处理
 *
 * @export
 * @class FormModule
 * @extends {SValidService}
 */
export class FormModule extends BaseAbstract<null> implements IFormModuleInterface {

    public name = 'Form';

    constructor (
        public validate: Validate
    ) {
        super(null);
        this.setDefaultOptions(null);
    }

    /**
     * 初始化表单列表
     *
     * @param {(IInitControlData[][]|IFormControlDataMap[])} formList 表单基础数据列表
     * @param {IKeyValue[]} [defaultData] 表单默认数据
     * @param {IFormFilter} [callback] 设置每项值前的回调
     * @returns {FormArray}
     * @memberof FormModule
     */
    initFormArray(formList: IInitControlData[][]|IFormControlDataMap[],
        defaultData?: IKeyValue[],
        callback?: IFormFilter): FormArray {
        let _that = this;
        let _groupList: FormGroup[] = [];
        if (formList instanceof Array && formList.length > 0) {
            for (let index = 0; index < formList.length; index++) {
                const item = formList[index];
                if (item instanceof Array && item.length) {
                    _groupList.push(_that.initFormData(item));
                } else if (item instanceof Object) {
                    _groupList.push(_that.initFormGroup(item));
                }
            }
        }
        let res = new FormArray(_groupList);
        res.setValue(defaultData || [], callback);
        return res;
    }
    /**
     * 列表数据生成对象表单
     *
     * @param {IInitControlData[]} formList 列表
     * @param {IFormFilter} callback 过滤回调函数
     * @memberof FormModule
     */
    public initFormData(
        formList: IInitControlData[],
        defaultData?: IKeyValue,
        callback?: IFormFilter
    ): FormGroup {
        let _that = this;
        let _conMap: IFormControlDataMap = {};
        let _formList: IInitControlData[] = formList || [];
        flattenedArray(_conMap, _formList);
        if (defaultData) { // 设置默认数据
            for (const key in defaultData) {
                if (defaultData.hasOwnProperty(key)) {
                    if (_conMap[key]) {
                        _conMap[key].defaultValue = defaultData[key];
                    } else {
                        _conMap[key] = {
                            controlName: key,
                            defaultValue: defaultData[key]
                        };
                    }
                }
            }
        }
        return _that.initFormGroup(_conMap, defaultData, callback);
        /**
         * nage
         *
         * @param {IFormControlDataMap} _conMap
         * @param {any[]} list
         */
        function flattenedArray(conMap: IFormControlDataMap, list: any[]) {
            list.forEach(item => {
                if (item && item.formControl && item.formControl.length > 0) {
                    flattenedArray(conMap, item.formControl);
                } else if (item && item.controlName) {
                    conMap[item.controlName] = item;
                }
            });
        }
    }

    /**
     * 初始化表单数据
     *
     * @param {IQueryMemberInfoData} data 控件map生成表单
     * @memberof PersonalInfoComponent
     */
    public initFormGroup(
        formData: IFormControlDataMap|IFormControlOptions[],
        defaultData?: IKeyValue,
        callback?: IFormFilter
    ): FormGroup {
        let _that = this;
        let _conMap: any = formData;
        let _data: IGroupOptions<FormControl>|IGroupOptions<FormControl>[] = {};
        if (formData instanceof Array) {
            _data = [];
        }
        for (let key in _conMap) {
            if (_conMap.hasOwnProperty(key)) {
                let _conItem: IFormControlOptions = _conMap[key];
                let _groupValid: IGroupValidFunction[] = _that.setGroupValid(_conItem);
                let _val = _conItem.defaultValue;
                if (callback instanceof Function) {
                    _val = callback(_conItem, _val);
                }
                if (_conItem instanceof FormControl || _conItem instanceof FormGroup) {
                    if (_conItem instanceof FormGroup && _groupValid && _groupValid.length) {
                        _conItem._setValidOption(_groupValid);
                    }
                    _data[key] = _conItem;
                } else if (_val instanceof FormControl || _val instanceof FormGroup) {// 表单对象
                    if (_val instanceof FormGroup && _groupValid && _groupValid.length) {
                        _val._setValidOption(_groupValid);
                    }
                    _data[key] = _val;
                } else {
                    _data[key] = _that.initFormControl(_conItem);
                }
            }
        }
        let res = new FormGroup(_data);
        defaultData && res.setValue(defaultData, callback);
        return res;
    }
    /**
     * 初始化表单控件
     *
     * @param {IFormControlOptions} controlsData 控件选项数据
     * @returns {FormControl}
     * @memberof FormModule
     */
    public initFormControl(controlsData: IFormControlOptions) {
        let _contOptions: IControlsOptions = {};
        let _valid: IValidFunction[] = this.setValid(controlsData);
        if (typeof controlsData.defaultValue !== 'undefined') {
            _contOptions.defaultValue = controlsData.defaultValue;
        }
        if (_valid && _valid.length) {
            _contOptions.valid = _valid;
        }
        if (typeof controlsData.validFlowTime === 'number') {
            _contOptions.validFlowTime = controlsData.validFlowTime;
        }
        return new FormControl(_contOptions);
    }

    /**
     * 验证列表
     *
     * @param {FormGroup|FormArray} formMd 表单模型
     * @param {IFormValidErrorBack} errorBack 取得第一项错误数据的回调
     * @returns {boolean} 验证通过返回false
     * @memberof FormModule
     */
    public validForm(formMd: FormGroup, errorBack: IFormValidErrorBack): boolean {
        let _that = this;
        let controls: any = formMd.getValid();
        return _that.validControls(controls, errorBack);
    }
    /**
     * 初始化数据缓存
     *
     * @returns
     * @memberof FormModule
     */
    public initFormCache(): FormDataCache {
        return new FormDataCache();
    }

    /**
     * 设置校验方法
     *
     * @private
     * @param {IFormControlOptions} _conItem 表单项
     * @returns {IValidFunction[]}
     * @memberof PersonalInfoComponent
     */
    private setGroupValid(
        _conItem: IFormControlOptions
    ): IGroupValidFunction[] {
        let _that = this;
        let _valid: IGroupValidFunction[] = [];

        eachValid(_conItem && _conItem.groupValid, (validItem: any) => { // 把验证参数生成验证方法
            if (validItem instanceof Function) {
                _valid.push(validItem);
            } else {
                let _validParams: IValidetorParams = Object.assign({
                    name: _conItem.nameText || ''
                }, _conItem.valid || {});
                _valid.push(_that.validate.generateArrayValidetor(_validParams));
            }
        });

        return _valid;
    }

    /**
     * 设置校验方法
     *
     * @private
     * @param {IFormControlOptions} _conItem 表单项
     * @returns {IValidFunction[]}
     * @memberof PersonalInfoComponent
     */
    private setValid(
        _conItem: IFormControlOptions
    ): IValidFunction[] {
        let _that = this;
        let _valid: IValidFunction[] = [];

        eachValid(_conItem && _conItem.valid, (validItem: any) => { // 把验证参数生成验证方法
            if (validItem instanceof Function) {
                _valid.push(validItem);
            } else {
                let _validParams: IValidetorParams = Object.assign({
                    name: _conItem.nameText || ''
                }, validItem || {});
                _valid.push(_that.validate.generateValidetor(_validParams));
            }
        });

        return _valid;
    }

    /**
     * 验证表单项
     *
     * @private
     * @param {Array<any>|Object}} controls 表单控件错误对象
     * @param {IFormValidErrorBack} errorBack 取得第一项错误数据的回调
     * @returns {boolean}  取得第一项错误数据的回调
     * @memberof FormModule
     */
    private validControls(controls: any, errorBack: IFormValidErrorBack): boolean {
        let _that = this;
        let res = false;
        if (controls) {
            res = true;
            for (let key in controls) {
                if (controls.hasOwnProperty(key)) {
                    let controlItem = controls[key];
                    let errInfo = controlItem && controlItem.errInfo;
                    if (errInfo) {
                        if (errInfo && errInfo.desc) {
                            if (errorBack) {
                                errorBack(errInfo.desc);
                            }
                        }
                        return true;
                    }
                    if (controlItem instanceof Array) {
                        if (_that.validControls(controlItem, errorBack)) {
                            return true;
                        }
                    }
                    if (controlItem && controlItem.controls) {
                        if (_that.validControls(controlItem.controls, errorBack)) {
                            return true;
                        }
                    }
                    if (controlItem instanceof Object) {
                        if (_that.validControls(controlItem, errorBack)) {
                            return true;
                        }
                    }
                }
            }
        }
        if (res) {
            console.error('未定义错误！');
        }
        return res;
    }
}

/**
 * 展开验证选项列表
 *
 * @param {(false|IValidOptions|IValidFunction|Array<IValidOptions|IValidFunction>)} valid 验选项
 * @param {((p: IValidOptions|IValidFunction) => void)} cb 回调
 */
function eachValid(valid?: any,
    cb?: (p: IValidOptions|IValidFunction|IValidetorArrayParams|IGroupValidFunction) => void): void {
    if (valid) {
        if (valid instanceof Array) {
            valid.forEach(validItem => {
                callFn(cb, [validItem]);
            });
        } else {
            callFn(cb, [valid]);
        }
    }
    // if (valid && cb instanceof Function) {
    //     if (valid instanceof Array) {
    //         valid.forEach(validItem => {
    //             cb(validItem);
    //         });
    //     } else {
    //         cb(valid);
    //     }
    // }
}

/**
 * factory方法
 *
 * @export
 * @param {Object} env 全局参数
 * @param {Http} http Http服务
 * @returns {SHttpService} 返回服务的实例
 */
export default function formFactory(validate: Validate): FormModule {
    return new FormModule(validate);
}

export {
    FormGroup,
    IGroupOptions,
    FormControl,
    IControlsOptions,
    IFormControlDataMap,
    IFormFilter,
    IFormValidErrorBack,
    IInitControlData,
    IFormControlOptions,
    FormArray,
    IFormInterface,
    IFormModuleInterface,
    FormDataCache,
    IFormCacheKey,
    IFormCacheItem,
    ICacheKeyConfigKey,
    ICacheKeyConfig
    // tslint:disable-next-line:max-file-line-count
};
