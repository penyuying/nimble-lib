import VmBind from '../../library/util/vmBind';
import { IValidetorData, IValidFunction, Validate } from './validate';
import { IControlsOptions} from './interface/formControlConfig';
import { FormGroup } from './formGroup';
import { BaseAbstract } from '../../library/basic/base.abstrat';
import { IFormInterface } from './interface/form';
import callFn from '../../library/util/callFn';


/**
 * 表单项
 *
 * @export
 * @class FormControl
 */
export class FormControl extends BaseAbstract<IControlsOptions> implements IFormInterface<any, IValidetorData|boolean> {
    parent: FormControl | FormGroup<any> | null = null;
    formGroup: FormGroup<any> | null = null;
    validate: Validate|undefined;

    protected defaultOption: IControlsOptions = {
        /**
         * 导步验证等待中的信息
         */
        asyncWaitInfo: {
            errInfo: {
                desc: '验证中，请验证无误后再次保存~',
                type: 'asyncWaitErr'
            }
        },
        validFlowTime: 20,
        /**
         * 异步出错信息
         */
        asyncErrInfo: null
    };
    /**
     * 是否更新数据（0否1是）
     *
     * @private
     * @type {(0|1)}
     * @memberof FormControl
     */
    private isValidStatus: 0|1 = 1;

    /**
     * 验证是否是否等待中（0否1是）
     *
     * @private
     * @type {(0|1)}
     * @memberof FormControl
     */
    private isWaitStatus: 0|1 = 0;

    /**
     * 设置数据时验证节流的setTimeout
     */
    private _timeout: NodeJS.Timer|undefined;

    /**
     * 是否失去过焦点
     *
     * @type {boolean}
     * @memberof FormControl
     */
    public isBlur: boolean = false;

    /**
     * 是否获取过焦点
     *
     * @type {boolean}
     * @memberof FormControl
     */
    public isFocus: boolean = false;

    /**
     * 值是否发生过改变
     *
     * @type {boolean}
     * @memberof FormControl
     */
    public isChange: boolean = false;

    /**
     * 表单项的值
     *
     * @type {*}
     * @memberof FormControl
     */
    @VmBind({
        set(val: any) {
            let _that: any = this;
            let options: IControlsOptions = _that.getOptions();
            _that.isValidStatus = 1;
            _that.isChange = true;
            _that._stopTimeout();
            _that._timeout = setTimeout(() => {
                _that.getValid();
            }, options.validFlowTime);
        }
    })
    value: any = null;

    /**
     * 表单项的验证结果
     *
     * @type {(IValidetorData|null|boolean)}
     * @memberof FormControl
     */
    @VmBind({
        set(val: any) {
            let _that: any = this;
            _that.isValid = !!val;
        },
        get(val: any) {
            let _that: any = this;
            let _res: any;
            if (_that.isValidStatus !== 0 && _that.isWaitStatus !== 1) {
                _res = _that.getValid();
            }
            return _res;
        }
    })
    valid: IValidetorData|null|boolean = null;
    /**
     * 最后的验证结果是否有错误
     */
    isValid: boolean = false;

    constructor(private options: IControlsOptions) {
        super(options);
        this.setDefaultOptions(options);
        let _that = this;
        if (this.defaultOption && typeof this.defaultOption.defaultValue !== 'undefined') {
            _that.setValue(this.defaultOption.defaultValue);
        } else {
            _that.getValid();
        }
    }
    /**
     * 停止设置数据时验证节流的setTimeout
     *
     * @memberof FormControl
     */
    _stopTimeout() {
        let _that = this;
        if (typeof _that._timeout !== 'undefined') {
            clearTimeout(_that._timeout);
            _that._timeout = undefined;
        }
    }
    /**
     * 设置值
     *
     * @param {*} val 值
     * @memberof Controls
     */
    setValue(val: any): void {
        let _that = this;
        if (_that.value !== val) {
            _that.value = val;
        }
    }

    /**
     * 获取验证结果
     *
     * @returns {(IValidetorData|null|boolean)}
     * @memberof FormControl
     */
    getValid(): IValidetorData|null|boolean|undefined {
        let _that = this;
        let options = _that.getOptions();
        if (_that.isValidStatus === 0 || _that.isWaitStatus === 1) {
            return _that.valid;
        }
        _that._stopTimeout();
        _that.isValidStatus = 0;
        return eachValid(options && options.valid, 0) || null;


        function eachValid(valids?: IValidFunction|IValidFunction[], index: number = 0, len: number = 0) {
            let res: IValidetorData|null = null;
            let _valid: IValidetorData|Promise<IValidetorData>|null = null;
            if (valids) {
                if (valids instanceof Array) {
                    len = valids.length;

                    for (let i = index; i < len; i++) {
                        const fn = valids[i];
                        _valid = _callFn(fn);
                        index = index + 1;
                        if (_valid) {
                            break;
                        }
                    }
                } else {
                    _valid = _callFn(valids);
                }
            }
            if (_valid instanceof Promise) { // 处理异步
                _that.isWaitStatus = 1;
                res = options.asyncWaitInfo || null;
                _valid.then(function(info) {
                    if (info || index >= len) { // 结束验证
                        _that.valid = info || null;
                        _that.isWaitStatus = 0;
                        _that.getValid();
                    } else {
                        res = eachValid(valids, index, len);
                    }
                }, () => {
                    _that.valid = options.asyncErrInfo || null;
                    _that.isWaitStatus = 0;
                    // _that.getValid();
                    _that.isValidStatus = 1; // 出错的时候把
                });
            } else {
                res = _valid || null;
                _that.isWaitStatus = 0;
            }
            _that.valid = res;
            return res;
        }
        /**
         * 调用验证方法
         *
         * @param {*} fn 验证方法
         */
        function _callFn(fn: any): IValidetorData|Promise<IValidetorData> {
            return callFn(fn || null, [_that.value instanceof FormGroup ? _that.value : _that]);

            // if (fn instanceof Function) {
            //     __valid = fn(_that.value instanceof FormGroup ? _that.value : _that);
            // }
            // return __valid || null;
        }
    }

    /**
     * 获取当前FormControl
     *
     * @returns {this}
     * @memberof FormControl
     */
    get(): this {
        return this;
    }
    /**
     * 获取值
     *
     * @returns
     * @memberof FormControl
     */
    getValue(): any {
        let _that = this;
        return _that.value;
    }
}

export {
    IControlsOptions
};
