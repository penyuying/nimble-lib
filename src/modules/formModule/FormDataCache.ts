import { IFormCacheKey, IFormCacheItem, ICacheKeyConfigKey, ICacheKeyConfig } from './interface/FormDataCacheConfig';

/**
 * 缓存FormModule数据
 *
 * @class FormDataCache
 */
export class FormDataCache {
    /**
     * 缓存的数据map
     */
    __data: object|object[]|undefined;
    /**
     * 当前数据是否为数组
     *
     * @param {(object|object[])} data
     * @returns {(boolean|undefined)}
     * @memberof FormDataCache
     */
    private _isArray(data: object|object[]): boolean|undefined {
        let _isArray: boolean|undefined;
        if (data) {
            _isArray = false;
            if (data instanceof Array) {
                _isArray = true;
            }
        }
        this._isArray = function() {
            return _isArray;
        };
        return _isArray;
    }
    // constructor() {}
    /**
     * 设置map列表
     * @param {Object[]} dataItem 当前缓存里的数据
     * @param {Object[]} list formArray
     * @param {IFormCacheKey} listItemKey 列表项取key的字段名或字段名列表
     * @returns {object}
     */
    private _setList(dataItem: object|object[]|undefined, list: object[], listItemKey: IFormCacheKey) {
        let _that = this;
        let _data = dataItem || [];
        (list || []).forEach((listItem, index) => {
            let _temp = _that._setItem(_data[index], listItem, _that._getKeyItem(listItemKey, index));
            if (typeof _temp !== 'undefined') {
                _data[index] = _temp;
            }
        });
        return _data;
    }
    /**
     * 设置map项
     * @param {Object} dataItem 当前缓存里的数据项
     * @param {Object} groupData formgroup
     * @param {IFormCacheItem} listItemKey 列表项取key的字段名或字段名列表
     * @returns {Array<Object>}
     */
    private _setItem(dataItem: object|object[]|undefined, groupData: object, listItemKey: IFormCacheKey) {
        let _that = this;
        let key = _that._getKey(groupData, listItemKey);
        if (key) {
            dataItem = dataItem || {};
            dataItem[key] = Object.assign({}, groupData);
        }
        return dataItem;
    }


    /**
     * 获取列表map
     * @param {Object[]} dataItem 当前缓存里的数据
     * @param {Object[]} list formArray
     * @param {IFormCacheKey} listItemKey 列表项取key的字段名或字段名列表
     * @returns {Array<Object>}
     */
    private _getList(dataItem: object[], list: object[], listItemKey: IFormCacheKey) {
        let _that = this;
        let res: object[] = [];
        let _data = dataItem;
        _data && (list || []).forEach((listItem, index) => {
            let _temp = _that._getItem(_data[index], listItem, _that._getKeyItem(listItemKey, index));
            if (typeof _temp !== 'undefined') {
                res[index] = _temp;
            }
        });
        return res;
    }

    /**
     * 获取列表map
     * @param {Object} dataItem 当前缓存里的数据项
     * @param {Object} groupData formGroup的value
     * @param {IFormCacheKey} listItemKey 列表项取key的字段名或字段名列表
     * @returns {Array<Object>}
     */
    private _getItem(dataItem: object, groupData: object, listItemKey: IFormCacheKey): object|undefined {
        let _that = this;
        let key = _that._getKey(groupData, listItemKey);
        let item = dataItem;
        let res: object|undefined;
        if (key && item && item[key]) {
            res = item[key];
        }
        return res;
    }
    /**
     * 获取formArray对应的key
     *
     * @param {IFormCacheKey} listItemKey
     * @param {number} index
     * @returns
     * @memberof FormDataCache
     */
    private _getKeyItem(listItemKey: IFormCacheKey, index: number): IFormCacheKey {
        let res: IFormCacheKey = listItemKey;
        if (listItemKey instanceof Array) {
            res = listItemKey[index];
        }
        return res;
    }
    /**
     * 获取列表map
     * @param {Object} listItem 列表项
     * @param {Object|string} keyItem 列表项取key的字段名(string的话为keyName))
     * @returns {string[]}
     */
    private _getKey(listItem: object, keyItem: IFormCacheKey): string {
        let key = '';
        let _keyItem: string|string[] = filterKey(keyItem);
        if (typeof _keyItem === 'string') {
            key = _keyItem;
        } else if (_keyItem instanceof Array) {
            _keyItem.forEach(item => {
                key = key + ((listItem && listItem[item]) || '');
            });
        }
        return key;

        /**
         * 序列化参数
         *
         * @param {*} params 参数
         * @returns {string|string[]}
         */
        function filterKey(params: IFormCacheKey): string|string[] {
            let res: string|string[] = '';
            if (params && typeof params === 'string') {
                res = [params];
            } else if (params instanceof Array) {
                res = <string[]>params;
            } else if (params instanceof Object) {
                let _params: ICacheKeyConfigKey|ICacheKeyConfig = <ICacheKeyConfigKey|ICacheKeyConfig>params;
                if (_params.key) {
                    res = _params.key;
                } else if (_params.keyName) {
                    if (typeof _params.keyName === 'string') {
                        res = [_params.keyName];
                    } else if (_params.keyName instanceof Array) {
                        res = _params.keyName;
                    }
                }
            }
            return res;
        }
    }
    /**
     * 设置缓存
     *
     * @param {(object|object[])} data formGroup或formArray的value
     * @param {IFormCacheKey} listItemKey 列表项取key的字段名或字段名列表
     * @memberof FormDataCache
     */
    setCache(data: object|object[], listItemKey: IFormCacheKey) {
        let _that = this;
        let _isArray = _that._isArray(data);
        if (_isArray) {
            _that.__data = _that._setList(_that.__data, <Array<object>>data, listItemKey);
        } else if (_isArray === false) {
            _that.__data = _that._setItem(_that.__data, data, <IFormCacheItem>listItemKey);
        }
    }
    /**
     * 获取缓存的数据
     *
     * @param {(object|object[])} data formGroup或formArray的value
     * @param {(IFormCacheKey|IFormCacheKey[])} listItemKey 列表项取key的字段名或字段名列表
     * @returns {(object|object[]|undefined)}
     * @memberof FormDataCache
     */
    getCache(data: object|object[], listItemKey: IFormCacheKey|IFormCacheKey[]): object|object[]|undefined {
        let _that = this;
        let _isArray = _that._isArray(data);
        let res: object|object[]|undefined;
        if (_isArray) {
            res = _that._getList(<object[]>_that.__data, <object[]>data, <IFormCacheKey>listItemKey);
        } else if (_isArray === false) {
            res = _that._getItem(<object>_that.__data, <object>data, <IFormCacheKey>listItemKey);
        }
        return res;
    }

    /**
     * 增加项
     * @param {number} index 增加项对应的索引号
     * @param {object} dataItem 增加项对应的索引号
     */
    addItem(index: number, dataItem?: object): object[]|undefined {
        let _that = this;
        let res: object[]|undefined;
        if (_that.__data instanceof Array) {
            res = _that.__data.splice(index + 1, 0, dataItem || {});
        }
        return res;
    }
    /**
     * 移除项
     * @param {number} index 移除项对应的索引号
     * @returns {Object} 返回整个项的map
     */
    removeItem(index: number): object[]|undefined {
        let _that = this;
        let res: object[]|undefined;
        if (_that.__data instanceof Array && _that.__data[index]) {
            res = _that.__data.splice(index, 1);
        }
        return res;
    }

    /**
     * 清除缓存
     *
     * @memberof FormDataCache
     */
    clear() {
        this.__data = undefined;
    }
}

export {
    IFormCacheKey,
    IFormCacheItem,
    ICacheKeyConfigKey,
    ICacheKeyConfig
};
