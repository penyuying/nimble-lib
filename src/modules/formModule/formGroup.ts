
import { IGroupOptions } from './interface/formGroupConfig';
import { FormControl } from './formControl';
import { IFormInterface } from './interface/form';
import { FormGroupCore } from './formGroup.core';
import { IFormFilter } from './interface/formConfig';
import { IKeyValue } from '../../library/helpers/IKeyValue';
import { FormArray } from './formArray';
import VmBind from '../../library/util/vmBind';
import callFn from '../../library/util/callFn';

/**
 * 表单对像
 *
 * @export
 * @class FormGroup
 */
export class FormGroup<T = any> extends FormGroupCore<IGroupOptions<FormControl>|IGroupOptions<FormControl>[], T>
    implements IFormInterface<T> {
    parent: FormGroup<T> | FormArray<T[]> | null = null;
    formGroup: FormGroup<T> | null = null;
    formArray: FormArray<T[]> | null = null;
    /**
     * 是否点过保存
     *
     * @type {boolean}
     * @memberof FormGroupCore
     */
    @VmBind({
        set(val: any) {
            let _that: any = this;
            _that.setGroupsIsSave(_that.controls, val);
        }
    })
    isSave: boolean = false;
    /**
     * 表单的验证结果
     *
     * @type {*}
     * @memberof FormGroup
     */
    @VmBind({
        set(val: any) {
            let _that: any = this;
            _that.isValid = !!val;
        },
        get(val: any) {
            let _that: any = this;
            return _that.getValid();
        }
    })
    public valid: any;
    /**
     * 最后的验证结果是否有错误
     */
    public isValid: boolean = false;

    /**
     * 表单的内容数据
     *
     * @type {(T|null)}
     * @memberof FormGroup
     */
    @VmBind({
        get(val: any) {
            let _that: any = this;
            return _that.getValue();
        }
    })
    public value: T|null = null;
    // private _group: IGroupOptions<FormControl>|IGroupOptions<FormControl>[] = {};
    public controls: IGroupOptions<FormControl>|IGroupOptions<FormControl>[] = {};
    constructor(groupData: IGroupOptions<FormControl>|IGroupOptions<FormControl>[]) {
        super(groupData);
        this.setDefaultOptions(null);
        let _that = this;
        // _that._group = groupData;
        _that.setControls(groupData);
    }
    private setControls (groupData: IGroupOptions<FormControl>|IGroupOptions<FormControl>[]) {
        let _that = this;
        for (const key in groupData) {
            if (groupData.hasOwnProperty(key)) {
                const control = groupData[key];
                if (control instanceof FormControl || control instanceof FormGroup) {
                    control.formGroup = _that;
                    control.parent = _that;
                }
            }
        }
        _that.controls = groupData;
    }
    /**
     * 设置FormGroup的isSave的值
     */
    protected setGroupsIsSave(groups: any, isSave: boolean) {
        groups = groups || [];
        return eachGronps(groups, isSave);
        /**
         * 遍历集合设置isSave
         *
         * @param {*} _groups 需要遍历的集合
         * @param {*} _isSave 保存的值
         */
        function eachGronps(_groups: any, _isSave: any) {
            for (const key in _groups) {
                if (_groups.hasOwnProperty(key)) {
                    const item = _groups[key];
                    if (item instanceof FormGroup) {
                        item.isSave = _isSave;
                    }
                }
            }
            return true;
        }
    }
    /**
     * 获取FormControl
     *
     * @param {string} key Controls的key名称
     * @returns
     * @memberof FormGroup
     */
    get(key: string|number): FormControl|FormGroup|null { // FormControl|FormGroup|null
        let _that = this;
        let _group = _that.controls;
        key = key || 0;
        if (_group && _group[key] instanceof FormControl) {
            return _group[key];
        }
        if (_group && _group[key] instanceof FormGroup) {
            return _group[key];
        }
        return null;
    }
    /**
     * 验证数据
     *
     * @memberof FormGroup
     */
    getValid(): any {
        let _that = this;
        this.valid = null;
        return _that.getThisValid() || _that._getValid(this.controls);

        // function getValid(data: any) {
        //     let res;
        //     if (data instanceof FormArray) {
        //         res = _that._getValid(data.groups, (item: FormArray|FormGroup) => {
        //             return getValid(item);
        //         });
        //     } else if (data instanceof FormGroup) {
        //         res = _that._getValid(data.controls, (item: FormArray|FormGroup) => {
        //             return getValid(item);
        //         });
        //     }
        //     return res;
        // }
    }
    /**
     * 获取值
     *
     * @memberof FormGroup
     */
    getValue(): T {
        let _that = this;
        return _that._getValue(this.controls);
    }

    /**
     * 设置表单数据
     *
     * @param {IKeyValue} defaultData 数据
     * @param {IFormFilter} [callback] 数据过滤的回调
     * @returns {*}
     * @memberof FormGroup
     */
    setValue(defaultData: IKeyValue, callback?: IFormFilter): any {
        let _that = this;
        let formMap = this.controls;
        if (formMap && defaultData) { // 设置默认数据
            for (const key in defaultData) {
                if (defaultData.hasOwnProperty(key)) {
                    if (formMap[key]) {
                        if (formMap[key] instanceof FormControl) {
                            formMap[key].setValue(defaultData[key]);
                        } else if (formMap[key] instanceof FormGroup) {
                            if (formMap[key].controls && defaultData[key] instanceof Object) {
                                _that.setValue(formMap[key].controls, defaultData[key]);
                            }
                        } else {
                            formMap[key].defaultValue = defaultData[key];
                        }
                    } else {
                        let _val = defaultData[key];
                        callFn(callback, [{}, _val]);
                        // if (callback instanceof Function) {
                        //     _val = callback({}, _val);
                        // }
                        if (_val instanceof FormControl || _val instanceof FormGroup) {
                            formMap[key] = _val;
                        } else {
                            formMap[key] = new FormControl({
                                defaultValue: _val
                            });
                        }
                    }
                }
            }
        }
        return formMap;
    }
    /**
     * 添加/设置项
     *
     * @param {(FormControl|FormGroupCore<any, any>)} item
     * @param {(string|number)} [key]
     * @returns {any} 返回false为修改失败
     * @memberof FormGroup
     */
    setItem(item: FormControl|FormGroupCore<any, any>, key?: string|number) {
        let _that = this;
        let res = _that._setItem(_that.controls, item, key, info => {
            if (info && info.flag === 'itemTypeErr') {
                console.error('传入的item类型不正确, item只能为FormControl');
            }
        }, control => {
            control.formGroup = _that;
            control.parent = _that;
        });
        res && _that.getValid();
        return res && _that.getValue() || res;
    }

    /**
     * 移除项
     * @param {(string|number)} key 移除项的key
     * @returns {any} 返回false为移除失败
     * @memberof FormGroup
     */
    removeItem(key: string|number) {
        let res = this._removeItem(this.controls, key);
        res && this.getValid();
        return res && this.getValue() || res;
    }
    /**
     * 合并FormGroup
     * @param {FormGroup} formGroup 需要合并的FormGroup
     * @memberof FormGroup
     */
    merge(formGroup: FormGroup) {
        let _that = this;
        if (formGroup instanceof FormGroup) {
            let control = _that.controls;
            let mergeControl = formGroup.controls;
            if ((control instanceof Array && !(mergeControl instanceof Array)) ||
                (!(control instanceof Array) && mergeControl instanceof Array)) {
                console.error('两个FormGroup的数据格式不一致不能合并');
            } else if (control instanceof Array && mergeControl instanceof Array) {
                mergeControl.forEach((item: any) => {
                    if (item instanceof FormGroup) {
                        item.formArray = _that.formArray;
                    }
                    item.formGroup = _that;
                    item.parent = _that;
                });
                _that.controls = control.concat(mergeControl);
            } else if (control instanceof Object && mergeControl instanceof Object) {
                for (const key in mergeControl) {
                    if (mergeControl.hasOwnProperty(key)) {
                        const item: any = mergeControl[key];
                        if (item instanceof FormGroup) {
                            item.formArray = _that.formArray;
                        }
                        item.formGroup = _that;
                        item.parent = _that;
                        control[key] = item;
                    }
                }
            }
        }
        return this;
    }
    /**
     * 清空内空
     * @memberof FormGroup
     */
    clear() {
        let _that = this;
        if (_that.controls instanceof Array) {
            _that.controls = [];
        } else if (_that.controls instanceof Object) {
            _that.controls = {};
        }
    }
}
export {
    IGroupOptions
};
