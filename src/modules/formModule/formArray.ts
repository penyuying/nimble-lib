import { IFormInterface } from './interface/form';
import { FormControl } from './formControl';
import { FormGroup } from './formGroup';
import { FormGroupCore } from './formGroup.core';
import { IKeyValue } from '../../library/helpers/IKeyValue';
import { IFormFilter } from './interface/formConfig';
import VmBind from '../../library/util/vmBind';

export class FormArray<T = any[]>  extends FormGroupCore<FormGroup[], T> implements IFormInterface<T> {
    parent: FormGroup<any> | FormArray<any[]> | null = null;
    formGroup: FormGroup<any> | null = null;
    /**
     * 表单的验证结果
     *
     * @type {*}
     * @memberof FormArray
     */
    @VmBind({
        set(val: any) {
            let _that: any = this;
            _that.isValid = !!val;
        },
        get(val: any) {
            let _that: any = this;
            return _that.getValid();
        }
    })
    public valid: any;
    /**
     * 最后的验证结果是否有错误
     */
    public isValid: boolean = false;


    /**
     * 表单的内容数据
     *
     * @type {(T|null)}
     * @memberof FormArray
     */
    @VmBind({
        get(val: any) {
            let _that: any = this;
            return _that.getValue();
        }
    })
    public value: T|null = null;

    /**
     * 是否点过保存
     *
     * @type {boolean}
     * @memberof FormGroupCore
     */
    @VmBind({
        set(val: any) {
            let _that: any = this;
            _that.setGroupsIsSave(_that.groups, val);
        }
    })
    isSave: boolean = false;

    /**
     * 表单列表
     *
     * @type {FormGroup[]}
     * @memberof FormArray
     */
    public groups: FormGroup[] = [];

    constructor(groupData: FormGroup[]) {
        super(groupData);
        this.setDefaultOptions(null);
        let _that = this;
        // _that._group = groupData;
        _that.groups = groupData;
    }
    /**
     * 设置FormGroup的isSave的值
     */
    protected setGroupsIsSave(groups: any, isSave: boolean) {
        groups = groups || [];
        return eachGronps(groups, isSave);
        /**
         * 遍历集合设置isSave
         *
         * @param {*} _groups 需要遍历的集合
         * @param {*} _isSave 保存的值
         */
        function eachGronps(_groups: any, _isSave: any) {
            for (const key in _groups) {
                if (_groups.hasOwnProperty(key)) {
                    const item = _groups[key];
                    if (item instanceof FormGroup) {
                        item.isSave = _isSave;
                    }
                }
            }
            return true;
        }
    }
    /**
     * 获取FormControl
     *
     * @param {number} index 列表的索引
     * @returns
     * @memberof FormArray
     */
    get(index: string|number) {
        index = index || 0;
        return this.groups[index] || null;
    }
    /**
     * 验证数据
     *
     * @memberof FormArray
     */
    getValid(): any {
        let _that = this;
        this.valid = null;
        return _that.getThisValid() || _that._getValid(this.groups);

        // function getValid(data: any) {
        //     let res;
        //     if (data instanceof FormArray) {
        //         res = _that._getValid(data.groups, (item: FormArray|FormGroup) => {
        //             return getValid(item);
        //         });
        //     } else if (data instanceof FormGroup) {
        //         res = _that._getValid(data.controls, (item: FormArray|FormGroup) => {
        //             return getValid(item);
        //         });
        //     }

        //     return res;
        // }
    }
    /**
     * 获取值
     *
     * @memberof FormArray
     */
    getValue(): T {
        let _that = this;
        return _that._getValue(this.groups);
    }

    /**
     * 设置表单列表数据
     *
     * @param {IKeyValue[]} defaultData 数据
     * @param {IFormFilter} [callback] 数据过滤的回调
     * @returns {*}
     * @memberof FormArray
     */
    setValue(defaultData: IKeyValue[], callback?: IFormFilter): any {
        let res: any;
        defaultData && defaultData.length > 0 && this.groups && this.groups.forEach((group, index) => {
            if (group && typeof defaultData[index] !== 'undefined') {
                res = res || [];
                res.push(group.setValue(defaultData[index]));
            }
        });
        return res;
    }
    /**
     * 添加/设置项
     *
     * @param {(FormControl|FormGroupCore<any, any>)} item
     * @param {(string|number)} [key]
     * @returns {boolean} 返回false为修改失败
     * @memberof FormArray
     */
    setItem(item: FormControl|FormGroupCore<any, any>, key?: string|number) {
        let _that = this;
        let res = _that._setItem(_that.groups, item, key, info => {
            if (info && info.flag === 'itemTypeErr') {
                console.error('传入的item类型不正确, item只能为FormGroup');
            }
        }, group => {
            group.formArray = _that;
            group.parent = _that;
            group.formGroup = null;
        });
        res && _that.getValid();
        res && _that.getValue();
        return res;
    }

    /**
     * 移除项
     *
     * @param {(number)} key 移除项的key
     * @returns {boolean} 返回false为移除失败
     * @memberof FormArray
     */
    removeItem(key: number): any|false {
        let res = this._removeItem(this.groups, key);
        res && this.getValid();
        res && this.getValue();
        return res;
    }
    /**
     * 合并FormArray
     *
     * @param {FormGroup} formGroup 需要合并的FormGroup
     * @memberof FormArray
     */
    merge(formArray: FormArray) {
        let _that = this;
        if (formArray instanceof FormArray) {
            let groups = _that.groups;
            let mergeGroups = formArray.groups;
            if (groups instanceof Array && mergeGroups instanceof Array) {
                mergeGroups.forEach((group: any) => {
                    let isMerge = groups.every(item => {
                        if (item === group) {
                            return false;
                        }
                        return true;
                    });
                    if (isMerge) {
                        group.formArray = _that;
                        group.parent = _that;
                        group.formGroup = null;
                        groups.push(group);
                    }
                });

                _that.groups = groups;
            } else {
                console.error('FormArray的数据格式不正确不能合并');
            }
        }
        return this;
    }

    /**
     * 清空内空
     *
     * @memberof FormArray
     */
    clear() {
        let _that = this;
        if (_that.groups instanceof Array) {
            _that.groups = [];
        }
    }
}
