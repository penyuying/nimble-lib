import { IRules, IValidetorParams } from './interface/validateConfig';
import getBLen from '../../library/util/getBLen';

/**
 * 验证字符串字节数
 *
 * @param {IValidetorParams} options 验证选项
 * @param {any} _val 验证的内容
 * @returns {boolean|string}
 */
export function verifySize(options: IValidetorParams, _val: string) {
    let res: boolean | string = false;
    let _size = getBLen(_val || '') || 0;
    let _minSize = Math.abs(parseInt(options.minSize + '', 10));
    let _maxSize = Math.abs(parseInt(options.maxSize + '', 10));
    if (!res && _minSize >= 0) {// 校验最小字节
        if (_size < _minSize) {
            res = options.minSizeErr || (options.name || '') + '最少不能少于' + _minSize + '字节';
        }
    }

    if (!res && _maxSize >= 0) {// 校验最大字节
        if (_size > _maxSize) {
            res = options.maxSizeErr || (options.name || '') + '最多不能超过' + _maxSize + '字节';
        }
    }
    return res;
}
/**
 * 验证字符串长度
 *
 * @param {IValidetorParams} options 验证选项
 * @param {any} _val 验证的内容
 * @returns {boolean|string}
 */
export function verifyLength(options: IValidetorParams, _val: string) {
    let res: boolean | string = false;
    if (!res && options.minLength) {// 校验最小长度
        if (_val.length < Math.abs(options.minLength)) {
            res = options.minLengthErr || (options.name || '') + '最少不能少于' + Math.abs(options.minLength) + '字';
        }
    }

    if (!res && options.maxLength) {// 校验最大长度
        if (_val.length > Math.abs(options.maxLength)) {
            res = options.maxLengthErr || (options.name || '') + '最多不能超过' + Math.abs(options.maxLength) + '字';
        }
    }
    return res;
}
/**
 * 校验空
 *
 * @param {IValidetorParams} options 验证选项
 * @param {any} _val 验证的内容
 * @returns {boolean|string}
 */
export function verifyEmpty(options: IValidetorParams, _val: any) {
    let res: boolean | string = false;
    if (options.isChecked && !_val && _val !== '0') {// 校验空
        res = options.requiredErr || '请选择' + (options.name || '');
    }

    if (!res && (!_val && _val !== '0' || !(((_val || '') + '').replace(/^\s+|\s+$/ig, ''))) && options.required) {// 校验空
        res = options.requiredErr || '请输入' + (options.name || '');
    }
    return res;
}
/**
 * 校验正则格式
 *
 * @param {IRules} _formatData 格式对象
 * @param {(number|string)} _text 需要校验的内容
 * @returns {(boolean|string)} 返回flase为校验通过；其它为错误信息
 */
export function validFormat(_formatData: IRules, _text: number|string): boolean|string {
    let _res: boolean | string = false;
    let _valid: boolean;
    if (_formatData && _formatData.reg && _text) {
        _formatData.reg.lastIndex = 0;
        _valid = _formatData.reg.test(_text + '');
        if (!_valid) {
            _res = _formatData.infoTxt || '格式错误';
        }
    }

    return _res;
}
