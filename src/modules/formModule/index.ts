import formFactory, {
    FormGroup,
    IGroupOptions,
    FormControl,
    IControlsOptions,
    IFormControlDataMap,
    IFormFilter,
    IFormValidErrorBack,
    IInitControlData,
    IFormControlOptions,
    FormArray,
    IFormInterface,
    IFormModuleInterface,
    FormDataCache,
    IFormCacheKey,
    IFormCacheItem,
    ICacheKeyConfigKey,
    ICacheKeyConfig
} from './formModule.imp';
import validFactory, {
    IValidetorParams,
    IValidFunction,
    IValidetorArrayParams,
    IValidateInitConfig,
    IRules,
    IValidOptions,
    IValidetorData,
    IValidateInterface
} from './validate';

export default function FormFactory(config?: {
    validate?: IValidateInitConfig
}) {
    return formFactory(validFactory(config && config.validate || {}));
}

export {
    FormGroup,
    IGroupOptions,
    FormControl,
    IControlsOptions,
    IFormControlDataMap,
    IFormFilter,
    IFormValidErrorBack,
    IInitControlData,
    IFormControlOptions,
    FormArray,
    IFormInterface,
    IFormModuleInterface,
    IValidetorParams,
    IValidFunction,
    IValidetorArrayParams,
    IValidateInitConfig,
    IRules,
    IValidOptions,
    IValidetorData,
    IValidateInterface,

    FormDataCache,
    IFormCacheKey,
    IFormCacheItem,
    ICacheKeyConfigKey,
    ICacheKeyConfig
};
