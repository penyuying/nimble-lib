import { IRequestParams } from './httpClientConfig';
import { Observable } from 'rxjs/Observable';
import { IKeyValue } from '../../../library/helpers/IKeyValue';
import { AxiosRequestConfig } from 'axios';
import { IHttpCacheParams, ICacheType } from '../../helpers/httpCache/interface/httpCacheConfig';

/**
 * 过滤参数
 */
export interface IFilteParams<T = any, H = any> {
    service: string;
    options: IRequestParams<T, H>;
    config: IHttpClientConfig<T, H>;
}

/**
 * http请求配置
 */
export interface IHttpClientConfig<T = any, H = any> {
    /**
     * 请求参数
     */
    requestConfig?: IRequestParams<T, H>;

    apiServer?: string;
    /**
     * 环境
     */
    env?: IKeyValue;
    apiServerFilter?: (data: { service: string; }) => any;
    /**
     * 请求参数过滤器
     */
    optionsFilter?: (data: IFilteParams<T, H>) => Promise<any>|any;
    /**
     * 开始请求前
     */
    beforeSend?: (data: IFilteParams<T, H>) => Promise<any>|any;
    /**
     * 开始请求前
     */
    afterSend?: (data: IFilteParams<T, H> & { data?: any; err?: any }) => Promise<any>|void;
    /**
     * 请求出错后尝试次数
     */
    attempt?: number;

    /**
     * 返回false表示不再尝试请求
     */
    attemptFilter?: (data: IFilteParams<T, H> & {err: ErrorEventInit; count: number}) => Promise<IRequestParams<T, H>|false>;
    /**
     * 错误数据过滤（返回true表示有错误，false表示无错误）
     */
    onFilterDataError?: (data: IFilteParams<T, H> & { data: any }) => Promise<boolean>|boolean;
    /**
     * 请求出错（false表示不返回给调用方,其它则返回）
     */
    onHttpError?: (data: IFilteParams<T, H> & { err: any }) => Promise<boolean>|boolean|void;
}

/**
 * HttpClient需要的参数
 */
export interface IHttpRequestParams {
    /**
     * 兼容属性===data
     */
    body?: any;
    /**
     * 请求的服务器key
     */
    apiServer?: string;
    /**
     * 缓存请求参数
     */
    cacheData?: boolean|(IHttpCacheParams & {cacheKey?: string; cacheType?: ICacheType});
    /**
     * 请求类型
     */
    dataType?: 'jsonp';
    /**
     * 是否已登录
     */
    isLoading?: boolean;
    /**
     * 出错时是否要返回数据
     */
    isErrorData?: boolean;
    /**
     * 出错时是否要提示错误消息
     */
    isErrorMsg?: boolean;
    /**
     * 请求是否需要加session
     */
    session?: boolean;
}

/**
 * 请求参数
 */
export interface IRequestParams<T= any, H= any> extends AxiosRequestConfig, IHttpRequestParams {
    url?: string;
    method?: string;
    baseURL?: string;
    headers?: H;
    params?: T;
    data?: T;
    timeout?: number;
    withCredentials?: boolean;
    responseType?: string;
    onUploadProgress?: (progressEvent: any) => void;
    onDownloadProgress?: (progressEvent: any) => void;
    maxContentLength?: number;
    maxRedirects?: number;
}

/**
 * HttpClient插件
 */
export interface IHttpClientPlugin<T = any, H = any> {
    handler<B = any>(data: Function|Observable<any>, url: string, options?: IKeyValue, config?: IHttpClientConfig<T, H>): Observable<B>;
}


// export interface IHttpParams {
//     cacheData?: boolean|(IHttpCacheParams & {cacheKey?: string; cacheType?: ICacheType});
// }

