import { IHttpClientConfig } from './interface/httpClientConfig';

export const HTTP_CLIENT_DEFAULT_CONFIG: IHttpClientConfig = {
    requestConfig: {
        withCredentials: true,
        headers: {}
    },
    attempt: 0
};
