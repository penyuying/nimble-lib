import 'rxjs/add/operator/publishReplay';
import { Observable } from 'rxjs/Observable';
import getBackData from '../../library/util/getBackData';

import { HttpClientCore } from './HttpClient.core';
import { IHttpClientConfig, IRequestParams, IHttpClientPlugin } from './interface/httpClientConfig';
import { Observer } from 'rxjs/Observer';
import extend from '../../library/util/extend';
import { IFactoryUse } from '../../library/helpers/IFactoryUse';
import toFormData from '../../library/util/toFormData';

export class HttpClient<T = any, H = any, R = any> extends HttpClientCore<T, H, R> {
    public name = 'Http';
    constructor(options: IHttpClientConfig<T, H>) {
        super(options);
        this.setDefaultOptions(options);
    }

    /**
     * 发送请求
     * @param {*} url api url
     * @param {*} option 选项
     * @returns {Observable<object>}
     */
    getData<D = R>(url: string, option: IRequestParams<T, H>): Observable<D> {
        let _that = this;
        let config = _that.defaultOption || {};
        let _option = extend(config.requestConfig || {}, option);

        let isErrorData = !!_option.isErrorData || false;
        let isErrorMsg = !!_option.isErrorMsg || false;
        option.isErrorData = isErrorData;
        return Observable.create((observer: Observer<D>) => {
            _that.send(url, option).subscribe((back: any) => {
                let data = back && back.data;
                if ((isErrorData || isErrorMsg) && data) {
                    getBackData(config.onFilterDataError, [{
                        service: url,
                        options: _option,
                        config: config,
                        data: data
                    }]).then(_err => {
                        let isError = false;
                        if (_err) {
                            isError = true;
                        }
                        if (!isError || !isErrorData) {
                            observer.next(data);
                        }
                        observer.complete();
                    });
                } else {
                    observer.next(data);
                }
            }, (err: Error) => {
                if ((isErrorData || isErrorMsg)) {
                    getBackData(config.onHttpError || err, [{
                        service: url,
                        options: _option,
                        config: config,
                        err: err
                    }]).then(_err => {
                        if (_err === undefined) {
                            _err = err;
                        }
                        _err && observer.error(_err);
                        observer.complete();
                    });
                } else {
                    observer.error(err);
                    observer.complete();
                }
            });
        }).publishReplay().refCount();
    }
    /**
     * get请求
     * @param {*} url api url
     * @param {*} option 选项
     * @returns {Observable<object>}
     */
    get<D = R>(url: string, option: IRequestParams<T, H>): Observable<D> {
        return this.getData<D>(url, Object.assign({}, option, {
            method: 'get'
        }));
    }

    /**
     * post请求
     * @param {*} url api url
     * @param {*} option 选项
     * @returns {Observable<object>}
     */
    post<D = R>(url: string, option: IRequestParams<T, H>): Observable<D> {
        return this.getData<D>(url, Object.assign({}, option, {
            method: 'post'
        }));
    }
    /**
     * formData方式post请求
     * @param {*} url api url
     * @param {*} option 选项
     * @returns {Observable<object>}
     */
    formData<D = R>(url: string, option: IRequestParams<T, H>): Observable<D> {
        // let data = option.data;
        // // if (!(data instanceof FormData) && data instanceof Object) {
        // //     let formData = new FormData();
        // //     for (const key in data) {
        // //         if (data.hasOwnProperty(key)) {
        // //             const item = data[key];
        // //             formData.append(key, item);
        // //         }
        // //     }
        // //     option.data = formData;
        // // }
        let data: any = toFormData(option.data);
        if (data) {
            option.data = data;
        } else {
            delete option.data;
        }
        return this.getData<D>(url, Object.assign({
            // headers: { 'Content-Type': 'multipart/form-data' }
        }, option, {
            method: 'post'
        }));
    }
    /**
     * jsonp请求
     * @param {*} url api url
     * @param {*} option 选项
     * @returns {Observable<object>}
     */
    jsonp<D = R>(url: string, option: IRequestParams<T, H>): Observable<D> {
        option = Object.assign({
            dataType: 'jsonp'
        }, option);
        // option.params = serializeQueryParams(option.params) Util.query2Str(option.params) || '';
        return this.getData<D>(url, option);
    }
}

/**
 * 实例化HttpClient
 *
 * @export
 * @param {*} args 参数
 * @returns {HttpClient}
 */
const httpClientFactory: IFactoryUse<IHttpClientConfig<any, any>, HttpClient,
    IHttpClientPlugin<any, any>> = <T= any, H= any>(options: IHttpClientConfig<T, H>) => {
        return new HttpClient(options);
    };
httpClientFactory.use = HttpClient.use.bind(HttpClient);
export default httpClientFactory;

export {
    IHttpClientConfig,
    IRequestParams
};
