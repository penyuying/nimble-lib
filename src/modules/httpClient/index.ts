import httpClientFactory, {
    HttpClient,
    IHttpClientConfig,
    IRequestParams
} from './HttpClient.imp';

export default httpClientFactory;
export {
    HttpClient,
    IHttpClientConfig,
    IRequestParams
};
