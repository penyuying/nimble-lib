import imgClipFactory, {
    ImgClip,
    IImgClipConfig,
    IImgSrcData,
    IImgSizeData,
    ICurImgSize
} from './imgClip.imp';


export default imgClipFactory;

export {
    ImgClip,
    IImgClipConfig,
    IImgSrcData,
    IImgSizeData,
    ICurImgSize
};
