import { IImgClipConfig } from './interface/ImgClipConfig';
import getStyle from '../../library/util/getStyle';

/**
 * 根元素的字号
 */
const ROOT_FONT_SIZE = parseFloat(getStyle(document && document.documentElement, 'font-size'));

export const IMG_CLIP_DEFAULT_CONFIG: IImgClipConfig = { // 默认数据
    remRoot: ROOT_FONT_SIZE, // rem根元素字号大小
    // contWidth: null, // window.innerWidth, // 内容宽度(Number)
    // contHeight: null, // window.innerHeight, // 内容高度(Number)
    // maxWidth: null, // 内容宽度(Number)
    // maxHeight: null, // 内容高度(Number)
    ratio: 2, // 缩放的倍数
    cutter: 'auto', // 裁剪方式

    /**
     * 判断是否支持webp
     * @return {Boolean}
     */
    supportWebp: (function() {
        let support = true;
        const d = document;
        try {
            let el = d.createElement('object');
            el.type = 'image/webp';
            el.style.visibility = 'hidden';
            el.innerHTML = '!';
            d.body.appendChild(el);
            support = !el.offsetWidth;
            d.body.removeChild(el);
        } catch (err) {
            support = false;
        }
        return support;
    })()
};
