import { IKeyValue } from '../../library/helpers/IKeyValue';
import getBackData from '../../library/util/getBackData';
import { BaseAbstract } from '../../library/basic/base.abstrat';
import { IImgClipConfig, IImgSrcData, IImgSizeData, ICurImgSize } from './interface/ImgClipConfig';
import { IMG_CLIP_DEFAULT_CONFIG } from './constant';
/**
 * 获取图片大小
 *
 * @export
 */
export class ImgClip extends BaseAbstract<IImgClipConfig> {
    public name = 'ImgClip';
    protected defaultOption = IMG_CLIP_DEFAULT_CONFIG;

    constructor(options: IImgClipConfig) {
        super(options);
        /**
         * 默认数据
         */
        this.setDefaultOptions(options);
        let _options = this.defaultOption;

        if (!_options.contWidth) {
            let _w = window.innerWidth;
            _options.contWidth = Math.min(_options.maxWidth || _w, _w);
        }
        if (!_options.contHeight) {
            let _h = window.innerHeight;
            _options.contHeight = Math.min(_options.maxHeight || _h, _h);
        }
    }
    /**
     * 获取图片宽高（必填wEl或hEl）
     *
     * @param {String} src 图片地址
     * @param {IImgClipConfig} options 选项参数
     * @memberof ImgClip
     * @return {Object} res
     */
    public getSrc(src: string, options: IImgClipConfig): Promise<IImgSrcData|IKeyValue> {
        let _that = this;
        let _options = _that.getOptions(options);
        src = (src || '') + '';
        return new Promise((resolve, reject) => {
            getBackData(_options.ratio, [_options]).then(res => {
                resolve(_that._getSrc(src, res, _options));
            });
        });
    }
    /**
     * 获取图片宽高（必填wEl或hEl）
     *
     * @param {String} src 图片地址
     * @param {Object} options 选项参数
     * @memberof ImgClip
     * @return {Object} res
     */
    public getSize(src: string, options: IImgClipConfig): IImgSizeData|undefined {
        let _that = this;
        let res: IImgSizeData = {};
        let _options = _that.getOptions(options);
        let wEl: any = _options.width;
        let hEl: any = _options.height;
        src = (src || '') + '';
        let _elw: string|number = '';
        let _elh: string|number = '';
        if (!src || (!wEl && !hEl)) {
            return;
        }
        // 防止高度取不准(hEl instanceof HTMLElement || !hEl)
        if ((!hEl || hEl instanceof HTMLElement) && wEl instanceof HTMLElement) {
            _elw = wEl.clientWidth;
        }
        if ((!_elw || _options.cutter && _options.cutter !== 'auto') &&
            (!wEl || wEl instanceof HTMLElement) && hEl instanceof HTMLElement) {
            _elh = hEl.clientHeight;
        }
        if (_elw || _elh) {
            wEl = (_elw && _elw + 'px') || '';
            hEl = (_elh && _elh + 'px') || '';
        }
        if (!wEl && !hEl) {
            return res;
        }
        wEl = wEl && _that._toPx(wEl, _options);
        hEl = hEl && _that._toPx(hEl, _options, true);
        res = _that._getImgSize(src) || {};
        if (res.imgWidth || res.imgHeight || _options.forceCut) {
            let _scale = res.scale || 1;
            if (wEl) {
                res.width = wEl;
            }
            if (hEl) {
                res.height = hEl;
            }
            if (wEl && !hEl) {
                res.height = wEl / _scale;
            }
            if (!wEl && hEl) {
                res.width = hEl * _scale;
            }
            if (res.width) {
                res.width = Math.floor(res.width);
            }
            if (res.height) {
                res.height = Math.floor(res.height);
            }
        }
        return res;
    }
    /**
     * 设置src
     *
     * @param {String} url 图片地址
     * @param {number} ratio 图片缩放倍数
     * @param {Object} opts 选项
     * @return {Object}
     */
    private _getSrc(url: string, ratio: number, opts: IImgClipConfig): IImgSrcData|IKeyValue {
        let _that = this;
        let _options = opts;
        const isSupportWebp = _options.supportWebp;
        const size = _that.getSize(url, _options); // 在没有去除@后缀之前获取大小
        if (!url) {
            return {};
        }
        let _src = url.replace(/@.+/, ''); // 去除@后面的
        let _srcArr = _src.match(/\.([^.]+)$/);
        let _extFix = (_srcArr && _srcArr[1]) || '';
        let _oldExtFix = _extFix;
        let _oldSrc = url;

        // 设置后缀
        _extFix = (isSupportWebp && _extFix) ? 'webp' : (_extFix === 'webp' ? 'jpg' : _extFix);
        let _clipCode = size && _that._getClipCode(size, ratio, _options.cutter || 'auto') || '';
        let imgUrl = _that._genSrc(_src, _clipCode, _extFix);
        let res = Object.assign({}, size, {
            src: imgUrl || _oldSrc,
            extFix: _extFix,
            cutterSrc: imgUrl || _oldSrc,
            oldExtFix: _oldExtFix,
            oldSrc: _oldSrc
        });
        return res;
    }
    /**
     * 获取图片裁剪码
     * @param {IImgSizeData} size 图片大小
     * @param {number} ratio 图片缩放倍数
     * @param {'auto'|'contain'|'cover'} cutter 裁剪模式
     */
    private _getClipCode(size: IImgSizeData, ratio: number, cutter: 'auto'|'contain'|'cover'): string {
        let str = '';
        // 获取大小
        if (size) {
            let _cutter = (cutter + '').toLowerCase();
            let _cWidth = size.width;
            let _cHeight = size.height;
            let _oldScale = size.scale;
            let _scale = ((size.width || 0) / (size.height || 1));
            ratio = parseFloat(ratio + '') || 1;
            if (_oldScale && _scale) {
                switch (_cutter) {
                    case 'contain': // 最大的一边占满内容(整图显示)
                        if (_oldScale >= _scale) {
                            _cHeight = 0;
                        } else {
                            _cWidth = 0;
                        }
                        break;
                    case 'cover': // 最小的一边占满内容(铺满显示)
                        if (_oldScale <= _scale) {
                            _cHeight = 0;
                        } else {
                            _cWidth = 0;
                        }
                        break;
                    case 'auto': // 默认有宽度以宽度，没有宽度有高度以高度
                    default:
                        break;
                }
            }
            if (_cHeight) {
                let h = _ceil(_ceil(_cHeight) * ratio);
                str = Math.min(h, size.imgHeight || h) + 'h_';
            } else if (_cWidth) {
                let w = _ceil(_ceil(_cWidth) * ratio);
                str = Math.min(w, size.imgWidth || w) + 'w_';
            }
        }
        return str;
        /**
         * 个位数向上取整
         *
         * @param {*} num 取整的数字
         * @return {Number}
         */
        function _ceil(num: number) {
            return Math.ceil(num / 10) * 10;
        }
    }
    /**
     * 生成url
     *
     * @param {string} src 图片地址
     * @param {string} clipCode 裁剪码
     * @param {string} extFix 扩展名
     * @returns {string}
     * @memberof ImgClip
     */
    private _genSrc(src: string, clipCode: string, extFix: string) {
        let _supfix = '';
        // 拼接类型
        if (clipCode) {
            if (extFix === 'gif') {
                _supfix = '?@' + clipCode + '99q.src';
            } else {
                _supfix = '@' + clipCode + '99q.' + extFix;
            }
        } else if (extFix === 'webp') {
            _supfix = '@.' + extFix;
        }
        let imgUrl = (src + _supfix);

        return imgUrl;
    }
    /**
     * 单位转化成像素
     * @param {string|number} size 大小的值
     * @param {IImgClipConfig} options 选项
     * @param {Boolean} isHeight 是否为高度
     * @param {Boolean} noOffset 是否不计算offset
     * @return {Number}
     */
    private _toPx(size: string|number, options: IImgClipConfig, isHeight?: boolean, noOffset?: boolean): number|undefined {
        let res;
        const _options = options;
        const match = (size + '').match(/\s*([0-9]+\.?[0-9]*)(%|[a-z]*)?\s*$/i);
        if (match && match[1]) {
            let n = parseFloat(match[1]);
            let unit = ((match[2] || '') + '').toLowerCase();
            switch (unit) {
                case 'px':
                    res = n;
                    break;
                case 'vw':
                case 'vh':
                case '%':
                    if (unit === 'vw' || unit === 'vh') {
                        n = n * 100;
                        if (unit === 'vh') {
                            isHeight = true;
                        }
                    }
                    let base;
                    if (isHeight) {
                        base = _options.contHeight || window.innerHeight;
                        if (_options.contHeightOffset && !noOffset) {
                            base = base - (this._toPx(_options.contHeightOffset, {
                                contHeight: base
                            }, true, true) || 0);
                        }
                    } else {
                        base = _options.contWidth || window.innerWidth;
                        if (_options.contWidthOffset && !noOffset) {
                            base = base - (this._toPx(_options.contWidthOffset, {
                                contWidth: base
                            }, false, true) || 0);
                        }
                    }
                    res = n * base / 100;
                    break;
                case 'rem':
                default:
                    res = n * (_options.remRoot || 14);
                    break;
            }
        }
        return res;
    }
    /**
     * 获取图片的大小
     * @param {String} src 图片路径
     * @return {Object} res
     */
    private _getImgSize(src: string): ICurImgSize|void {
        let res;
        let match = src.match(/(\d+)w_(\d+)h/);
        if (!match) {
            match = src.match(/w(\d+)_h(\d+)/);
        }
        if (match && match[1] && match[2]) {
            let w = parseInt(match[1], 10);
            let h = parseInt(match[2], 10);
            res = {
                imgWidth: w, // 宽
                imgHeight: h, // 高
                scale: w / h // 比例
            };
        }
        return res;
    }
}
/**
 * 实例化工厂方法
 *
 * @export
 * @param {IImgClipConfig} options 配置选项
 * @returns {ImgClip}
 */
export default function (options: IImgClipConfig): ImgClip {
    return ImgClip.instance<ImgClip>(options);
}

export {
    IImgClipConfig,
    IImgSrcData,
    IImgSizeData,
    ICurImgSize
    // tslint:disable-next-line
};
