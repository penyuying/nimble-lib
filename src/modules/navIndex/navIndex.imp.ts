
import { BaseAbstract } from '../../library/basic/base.abstrat';
import getElementTop from '../../library/util/getElementTop';
import getStyle from '../../library/util/getStyle';
import scrollParent from '../../library/util/scrollParent';
import { INDEX_LIST_EVENT_TYPE } from './constant';
import { INavIndexConfig, IOffsetItem, IElementItem, IIndexRegion } from './interface/navIndexConfig';

export class NavIndex extends BaseAbstract<INavIndexConfig> {
    public name = 'NavIndex';
    /**
     * 最大高度
     */
    private _maxHeight: number = 0;
    /**
     * 最小高度
     *
     * @private
     * @type {number}
     * @memberof NavIndex
     */
    private _minHeight: number = 0;
    private _offsetList: Array<IOffsetItem>|null = null;
    /**
     * 当前元素元素的父节点是否为fixed定位
     *
     * @private
     * @type {boolean}
     * @memberof NavIndex
     */
    private _isFixed?: boolean;
    constructor(options?: INavIndexConfig) {
        super(options);
        this.refresh();
    }
    /**
     * 刷新数据
     * @param {Array} navItems navItem组件或元素列表
     */
    public refresh(navItems?: IElementItem[]) {
        let _that = this;
        _that._maxHeight = 0;
        _that._minHeight = 0;
        _that._offsetList = (navItems && _that._initListOffset(navItems, true)) || null;
    }
    /**
     * 根据indexa获取item位置数据
     * @param {Number} index 索引
     * @param {Array} navItems navItem组件或元素列表
     * @returns {Object}
     */
    public getItem(index: number, navItems?: IElementItem[]) {
        let _that = this;
        let _offsetList = _that._initListOffset(navItems) || [];
        return _offsetList[index] || {};
    }
    /**
     * 获取滚动条距离顶部的位置
     * @param {Array} navItems navItem组件或元素列表
     * @returns {Number}
     */
    public getScrollTop(navItems?: IElementItem[]) {
        let _that = this;
        let _offsetList = _that._initListOffset(navItems);
        let startItem = _offsetList && _offsetList[0];
        let _scrollTop = 0;
        let _scrollEl;
        if (startItem) {
            let _el: any = startItem.el;
            let isFixed: boolean|undefined = _that._isFixed;
            if (typeof isFixed === 'undefined') {
                isFixed = (_el && _el.offsetParent && getStyle(_el.offsetParent, 'position') === 'fixed') || false;
            }
            _that._isFixed = isFixed;
            if (isFixed) {
                _el = document.body;
            }
            while (_el) {
                _el = scrollParent(_el);
                _scrollTop += (_el && (_el.scrollTop || (_el.scrollY || 0))) || 0;
                _scrollEl = _el;
                _el = _el.parentNode;
            }
            _scrollTop = (isFixed ? 0 - _scrollTop : _scrollTop) || 0;
        }
        return {
            scrollTop: _scrollTop,
            el: _scrollEl,
            isFixed: !!_that._isFixed
        };
    }
    /**
     * 获取索引
     * @param {Number} current 当前位置
     * @param {Array} navItems navItem组件或元素列表
     * @return {Number}
     */
    public getIndex(current: number, navItems?: IElementItem[]) {
        let _that = this;
        current = current || 0;
        let _offsetList: IOffsetItem[] = _that._initListOffset(navItems) || [];
        let _offsetItem = _offsetList[0] || {};
        let _itemTop: number = (_offsetItem && _offsetItem.top) || 0;
        let len: number = _offsetList.length || 0;
        let _region: IIndexRegion = _that._getRegion(current - _itemTop, _that._minHeight, _that._maxHeight);
        let endIndex = Math.min(Math.max(len, 1), _region.endIndex);
        let startIndex = Math.max(0, Math.min(_region.startIndex, endIndex - 1));
        // count = 0;
        let index: number|null = _that._calcIndex(startIndex, endIndex - 1, current, _offsetList);
        if (index === null) {
            // count++;
            if (_offsetList[0] && (_offsetList[0].bottom || 0) >= current) {
                index = 0;
            } else if (_offsetList[len - 1] && (_offsetList[len - 1].bottom || 0) <= current) {
                index = len - 1;
            }
        }
        let currentItem = _offsetList && _offsetList[index || 0];
        if (currentItem) {
            let bottom = (currentItem.bottom || 0) - current;
            let top = (currentItem.top || 0) - current;
            _that.$emit(INDEX_LIST_EVENT_TYPE.ITEM_MOVE, {
                index: index,
                top: Math.min(top, 0),
                bottom: Math.max(bottom, 0)
            });
        }
        return index || 0;
    }
    /**
     * 初始化偏移量列表
     * @param {Array} navItems navItem组件或元素列表
     * @param {Array} isReset 重置偏移量列表
     * @returns {Array}
     */
    private _initListOffset(navItems?: IElementItem[], isReset?: boolean) {
        let _that = this;
        let res = _that._offsetList;
        if (!isReset && res && res.length) {
            return res;
        } else {
            res = navItems && _that._getListOffset(navItems) || null;
        }
        _that._offsetList = res;
        return res;
    }
    /**
     * 计算偏移量
     * @param {Array} navItems navItem组件或元素列表
     * @returns {Array}
     */
    private _getListOffset(navItems: IElementItem[]): IOffsetItem[] {
        let _that = this;
        let _navItems = navItems || [];
        let _res: IOffsetItem[]  = [];
        if (_navItems && _navItems.length) {
            _navItems.forEach((item, index) => {
                let _el = item.$el || item;
                let itemData: IOffsetItem = {};
                if (_el) {
                    let _height = _el.offsetHeight;
                    let _top = 0;
                    if (index === 0) {
                        _top = getElementTop(_el);
                    } else {
                        _top = _res[index - 1].bottom || 0;
                    }
                    itemData = {
                        top: _top,
                        height: _height,
                        bottom: _top + _height,
                        el: _el
                    };
                }
                if (itemData.height) {
                    if ((_that._maxHeight || 0) < itemData.height) {
                        _that._maxHeight = itemData.height;
                    }
                    if (!_that._minHeight || _that._minHeight > itemData.height) {
                        _that._minHeight = itemData.height;
                    }
                }
                _res.push(itemData);
            });
        }
        return _res;
    }
    /**
     * 获取当前索引的范围
     *
     * @param {Number} minHeight 最小的item高度
     * @param {Number} maxHeight 最高的item高度
     * @param {Number} current 当前位置
     * @returns {Object}
     */
    private _getRegion(current: number, minHeight: number, maxHeight: number): IIndexRegion {
        minHeight = minHeight || 1;
        maxHeight = maxHeight || 1;
        current = current || 0;
        let startIndex = Math.floor((current / maxHeight) - 0.5);
        let endIndex = Math.ceil((current / minHeight) + 0.5);
        return {
            startIndex: Math.max(Math.min(startIndex, endIndex - 1), 0),
            endIndex: Math.max(endIndex, 1)
        };
    }
    /**
     * 比较数据
     * @param {Number} startIndex 开始索引
     * @param {Number} endIndex 结束索引
     * @param {Number} current 当前位置
     * @param {Array} list 数据列表
     * @returns {Number|null}
     */
    private _calcIndex(startIndex: number, endIndex: number, current: number, list: any[]): number|null {
        let _that = this;
        let res: number|null = null;
        let _center = Math.ceil((endIndex - startIndex) / 2);
        if (endIndex >= startIndex && endIndex >= 0 && startIndex >= 0 && list && list.length) {
            if (_center > 1) {
                // count++;
                let j = startIndex + _center;
                let item = list[j];
                if (item.top <= current && item.bottom > current) {
                    res = j;
                } else if (item.top > current && list[startIndex].top <= current) { // 前半部分
                    res = list[startIndex].bottom > current ? startIndex : _that._calcIndex(startIndex, j - 1, current, list);
                } else if (item.bottom <= current && list[endIndex].bottom > current) { // 后半部分
                    res = list[endIndex].top <= current ? endIndex : _that._calcIndex(j + 1, endIndex - 1, current, list);
                }
            } else if (_center >= 0) {
                for (let i = startIndex; i <= endIndex; i++) {
                    // count++;
                    const _item = list[i];
                    if (_item.top <= current && _item.bottom > current) {
                        res = i;
                        break;
                    }
                }
            }
        }
        return res;
    }
}

/**
 * 原生交互
 *
 * @export
 * @param {*} options 选项
 * @return {Object}
 */
export default function navIndexFactory(options: INavIndexConfig): NavIndex {

    return new NavIndex(options);
}

export {
    INavIndexConfig,
    IOffsetItem,
    IElementItem,
    IIndexRegion
};
