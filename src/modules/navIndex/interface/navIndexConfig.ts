/**
 * indexLinst选项
 */
export interface INavIndexConfig {

}


/**
 * 元素偏移量的item
 */
export interface IOffsetItem {
    /**
     * 元素顶部距离页面顶部的位置
     */
    top?: number;
    /**
     * 高度
     */
    height?: number;
    /**
     * 元素低部距离页面顶部的位置
     */
    bottom?: number;
    /**
     * 元素
     */
    el?: HTMLElement;
}

/**
 * 元素或组件
 */
export type IElementItem = IComponent & HTMLElement;

/**
 * 组件
 */
export interface IComponent {
    $el: HTMLElement;
}

/**
 * 索引的区域
 */
export interface IIndexRegion {
    /**
     * 开始的索引
     */
    startIndex: number;
    /**
     * 结束的索引
     */
    endIndex: number;
}
