///<reference path="./interface/zip.d.ts" />
import JSZipUtils from 'jszip-utils';
import JSZip from 'jszip';
import { BaseAbstract } from '../../../library/basic/base.abstrat';
import {ILottieFile, ILottieOtherFile } from '../../aeAnimation/aeAnimation.imp';
import getFileExt from '../../../library/util/getFileExt';

export class AeZipPreset<T> extends BaseAbstract<any>  {
    /**
     * 解析的方法后缀名
     *
     * @type {'zip'}
     * @memberof AeZipPreset
     */
    ext: string =  'zip';
    preset(path: string, options: T, ext: string) {
        let _that = this;
        return _that._unpackZip((resCb, errCb) => {
            _that._loadZip(path || '').then(resCb, errCb);
        }, options);
    }
    /**
     * 解包zip文件
     *
     * @param {*} fileCb 获取文件的回调
     * @returns {Promise}
     * @memberof AeAnimation
     */
    private _unpackZip(fileCb: (resolve: (res: any) => void, reject: (err: any) => void) => void, options: T): Promise<ILottieFile> {
        let _options: any = options || {};
        return new Promise((resolve, reject) => {
            new JSZip.external.Promise(fileCb).then(JSZip.loadAsync).then((zip: any) => {
                getFileMap(zip.files).then((fileData) => {
                    // let _jsons = fileData && fileData.json || [];
                    // if (_jsons && _jsons.length) {
                    //     let _jsonList: ILottieFileJsons[] = [];
                    //     _jsons.forEach(_json => {
                    //         _jsonList.push(extend<any>(_json, {
                    //             url: this._filtersJson(_json.url, fileData.files)
                    //         }));
                    //     });
                    //     fileData.json = _jsonList;
                    // }
                    resolve(fileData);
                }, reject);
            }).catch(reject);
        });
        /**
         * 映射文件
         *
         * @param {*} files 文件集
         * @return {Promise}
         */
        function getFileMap(files: ILottieOtherFile): Promise<ILottieFile> {
            let res: ILottieFile = <ILottieFile>{};
            let _files = files || {};
            return new Promise((resolve, reject) => {
                let _keys = Object.keys(_files);
                let _len = _keys.length;
                if (_len) {
                    for (let i = 0; i < _keys.length; i++) {
                        const key = _keys[i];
                        const _file = _files[key];
                        if (_file && !_file.dir && !/\/?__MACOSX\/?/ig.test(key)) { // mac打包出来的文件多一个__MACOSX目录会出问题
                            let _ext = getFileExt(key);
                            let _type = _ext === 'json' ? 'string' : (_options.imgType || 'blob');
                            _file.async(_type).then((content: any) => {
                                _len--;
                                if (_ext === 'json') {
                                    res.json = res.json || [];
                                    res.json.push(jsonFilter(key, content));
                                } else {
                                    let _files: any = res.files || {};
                                    res.files = _files;
                                    if (!res.files[key]) {
                                        res.files[key] = otherFileFilter(key, content, _type, _ext);
                                    }
                                }
                                if (!_len || _len < 0) {
                                    resolve(res);
                                }
                            });
                        } else {
                            _len--;
                            if (!_len || _len < 0) {
                                resolve(res);
                            }
                        }
                    }
                } else {
                    resolve(res);
                }
            });
        }


        /**
         * 过滤json文件
         *
         * @param {string} path 文件路径
         * @param {*} content 文件内容
         * @returns
         */
        function jsonFilter(path: string, content: any) {
            let _json = null;
            if (typeof content === 'string') {
                try {
                    _json = JSON.parse(content);
                } catch (error) {
                    _json = {};
                }
            } else {
                _json = content || {};
            }
            return {
                path: path,
                url: _json
            };
        }

        /**
         * 其它文件过滤
         *
         * @param {string} path 文件路径
         * @param {*} content 文件内容
         * @param {string} type 内容类型
         * @param {string} ext 文件扩展名
         * @returns
         */
        function otherFileFilter(path: string, content: any, type: string, ext: string) {
            let res = null;
            if (content) {
                try {
                    res = {
                        path: path,
                        url: type === 'base64' ? ('data:image/' + ext + ';base64,' + content) :
                            URL.createObjectURL(new File([content], path, {type: 'image/' + ext}))
                    };
                } catch (error) {
                }
            }
            if (!res) {
                res = {
                    path: path,
                    url: ''
                };
            }
            return res;
        }
    }
    /**
     * 加载zip文件
     *
     * @param {String} url zip文件地址
     * @returns {Promise}
     * @memberof AeAnimation
     */
    private _loadZip(url: string) {
        return new Promise((resolve, reject) => {
            if (url) {
                JSZipUtils.getBinaryContent(url, (err: Error, data: any) => {
                    err ? reject(err) : resolve(data);
                });
            } else {
                reject(new Error('zip路径不能为空'));
            }
        });
    }
}

/**
 * 实例化工厂方法
 *
 * @export
 * @param {any} options 配置选项
 * @returns {AeZipPreset}
 */
export default function <T>(options: T): AeZipPreset<T> {
    return AeZipPreset.instance<AeZipPreset<T>>(options);
}
