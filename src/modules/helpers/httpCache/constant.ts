import { IHttpCacheConfig } from './interface/httpCacheConfig';
import StorageClient from '../../storageClient';

export const HTTP_CACHE_DEFAULT_CONFIG: IHttpCacheConfig = {
    storage: StorageClient({})
};

