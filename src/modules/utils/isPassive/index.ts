import isPassive from '../../../library/util/isPassive';

export default isPassive;
