import { BaseAbstract } from '../../library/basic/base.abstrat';
import { DEFAULT_CONFIG, VIDEO_LISTENER_EVENT_TYPE, VIDEO_STATE } from './constant';
import { ListenerQueue, IListener } from '../../library/helpers/listenerQueue/ListenerQueue';
import { IItersectionConfig, IScrollElementItem } from '../../library/helpers/intersection/interface/intersectionConfig';
import { Intersection } from '../../library/helpers/intersection/Intersection';
import { ScrollIntersection } from '../../library/helpers/intersection/ScrollIntersection';
import { IVideoPlayConfig, IVideoStateType, IStateEvent } from './interface/videoPlayConfig';
import { VideoListener } from './VideoListener';
import callFn from '../../library/util/callFn';
import throttle from '../../library/util/throttle';
/**
 * 视频播放
 * @export
 * @class VideoPlay
 * @extends {IntersectionPattern}
 */
export class VideoPlay extends BaseAbstract<IVideoPlayConfig> {
    public name = 'VideoPlay';
    /**
     * 默认数据
     *
     * @protected
     * @memberof VideoPlay
     */
    protected defaultOption = DEFAULT_CONFIG;
    /**
     * 锁定状态（处理x5内核安卓的兼容）
     *
     * @private
     * @type {boolean}
     * @memberof VideoPlay
     */
    private _videoLocked: boolean = false;
    private _intersection: ListenerQueue<IItersectionConfig, any, IVideoStateType> = this._init();
    /**
     * 当前播放的视频元素
     *
     * @private
     * @type {(IListener<IScrollElementItem|null, IVideoStateType>)}
     * @memberof VideoPlay
     */
    private _playListener?: IListener<IScrollElementItem|null, IVideoStateType>;
    /**
     * 显示的元素
     *
     * @private
     * @type {(IListener<IScrollElementItem|null, IVideoStateType>[])}
     * @memberof VideoPlay
     */
    private _displayList: IListener<IScrollElementItem|null, IVideoStateType>[] = [];
    /**
     * 当前显示的元素
     *
     * @private
     * @type {(IListener<IScrollElementItem|null, IVideoStateType>|null)}
     * @memberof VideoPlay
     */
    private _upDisplay: IListener<IScrollElementItem|null, IVideoStateType>|null = null;
    /**
     * 是否自动播放
     *
     * @private
     * @type {Boolean}
     * @memberof VideoPlay
     */
    private _isAutoplay: Boolean = true;
    /**
     * 播放调用节流
     *
     * @private
     * @type {Function}
     * @memberof VideoPlay
     */
    private _playThrottle: Function = throttle((video, cb) => {
        let _that = this;
        if (_that._playListener && _that._playListener.videoEl === video && video && video.paused) {
            _that.refreshDisplay();
            callFn(video.play, [], video);
            if (video && !video.paused) {
                _that.$emit('play', {
                    el: video
                });
                callFn(cb, [video]);
            }
        }
    }, 100, this);
    /**
     * Creates an instance of VideoPlay.
     * @param {Object} options 数据选项
     * @memberof VideoPlay
     */
    constructor(options: IVideoPlayConfig) {
        super(options);
        let _that = this;
        _that.setDefaultOptions(options);
    }
    private _autoPlay(el: HTMLVideoElement, options?: IVideoPlayConfig) {}
    /**
     * 初始化
     * @private
     * @memberof VideoPlay
     */
    private _init(): ListenerQueue<IItersectionConfig, any> {
        let _that = this;
        let _options = _that.defaultOption;
        let device = _options && _options.device;
        let _intersection: any;
        _that.refreshDisplay = _options.throttleWait && throttle(() => { // 重置显示列表
            _that.resetDisplayItem();
        }, _options.throttleWait, _that) || _that.resetDisplayItem;
        _that._autoPlay = throttle((videoEl, _opts) => { // 自动播放
            if (videoEl && _opts && _opts.isAutoplay && _that._isAutoplay) {
                videoEl.setAttribute('autoplay', 'autoplay');
                _that.play(videoEl);
            }
        }, 100, _that);
        if (_options && _options.intersectionPattern && (!device || device.kernel !== 'x5')) {
            _intersection = new Intersection(_options);
        } else {
            _intersection = new ScrollIntersection(_options);
        }
        _that._init = (): ListenerQueue<IItersectionConfig, any> => {
            return _intersection;
        };
        return _intersection;
    }
    /**
     * 刷新display
     */
    refreshDisplay () {}
    /**
     * 添加数据
     * @param {HTMLImageElement} el 当前图片节点
     * @param {Object} options 数据选项
     * @return {Object}
     */
    push(el: HTMLVideoElement, options: IVideoPlayConfig) {
        return this._queueHandler(el, options);
    }
    /**
     * 更新url
     * @param {HTMLImageElement} el 当前图片节点
     * @param {Object} options 数据选项
     * @memberof VideoPlay
     * @return {Object}
     */
    update(el: HTMLElement, options: IVideoPlayConfig) {
        return this._queueHandler(el, options, true);
    }
    /**
     * 移除节点Listener
     * @param {HTMLElement} el 当前图片节点
     */
    remove(el: HTMLElement) {
        let _that = this;
        let item = _that.getItme(el);
        if (item) {
            _that._intersection.removeItem(item.index);
        }
    }
    /**
     * 播放
     * @param {HTMLVideoElement} video 视频节点
     */
    play(video: HTMLVideoElement) {
        let _that = this;
        if (video) {
            _that._isAutoplay = false;
            let item = _that.getItme(video);
            if (item) {
                _that._playListener = item.listener;
            }
            _that.pause(video);
            _that._playThrottle(video);
        }
    }
    /**
     * 暂停播放
     * @param {HTMLVideoElement} playVideo 当前需要播放的视频
     */
    pause(playVideo?: HTMLVideoElement) {
        let _that = this;
        _that._intersection.listenerQueue.forEach(item => {
            let _el = item.videoEl;
            if (_el) {
                if (_el === playVideo) {
                    return true;
                } else {
                    if (!_el.paused) {
                        _that.refreshDisplay();
                        _that.$emit('pause', {
                            el: _el
                        });
                        callFn(_el.pause, [], _el);
                    }
                }
            }
        });
    }
    /**
     * 重新设置浮动显示项
     */
    resetDisplayItem() {
        let _that = this;
        let _listener = _that._playListener;
        let _displayList = _that._displayList;
        let _isDisplay = false;
        let _isUpdata = false;
        let _options = _that.defaultOption;
        let device = _options && _options.device;
        if (_displayList && _displayList.length) {
            for (let i = 0; i < _displayList.length; i++) {
                const item = _displayList[i];
                if (item === _listener) {
                    _isDisplay = true;
                    break;
                }
            }
        }
        if (_isDisplay) {
            _isUpdata = (_that._upDisplay !== null);
            _that._upDisplay = null;
        } else {
            _isUpdata = (_that._upDisplay !== _listener);
            _that._upDisplay = _listener || null;
        }
        if (_isUpdata) {
            _that._videoLocked = device && device.android && device.kernel === 'x5' || false;
            _that.$emit('display', {
                el: _that._upDisplay && _that._upDisplay.videoEl
            });
        }
    }
    /**
     * 根据el获取Listener
     * @param {HTMLElement} el 元素
     */
    getItme(el: HTMLElement) {
        let _that = this;
        let list = _that._intersection.listenerQueue;
        if (el && list && list.length) {
            for (let i = 0; i < list.length; i++) {
                const item = list[i];
                if (item && item.videoEl === el) {
                    return {
                        listener: item,
                        index: i
                    };
                }
            }
        }
    }
    /**
     * 加入和更新队列
     * @param {HTMLImageElement} el 元素
     * @param {Object} options 选项
     * @param {Boolean} isUpdata 是否为更新
     * @returns {ImgListener}
     * @memberof VideoPlay
     */
    private _queueHandler(el: HTMLElement, options: IVideoPlayConfig, isUpdata?: boolean) {
        let _that = this;
        let _options: IVideoPlayConfig = _that.getOptions(options);
        let cb = () => {
            return _that._initListener(el, _options);
        };
        let _intersection = _that._intersection;
        return isUpdata ? _intersection.updateQueue(el, _options, cb) : _intersection.addQueue(cb);
    }
    /**
     * 初始化VideoListener
     * @param {HTMLImageElement} el 元素
     * @param {Object} options 选项
     */
    private _initListener(el: HTMLElement, options: IVideoPlayConfig): VideoListener {
        let _that = this;
        let _videoEl = options && options.videoEl;
        if (_videoEl) {
            _videoEl.addEventListener('play', (evt) => {
                let videoEl = _that._playListener && _that._playListener.videoEl;
                let _target: any = evt && evt.target;
                _that.refreshDisplay();
                if (_target) {
                    _that.$emit('play', {
                        el: _target
                    });
                    if (_target !== videoEl) {
                        _that.play(_target);
                    }
                }
            });
            _videoEl.addEventListener('pause', (evt) => {
                let _playListener = _that._playListener;
                let isViewDisplay = _playListener && _playListener.viewDisplay && _playListener.viewDisplay();
                if (_that._videoLocked && !isViewDisplay) {
                    _that._videoLocked = false;
                    return;
                }
                let videoEl = _that._playListener && _that._playListener.videoEl;
                let _target: any = evt && evt.target;
                _that.refreshDisplay();
                if (_target && _target === videoEl) {
                    _that._playListener = undefined;
                    _that.$emit('pause', {
                        el: _target
                    });
                    _that.pause();
                }
            });
        }
        let _videoListener = new VideoListener(el, options);
        _videoListener.$on(VIDEO_LISTENER_EVENT_TYPE.STATUS, (evt: IStateEvent) => {
            _that._setDisplayList(evt);
        });
        return _videoListener;
    }
    /**
     * 设置显示的列表
     */
    private _setDisplayList(evt: IStateEvent) {
        let _that = this;
        let _type: any = evt && evt.type;
        let _listener = evt && evt.listener;
        let _displayList = _that._displayList;
        let _options = evt && evt.options;
        switch (_type) {
            case VIDEO_STATE.REFRESH:
                _that._displayList = [];
                break;
            case VIDEO_STATE.DISPLAY:
                let isAdd = true;
                _that._autoPlay(_listener && _listener.videoEl, _options);
                for (let i = 0; i < _displayList.length; i++) {
                    const item = _displayList[i];
                    if (_listener === item) {
                        isAdd = false;
                        break;
                    }
                }
                isAdd && _displayList.push(_listener);
                break;
            case VIDEO_STATE.SHIFT_OUT:
                for (let i = 0; i < _displayList.length; i++) {
                    const item = _displayList[i];
                    if (item === _listener) {
                        _displayList.splice(i, 1);
                    }
                }
                break;
        }
        _that.refreshDisplay();
        if (_that._playListener && evt && evt.listener === _that._playListener) {
            _that.$emit(VIDEO_LISTENER_EVENT_TYPE.STATUS, evt);
        }
    }
}
/**
 * 实例化工厂方法
 * @export
 * @param {IVideoPlayConfig} options 配置选项
 * @returns {VideoPlay}
 */
export default function (options: IVideoPlayConfig): VideoPlay {
    return VideoPlay.singleton<VideoPlay>(options);
}
export {
    IVideoPlayConfig,
    VideoListener,
    IVideoStateType
// tslint:disable-next-line:max-file-line-count
};
