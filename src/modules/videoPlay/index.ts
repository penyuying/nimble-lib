import videoPlayFactory, {
    IVideoPlayConfig,
    VideoPlay,
    VideoListener,
    IVideoStateType
} from './VideoPlay.imp';


export default videoPlayFactory;

export {
    IVideoPlayConfig,
    VideoPlay,
    VideoListener,
    IVideoStateType
};
