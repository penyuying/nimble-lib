// import extend from '../utils/extend';
// import { LOAD_STATE, DEFAULT_CONFIG } from '../constant';
import { ViewRegion } from '../../library/helpers/viewRegion/ViewRegion';
import { IListener } from '../../library/helpers/listenerQueue/ListenerQueue';
import { IScrollElementItem } from '../../library/helpers/intersection/interface/intersectionConfig';
import { IVideoListenerConfig, IStateEvent, IVideoStateType } from './interface/videoPlayConfig';
import { VIDEO_LISTENER_DEFAULT_CONFIG, VIDEO_LISTENER_EVENT_TYPE, VIDEO_STATE } from './constant';

/**
 * 图片加载队列项
 *
 * @export
 * @class Listener
 * @extends {ViewRegion}
 */
export class VideoListener extends ViewRegion<IVideoListenerConfig> implements IListener<IScrollElementItem|null, IVideoStateType> {
    defaultOption = VIDEO_LISTENER_DEFAULT_CONFIG; // 默认数据

    /**
     * 是否删除
     *
     * @memberof VideoListener
     */
    isDel = false;

    /**
     * 状态
     *
     * @memberof VideoListener
     */
    state = VIDEO_STATE.INIT;

    /**
     * 视频节点
     *
     * @memberof VideoListener
     */
    videoEl?: HTMLVideoElement;

    /**
     * 当前对应的滚动条数据
     *
     * @memberof VideoListener
     */
    _$scroll = null;

    /**
     * 老的src
     *
     * @memberof VideoListener
     */
    oldSrc?: string|null = null;
    /**
     * Creates an instance of VideoListener.
     * @param {HTMLElement} el 元素
     * @param {Object} options 选项
     * @memberof VideoListener
     */
    constructor(el: HTMLElement, options: IVideoListenerConfig) {
        // let _options = extend({}, DEFAULT_CONFIG, options);
        super(el, options);
        let _that = this;
        _that.setDefaultOptions(options);
        _that.content = el;
        _that.videoEl = options && options.videoEl;
        /**
         * 默认数据
         */
        // _that._options = _options;

        setTimeout(() => {
            _that._init && _that._init();
            _that.setVideoAttr();
        }, 0);
    }
    /**
     * 设置video自定义属性
     * @param {Object} options 选项
     */
    private setVideoAttr(options?: IVideoListenerConfig) {
        let _that = this;
        let _options: IVideoListenerConfig = _that.getOptions(options || {});
        let videoAttr = _options.videoAttr;
        setTimeout(() => {
            if (videoAttr) {
                for (const key in videoAttr) {
                    if (videoAttr.hasOwnProperty(key)) {
                        const val = videoAttr[key];
                        _that.videoEl && _that.videoEl.setAttribute(key, val);
                    }
                }
            }
        }, 50);
    }
    /**
     * 加载
     *
     * @param {Object} opts 选项
     * @param {Boolean} refresh 刷新
     *
     * @returns {Listener}
     * @memberof Listener
     */
    display(opts?: IVideoListenerConfig, refresh?: boolean) {
        let _that = this;
        if (refresh) {
            _that._stateHandler(VIDEO_STATE.REFRESH);
        }
        _that._stateHandler(VIDEO_STATE.DISPLAY);
        return _that;
    }
    /**
     * 移出视图
     */
    shiftOut() {
        this._stateHandler(VIDEO_STATE.SHIFT_OUT);
    }
    /**
     * 当前在图中的时候加载
     * @param {Object} opts 选项
     * @param {Boolean} refresh 刷新
     *
     * @memberof Listener
     */
    viewDisplay(opts?: IVideoListenerConfig, refresh?: boolean): boolean {
        let _that = this;
        if (refresh) {
            _that._stateHandler(VIDEO_STATE.REFRESH);
        }
        const catIn = _that.checkInView(opts);
        if (!catIn) {
            return catIn;
        }
        _that.display();
        return catIn;
    }

    /**
     * 更新src
     *
     * @param {*} src 更新的src
     * @param {*} options 配置参数
     * @returns {Boolean} 是否有更新
     * @memberof Listener
     */
    update(el: HTMLElement, options: IVideoListenerConfig) {
        let _that = this;
        _that.content = el || _that.content;
        if (options && options.videoEl && (_that.videoEl !== options.videoEl)) {
            _that.videoEl = options && options.videoEl;
            _that.setVideoAttr(options);
        }
    }
    /**
     * 销毁
     */
    destroyed() {
        let _that = this;
        _that._$scroll = null; // 当前对应的滚动条数据
        _that.oldSrc = null; // 老的src
        _that._stateHandler(VIDEO_STATE.DESTROYED);
        _that.content = null;
        _that._events = {};
    }
    /**
     * 设置状态、事件
     *
     * @param {*} state 状态
     * @param {*} evt 事件对像
     * @memberof Listener
     */
    private _stateHandler(state: IVideoStateType, evt?: any) {
        let _that = this;
        let _options = _that.defaultOption;
        // _that._init && _that._init();
        // _that.state = state;
        // switch (state) {
        //     case LOAD_STATE.DESTROYED:
        //     case LOAD_STATE.LOAD_END:
        //         _that.isDel = true;
        //         break;
        //     default:
        //         break;
        // }
        let _res: IStateEvent = Object.assign<any, any>(evt || {}, {
            type: state,
            options: _options,
            el: _that.content,
            listener: _that
        });
        // if (_that.oldSrc) {
        //     _res.oldSrc = _that.oldSrc;
        // }
        _that.$emit(VIDEO_LISTENER_EVENT_TYPE.STATUS, _res);
        // callFn(_options.loading, [_res]);
    }
    /**
     * 初始化事件
     */
    private _init() {
        let _that = this;
        _that._init = () => {};
        _that._stateHandler(VIDEO_STATE.INIT);
    }
}
