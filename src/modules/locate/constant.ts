import jsonp from '../../library/util/jsonp';
import deviceInfo from '../../library/util/browser/deviceInfo';
import lStorage from '../../library/util/lStorage';
import nativeDeviceFilter from '../../library/util/nativeDeviceFilter';
import callFn from '../../library/util/callFn';
import { IKeyValue } from '../../library/helpers/IKeyValue';
import filterLocateData from './helpers/filterLocateData';

import {
    ILocateConfig, IMapType, ILocateType, IEventType, IAppApiType, ILocateDataType, INavigationType
} from './interface/locateConfig';
import diffData from '../../library/util/diffData';

/**
 * 导航类型
 */
export const NAVIGATION_TYPE: IKeyValue<INavigationType> = {
    BAIDU: 'baidu', // 百度
    GD: 'gd' // 高德
};

/**
 * 默认配置信息
 * @exports
 */
export const DEFAULT_CONFIG: ILocateConfig = {
    isNativeNavigation: true, // 默认是否跳转原生导航
    // latitude: null, // 导航的纬度
    // longitude: null, // 导航的经度
    // fromLatitude: null, // 当前纬度
    // fromLongitude: null, // 当前经度
    // fromCity: null, // 当前经度
    // fromProvince: null, // 当前省
    // name: null, // 地理位置名
    // address: null, // 地址
    switchLocate: 0, // 0为用户选择是否使用本地地理位置，1直接使用，2为不使用
    cacheTimeout: 1000 * 60 * 5, // 缓存地理位置时间
    timeout: 1000 * 10, // 获取地理位置超时时间(和产品经理（黄晓蝶）确认过3秒没取到地理位置就不等了，不要地理位置了)
    attempt: 1, // 尝试一次
    attemptFilter: true, // 是否要尝试
    isCommit: true, // 是否提交完成数据
    checkLocate: false, // 是否检查用户保存的位置和当前位置
    isReset: false, // 是否重置地理位置
    device: deviceInfo, // 硬件信息
    isNative: true, // 是否调用原生
    actionWithNative: undefined, // 调用app的方法
    storage: lStorage, // 本地存储
    diffData: diffData,
    // handlerWxApi: null, // 调用微信api
    filterLocateData: filterLocateData, // 过滤定位数据
    jsonp: jsonp,
    successfilter: (locate, _options) => {
        let res: any = locate;
        if (_options && !callFn(_options.filterLocateData, [locate])) {
            res = callFn(_options.filterLocateData, [_options]);
        }
        return res;
    }, // 完成后过滤数据的回调
    diffLocateKeys: ['province', 'latitude', 'longitude', 'city', 'district', 'info', 'address'], // 比较数据的key
    // defaultLocate: { // 获取定位失败时的默认定位信息

    // },
    wxConfig: { // 微信地理位置sdk配置
        type: '3',
        output: 'jsonp',
        key: 'FQBBZ-RC5K4-E4CU3-XMZ43-4QEHZ-EZB5X'
    },
    baiduConfig: { // 百度sdk配置
        v: '2.0',
        ak: 'w742K60CZ4fOaGEQ3n6HeB05'
    },
    gdFromLocateConfig: { // 设置高德转换经纬度的配置
        coordsys: 'baidu',
        output: 'JSON',
        key: 'e1a4e5f544003203fdb8ddb9f0f8062f'
    },
    isToNativeFilter: (options, type) => { // 是否调用app
        let res = nativeDeviceFilter(options, options && options.device, type);
        return res;
    },
    locateToConfig: { // 跳转时的参数
        curTag: '我的位置', // 当前位置标签
        shopTag: '途虎门店', // 店铺名称
        tag: '途虎养车' // 标签名称
    }
    // navigationList: null // 过滤跳转第三方导航app列表
};

/**
 * 第三方地图SDK
 */
export enum MAP_SDK {
    BAIDU_API = 'https://api.map.baidu.com/api', // 获取百度定位的api地址
    WX_API= 'https://apis.map.qq.com/ws/coord/v1/translate', // 获取微信定位的api地址
    GD_FROM_LOCATE= 'https://restapi.amap.com/v3/assistant/coordinate/convert' // 高德转绝经纬度
}

// 调用第三方地图app
export enum MAP_APP_API {
    BAIDU_ANDROID = 'baidumap://map/direction', // 百度安卓
    BAIDU_IOS = 'androidamap://navi', // 百度IOS
    BAIDU_H5 = 'https://api.map.baidu.com/direction', // 百度H5
    // GD_ANDROID: 'androidamap://navi', // 高德安卓导航
    GD_ANDROID = 'amapuri://route/plan/', // 高德安卓路线规划
    // GD_IOS: 'iosamap://navi', // 高德IOS导航
    GD_IOS = 'iosamap://path', // 高德IOS路线规划
    GD_H5 = 'https://uri.amap.com/navigation' // 高德H5
}

/**
 * 地图地理类型
 */
export const MAP_TYPE: IKeyValue<IMapType> = {
    BAIDU: 'baidu', // 百度
    WX: 'wx' // 微信
};

/**
 * 地图地理位置类型
 */
export const LOCATE_TYPE: IKeyValue<ILocateType> = {
    BAIDU: 'baidu', // 百度
    API: 'api', // api
    WX: 'wx' // api
};

/**
 * 获取接口类型
 * @exports
 */
export const LOCATE_DATA_TYPE: IKeyValue<ILocateDataType> = {
    CONVERT_BAIDU: 'convertBaidu', // 转换成百度经纬度
    SELECT_MAP: 'selectMap' // 根据经纬度获取地理位置
};

/**
 * 调用app的api
 */
export const APP_API_TYPE: IKeyValue<IAppApiType> = {
    LOCATE: 'locate', // 定位x
    LOCAL_LOCATE: 'localLocate', // 本地位置信息
    UPDATE_LOCATE: 'updateLocate', // 同步地理位置
    CHECK_THIRD_APP: 'checkThirdApp' // 检查第三方app x
};

/**
 * 事件类型
 */
export const EVENT_TYPE: IKeyValue<IEventType> = {
    CLICK_NAVIGATION: 'clickNavigation', // 点击导航按钮
    SUCCESS: 'success', // 获取数据完成
    TO_MAP: 'toMap', // 跳转到地图页面
    SHOW_NAVIGATION: 'showNavigation', // 显示跳转原生的选项
    CHECK_NAVIGATION: 'checkNavigation', // 选中、关闭跳转原生的选项
    CLOSE_MAP: 'closeMap', // 关闭地图页面
    ERROR: 'error', // 出错
    NAVIGATION: 'navigation' // 跳转导航
};

/**
 * 本地存储
 * @exports
 */
export enum STORAGE_KEY {
    /**
     * 存储用户保存的地址信息
     */
    LOCATE_USER_SELECTED = '_locate_user_selected'
}
