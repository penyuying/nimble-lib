import { ILocateData } from '../interface/locateConfig';
import extend from '../../utils/extend';

/**
 * 设置默认的数据
 *
 * @export
 * @param {*} data 数据
 * @returns {ILocateData|null}
 */
export default function filterLocateData(data?: ILocateData & {
    Province: string,
    City: string,
    District: string
}): ILocateData|null {
    let defKey = ['latitude', 'longitude', 'province', 'city', 'district', 'info', 'address'];
    let res: any = null;
    if (!data) {
        return null;
    }
    let _data = extend(true, {
        province: data.province || data.Province,
        city: data.city || data.City,
        district: data.district || data.District
    }, data);
    let delKeys: string[] = ['District', 'City', 'Province'];
    for (let j = 0; j < delKeys.length; j++) {
        delete _data[delKeys[j]];
    }
    for (let i = 0; i < defKey.length; i++) {
        const key = defKey[i];
        const item = _data[key];
        if (item) {
            res = res || {};
            res[key] = item;
        }
    }
    return res || null;
}
