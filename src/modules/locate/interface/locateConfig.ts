import { IDevice } from '../../../library/util/browser/interface/browser';
import { IStorageLocal } from '../../storageClient/interface/storage';
import { IKeyValue } from '../../../library/helpers/IKeyValue';

/**
 * 默认配置参数
 */
export interface ILocateConfig extends ILocateData {
    /**
     * 地图标记图标大小[长，宽]
     */
    size?: [number, number];
    /**
     * 地图标记图标
     */
    iconUrl?: string;
    /**
     * 是否显示label
     */
    notLabel?: boolean;
    /**
     * 默认是否自动跳转原生导航
     */
    isNativeNavigation?: boolean;
    /**
     * 导航的纬度
     */
    toLatitude?: number|string;
    /**
     * 导航的经度
     */
    toLongitude?: number|string;
    /**
     * 地理位置名
     */
    name?: string;
    /**
     * 0为用户选择是否使用本地地理位置，1直接使用，2为不使用
     */
    switchLocate?: 0|1|2;
    /**
     * 缓存地理位置时间
     */
    cacheTimeout?: number;
    /**
     * 获取地理位置超时时间(和产品经理（黄晓蝶）确认过3秒没取到地理位置就不等了，不要地理位置了)
     */
    timeout?: number;
    /**
     * 是否提交完成数据（保存本地和派发success）
     */
    isCommit?: boolean;
    /**
     * 是否检查用户保存的位置和当前位置
     */
    checkLocate?: boolean;
    /**
     * 是否重置地理位置
     */
    isReset?: boolean;
    /**
     * 定位出错后尝试次数
     */
    attempt?: number;
    /**
     * 尝试时的过滤器回调方法接收三个参数(count: 第几次尝试, options: 初始化时的选项)=> (Promise<Boolean>|Boolean)
     */
    attemptFilter?: Function|Boolean;
    /**
     * 设备信息
     */
    device?: IDevice;
    /**
     * 是否调用原生
     */
    isNative?: boolean;
    /**
     * 本地存储
     */
    storage?: IStorageLocal;
    /**
     * 比较数据的key
     */
    diffLocateKeys?: string[];
    // /**
    //  * 获取定位失败时的默认定位信息
    //  */
    // defaultLocate?: ILocateData;
    /**
     * 微信地理位置sdk配置
     */
    wxConfig?: IKeyValue<string>;
    /**
     * 百度sdk配置
     */
    baiduConfig?: IKeyValue<string>;
    /**
     * 设置高德转换经纬度的配置
     */
    gdFromLocateConfig?: IKeyValue<string>;
    /**
     * 跳转时的参数
     */
    locateToConfig?: ILocateToInfo;
    /**
     * 过滤跳转第三方导航app列表
     */
    navigationList?: INavigationItem[]|((navigationList?: INavigationItem[]) => INavigationItem[]);
    /**
     * jsonp请求
     * @param {RequestInfo} url 请求url
     * @param {{
     *       timeout?: number;
     *       jsonpCallback?: string;
     *   }} options  选项
     * @return {Promise<any>}
     */
    jsonp?(url: RequestInfo, options?: {
        timeout?: number;
        jsonpCallback?: string;
    }): Promise<any>;
    /**
     * 数据对比方法
     * @param {any} defObj 比较对像1
     * @param {any} obj 比较对像2
     * @param {string[]} keys 对比的key
     * @return {boolean}
     */
    diffData?(defObj: any, obj: any, keys?: string[]): boolean;
    /**
     * 消息提示的回调
     * @param {String} msgCont 消息内容
     * @param {any} param 选项
     */
    message?(msgCont: string, param?: {type?: string}): void;
    /**
     * 获取数据
     * @param {ILocateDataType} type 获取数据的类型
     * @param {IKeyValue} params 获取数据的参数
     * @return {Promise<any>}
     */
    getLocateData?(type: ILocateDataType, params?: IKeyValue): Promise<any>;
    /**
     * 过滤定位数据
     * @param {ILocateData} locate 定位数据
     * @return {ILocateData|null}
     */
    filterLocateData?(locate?: ILocateData): ILocateData|null;
    /**
     * 调用微信api
     * @param {jsApi| jsApi[]} apis 调用的微信api
     * @param {Object|Function} wxApiParam 调用参数
     * @param {Boolean} isReset 是否重新初始化微信config
     */
    handlerWxApi?(apis: jsApi| jsApi[], wxApiParam?: object| ((type: jsApi) => object), isReset?: boolean): void;
    /**
     * 调用app的方法
     * @param {IAppApiType} type app的api类型
     * @param {any} options 选项
     * @return {Promise<any>}
     */
    actionWithNative?(type: IAppApiType, options?: any): Promise<any>;
    /**
     * 是否调用app
     * @param {ILocateConfig} options 地理位置默认配置参数
     * @param {IAppApiType} type api类型
     * @return {boolean}
     */
    isToNativeFilter?(options: ILocateConfig, type: IAppApiType): boolean;
    /**
     * 完成后过滤数据的回调
     * @param {ILocateData} locate 定位数据
     * @param {ILocateConfig} options 地理位置默认配置参数
     * @return {ILocateData|null|Promise<ILocateData|null>}
     */
    successfilter?(locate: ILocateData, options?: ILocateConfig): ILocateData|null|Promise<ILocateData|null>;
}

/**
 * 定位详细信息
 */
export interface ILocateData {
    /**
     * 纬度
     */
    latitude?: number|string;
    /**
     * 经度
     */
    longitude?: number|string;
    /**
     * 省
     */
    province?: string;
    /**
     * 市
     */
    city?: string;
    /**
     * 区
     */
    district?: string;
    /**
     * 详细地址
     */
    address?: string;
    /**
     * 详细地址和address一样
     */
    info?: string;
    /**
     * 是否不是当前城市
     */
    noCurrentCity?: boolean;
    /**
     * 当前城市（当选中的不是当前城市的时候把当前城市放入此字段）
     */
    currentCity?: null|ILocateData & {currentCity: undefined; noCurrentCity?: undefined; };
}
/**
 * 原生选择app选项的参数
 */
export interface INavigationItem {
    /**
     * 名称
     */
    text: string;
    /**
     * class名
     */
    className?: string;
    /**
     * 原生app的类型
     */
    type: INavigationType;
}
// 原生app的类型(百度|高德)
export type INavigationType = 'baidu'|'gd';
// 地图类型
export type IMapType = 'baidu'|'wx';
/**
 * 定位的类型
 */
export type ILocateType = 'baidu'|'api'|'wx';
/**
 * 跳转时的参数
 */
export interface ILocateToInfo {
    /**
     * 当前位置标签
     */
    curTag: string;
    /**
     * 店铺名称
     */
    shopTag: string;
    /**
     * 标签名称
     */
    tag: string;
}
// app的api类型(定位x|本地位置信息|同步地理位置|检查第三方app x)
export type IAppApiType = 'locate'|'localLocate'|'updateLocate'|'checkThirdApp';
/**
 * 事件类型(点击导航按钮|获取数据完成|跳转到地图页面|显示跳转原生的选项|选中、关闭跳转原生的选项|关闭地图页面|出错|跳转导航)
 */
export type IEventType = 'clickNavigation'|'success'|'toMap'|'showNavigation'|'checkNavigation'|'closeMap'|'error'|'navigation';
/**
 * 获取接口类型(转换成百度经纬度|根据经纬度获取地理位置)
 */
export type ILocateDataType = 'convertBaidu'|'selectMap';
