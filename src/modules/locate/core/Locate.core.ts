import { IDevice } from './../../../library/util/browser/interface/browser';
import getBackData from '../../../library/util/getBackData';
import callFn from '../../../library/util/callFn';
import { IStorageLocal } from '../../storageClient/interface/storage';
import {
    LocateBase,
    ILocateConfig,
    IEventType,
    INavigationItem,
    IAppApiType,
    ILocateData,
    IMapType,
    ILocateType,
    INavigationType
} from './Locate.base';
import { LOCATE_TYPE, EVENT_TYPE, NAVIGATION_TYPE, APP_API_TYPE, STORAGE_KEY, MAP_TYPE } from '../constant';
import bdLocateFactory from './LocatePart/BdLocate';
import apiLocateFactory from './LocatePart/ApiLocate';
import gdLocateFactory from './LocatePart/GdLocate';
import wxLocateFactory from './LocatePart/WxLocate';
import { LocatePart } from './LocatePart/LocatePart.base';
import extend from '../../utils/extend';

export class LocateCore extends LocateBase {
    private _isShowNavigation = false; // 是否显示了选择列表
    private _getLocatePromise: any = null; // 缓存获取地理位置的promise
    private _isOpenPage: boolean = false; // 是否打开页面
    constructor(options: ILocateConfig) {
        super(options);
        const _that = this;
        _that.setDefaultOptions(options);
    }
    /**
     * 显示导航列表
     *
     * @param {Object} options 地理位置信息
     * @param {Boolean} isNative 是否跳转原生
     * @returns {Promise}
     * @memberof LocateCore
     */
    showNavigation(options: ILocateConfig, isNative?: boolean) {
        let _that = this;
        return new Promise((resolve, reject) => {
            _that.$once(EVENT_TYPE.CHECK_NAVIGATION, (selectItem: INavigationItem) => {
                _that._isShowNavigation = false;
                if (selectItem) {
                    switch (selectItem.type) {
                        case NAVIGATION_TYPE.GD:
                        case NAVIGATION_TYPE.BAIDU:
                            options = options || {};
                            let fromLatitude = options.latitude;
                            let fromLongitude = options.longitude;
                            if (fromLatitude && fromLongitude) {
                                _that._navigation(options, selectItem.type, isNative).then(resolve, reject);
                            } else {
                                _that._getLocate(options).then((from: ILocateData) => {
                                    _that._navigation(_that.getOptions(options || {}, from),
                                        selectItem.type, isNative).then(resolve, reject);
                                }, reject);
                            }
                            break;
                        default:
                            break;
                    }
                }
            });
            if (!_that._isShowNavigation) {
                _that._isShowNavigation = true;
                _that._getNavigationList().then(list => {
                    _that.$emit(EVENT_TYPE.SHOW_NAVIGATION, list);
                }, reject);
            }
        });
    }
    /**
     * 调用Locate方法
     *
     * @param {*} type 类型
     * @param {*} fnName 方法名
     * @param {*} argList 调用方法的参数列表
     * @returns {Promise}
     * @memberof LocateCore
     */
    handlerLocate(type: ILocateType|INavigationType|IMapType, fnName: string, argList: any[]) {
        let _that = this;
        let _locate = _that._locateFactory(type);
        if (fnName === 'navigation') {
            _locate && _locate.$once(EVENT_TYPE.NAVIGATION, (evt: any) => {
                _that.$emit(EVENT_TYPE.NAVIGATION, evt);
            });
        }
        let res: Promise<any|null>;
        if (type === LOCATE_TYPE.WX && fnName === 'getLocate') {
            res = new Promise((resolve, reject) => {
                getBackData(_locate && _locate[fnName] && _locate[fnName].bind(_locate), argList).then((data) => {
                    if (data && (data.latitude || data.latitude + '' === '0') && (data.longitude || data.longitude + '' === '0')) {
                        argList = argList || [];
                        argList[0] = extend(argList[0] || {}, data);
                        _that.handlerLocate(LOCATE_TYPE.API, 'getLocate', argList).then(resolve, reject);
                    } else {
                        resolve(null);
                    }
                }, reject);
            });
        } else {
            res = getBackData(_locate && _locate[fnName] && _locate[fnName].bind(_locate), argList);
        }
        return res;
    }
    /**
     * 定位完成
     * @param {*} locate 地理位置数据
     * @param {*} options 选项
     * @param {*} isNotEmit 是否不派发事件
     * @returns {Promise}
     * @memberof CarCroe
     */
    protected _successfilter(locate: ILocateData|null, options: ILocateConfig, isNotEmit?: boolean): Promise<ILocateData|null> {
        let _that = this;
        let _options = _that.getOptions(options);
        return new Promise((resolve, reject) => {
            getBackData(_options.successfilter || locate, [locate, _options]).then(res => {
                let diffKeys = _options.diffLocateKeys;
                let isChange = false;
                if (diffKeys && diffKeys.length) {
                    isChange = callFn(_options.diffData || false, [res, _that._nowLocate, diffKeys]);
                }
                // 如果放在success事件后面，数据发生变化接口重新接口重新请求，这里候这里还没返回，会造再次请求的接口拿到的还是没地理位置信息
                resolve(res && extend(true, res) || res);
                if (!isNotEmit) {
                    if (!isChange) {
                        _that._saveLocate(res && extend(true, res) || res);
                        _that.$emit(EVENT_TYPE.SUCCESS, res && extend(true, res) || res);
                    }
                    _that._nowLocate = res;
                    _that._cacheLocate = res;
                }
            });
        });
    }
    /**
     * 设置地理位置信息
     * @param {*} locate 地理位置信息
     * @returns {Promise}
     * @protected
     */
    private _saveLocate (locate: ILocateData) {
        const _that = this;
        const _opts = _that.defaultOption;
        return new Promise((resolve, reject) => {
            if (_that._isToNative(APP_API_TYPE.UPDATE_LOCATE)) {
                _that._toNative(APP_API_TYPE.UPDATE_LOCATE, locate).then(resolve, reject);
            } else {
                let _storagePromise = null;
                let storage = _opts.storage || <IStorageLocal>{};
                if (locate) {
                    _storagePromise = getBackData(storage.setLocal, [STORAGE_KEY.LOCATE_USER_SELECTED, locate]);
                } else {
                    _storagePromise = getBackData(storage.removeLocal, [STORAGE_KEY.LOCATE_USER_SELECTED]);
                }
                _storagePromise && _storagePromise.then(resolve, reject);
            }
        });
    }
    /**
     * 获取定位对象
     *
     * @param {NAVIGATION_TYPE|LOCATE_TYPE} type 对象类型
     * @returns {LocateObject}
     * @memberof LocateCore
     */
    private _locateFactory(type: ILocateType|INavigationType|IMapType): LocatePart|null  {
        let res: LocatePart|null = null;
        let options = this.defaultOption;
        switch (type) {
            case LOCATE_TYPE.BAIDU:
            case NAVIGATION_TYPE.BAIDU:
                res = bdLocateFactory(options);
                break;
            case LOCATE_TYPE.API:
                res = apiLocateFactory(options);
                break;
            case NAVIGATION_TYPE.GD:
                res = gdLocateFactory(options);
                break;
            case MAP_TYPE.WX:
                res = wxLocateFactory(options);
                break;
        }
        return res;
    }
    /**
     * 获取本地的地理位置信息
     * @param {Object} options 配置参数
     * @returns {Promise} 返回地理位置信息
     */
    protected _getCurLocate (options: ILocateConfig): Promise<any> {
        const _that = this;
        const _opts = _that.getOptions(options);
        return new Promise((resolve, reject) => {
            if (_that._isToNative(APP_API_TYPE.LOCAL_LOCATE)) {
                let _res = _that._toNative(APP_API_TYPE.LOCAL_LOCATE);
                Promise.race([new Promise((_resolve) => {
                    setTimeout(() => {
                        _resolve();
                    }, 10000);
                }), _res]).then(_success, reject);
            } else {
                getBackData(_opts.storage && _opts.storage.getLocal, [STORAGE_KEY.LOCATE_USER_SELECTED]).then(_success, reject);
            }
            /**
             * 过滤定位数据
             *
             * @param {*} locateInfo
             */
            function _success(locateInfo: any) {
                let res = callFn(_opts.filterLocateData || locateInfo, [locateInfo]);
                resolve(res);
            }
        });
    }
    /**
     * 获取定位
     *
     * @param {*} options 选项
     * @param {LOCATE_TYPE} type 类型
     * @returns {Promise}
     * @memberof LocateCore
     */
    protected _getLocate(options: ILocateConfig, type?: ILocateType): Promise<ILocateData> {
        const _that = this;
        const _options = _that.getOptions(options);
        let device = _options.device || <IDevice>{};
        let res = (!type && !_options.isReset && _that._getLocatePromise) || new Promise((resolve, reject) => {
            let _lng = _options.longitude; // 当前经度
            let _lat = _options.latitude; // 当前纬度
            if (type) {
                return _that.handlerLocate(type, 'getLocate', [options]).then(resolve, reject);
            }
            if ((_lng || _lng + '' === '0') && (_lat || _lat + '' === '0')) {
                _that.handlerLocate(LOCATE_TYPE.API, 'getLocate', [options]).then(resolve, _getLocate);
            } else {
                _getLocate();
            }
            /**
             * 获取定位
             *
             */
            function _getLocate () {
                if (_that._isToNative(APP_API_TYPE.LOCATE)) {
                    _that._toNative(APP_API_TYPE.LOCATE).then((back) => {
                        if ((back.latitude || back.latitude + '' === '0') && (back.longitude || back.longitude + '' === '0')) {
                            resolve(back);
                        } else {
                            _h5GetLocate();
                        }
                    }, _h5GetLocate);
                } else {
                    _h5GetLocate();
                }
            }
            /**
             * h5定位
             */
            function _h5GetLocate() {
                if (device.isWeixin) {
                    _that.handlerLocate(LOCATE_TYPE.WX, 'getLocate', [options]).then((back) => {
                        back ? resolve(back) : bdLocate();
                    }, bdLocate);
                } else {
                    bdLocate();
                }
            }
            /**
             * 百度定位
             */
            function bdLocate() {
                _that.handlerLocate(LOCATE_TYPE.BAIDU, 'getLocate', [options]).then(resolve, err => {
                    _that._getLocatePromise = null;
                    reject(err);
                });
            }
        });
        if (!type) {
            _that._getLocatePromise = res;
        }
        return res;
    }
    /**
     * 显示地图
     * @param {Object} options 地理位置信息
     * @param {*} type 地图类型
     * @returns {Promise}
     * @memberof LocateCore
     */
    protected _showMap(options: ILocateConfig, type: IMapType): Promise<any> {
        let _that = this;
        let res: Promise<any>;
        switch (type) {
            case MAP_TYPE.BAIDU:
                res = _that._showBdMap(options);
                break;
            case MAP_TYPE.WX:
                res = _that.handlerLocate(type, 'showMap', [options]);
                break;
            default:
                res = new Promise((resolve, reject) => {
                    reject(new Error('type:' + type + 'error'));
                });
                break;
        }
        return res;
    }
    /**
     * 唤起百度地图
     * @param {Object} options 地理位置信息
     * @returns {Promise}
     */
    protected _showBdMap(options: ILocateConfig): Promise<any> {
        let _that = this;
        return new Promise((resolve, reject) => {
            let _options = _that.getOptions(options);
            if (!_that._isOpenPage) {
                _that._isOpenPage = true;
                _that.$once(EVENT_TYPE.CLOSE_MAP, () => {
                    _that._isOpenPage = false;
                    _that.$off(EVENT_TYPE.CLICK_NAVIGATION, _navigation);
                    resolve(null);
                });
                _that.$on(EVENT_TYPE.CLICK_NAVIGATION, _navigation);
                _that.$emit(EVENT_TYPE.TO_MAP, _options);
            }
            /**
             * 显示选择导航列表
             *
             * @param {*} res 位置信息
             */
            function _navigation(res: ILocateData) {
                _that.showNavigation(res, !!_options.isNativeNavigation);
            }
        });
    }
    /**
     * 唤起导航
     *
     * @param {Object} options 地理位置信息
     * @param {NAVIGATION_TYPE} type 类型
     * @param {Boolean} isNative 是否跳转原生
     * @returns {Promise}
     * @memberof LocateCore
     */
    protected _navigation(options: ILocateConfig, type: ILocateType|INavigationType, isNative?: boolean) {
        let _that = this;
        return new Promise((resolve, reject) => {
            if (isNative === undefined) {
                isNative = true;
            }

            if (NAVIGATION_TYPE.WX !== type && isNative && _that._isToNative(APP_API_TYPE.CHECK_THIRD_APP)) {
                let _locate = _that._locateFactory(type);
                let _appName = '';
                switch (type) { // 设置提示消息的app名称
                    case NAVIGATION_TYPE.BAIDU:
                        _appName = '百度';
                        break;
                    case NAVIGATION_TYPE.GD:
                        _appName = '高德';
                        break;
                }
                _locate && _locate.getCheckParam(options).then(pkg => {
                    _that._toNative(APP_API_TYPE.CHECK_THIRD_APP, {
                        package: pkg
                    }).then(res => {
                        res = res + '';
                        if (res === '1') {
                            isNative = true;
                        } else if (res === '0') {
                            isNative = false;
                            _that.message('您的手机未安装' + _appName + '地图app', {
                                type: 'toast'
                            });
                        } else if (res === '2') {
                            isNative = false;
                            _that.message(_appName + '地图未通过途虎养车白名单～', {
                                type: 'toast'
                            });
                        }
                        _that.handlerLocate(type, 'navigation', [options, isNative]).then(resolve, reject);
                    });
                });
            } else {
                _that.handlerLocate(type, 'navigation', [options, isNative]).then(resolve, reject);
            }
        });
    }

    /**
     * 切换城市
     * @param {*} locate 当前地理位置
     * @param {*} sLocate 选着的地理位置
     * @param {*} options 可选参数
     * @returns {*}
     */
    protected _switchLocate (locate?: ILocateData|null, sLocate?: ILocateData|null, options?: ILocateConfig): Promise<ILocateData|null> {
        const _that = this;
        const _opts = _that.getOptions(options || <ILocateConfig>{});
        return new Promise((resolve, reject) => {
            if (!sLocate || !locate) {
                _resolveCb(locate || sLocate);
                return;
            }
            if (locate.city && locate.city !== sLocate.city) {
                if (_opts.switchLocate + '' === '2') {
                    _resolveCb(locate);
                } else if (_opts.switchLocate + '' === '1') {
                    _resolveCb(Object.assign({
                        latitude: locate && locate.latitude,
                        longitude: locate && locate.longitude
                    }, sLocate), true, locate);
                } else {
                    _that.message(`你当前城市是${locate.city}，是否切换到当前位置`, {
                        title: '温馨提示',
                        type: 'confirm'
                    }).then(isLocate => {
                        _resolveCb(isLocate ? locate : Object.assign({
                            latitude: locate && locate.latitude,
                            longitude: locate && locate.longitude
                        }, sLocate), !isLocate, !isLocate && locate || null);
                    });
                }
            } else {
                _resolveCb(locate);
            }
            /**
             * resolve回调
             * @param {*} _locate 定位
             * @param {*} sLocate 选着
             */
            function _resolveCb (_locate?: ILocateData|null, noCurrentCity?: boolean, curCity?: any) {
                if (_locate && noCurrentCity) {
                    _locate.noCurrentCity = noCurrentCity;
                    if (curCity) {
                        delete curCity.noCurrentCity;
                        delete curCity.currentCity;
                    }
                    _locate.currentCity = curCity;
                }
                resolve(_locate || null);
            }
        });
    }
}
export {
    LocateBase,
    ILocateConfig,
    IEventType,
    INavigationItem,
    IAppApiType,
    ILocateData,
    IMapType,
    ILocateType,
    INavigationType
// tslint:disable-next-line:max-file-line-count
 };
