import { BaseAbstract } from '../../../../library/basic/base.abstrat';
import { ILocatePart } from '../../interface/locate';
import { ILocateConfig, ILocateData } from '../../interface/locateConfig';
import { DEFAULT_CONFIG } from '../../constant';

export abstract class LocatePart extends BaseAbstract<ILocateConfig> implements ILocatePart {
    protected defaultOption: ILocateConfig = DEFAULT_CONFIG; // 默认选项
    constructor(options: ILocateConfig) {
        super(options);
        this.setDefaultOptions(options);
    }
    /**
     * 唤起微信导航
     * @param {Object} options 地理位置信息
     * @returns {Promise}
     */
    showMap(options: ILocateConfig): Promise<{ type: 'success' | 'cancel'; }> {
        throw new Error('showMap not implemented.');
    }
    /**
     * 获取跳转原生的参数
     *
     * @param {ILocateConfig} options 选项
     * @returns {(Promise<string|null>)}
     * @memberof ILocatePart
     */
    getCheckParam(options: ILocateConfig): Promise<string | null> {
        throw new Error('getCheckParam not implemented.');
    }
    /**
     * 获取定位
     *
     * @param {ILocateConfig} options 选项
     * @returns {Promise<ILocateData>}
     * @memberof ILocatePart
     */
    getLocate(options: ILocateConfig): Promise<ILocateData|null> {
        throw new Error('getLocate not implemented.');
    }
    /**
     * 根据IP获取定位
     *
     * @param {ILocateConfig} options 选项
     * @returns {Promise<ILocateData>}
     * @memberof ILocatePart
     */
    getIpLocate(options: ILocateConfig): Promise<ILocateData> {
        throw new Error('getIpLocate not implemented.');
    }
    /**
     * 导航
     *
     * @param {ILocateConfig} options 选项
     * @param {boolean} isNative 是否调用原生
     * @returns {Promise<any>}
     * @memberof ILocatePart
     */
    navigation(options: ILocateConfig, isNative: boolean): Promise<{ type: 'success'; }> {
        throw new Error('navigation not implemented.');
    }
}
