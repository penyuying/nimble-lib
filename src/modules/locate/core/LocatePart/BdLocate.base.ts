/// <reference path="../../interface/window.d.ts" />
import callFn from '../../../../library/util/callFn';
import genGlobalBackName from '../../../../library/util/genGlobalBackName';
import serializeQueryParams from '../../../../library/util/serializeQueryParams';
import extend from '../../../../library/util/extend';
import { IKeyValue } from '../../../../library/helpers/IKeyValue';
import loadSource from '../../../../library/util/loadSource';
import { DEFAULT_CONFIG, MAP_SDK } from '../../constant';
import { ILocateConfig, ILocateData } from '../../interface/locateConfig';
import { LocatePart } from './LocatePart.base';

export class BdLocateBase extends LocatePart {
    /* eslint-disable no-undef */
    protected defaultOption = DEFAULT_CONFIG; // 默认选项
    private _getBMapPromise: any = null; // 缓存百度sdk
    /* eslint-enable */
    constructor(options: ILocateConfig) {
        super(options);
        this.setDefaultOptions(options);
    }
    /**
     * 调用百度api
     * @param {*} type api类型
     * @param {*} options 选项
     * @returns {Promise}
     * @memberof BdLocate
     * 关于状态码
     * BMAP_STATUS_SUCCESS 检索成功。对应数值“0”。
     * BMAP_STATUS_CITY_LIST 城市列表。对应数值“1”。
     * BMAP_STATUS_UNKNOWN_LOCATION 位置结果未知。对应数值“2”。
     * BMAP_STATUS_UNKNOWN_ROUTE 导航结果未知。对应数值“3”。
     * BMAP_STATUS_INVALID_KEY 非法密钥。对应数值“4”。
     * BMAP_STATUS_INVALID_REQUEST 非法请求。对应数值“5”。
     * BMAP_STATUS_PERMISSION_DENIED 没有权限。对应数值“6”。(自 1.1 新增)
     * BMAP_STATUS_SERVICE_UNAVAILABLE 服务不可用。对应数值“7”。(自 1.1 新增)
     * BMAP_STATUS_TIMEOUT 超时。对应数值“8”。(自 1.1 新增)
     */
    protected handlerApi(type: string, options: ILocateConfig): Promise<ILocateData> {
        let _that = this;
        let _opts = _that.getOptions(options);
        return new Promise((resolve, reject) => {
            _that.getBMap(_opts).then(_bMap => {
                try {
                    let geolocation = new _bMap[type]();
                    if (type === 'LocalCity') {
                        geolocation.get(function(res: any) {
                            res = res || {};
                            let _addr = res || {};
                            let _point = res.center || {};
                            resCb({
                                latitude: _point.lat,
                                longitude: _point.lng,
                                province: _addr.province,
                                city: _addr.name,
                                district: '',
                                address: (_addr.name || '') + (_addr.district || '') + (_addr.street || '') + (_addr.street_number || ''),
                                // 废弃
                                info: (_addr.name || '') + (_addr.district || '') + (_addr.street || '') + (_addr.street_number || '')
                            });
                        });
                    } else {
                        geolocation.getCurrentPosition(function(back: any) {
                            const _status = geolocation.getStatus();
                            if (_status === window.BMAP_STATUS_SUCCESS) {
                                back = back || {};
                                const _addr = back.address || {};
                                const address = (_addr.city || '') + (_addr.district || '') +
                                    (_addr.street || '') + (_addr.street_number || '');
                                resCb({
                                    latitude: back.latitude,
                                    longitude: back.longitude,
                                    province: _addr.province,
                                    city: _addr.city,
                                    district: _addr.district,
                                    address: address,
                                    // 废弃
                                    info: address
                                });
                            }
                        }, {enableHighAccuracy: true});
                    }
                } catch (error) {
                    reject(error);
                }
            }, reject);

            /**
             * 完成的回调
             *
             * @param {*} res 数据
             */
            function resCb(res: ILocateData) {
                resolve(callFn(_opts.filterLocateData, [res]));
            }
        });
    }

    /**
     * 获取百度地图对象
     *
     * @param {*} options 选项
     * @returns {Promise}
     * @memberof BdLocate
     */
    protected getBMap(options: ILocateConfig): Promise<IKeyValue> {
        let _that = this;
        let _options = _that.getOptions(options || {});
        let callbackName = genGlobalBackName('__baidu__');
        let param = serializeQueryParams(extend({}, _options.baiduConfig || {}, {
            callback: callbackName
        }), '?');
        let _src = MAP_SDK.BAIDU_API + param;
        let time = 50; // 尝试的间隔时间
        let attemptTime = 2000; // 尝试等待的总时间
        let timeout = 0; // 已用的时间
        let attempt = 1; // 加载尝试次数
        let cont = 0; // 已尝试的次数
        let lock = false;
        let startTimeout: NodeJS.Timeout; // 开始执行超时处理的定时器
        let res = _that._getBMapPromise || new Promise((resolve, reject) => {
            window.addEventListener('error', loadError, true);
            window[callbackName] = function() {
                seccussCb();
            };
            load(_src, errCb);
            /**
             * 成功的回调
             */
            function seccussCb() {
                if (startTimeout !== undefined) {
                    clearTimeout(startTimeout);
                }
                if (lock) {
                    return;
                }
                let _BMap = window.BMap;
                if ('BMap' in window && _BMap.Geolocation) {
                    lock = true;
                    delete window[callbackName];
                    window.removeEventListener('error', loadError);
                    resolve(_BMap);
                } else if (attemptTime - timeout > 0) {
                    setTimeout(seccussCb, time);
                    timeout += time;
                } else {
                    lock = true;
                    _that._getBMapPromise = null;
                    delete window[callbackName];
                    window.removeEventListener('error', loadError);
                    reject(new Error('BMap error'));
                }
            }
            /**
             * 出错的处理回调
             *
             * @param {*} err 出信息
             */
            function errCb(err: Error) {
                _that._getBMapPromise = null;
                cont = 0;
                window.removeEventListener('error', loadError);
                reject(err);
            }
            /**
             * 加载资源
             *
             * @param {*} src 资源路径
             * @returns {Promise}
             */
            function load(src: string, errCb: Function) {
                return new Promise((resolve, reject) => {
                    loadSource(src, 'js').then(() => {
                        startTimeout = setTimeout(() => {
                            seccussCb();
                        }, 100);
                    }).catch((err) => {
                        if (attempt - cont > 0) {
                            load(src, errCb).catch(reject);
                            cont += 1;
                        } else {
                            callFn(errCb, [err]);
                        }
                    });
                });
            }
            /**
             * 出错后的事件
             *
             * @param {*} evt 事件对象
             */
            function loadError(evt: any) {
                const _target = evt && evt.target;
                const _scriptSrc = _target && _target.src;
                if (((_scriptSrc || '') + '').indexOf('api.map.baidu.com/getscript') > -1) {
                    errCb(new Error('百度地址script加载异常.'));
                }
            }
        });
        _that._getBMapPromise = res;
        return res;
    }
}
