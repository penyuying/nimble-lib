import { IDevice } from '../../../../library/util/browser/interface/browser';
import serializeQueryParams from '../../../../library/util/serializeQueryParams';
import { MAP_APP_API, EVENT_TYPE } from '../../constant';
import { BdLocateBase } from './BdLocate.base';
import { ILocateConfig, ILocateData, ILocateToInfo } from '../../interface/locateConfig';
import { ILocatePart } from '../../interface/locate';

/**
 * 百度定位
 *
 * @export
 * @class BdLocate
 * @extends {BdLocateBase}
 * @implements {ILocatePart}
 */
export class BdLocate extends BdLocateBase implements ILocatePart {
    constructor(options: ILocateConfig) {
        super(options);
        this.setDefaultOptions(options);
    }
    /**
     * 优先调用浏览器H5定位接口，如果失败会调用IP定位
     * @param {*} options 选项
     * @returns {Promise}
     * @memberof BdLocate
     */
    getLocate(options: ILocateConfig): Promise<ILocateData> {
        return this.handlerApi('Geolocation', options);
    }
    /**
     * 优先调用浏览器H5定位接口，如果失败会调用IP定位
     * @param {*} options 选项
     * @returns {Promise}
     * @memberof BdLocate
     */
    getIpLocate(options: ILocateConfig): Promise<ILocateData> {
        return this.handlerApi('LocalCity', options);
    }

    /**
     * 唤起百度导航
     * @param {Object} options 地理位置信息
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @param {String} options.toLatitude 导航去的纬度
     * @param {String} options.toLongitude 当前经度
     * @param {String} options.city 当前市
     * @param {String} options.province 当前省
     * @param {String} options.locateToConfig 跳转的配置
     * @param {Boolean} isNative 是否跳转原生
     * @returns {Promise}
     */
    navigation (options: ILocateConfig, isNative: boolean): Promise<{
        type: 'success'
    }> {
        let _that = this;
        return new Promise((resolve, reject) => {
            _that._getLink(options, isNative).then(link => {
                if (link && typeof link === 'string') {
                    _that.$emit(EVENT_TYPE.NAVIGATION, {
                        link: link
                    });
                    resolve({
                        type: 'success'
                    });
                    window.location.href = link;
                } else {
                    reject(new Error('link error'));
                }
            }, reject);
        });
    }
    /**
     * 获取查询是否安装百度地图app的参数
     *
     * @param {*} options 选项
     * @returns {Promise}
     * @memberof GdLocate
     */
    getCheckParam (options: ILocateConfig): Promise<string|null> {
        let _that = this;
        let _opts = _that.getOptions(options);
        let device = _opts.device || <any>{};
        return new Promise((resolve, reject) => {
            let res: string|null = null;
            if (device.android) {
                res = 'com.baidu.BaiduMap';
            } else if (device.ios) {
                res = 'baidumap://';
            }
            resolve(res);
        });
    }
    /**
     * 获取链接
     * @param {Function} options 定位信息
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @param {String} options.toLatitude 导航去的纬度
     * @param {String} options.toLongitude 当前经度
     * @param {String} options.city 当前市
     * @param {String} options.province 当前省
     * @param {Boolean} isNative 是否跳转原生
     * @returns {Promise} 返回链接
     */
    private _getLink (options: ILocateConfig, isNative: boolean): Promise<string> {
        const _that = this;
        return new Promise((resolve, reject) => {
            // _that.getLocate(options).then(from => {
            // options = options || {};
            const _opts = _that.getOptions(options);
            const _config = _opts.locateToConfig || <ILocateToInfo>{};
            const _cLat = _opts.latitude || '';
            const _cLng = _opts.longitude || '';
            const _lat = _opts.toLatitude || '';
            const _lng = _opts.toLongitude || '';
            // const _cCity = options.fromCity || '';
            // const _province = options.fromProvince || '';
            let device = _opts.device || <IDevice>{};
            let res;
            let _params;
            let _point = `${_lat},${_lng}`; // 坐标点
            if (isNative) {
                if (device.android) {
                    _params = {
                        origin: `${_cLat},${_cLng}`,
                        destination: _point,
                        mode: 'driving',
                        region: _opts.city || ''
                    };
                    res = MAP_APP_API.BAIDU_ANDROID;
                } else if (device.ios) {
                    _params = {
                        location: _point,
                        type: 'BLK',
                        src: 'ios.baidu.xxx'
                    };
                    res = MAP_APP_API.BAIDU_IOS;
                }
            }
            if (!res) {
                _params = {
                    origin: 'latlng:' + _cLat + ',' + _cLng + '|name:' + (_config.curTag || ''),
                    destination: 'latlng:' + _point + '|name:' + (_config.shopTag || ''),
                    mode: 'driving',
                    region: _opts.province || '',
                    output: 'html',
                    src: _config.tag || ''
                };
                res = MAP_APP_API.BAIDU_H5;
            }
            resolve(res + serializeQueryParams(_params || {}, '?'));
        // }, reject);
        });
    }
}
let _locate: BdLocate;
/**
 * 实例化百度地图
 *
 * @export
 * @param {*} options 选项
 * @returns {BdLocate}
 */
export default function bdLocateFactory(options: ILocateConfig) {
    _locate = _locate || new BdLocate(options);
    return _locate;
}
