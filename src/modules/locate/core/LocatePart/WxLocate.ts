import getBackData from '../../../../library/util/getBackData';
import serializeQueryParams from '../../../../library/util/serializeQueryParams';
import extend from '../../../../library/util/extend';
import callFn from '../../../../library/util/callFn';
import { LocatePart } from './LocatePart.base';
import { MAP_SDK } from '../../constant';
import { ILocateConfig, ILocateData } from '../../interface/locateConfig';

/**
 * 微信定位
 *
 * @export
 * @class WxLocate
 * @extends {LocatePart}
 */
export class WxLocate extends LocatePart {
    constructor(options: ILocateConfig) {
        super(options);
        this.setDefaultOptions(options);
    }
    /**
     * 唤起微信导航
     * @param {Object} options 地理位置信息
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @param {String} options.name 地理位置名
     * @param {String} options.address 地址
     * @returns {Promise}
     */
    showMap(options: ILocateConfig): Promise<{
        type: 'success'|'cancel'
    }> {
        const _that = this;
        return new Promise((resolve, reject) => {
            const _opts = _that.getOptions(options);
            _that._fromLocate(_opts).then((res: any) => {
                res = res || {};
                _that._openLocation(res).then(resolve, reject);
            }, reject);
        });
    }
    /**
     * 调用微信api
     *
     * @param {*} options 其它选项
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @returns {Promise}
     * @memberof WxLocate
     */
    private _openLocation(options: ILocateConfig): Promise<{
        type: 'success'|'cancel'
    }> {
        const _that = this;
        const _opts = _that.getOptions(options);
        return new Promise((resolve, reject) => {
            getBackData(_opts.handlerWxApi, ['openLocation', () => {
                return {
                    latitude: _opts.latitude || 0, // 纬度，浮点数，范围为90 ~ -90
                    longitude: _opts.longitude || 0, // 经度，浮点数，范围为180 ~ -180。
                    name: _opts.name, // 位置名
                    address: _opts.address, // 地址详情说明
                    scale: 28, // 地图缩放级别,整形值,范围从1~28。默认为最大
                    infoUrl: '', // 在查看位置界面底部显示的超链接,可点击跳转
                    success: function () {
                        resolve({
                            type: 'success'
                        });
                    },
                    cancel: function () {
                        resolve({
                            type: 'cancel'
                        });
                    },
                    fail: function (err: Error) {
                        reject(err);
                    }
                };
            }]).catch(reject);
        });
    }
    /**
     * 调用微信api
     *
     * @param {*} options 其它选项
     * @param {String} options.handlerWxApi 调用微信api的方法
     * @returns {Promise}
     * @memberof WxLocate
     */
    getLocate(options: ILocateConfig): Promise<ILocateData|null> {
        const _that = this;
        const _opts = _that.getOptions(options);
        return new Promise((resolve, reject) => {
            if (_opts.handlerWxApi instanceof Function) {
                getBackData(_opts.handlerWxApi, ['getLocation', () => {
                    return {
                        success: function (data: ILocateData) {
                            resolve(data || null);
                        },
                        cancel: function () {
                            resolve(null);
                        },
                        fail: function (err: Error) {
                            reject(err);
                        }
                    };
                }]).catch(reject);
            } else {
                resolve(null);
            }
        });
    }
    /**
     * 转换经纬度
     * @param {Object} options 配置参数
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @returns {Promise}
     * @memberof WxLocate
     */
    private _fromLocate(options: ILocateConfig) {
        let _that = this;
        let _options = _that.getOptions(options) || {};
        const _lat = _options.latitude || '';
        const _lng = _options.longitude || '';
        let param = serializeQueryParams(extend({}, _options.wxConfig || {}, {
            locations: `${_lat},${_lng}`
        }), '?');
        return new Promise((resolve, reject) => {
            callFn(_options.jsonp, [MAP_SDK.WX_API + param]).then((back: any) => {
                let _back = null;
                if (back && (back.status + '' === '0')) {
                    const _coverData = back && back.locations && back.locations[0];
                    _back = (_coverData && {
                        latitude: _coverData.lat, // 纬度，浮点数，范围为90 ~ -90
                        longitude: _coverData.lng
                    }) || null;
                }
                if (_back) {
                    resolve(_back);
                } else {
                    reject(new Error('转换微信地图经纬度出错'));
                }
            }, reject);
        });
    }
}

let _locate: WxLocate;
/**
 * 实例化微信地图
 *
 * @export
 * @param {*} options 选项
 * @returns {WxLocate}
 */
export default function wxLocateFactory(options: ILocateConfig) {
    _locate = _locate || new WxLocate(options);
    return _locate;
}
