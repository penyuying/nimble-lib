
import { IKeyValue } from '../../../library/helpers/IKeyValue';
import getBackData from '../../../library/util/getBackData';
import callFn from '../../../library/util/callFn';
import { BaseAbstract } from '../../../library/basic/base.abstrat';
import { DEFAULT_CONFIG, NAVIGATION_TYPE } from '../constant';
import {
    ILocateConfig, IEventType, INavigationItem, IAppApiType, ILocateData,
    IMapType, ILocateType, INavigationType
} from '../interface/locateConfig';

export class LocateBase extends BaseAbstract<ILocateConfig> {
    /**
     * 默认配置信息
     * @memberof LocateBase
     */
    protected defaultOption = DEFAULT_CONFIG;
    protected isCheckLocate = null; // 记录上次是否要切换地理位置
    protected _nowLocate?: ILocateData|null = null; // 当前的已存在的信息
    protected _cacheLocate?: ILocateData|null = null; // 缓存的信息

    constructor(options: ILocateConfig) {
        super(options);
        const _that = this;
        _that.setDefaultOptions(options);
    }
    /**
     * 初始化数据
     *
     * @param {*} options 选项
     * @memberof LocateBase
     */
    protected _initData(options: ILocateConfig) {
        let _that = this;
        if (options && typeof options.checkLocate === 'undefined') {
            _that.isCheckLocate = null;
        }
        _that._cacheLocate = null;
    }
    /**
     * 地图事件
     *
     * @param {*} type 事件类型
     * @param {*} evt 事件对象
     * @memberof LocateCore
     */
    mapEvent(type: IEventType, evt: any) {
        this.$emit(type, evt);
    }
    /**
     * 提示消息
     *
     * @param {*} msgCont 消息内容
     * @param {*} param 类型|参数
     * @returns {Promise}
     * @memberof CarBase
     */
    message(msgCont: string, param: IKeyValue) {
        let _that = this;
        let _options = _that.defaultOption;
        return getBackData(_options.message, [msgCont, param]);
    }
    /**
     * 调用原生
     * @param {String} type 原生接口名称
     * @param {Object} params 接口请求参数
     * @returns {Promise<Object|String>} 结果数据
     * @memberof LocateBase
     */
    protected _toNative (type: IAppApiType, params?: any) {
        const _that = this;
        const _options = _that.getOptions();
        return getBackData(_options.actionWithNative, [type, params]);
    }
    /**
     * 是否调用原生
     *
     * @param {String} type 调用类型
     * @return {Boolean}
     * @memberof LocateBase
     */
    protected _isToNative(type: IAppApiType) {
        const _that = this;
        const _options = _that.getOptions();
        let res = callFn(_options && _options.isToNativeFilter || false, [_options, type]);
        return res;
    }

    /**
     * 获取跳转原生导航列表
     *
     * @returns {Promise}
     * @memberof LocateBase
     */
    protected _getNavigationList(): Promise<INavigationItem[]> {
        const _that = this;
        const _options = _that.getOptions();
        return new Promise((resolve, reject) => {
            let list = [[{ // 显示的跳转的导航
                text: '高德地图',
                className: 'th_modal-button th_bold',
                type: NAVIGATION_TYPE.GD
            }, {
                text: '百度地图',
                className: 'th_modal-button th_bold',
                type: NAVIGATION_TYPE.BAIDU
            }], [{
                className: 'th_modal-button th_bold',
                text: '取消'
            }]];
            getBackData(_options.navigationList || list, [list]).then(resolve, reject);
        });
    }
}

export {
    ILocateConfig,
    IEventType,
    INavigationItem,
    IAppApiType,
    ILocateData,
    IMapType,
    ILocateType,
    INavigationType
};
