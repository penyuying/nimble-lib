import { ILoadEvent, IImgRendererOptions } from '../interface/lazyLoadImgConfig';
import { IMT_LISTENER_STATE } from '../constant';

/**
 * 图片渲染
 *
 * @param {Object} data 当前加载的数据
 * @param {LOAD_STATE} data.type 事件类型
 * @param {Object} data.options 选项数据
 * @param {String} data.options.src 加载时的图片地址
 * @param {String} data.options.loadingSrc 加载时的图片地址
 * @param {Boolean?} data.options.isSetStyle 是否设置图片style
 * @param {String?} data.options.isWrap 加载时是否加包裹元素
 * @param {String?} data.options.lazyWrapCla 包裹的class
 * @param {String?} data.options.lazyCla 当前加载项的class
 * @param {Number} data.options.width 宽度
 * @param {Number} data.options.height 高度
 * @param {HTMLImageElement} data.el 元素
 * @param {Object?} data.data 事件对象
 * @memberof ImgLazy
 */
export default function imgRenderer(data: ILoadEvent) {
    if (data) {
        let _options = data.options;
        let _el = data.el;
        if (_el && _options) {
            if (data.type === IMT_LISTENER_STATE.DESTROYED) {
                return;
            }
            switch (data.type + '') {
                case IMT_LISTENER_STATE.LOAD_END: // 加载结束
                    _setSrc(_el, _options.src, _options, true);
                    break;
                case IMT_LISTENER_STATE.LOAD_ERROR: // 加载出错
                    _setSrc(_el, _options.errorSrc, _options);
                    break;
                // case IMT_LISTENER_STATE.INIT: // 加载初始化
                default:
                    _setSrc(_el, _options.loadingSrc, _options);
                    break;
            }
        }
    }
    /**
     * 设置图片url
     *
     * @param {HTMLElement} el 元素
     * @param {String} url 图片url
     * @param {Object} options 选项
     * @param {Boolean} isLoadEnd 是否为加载结束
     */
    function _setSrc(el: any, url?: string|null, options?: IImgRendererOptions, isLoadEnd?: boolean) {
        let srcAttr = (options && options.srcAttr || 'src') + '';
        let isBg = /background/i.test(srcAttr);
        if (!el || !url) {
            return;
        }
        if (isBg) {
            el.style.backgroundImage = 'url(' + url + ')';
        } else {
            el[srcAttr] = url;
        }
        if (srcAttr.toLowerCase() !== 'src') {
            el._src = url;
        }
        if (options && options.onlyCut) {
            return;
        }
        if (isLoadEnd) {
            loadEnd(el, options);
        } else {
            eleWrap(el, options);
        }
    }
}

let elem = {};
/**
 * 包裹元素
 *
 * @param {*} el 当前元素
 * @param {Object} options 选项数据
 * @param {string} [tag='div'] 包裹的标签
 */
function eleWrap (el: any, options?: IImgRendererOptions, tag: string = 'div') {
    let _parent = el.parentNode;

    if (!options) {
        return;
    }
    options.lazyCla && el.classList.add(options.lazyCla);

    if (!_parent || el._isWrap) {
        return;
    }
    if (options.isWrap) {
        elem[tag] = elem[tag] || document.createElement(tag);
        let _wEl = elem[tag].cloneNode();
        if (options.width) {
            // _wEl.style.width = (options.width - 2) + 'px';
            _wEl.style.width = options.width + 'px';
        }
        if (options.height) {
            // _wEl.style.height = (options.height - 2) + 'px';
            _wEl.style.height = options.height + 'px';
        }
        el._isWrap = true;
        _parent.replaceChild(_wEl, el);
        _wEl.appendChild(el);
        _parent = _wEl;
    }

    options.lazyWrapCla && _parent && _parent.classList.add(options.lazyWrapCla);
}
/**
 * 加载结束
 *
 * @param {*} el 当前元素
 * @param {Object} options 选项数据
 */
function loadEnd(el: any, options?: IImgRendererOptions) {
    if (!el || !options) {
        return;
    }
    if (options.isSetStyle) {
        el.style.width = options.width + 'px';
        el.style.height = options.height + 'px';
    }
    let _parent: any = el.parentNode;

    options.lazyCla && el.classList && el.classList.remove(options.lazyCla);
    options.lazyWrapCla && _parent && _parent.classList && _parent.classList.remove(options.lazyWrapCla);

    if (!el._isWrap || !_parent) {
        return;
    }
    _parent.parentNode.replaceChild(el, _parent);
    el._isWrap = null;
}
