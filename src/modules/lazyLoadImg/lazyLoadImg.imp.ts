import { ImgListener } from './ImgListener';
import { BaseAbstract } from '../../library/basic/base.abstrat';
import { ILazyLoadImgConfig, ILoadEvent, ILoadStateType } from './interface/lazyLoadImgConfig';
import { DEFAULT_CONFIG, IMG_LISTENER_EVENT_TYPE } from './constant';
import callFn from '../../library/util/callFn';
import { ListenerQueue } from '../../library/helpers/listenerQueue/ListenerQueue';
import { IItersectionConfig } from '../../library/helpers/intersection/interface/intersectionConfig';
import { Intersection } from '../../library/helpers/intersection/Intersection';
import { ScrollIntersection } from '../../library/helpers/intersection/ScrollIntersection';

/**
 * 图片懒加载
 *
 * @export
 * @class LazyLoadImg
 * @extends {IntersectionPattern}
 */
export class LazyLoadImg extends BaseAbstract<ILazyLoadImgConfig> {
    /**
     * 默认数据
     */
    protected defaultOption = DEFAULT_CONFIG;
    name = 'LazyLoadImg';
    private _intersection?: ListenerQueue<IItersectionConfig, any, ILoadStateType>;
    /**
     * Creates an instance of LazyLoadImg.
     * @param {Object} options 数据选项
     * @param {String} options.loadingSrc 加载时的图片地址
     * @param {String?} options.errorSrc 出错后的图片地址
     * @param {Function?} options.imgFilter 图片url过滤器 数据选项
     * @param {Function?} options.imgRenderer 渲染图片的回调
     * @memberof LazyLoadImg
     */
    constructor(options: ILazyLoadImgConfig) {
        super(options);
        let _that = this;
        _that.setDefaultOptions(options);

        _that._intersection = _that._init();
    }
    /**
     * 初始化
     *
     * @private
     * @memberof LazyLoadImg
     */
    private _init(): ListenerQueue<IItersectionConfig, any> {
        let _that = this;
        let _options = _that.defaultOption;
        let _intersection: any;
        if (_options && _options.intersectionPattern) {
            _intersection = new Intersection(_options);
        } else {
            _intersection = new ScrollIntersection(_options);
        }
        _that._init = (): ListenerQueue<IItersectionConfig, any> => {
            return _intersection;
        };
        return _intersection;
    }

    /**
     * 添加数据
     * @param {HTMLImageElement} el 当前图片节点
     * @param {Object} options 数据选项
     * @param {String} options.loadingSrc 加载时的图片地址
     * @param {String?} options.errorSrc 出错后的图片地址
     * @param {String?} options.imgFilter 图片url过滤器 数据选项
     * @memberof LazyLoadImg
     * @return {Object}
     */
    push(el: HTMLElement, options: ILazyLoadImgConfig) {
        return this._queueHandler(el, options);
    }

    /**
     * 更新url
     *
     * @param {HTMLImageElement} el 当前图片节点
     * @param {Object} options 数据选项
     * @param {String} options.loadingSrc 加载时的图片地址
     * @param {String?} options.errorSrc 出错后的图片地址
     * @param {String?} options.imgFilter 图片url过滤器 数据选项
     * @memberof LazyLoadImg
     * @return {Object}
     */
    update(el: HTMLElement, options: ILazyLoadImgConfig) {
        return this._queueHandler(el, options, true);
    }
    /**
     * 移除节点Listener
     * @param {HTMLElement} el 当前图片节点
     */
    remove(el: HTMLElement) {
        let _that = this;
        let _intersection = _that._intersection;
        if (_intersection) {
            let item = _intersection.getListener(el);
            item && _intersection.removeItem(item.index);
        }
    }

    /**
     * 加入和更新队列
     *
     * @param {HTMLImageElement} el 元素
     * @param {Object} options 选项
     * @param {Boolean} isUpdata 是否为更新
     * @returns {ImgListener}
     * @memberof LazyLoadImg
     */
    private _queueHandler(el: HTMLElement, options: ILazyLoadImgConfig, isUpdata?: boolean) {
        let _that = this;
        let _options: ILazyLoadImgConfig = _that._urlFormatter(options);
        let cb = () => {
            let _imgListener = new ImgListener(el, _options);
            _imgListener.$on(IMG_LISTENER_EVENT_TYPE.LOADING, (evt: ILoadEvent) => {
                callFn(_options && _options.imgRenderer, [evt], _options);
            });
            return _imgListener;
        };
        let _intersection = _that._intersection;
        return _intersection && (isUpdata ? _intersection.updateQueue(el, _options, cb) : _intersection.addQueue(cb));
    }

    /**
     * 图片url格式化
     * @param {Object} options 选项
     * @return {Promise}
     */
    private _urlFormatter(options: ILazyLoadImgConfig) {
        let _that = this;
        for (const key in options) {
            if (options.hasOwnProperty(key)) {
                // if (options[key] === null || options[key] === undefined) {
                // 有些参数有可能会传null如：lazyWrapCla
                if (options[key] === undefined) {
                    delete options[key];
                }
            }
        }
        let _options = _that.getOptions(options);
        if (!_options.src) {
            console.error('imgLazy 传入参数不包含src', options);
        }
        if (!_options.errorSrc && _options.loadingSrc) {
            _options.errorSrc = _options.loadingSrc;
        }
        if (_options.onlyCut || !_options.loadingSrc) {
            _options.loadingSrc = _options.src;
        }
        // if (_options.imgFilter instanceof Function) {
        //     let _res = _options.imgFilter(_options);
        //     if (typeof _res !== 'undefined') {
        //         _options = _res;
        //     } else {
        //         console.error('imgLazy imgFilter必须有返回值');
        //     }
        // }
        let _imgFilter = _options.imgFilter;
        if (_imgFilter) {
            let _res = callFn(_imgFilter, [_options], _options);
            if (typeof _res !== 'undefined') {
                _options = _res;
            } else {
                console.error('imgLazy imgFilter必须有返回值');
            }
        }
        return _options;
    }
}


/**
 * 实例化工厂方法
 *
 * @export
 * @param {ILazyLoadImgConfig} options 配置选项
 * @returns {LazyLoadImg}
 */
export default function (options: ILazyLoadImgConfig): LazyLoadImg {

    return LazyLoadImg.instance<LazyLoadImg>(options);
}

export {
    ILazyLoadImgConfig,
    ILoadEvent
};
