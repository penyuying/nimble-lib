import lazyLoadImgFactory, {
    ILazyLoadImgConfig,
    ILoadEvent,
    LazyLoadImg
} from './lazyLoadImg.imp';


export default lazyLoadImgFactory;

export {
    ILazyLoadImgConfig,
    ILoadEvent,
    LazyLoadImg
};
