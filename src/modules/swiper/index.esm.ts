import swiperProxy from './swiperProxy';

/**
 * 实例化工厂方法
 *
 * @export
 * @param {IAeAnimationConfig} options 配置选项
 * @returns {AeAnimation}
 */
export default function (el: string | HTMLElement, options?: any)  {
    return swiperProxy(() => {
        return new Promise((resolve, reject) => {
            import(/* webpackChunkName: "_swiper_" */ './swiper.imp').then(back => {
                let swiperFactory = back && back.default;
                let _res = swiperFactory(el, options);
                resolve(_res);
            }, reject);
        });
    });
}
