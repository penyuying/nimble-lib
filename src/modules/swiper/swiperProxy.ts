import {ClassProxy} from '../../library/util/ClassProxy';

/**
 * 实例化工厂方法
 *
 * @export
 * @param {IAeAnimationConfig} options 配置选项
 * @returns {AeAnimation}
 */
export default function (cb: Function)  {
    let _proxy: any = new ClassProxy();
    let res: any = {
        preload() {
            let _that = this;
            let initRes = _proxy.initProxy(() => {
                return cb();
            }, 'preload')();
            _that.preload = () => {
                return initRes;
            };
            return initRes;
        }
    };
    _proxy.proxyHook(res, () => {
        return res.preload();
    }, [
        'slideNext',
        'slidePrev',
        'slideTo',
        'slideToLoop',
        'destroy',
        'getTranslate',
        'setTranslate',
        'updateSize',
        'updateSlides',
        'updateProgress',
        'updateSlidesClasses',
        'update',
        'detachEvents',
        'attachEvents',
        'appendSlide',
        'prependSlide',
        'addSlide',
        'removeSlide',
        'removeAllSlides',
        'setGrabCursor',
        'unsetGrabCursor',
        'updateAutoHeight',
        'slideToClosest'
    ], [
        'on',
        'once',
        'off'
    ]);
    return res;
}
