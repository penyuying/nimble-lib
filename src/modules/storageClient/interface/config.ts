import { IBaseInterface } from '../../../library/basic/base.interface';

/**
 * 分享的默认配置
 *
 * @export
 * @interface IStorageInitConfig
 * @extends {IBaseInterface}
 */
export interface IStorageInitConfig extends IStorageHandleOptions, IBaseInterface {

}
/**
 * 操作Storage的选项
 *
 * @export
 * @interface IStorageHandleOptions
 */
export interface IStorageHandleOptions {
    /**
     * 是否记录当前更新访问时间
     */
    isSetTime?: boolean;
    /**
     * 超时时间
     */
    timeout?: number;
}

/**
 * 操作MemoryStorage的选项
 *
 * @export
 * @interface IMemoryStorageHandleOptions
 * @extends {IStorageHandleOptions}
 */
export interface IMemoryStorageHandleOptions extends IStorageHandleOptions {
    /**
     * 创建MemoryStorage分类的key
     */
    storageKey?: string;
}

/**
 * 缓存的数据对象
 */
export interface IStorageItem {
    /**
     * 数据的大小
     */
    size: number;
    /**
     * 储存的数据
     */
    _key_?: any;
    /**
     * 读取时间
     */
    atime?: number;
    /**
     * 写入时间
     */
    ctime?: number;
    /**
     * 超时时间
     */
    timeout?: number;
}
