/**
 * 内存存储
 *
 * @export
 * @class MemoryStorage
 * @implements {Storage}
 */
export class MemoryStorage implements Storage {
    /**
     * 缓存数据
     */
    static _paramCache: any;
    /**
     * 缓存数据
     *
     * @protected
     * @memberof MemoryStorage
     */
    protected _paramCache = {};

    /**
     * 已存数据的个数
     *
     * @readonly
     * @memberof MemoryStorage
     */
    get length(): number {
        let keys = Object.keys(this._paramCache['_' + this._key + '_']);
        return keys && keys.length || 0;
    }
    /**
     * 内存储存数据
     * @param {*} key Storage的key
     * @memberof MemoryStorage
     */
    constructor(private _key: string) {
        this._paramCache = MemoryStorage._paramCache = MemoryStorage._paramCache || {};
        this._paramCache['_' + this._key + '_'] = this._paramCache['_' + this._key + '_'] || {};
    }
    /**
     * 获取保存的key
     *
     * @param {number} [index=0]
     * @returns
     * @memberof MemoryStorage
     */
    key(index: number = 0): string | null {
        let keys = Object.keys(this._paramCache['_' + this._key + '_']);
        return keys && keys[index];
    }

    /**
     * 储存数据
     * @param {String} key 缓存的key名
     * @param {*} value 值
     */
    setItem(key: string, value: any): void {
        if (key) {
            this._paramCache['_' + this._key + '_']['_' + key] = value;
        }
    }

    /**
     * 获取登录时的报文体
     * @param {String} key 缓存的key名
     * @returns {type}
     */
    getItem(key: string): any | null {
        let res;
        if (key) {
            res = this._paramCache['_' + this._key + '_']['_' + key];
        }
        return res;
    }

    /**
     * 移除指定Key的数据
     * @param {type} key 缓存的key名
     */
    removeItem(key: string): void {
        if (key) {
            this._paramCache['_' + this._key + '_']['_' + key] = undefined;
            delete this._paramCache['_' + this._key + '_']['_' + key];
        }
    }

    /**
     * 清除所有数据
     */
    clear(): void {
        this._paramCache['_' + this._key + '_'] = {};
    }
}
