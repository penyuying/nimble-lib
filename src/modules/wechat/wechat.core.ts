import { IWeChatInterface, IWeChatContructor } from './interface/wechat';
import { IWeChatInitConfig, ISign, ISignDataConfig, IShareOptions, IShareBack } from './interface/config';
import deviceInfo from '../../library/util/browser/deviceInfo';
import { BaseAbstract } from '../../library/basic/base.abstrat';
// import isType from '../../library/util/isType';
import callFn from '../../library/util/callFn';
import getBackData from '../utils/getBackData';

export abstract class WeChatCore

    extends BaseAbstract<IWeChatInitConfig>

    implements IWeChatInterface<IWeChatInitConfig, ICheckJsApiBackParams> {

    private wxConfigData?: Promise<ICheckJsApiBackParams>;
    /**
     * 正在发送的请求
     *
     * @private
     * @type {Promise<ICheckJsApiBackParams>}
     * @memberof WeChatCore
     */
    private _tempConfigPromise?: Promise<ICheckJsApiBackParams>|null;
    /**
     * 初始化时候的url
     */
    private _initUrl?: string;
    /**
     * 默认参数
     *
     * @protected
     * @type {IWeChatInitConfig}
     * @memberof WeChatCore
     */
    protected defaultOption: IWeChatInitConfig = {
        attemptTimeout: 500,
        attempt: 6,
        attemptFilter() {
            return true;
        },
        jsApiList: [
            'checkJsApi',
            'hideOptionMenu',
            'showMenuItems',
            'hideAllNonBaseMenuItem'
        ],
        shareJsApiList: [
            'onMenuShareTimeline',
            'onMenuShareAppMessage',
            'updateAppMessageShareData', // 自定义“分享给朋友”及“分享到QQ”按钮的分享内容（1.4.0）
            'updateTimelineShareData' // 自定义“分享到朋友圈”及“分享到QQ空间”按钮的分享内容（1.4.0）
        ]
    };
    /**
     * constructor
     *
     * @param {IWeChatInitConfig} options
     * @memberof WeChatCore
     */
    constructor(config: IWeChatInitConfig) {
        super(config);
        this.setDefaultOptions(config);
    }
    /**
     * init wx config
     *
     * @returns {Promise<ICheckJsApiBackParams>}
     * @memberof WeChatCore
     */
    private wxConfig(options?: IWeChatInitConfig, attempt?: number): Promise<ICheckJsApiBackParams> {
        let self = this;
        let config = self.getOptions(options || {});
        let _attempt: number = attempt || 0;

        let _jsApiList: jsApi[] = config.jsApiList || [];
        let _shareJsApiList: jsApi[] = config.shareJsApiList || [];
        let jsApiList = Array.from(new Set(_jsApiList.concat(_shareJsApiList)));
        let isSuccess = false;

        return new Promise<ICheckJsApiBackParams>((resolve, reject) => {
            self._initUrl = location.href;
            if (jsApiList && jsApiList.length > 0) {
                self.getSign(config)
                    .then((data: ISign) => {
                        if (typeof wx !== 'undefined') {
                            wx.config({
                                debug: config.debug || false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                                appId: data.appid, // 必填，公众号的唯一标识
                                timestamp: data.timestamp, // 必填，生成签名的时间戳
                                nonceStr: data.nonceStr, // 必填，生成签名的随机串
                                signature: data.signature, // 必填，签名，见附录1
                                jsApiList: jsApiList // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
                            });
                            wx.ready(() => {
                                wx.checkJsApi({
                                    jsApiList: config.jsApiList || [],
                                    success: function (res) {
                                        isSuccess = true;
                                        resolve(res);
                                    }
                                });
                            });
                            wx.error((back) => {
                                let attempt = config && config.attempt || 0;
                                if (!isSuccess && (attempt && (attempt - 1) > _attempt)) {
                                    getBackData(config.attemptFilter, [{
                                        config: config,
                                        count: _attempt + 1,
                                        err: back
                                    }], false, config).then(res => {
                                        if (res === false) {
                                            reject(back);
                                        } else {
                                            setTimeout(() => {
                                                let _res = {};
                                                if (res instanceof Object) {
                                                    _res = res;
                                                }
                                                self.wxConfig(_res, _attempt + 1).then(resolve, reject);
                                            }, config.attemptTimeout || 0);
                                        }
                                    });
                                } else {
                                    reject(back);
                                }
                            });
                        } else {
                            console.error('请先引入jweixin.js文件');
                        }
                    }, reject);
            }
        });
    }
    /**
     * get sign
     *
     * @param {ISignDataConfig} config
     * @returns {Promise<ISign>}
     * @memberof WeChatCore
     */
    private getSign(config: ISignDataConfig): Promise<ISign> {
        return new Promise<ISign>((resolve, reject) => {
            getBackData(config && config.signData).then((data: any) => {
                if (data && typeof data === 'string') {
                    try {
                        data = JSON.parse(data);
                    } catch (error) {
                    }
                }
                resolve(data);
            }, reject);
        });
    }
    /**
     * init config
     *
     * @private
     * @param {IWxConfig} [options]
     * @param {boolean} [isReset=false]
     * @memberof WeChatCore
     */
    protected initConfig(isReset: boolean = false): Promise<ICheckJsApiBackParams> {
        let self = this;
        let res = self._tempConfigPromise || (!isReset && self._initUrl === location.href && self.wxConfigData);
        res = res || new Promise<ICheckJsApiBackParams>((resolve, reject) => {
            if (deviceInfo.weixin) {
                self.wxConfig().then(resolve, reject);
            } else {
                reject(new Error('not is weixin'));
            }
        });
        self.wxConfigData = res;
        self._tempConfigPromise = res;
        res.then(() => {
            self._tempConfigPromise = null;
        });
        return res;
    }

    /**
     * set wx share
     *
     * @param {IShareOptions} options 分享选项
     * @param {jsApi|jsApi[]} type jsApi 分享的jsapi
     * @param {IShareBack} resolve resolve 分享完成的回调
     */
    protected setShare(options: IShareOptions, type: jsApi|jsApi[], resolve?: IShareBack) {
        // const link = (options.link || '').replace(/\{/g, '%7B').replace(/\}/g, '%7D');
        const link = (options.link || '').replace(/(\{)|(\})/g, ($0, $1, $2) => {
            let res = '';
            if ($1) {
                res = '%7B';
            } else if ($2) {
                res = '%7D';
            }
            return res;
        });

        this.handlerWxApi(type, _type => { // 调用微信api
            return {
                title: options.title,
                desc: options.desc,
                link: link,
                imgUrl: options.imgUrl,
                success: function () {
                    callFn(resolve, [{
                        type: _type
                    }]);
                },
                cancel: function () {
                    callFn(resolve, [{
                        type: 'cancel'
                    }]);
                },
                fail: function () {
                    callFn(resolve, [{
                        type: 'fail'
                    }]);
                },
            };
        }, options && options.isReset);
    }
    /**
     * 调用微信api
     *
     * @param {(jsApi| jsApi[])} apis 微信api或微信api列表
     * @param {object} [wxApiParam] api需要的参数
     * @param {boolean} [isReset=false] 是否重新初始化config
     * @memberof WeChatCore
     */
    public handlerWxApi(apis: jsApi| jsApi[], wxApiParam?: object| ((type: jsApi) => object), isReset: boolean = false) {
        let self = this;
        return self.initConfig(isReset)
            .then(() => {
                eachApi(apis, (type: jsApi) => {
                    if (type && wx[type] instanceof Function) {
                        let res = callFn(wxApiParam, [type]) || {};
                        if (!(res instanceof Object)) {
                            res = {};
                        }
                        let success = res.success;
                        let cancel = res.cancel;
                        let fail = res.fail;
                        res.success = function(...args: any[]) {
                            callFn(success, args, res);
                            self.$emit('success', {type: type});
                        };
                        res.cancel = function(...args: any[]) {
                            callFn(cancel, args, res);
                            self.$emit('cancel', {type: type});
                        };
                        res.fail = function(...args: any[]) {
                            callFn(fail, args, res);
                            self.$emit('fail', {type: type});
                        };
                        callFn(wx[type], [res], wx);
                    }
                });
            });

        /**
         * 遍历api
         *
         * @param {(jsApi|jsApi[])} api 微信api或微信api列表
         * @param {Function} cb 回调
         */
        function eachApi(api: jsApi|jsApi[], cb: Function) {
            if (api && typeof api === 'string') {

                api = [api];
            }

            if (api instanceof Array) {
                api.forEach(item => {
                    item && callFn(cb, [item]);
                });
            }
        }
    }
    /**
     * 分享
     *
     * @param {IShareOptions} options 分享参数
     * @param {IShareBack} resolve 分享完成后的回调
     * @returns
     * @memberof Weixin
     */
    abstract share(options: IShareOptions, resolve: IShareBack): void;
}

export {
    ISign,
    IWeChatInitConfig,
    ISignDataConfig,
    IShareOptions,
    IShareBack,
    IWeChatContructor
};
