
interface IWeixinShareParams {
    /**
     * 分享显示的标题
     */
    title?: string;
    /**
     * 分享显示的图标
     */
    imgUrl?: string;
    /**
     * 分享显示的描述
     */
    desc?: string;
    /**
     * 链接地址
     */
    link?: string;
    trigger?: ICallback;
    complete?: ICallback;
    /**
     * 分享成功
     */
    success?: ICallback;
    /**
     * 取消分享
     */
    cancel?: ICallback;
    /**
     * 失败的回调
     */
    fail?: ICallback;
    // callback?: IShareCallback; // 分享完成后的回调
}

interface ICallback {
    // tslint:disable-next-line:callable-types
    (res: any): void;
}

interface IWxConfig {
    /**
     * 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
     */
    debug?: boolean;
    /**
     * 需要使用的JS接口列表，所有JS接口列表见附录2
     */
    jsApiList?: jsApi[];
}

/**
 * 初始化配置
 */
interface IConfingOption extends IWxConfig{
    /**
     * 公众号appID公众号的唯一标识
     */
    appId: string;
    /**
     * 时间戳
     */
    timestamp: string;
    /**
     * 生成签名的随机串
     */
    nonceStr: string;
    /**
     * 签名
     */
    signature: string;
}

/**
 * 支付完成时的回调
 */
interface IpaySuccess{
    (res:any):void;
}

interface IchooseWXPayConfig{
    /**
     * 时间戳
     */
    timestamp: string;
    /**
     * 生成签名的随机串
     */
    nonceStr: string;
    /**
     * prepay_id
     */
    package: string;
    /**
     * 加密类型MD5
     */
    signType: 'MD5';
    /**
     * 支付签名
     */
    paySign: string;
    /**
     * 支付完成时的回调
     */
    success:IpaySuccess
}

/**
 * 初始化成功时的回调
 */
interface IReady{
    ():void;
}

/**
 * 初始化出错的回调
 */
interface Ierror{
    (err:any):void
}

/**
 * debug时候的验证结果，如： {"checkResult":{"chooseImage":true},"errMsg":"checkJsApi:ok"}
 */
interface ICheckJsApiBackParams {
    /**
     * 以键值对的形式返回，可用的api值true，不可用为false
     */
    checkResult: {
        [key: string]: boolean;
    },
    /**
     * 接口状态
     */
    errMsg: string;
}
/**
 * 检测js接口的参数
 */
interface ICheckJsApi {
    /**
     * 需要检测的JS接口列表，所有JS接口列表见附录2,
     * @memberof ICheckJsApi
     */
    jsApiList: string[];
    /**
     * 完成的回调，以键值对的形式返回，可用的api值true，不可用为false
     *
     * @memberof ICheckJsApi
     */
    success: (res: ICheckJsApiBackParams) => void;
}

/**
 * 微信JDK的命名空间
 * @preferred
 */
declare namespace wx{
    /**
     * 判断当前客户端版本是否支持指定JS接口
     *
     * @param {ICheckJsApi} confing
     */
    function checkJsApi(confing:ICheckJsApi):void;
    /**
     * 配置
     * @param confing 配置参数
     */
    function config(confing:IConfingOption):void;

    /**
     * 配置成功的回调
     *
     * @param {IReady} callback
     */
    function ready(callback:IReady):void;
    /**
     * 配置出错的回调
     *
     * @param {IReady} callback
     */
    function error(callback:Ierror):void;
    /**
     * 微信支付
     *
     * @param {IchooseWXPayConfig} confing 配置参数
     */
    function chooseWXPay(confing:IchooseWXPayConfig):void;
    /**
     * 分享给朋友
     *
     * @param {IWeixinShareParams} option  分享参数
     */
    function onMenuShareAppMessage(option:IWeixinShareParams):void;
    /**
     * 分享到朋友圈
     * @param {IWeixinShareParams} option  分享参数
     */
    function onMenuShareTimeline(option:IWeixinShareParams):void;
}
/**
 * the shart api from wx
 */
// declare enum jsApi {

//     addCard = 'addCard',

//     checkJsApi = 'checkJsApi',
//     chooseCard = 'chooseCard',
//     chooseImage = 'chooseImage',
//     chooseWXPay = 'chooseWXPay',
//     closeWindow = 'closeWindow',

//     downloadImage = 'downloadImage',
//     downloadVoice = 'downloadVoice',

//     getLocation = 'getLocation',
//     getNetworkType = 'getNetworkType',

//     hideAllNonBaseMenuItem = 'hideAllNonBaseMenuItem',
//     hideMenuItems = 'hideMenuItems',
//     hideOptionMenu = 'hideOptionMenu',

//     showAllNonBaseMenuItem = 'showAllNonBaseMenuItem',
//     showMenuItems = 'showMenuItems',
//     showOptionMenu = 'showOptionMenu',
//     stopVoice = 'stopVoice',

//     openCard = 'openCard',
//     openLocation = 'openLocation',
//     openProductSpecificView = 'openProductSpecificView',
//     onMenuShareTimeline = 'onMenuShareTimeline',
//     onMenuShareAppMessage = 'onMenuShareAppMessage',
//     onMenuShareQQ = 'onMenuShareQQ',
//     onMenuShareWeibo = 'onMenuShareWeibo',
//     onRecordEnd = 'onRecordEnd',

//     pauseVoice = 'pauseVoice',
//     playVoice = 'playVoice',
//     previewImage = 'previewImage',

//     scanQRCode = 'scanQRCode',
//     startRecord = 'startRecord',
//     stopRecord = 'stopRecord',

//     translateVoice = 'translateVoice',

//     uploadImage = 'uploadImage',
//     uploadVoice = 'uploadVoice',
// }
/**
 * 分享的jsApi
 */
declare type jsApi = 'checkJsApi'|
            'onMenuShareTimeline'|
            'onMenuShareAppMessage'|
            'updateAppMessageShareData'|
            'updateTimelineShareData'|
            'onMenuShareQQ'|
            'onMenuShareWeibo'|
            'hideMenuItems'|
            'showMenuItems'|
            'hideAllNonBaseMenuItem'|
            'showAllNonBaseMenuItem'|
            'translateVoice'|
            'startRecord'|
            'stopRecord'|
            'onRecordEnd'|
            'playVoice'|
            'pauseVoice'|
            'stopVoice'|
            'uploadVoice'|
            'downloadVoice'|
            'chooseImage'|
            'previewImage'|
            'uploadImage'|
            'downloadImage'|
            'getNetworkType'|
            'openLocation'|
            'getLocation'|
            'hideOptionMenu'|
            'showOptionMenu'|
            'closeWindow'|
            'scanQRCode'|
            'chooseWXPay'|
            'openProductSpecificView'|
            'addCard'|
            'chooseCard'|
            'openCard';
