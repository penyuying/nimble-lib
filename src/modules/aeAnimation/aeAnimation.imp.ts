///<reference path="./interface/ae.d.ts" />
import lottie from 'lottie-web';
import extend from '../../library/util/extend';
import { DEFAULT_CONFIG } from './constant';
import {
    IAeAnimationConfig, ILottieConfig, IAeAnimation
} from './interface/aeAnimationConfig';
import { IKeyValue } from '../../library/helpers/IKeyValue';
import getBackData from '../../library/util/getBackData';
import getFileExt from '../../library/util/getFileExt';
import { AeAnimationPreset } from './aeAnimation.preset';
import { ILottieFile, ILottieOtherFile, ILottieFileJsons, IPreset } from './interface/aePreset';
import { IFactoryUse } from '../../library/helpers/IFactoryUse';
let zipFilesMap: IKeyValue;
let promiseCache: IKeyValue;
/**
 * AE动画
 * @class AeAnimation
 */
export class AeAnimation extends AeAnimationPreset<IAeAnimationConfig> implements IAeAnimation {
    public name = 'AeAnimation';
    protected defaultOption: IAeAnimationConfig = DEFAULT_CONFIG;
    // private _files = {};
    constructor(options: IAeAnimationConfig) {
        super(options);
        this.setDefaultOptions(options);
    }
    /**
     * 初始化ae动画
     * @param {string} src 动画文件地址
     * @param {HTMLElement|ILottieConfig} options 选项或el
     * @returns {Promise}
     * @memberof AeAnimation
     */
    load(src: string, options: HTMLElement|ILottieConfig) {
        let _that = this;
        return new Promise((resolve, reject) => {
            _that._filterData(src, options).then((res) => {
                res.forEach(item => {
                    _that.aeJson(item).then(resolve, reject);
                });
            });
        });
    }
    /**
     * 渲染AE动画到dom元素中
     * @param {Object} options 动画数据
     * @return {Promise}
     */
    aeJson (options: ILottieConfig): Promise<boolean> {
        return new Promise((resolve, reject) => {
            try {
                lottie.loadAnimation(options);
                resolve(true);
            } catch (error) {
                reject(error);
            }
        });
    }
    /**
     * 过滤参数
     * @param {string} src 动画文件地址
     * @param {HTMLElement|ILottieConfig} options 选项或el
     * @return {Promise}
     */
    private _filterData(src: string, options: HTMLElement|ILottieConfig): Promise<ILottieConfig[]> {
        let _that = this;
        let _opts: any = _that.defaultOption || {};
        let _config: any = _that._parseOptions(src, options);
        let  _options: ILottieConfig = extend(true, _opts.lottieConfig, _config || {});
        return new Promise((resolve, reject) => {
            switch (_options.ext) {
                case 'json':
                    resolve([_options]);
                    break;
                default:
                    _that._cacheData(src, () => {
                        return new Promise((resolve, reject) => {
                            AeAnimation.preset(_options.path || '', _options, _options.ext || '').then(res => {
                                resolve(extend(false, {
                                    json: filterJsonList(res.json, res.files)
                                }));
                            }, reject);
                        });
                    }, _options.isReset).then((data) => {
                        let _jsons = data && data.json || [];
                        delete _options.path;
                        let _optionsList: ILottieConfig[] = [];
                        if (_jsons.length) {
                            _jsons.forEach(_json => {
                                _optionsList.push(extend(false, {}, _options, {
                                    animationData: _json && _json.url || ''
                                }));
                            });
                        }
                        resolve(_optionsList);
                    }, reject);
                    break;
            }
        });
        /**
         * 过滤json
         * @param {ILottieFileJsons[]} jsons 列表
         * @param {{[key: string]: ILottieOtherFile}} files 文件集
         * @returns {ILottieFileJsons[]}
         */
        function filterJsonList(jsons: ILottieFileJsons[], files: {[key: string]: ILottieOtherFile}): ILottieFileJsons[] {
            let _jsons: ILottieFileJsons[] = jsons || [];
            let _jsonList: ILottieFileJsons[] = [];
            if (_jsons && _jsons.length) {
                _jsons.forEach((_json: ILottieFileJsons) => {
                    _jsonList.push(extend(false, _json, {
                        url: _that._filtersJson(_json.url, files)
                    }));
                });
            }
            return _jsonList;
        }
    }
    /**
     * 过滤json数据
     * @param {String} jsonData json字符串
     * @param {Object} files 文件列表
     * @returns {Promise}
     * @memberof AeAnimation
     */
    private _filtersJson(jsonData: string|object, files: {[key: string]: ILottieOtherFile}): IKeyValue {
        let _jsonData: IKeyValue = {};
        if (typeof jsonData === 'string') {
            _jsonData = JSON.parse(jsonData);
        } else {
            _jsonData = jsonData;
        }
        let assets = _jsonData && _jsonData.assets || [];
        if (assets.length && files instanceof Object) {
            assets.forEach((item: any) => {
                item = item || {};
                let _path = (item.u || '') + (item.p || '');
                let _file = files[_path];
                if (_file && _file.url) {
                    let pathArr = _file.url.split('/');
                    item.p = pathArr.pop() || '';
                    item.u = pathArr && pathArr.length && pathArr.join('/') + '/' || '';
                }
            });
        }
        return _jsonData;
    }
    /**
     * 缓存数据
     * @param {*} key 缓存的key
     * @param {*} cb 数据的回调
     * @param {*} isReset 是否重置
     * @returns {Promise}
     * @memberof AeAnimation
     */
    private _cacheData(key: string, cb: () => void, isReset?: boolean): Promise<ILottieFile> {
        promiseCache = promiseCache || {};
        zipFilesMap = zipFilesMap || {};
        if (isReset) {
            delete zipFilesMap[key];
        }
        let res = promiseCache[key] || new Promise((resolve, reject) => {
            if (zipFilesMap[key]) {
                delete promiseCache[key];
                resolve(zipFilesMap[key]);
            } else {
                getBackData(cb).then((data) => {
                    delete promiseCache[key];
                    zipFilesMap[key] = data;
                    resolve(data);
                }, (err) => {
                    delete promiseCache[key];
                    reject(err);
                });
            }
        });
        promiseCache[key] = res;
        return res;
    }
    /**
     * 解析参数
     * @param {string} src 动画文件地址
     * @param {HTMLElement|ILottieConfig} options 选项或el
     * @returns {Object}
     * @memberof AeAnimation
     */
    private _parseOptions(src: string, options: HTMLElement|ILottieConfig): ILottieConfig|undefined {
        let _options: any = options || {};
        let el;
        if (options instanceof HTMLElement) {
            _options = {};
            el = options;
        } else {
            el = options.container;
        }
        src = (src || _options.path || '') + '';
        if ((!src && !_options.animationData) || !el) {
            return;
        }
        if (src) {
            _options.path = src;
            let ext = ((_options.ext || getFileExt(src) || '') + '').toLowerCase();
            if (ext) {
                _options.ext = ext;
            }
        }
        _options.container = el;
        return _options;
    }
}
/**
 * 实例化工厂方法
 * @export
 * @param {IAeAnimationConfig} options 配置选项
 * @returns {AeAnimation}
 */
let aeAnimationFactory: IFactoryUse<IAeAnimationConfig, AeAnimation, IPreset<ILottieConfig>> = (options: IAeAnimationConfig) => {
    return AeAnimation.instance<AeAnimation>(options);
};
aeAnimationFactory.use = AeAnimation.use.bind(AeAnimation);
export default aeAnimationFactory;

export {
    IAeAnimationConfig,
    ILottieConfig,
    ILottieFile,
    ILottieOtherFile,
    ILottieFileJsons,
    IPreset
};
