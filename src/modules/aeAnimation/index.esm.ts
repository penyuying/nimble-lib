import {
    IAeAnimationConfig, ILottieConfig, ILottieFile, ILottieOtherFile, ILottieFileJsons, IPreset, AeAnimation
} from './aeAnimation.imp';
import {ClassProxy} from '../../library/util/ClassProxy';
import { IFactoryUse } from '../../library/helpers/IFactoryUse';
import callFn from '../utils/callFn';
import mountService from '../helpers/mountService';


/**
 * 实例化工厂方法
 *
 * @export
 * @param {IAeAnimationConfig} options 配置选项
 * @returns {AeAnimation}
 */
let fn: IFactoryUse<IAeAnimationConfig, AeAnimation, IPreset<ILottieConfig>> = (options: IAeAnimationConfig) => {
    let _proxy: any = new ClassProxy();
    let res: any = {
        name: 'AeAnimation',
        preload() {
            let _that = this;
            let initRes = _proxy.initProxy(() => {
                return new Promise((resolve, reject) => {
                    import(/* webpackChunkName: "_aeAnimation_" */ './aeAnimation.imp').then(back => {
                        let aeFactory = back && back.default;
                        let _useList =  fn._useArgs || [];
                        _useList.forEach((item: any) => {
                            aeFactory.use && aeFactory.use.apply(aeFactory, item);
                        });
                        let _res: any = aeFactory(options);
                        callFn(_that._callInstall, [(...args: any[]) => {
                            _res._getParent = () => {
                                return _that._getParent();
                            };
                            callFn(_res.install, args, _res);
                        }], _that);
                        resolve(_res);
                    }, reject);
                });
            }, 'preload')();
            _that.preload = () => {
                return initRes;
            };
            return initRes;
        },
        install(vue: any, opts: IAeAnimationConfig) {
            mountService(vue, this);
            this._callInstall = function(cb: Function) {
                callFn(cb, [vue, opts]);
            };
        }
    };
    _proxy.proxyHook(res, () => {
        return res.preload();
    }, ['load', 'aeJson']);
    return res;
};

fn.use = function (preset: IPreset<ILottieConfig>) {
    fn._useArgs =  fn._useArgs || [];
    fn._useArgs.push([preset]);
};

export default fn;
export {
    IAeAnimationConfig,
    ILottieConfig,
    ILottieFile,
    ILottieOtherFile,
    ILottieFileJsons
};
