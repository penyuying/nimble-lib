import { IBaseInterface } from '../../../library/basic/base.interface';
import { IKeyValue } from '../../../library/helpers/IKeyValue';
import { IPreset } from './aePreset';

export interface IAeAnimation {
    /**
     * 加载动画
     */
    load: (src: string, options: HTMLElement|ILottieConfig) => void;
    /**
     * 加载JSON动画
     */
    aeJson: (options: ILottieConfig) => Promise<boolean>;
}

/**
 * 倒计时时间的对象参数
 *
 * @export
 * @interface ICountDown
 */
export interface IAeAnimationConfig extends IBaseInterface {
    lottieConfig: IConfigBase;
}
/**
 * ae公共参数
 *
 * @export
 * @interface IConfigBase
 */
export interface IConfigBase {
    /**
     * svg模式渲染
     */
    renderer?: 'svg'|'canvas'|'html';
    /**
     * 循环播放
     */
    loop?: boolean;
    /**
     * 自动播放
     */
    autoplay?: boolean;
    /**
     * 文件扩展名
     */
    ext?: string;
    /**
     * 挂载的节点
     */
    container?: HTMLElement;
    /**
     * 重置
     */
    isReset?: boolean;
    /**
     * 解压图片的类型
     */
    imgType?: 'blob'|'base64';
}

/**
 * 直接传内容的ae参数
 *
 * @interface IAnimationData
 * @extends {IConfigBase}
 */
interface IAnimationData extends IConfigBase {
    /**
     * 动画文件内容
     */
    animationData: IKeyValue;
    /**
     * 动画文件路径
     */
    path?: undefined;
}

/**
 * 直接传路径的ae参数
 *
 * @interface IAnimationPath
 * @extends {IConfigBase}
 */
interface IAnimationPath extends IConfigBase {
    /**
     * 动画文件内容
     */
    animationData?: undefined;
    /**
     * 动画文件路径
     */
    path: string;
}

export type ILottieConfig = IAnimationData|IAnimationPath;


// export interface IAeFactory {
//     // tslint:disable-next-line:callable-types
//     (options: IAeAnimationConfig): IAeAnimation;
//     use?: (preset: IPreset<ILottieConfig>) => void;
//     _useArgs?: Array<Array<IPreset<ILottieConfig>>>; // use的参数列表
// }
