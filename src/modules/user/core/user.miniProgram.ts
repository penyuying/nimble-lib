import { USER_DATA_TYPE } from '../constant';
import {
    UserThirdparty,
    UserCore,
    UserBase,
    IUserConfig,
    IUserInfo,
    IUserDataType,
    IValidDataType,
    IAppApiType
} from './user.thirdparty';
import { IDevice } from '../../../library/util/browser/interface/browser';
import getParams from '../../utils/getParams';
import { IMiniProgramLoginParams, IMiniProgramUrlParams } from '../interface/userConfig';
import getBackData from '../../utils/getBackData';

/**
 * 小程序用户登录模块
 *
 * @export
 * @class UserMiniProgram
 * @extends {UserThirdparty}
 */
export class UserMiniProgram extends UserThirdparty {
    name = 'UserMiniProgram';
    /**
     * 小程序登录参数
     */
    private _miniProgramLoginParams =  <IMiniProgramUrlParams>{};
    /**
     * 小程序登录的Promise
     */
    private _miniProgramLoginPromise?: Promise<IUserInfo|null>;
    /**
     * 用户模块
     * @param {IUserConfig} options 默认参数
     * @memberof User
     */
    constructor(options: IUserConfig) {
        super(options);
        let _that = this;
        _that.setDefaultOptions(options);
        const _params: IMiniProgramUrlParams =  Object.assign<any, any>({}, getParams());
        let params = <IMiniProgramUrlParams>{};
        for (const key in _params) {
            if (_params.hasOwnProperty(key)) {
                const element = _params[key];
                params[key.toLowerCase()] = element;
            }
        }
        _that._miniProgramLoginParams = params;
        if (params && params.needlogin + '' === '1') {
            _that._miniProgramLogin(options);
        }
        const _options = this.getOptions(options || {});
        const device = _options.device || <IDevice>{};
        if (device.isMiniProgram && _options.isMiniProgram) {
            getBackData(_options.getWxSdk);
            try {
                _that.$on('success', (userInfo: IUserInfo) => {
                    getBackData(_options.getWxSdk).then(() => {
                        if (window.wx && window.wx.miniProgram) {
                            window.wx.miniProgram.postMessage({
                                data: {
                                    invoke: 'SetUserInfo',
                                    message: userInfo || { islogin: false }
                                }
                            });
                        }
                    });
                });
            } catch (e) {
            }
        }
    }
    /**
     * 获授权登录数据
     * @param {IUserConfig} options 选项
     */
    protected _miniProgramLogin(options?: IUserConfig): Promise<IUserInfo|null> {
        const _that = this;
        const _options = this.getOptions(options || {});
        const device = _options.device || <IDevice>{};
        let res = _that._miniProgramLoginPromise || new Promise<IUserInfo|null>((resolve, reject) => {
            if (!device.isMiniProgram || !_options.isMiniProgram) {
                resolve(null);
                return;
            }
            const params: IMiniProgramUrlParams =  _that._miniProgramLoginParams;
            // 静默登录回调
            if (params && params.sign && params.source && params.timestap && params.token) {
                setTimeout(() => {
                    _that._getAuthInfo({
                        needLogin: (params.needlogin + '' === '1') ? 1 : 0,
                        timeStap: params.timestap || '',
                        source: params.source || '',
                        sign: params.sign || '',
                        token: params.token || '',
                    }).then(res => {
                        let _userSession = res && (res.UserSession || res.usersession) || '';
                        if (_userSession) {
                            _that._logout(_options).then(() => {
                                _that._setLoginCookie(_userSession).then(() => {
                                    _that._getUserInfo(options, {
                                        usersession: _userSession
                                    }).then(resolve, () => {
                                        resolve(null);
                                    });
                                });
                            });
                        } else {
                            resolve(null);
                        }
                    }, () => {
                        resolve(null);
                    });
                }, 0);
            } else {
                resolve(null);
            }
        });
        _that._miniProgramLoginPromise = res;
        return res;
    }
    /**
     * 获取微信小程序登录信息
     * @param {IUserConfig} options 选项
     */
    private _getAuthInfo(options: IMiniProgramLoginParams): Promise<any> {
        let _that = this;
        return new Promise((resolve, reject) => {
            _that._getData(USER_DATA_TYPE.GET_AUTH_INFO, options).then(resolve, reject);
        });
    }
}

export {
    UserThirdparty,
    UserCore,
    UserBase,
    IUserConfig,
    IUserInfo,
    IUserDataType,
    IValidDataType,
    IAppApiType
};
