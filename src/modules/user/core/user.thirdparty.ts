import {
    UserCore,
    UserBase,
    IUserConfig,
    IUserInfo,
    IUserDataType,
    IValidDataType,
    IAppApiType
 } from './user.core';
import { USER_DATA_TYPE, EVENT_TYPE } from '../constant';
import getParams from '../../../library/util/getParams';
import { IDevice } from '../../../library/util/browser/interface/browser';
import formatUrl from '../../../library/util/formatUrl';

/**
 * 用户模块核心
 *
 * @export
 * @class User
 * @extends {UserBase}
 */
export class UserThirdparty extends UserCore {
    /**
     * 登录的授权code
     */
    private _accessCode: string = '';
    /**
     * 微信授权Promise
     */
    private _wxAuthAccessPromise: any = null;
    /**
     * 微信登录授权失败
     */
    public isAccessFailure: boolean = false;
    /**
     * 用户第三方授权登录模块
     * @param {IUserConfig} options 默认参数
     * @memberof UserCore
     */
    constructor(options: IUserConfig) {
        super(options);
        let _that = this;
        _that.setDefaultOptions(options);
        _that._resetLogin();
        setTimeout(() => { // 等待依赖库加载（$http）
            _that._getAccess(options);
        }, 0);
    }
    /**
     * 授权code登录
     *
     * @param {string} code 授权Code
     * @returns
     * @memberof UserThirdparty
     */
    private _getAuthCode(code: string) {
        let _that = this;
        return new Promise((resolve, reject) => {
            if (!code) {
                reject(new Error('授权code不能为空'));
                return ;
            }
            _that._getData(USER_DATA_TYPE.AUTH_LOGIN, {
                authCode: code,
                platform: 'Weixin',
                channel: 'h5'
            }).then(resolve, reject);
        });
    }
    /**
     * 获授权登录数据
     * @param {IUserConfig} options 选项
     */
    protected _getAccess(options?: IUserConfig): Promise<IUserInfo|null> {
        const _that = this;
        const _options = this.getOptions(options || {});
        const device = _options.device || <IDevice>{};
        let res = _that._wxAuthAccessPromise || new Promise<IUserInfo|null>((resolve, reject) => {
            if (!device.isWeixin || device.isMiniProgram) {
                resolve(null);
                return;
            }
            const params: {
                _loginPage: 'login'; // 登录页面
                code: string; // 微信授权回调code
                access: string; // 微信授权回调参数
            } =  Object.assign<any, any>({}, getParams()) || {};
            const _code = params.code || '';
            const _isAccess = !!(params.access || '');
            // 静默登录回调
            if (_code && params._loginPage) {
                _that._accessCode = _code;
                window.history.replaceState(window.history.state || null, '', _that._resetUrl(false));
                _that._getAuthCode(_code).then((res: any) => {
                    if (res) {
                        const {Result, Success} = res;
                        if (Success) {
                            _that._h5LoginSuccess(Result).then((userInfo) => {
                                window.history.replaceState(window.history.state || null, '', _that._resetUrl(true));
                                resolve(userInfo);
                                _that.$emit(EVENT_TYPE.WX_AUTH_SUCCESS, userInfo);
                            }, reject);
                        } else {
                            if (_isAccess) {
                                // window.history.replaceState(window.history.state || null, '', _that._resetUrl(true));
                                // 授权登录失败 绑定
                                _that.isAccessFailure = true;
                                _that.login({popup: {transitionCls: ''}}).then(resolve, reject);
                            } else {
                                // 授权登录
                                _that.wxAuthorizeUserInfo();
                                _that.isAccessFailure = true;
                                _that.login({popup: {transitionCls: ''}}).then(resolve, reject);
                            }
                        }
                    }
                });
            } else {
                resolve(null);
            }
        });
        _that._wxAuthAccessPromise = res;
        return res;
    }
    /**
     * 重置url
     */
    private _resetUrl(isPageKey?: boolean, url?: string) {
        const regKey = (isPageKey && '_loginPage|' || '') + 'code|access|state';
        let reg = RegExp('&(' + regKey + ')=[a-z0-9]+', 'ig');
        let reg1 = RegExp('\\?(' + regKey + ')=[a-z0-9]+(&)?', 'ig');
        url = (formatUrl(url || location.href) || '').replace(reg, '')
            .replace(reg1, ($1, $2, $3) => {
                let res = '';
                if ($3 === '&') {
                    res = '?';
                }
                return res;
            });
        return url;
    }
    /**
     * 微信授权
     * @param {IUserConfig} options 选项
     */
    private _wxAuthorize(options?: IUserConfig): void {
        const _that = this;
        const _options = _that.getOptions(options || {});
        const appid = _options.appid;
        let _scope = _options.scope || 'snsapi_userinfo';
        let _url = _that._resetUrl(false, _options.returnUrl || location.href);
        if (_scope === 'snsapi_userinfo') {
            _url = _url.replace(/(&|\?)?access=1/g, '');
            _url = _url + (_url.indexOf('?') > 0 ? '&' : '?') + 'access=1';
        }
        const returnUrl = encodeURIComponent(_url);
        const accessUrl = 'https://open.weixin.qq.com/connect/oauth2/authorize?' +
        `appid=${appid}&redirect_uri=${returnUrl}&response_type=code&scope=${_scope}&state=wechat#wechat_redirect`;
        setTimeout(() => {
            window.location.href = accessUrl;
        }, 150);
    }

    /**
     * 绑定并登录
     * @param {Object} params 登录参数
     * @param {String} params.phone 手机号
     * @param {String} params.channel 设备标识
     * @param {String} params.code 验证码
     * @return {Promise<IUserInfo>}
     */
    weChatBind (params: {
        phone: string; // 手机号
        code: string; // 验证码
    }): Promise<IUserInfo> {
        const _that = this;
        return new Promise((resolve, reject) => {
            _that._getData(USER_DATA_TYPE.AUTH_LOGIN, {
                authCode: _that._accessCode,
                platform: 'Weixin',
                channel: 'h5',
                phone: params.phone,
                smsCode: params.code
            }).then(res => {
                const {Result, Success} = res;
                if (Success && Result && Result.UserSession) {
                    _that._h5LoginSuccess(Result).then((userInfo) => {
                        resolve(userInfo);
                        _that.$emit(EVENT_TYPE.WE_BIND_SUCCESS, userInfo);
                    }, reject);
                } else {
                    _that.isAccessFailure = false;
                    _that.message('登录异常，请重试', {
                        type: 'toast'
                    });
                }
            }, reject);
        });
    }
    /**
     * 授权登录
     *
     * @memberof UserThirdparty
     */
    wxAuthorizeUserInfo() {
        this._wxAuthorize({scope: 'snsapi_userinfo'});
    }
    /**
     * 静默登录
     *
     * @memberof UserThirdparty
     */
    wxAuthorize() {
        // 微信授权登录
        this._wxAuthorize({scope: 'snsapi_base'});
    }
}

export {
    UserCore,
    UserBase,
    IUserConfig,
    IUserInfo,
    IUserDataType,
    IValidDataType,
    IAppApiType
};
