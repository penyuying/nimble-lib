import { IKeyValue } from '../../../library/helpers/IKeyValue';
import { IDevice } from '../../../library/util/browser/interface/browser';
import getBackData from '../../../library/util/getBackData';
import MyError from '../../../library/util/MyError';
import {
    UserBase,
    IUserConfig,
    IUserInfo,
    IUserDataType,
    IValidDataType,
    IAppApiType
} from './user.base';
import { PAGE_TYPE, USER_DATA_TYPE, APP_API_TYPE, EVENT_TYPE, STORAGE_KEY } from '../constant';
import extend from '../../utils/extend';
import { ILoginConfig } from '../interface/userConfig';
import getCookie from '../../utils/getCookie';
import setCookie from '../../utils/setCookie';
/**
 * 用户模块核心
 *
 * @export
 * @class User
 * @extends {UserBase}
 */
export class UserCore extends UserBase {
    /**
     * 记录上次获取时间
     *
     * @protected
     * @memberof UserCore
     */
    protected _lastRun = 0;
    /**
     * 登录的Promise
     *
     * @private
     * @type {(Promise<any>|null)}
     * @memberof UserCore
     */
    private _loginPromise: Promise<any>|null = null;
    /**
     * 缓存获取用户信息接口
     *
     * @private
     * @type {(null|Promise<IUserInfo>)}
     * @memberof UserCore
     */
    private _getUserInfoPromise: null|Promise<IUserInfo> = null;
    /**
     * 缓存是否正在请求接口查询是否
     *
     * @private
     * @type {(null|Promise<boolean>)}
     * @memberof UserCore
     */
    private _getIsLoginPromise: null|Promise<boolean> = null;
    /**
     * 用户模块核心
     * @param {IUserConfig} options 默认参数
     * @memberof UserCore
     */
    constructor(options: IUserConfig) {
        super(options);
        let _that = this;
        _that.setDefaultOptions(options);
        _that._resetLogin();
    }
    /**
     * 获取是否登录
     *
     * @returns {Promise} 用户登录状态Promise
     * @memberof UserCore
     */
    protected _getIsLogin(): Promise<boolean> {
        let _that = this;
        _that._getIsLoginPromise = _that._getIsLoginPromise || new Promise((resolve, reject) => {
            let hd = {};
            _that._getLocalData().then(userInfo => {
                // if (userInfo && (process.env.NODE_ENV === 'development' || userInfo._localDataType !== 'h5')) {
                //     let _sersession = userInfo.usersession;
                //     if (_sersession) {
                //         hd = {
                //             authorization: 'Bearer ' + _sersession
                //         };
                //     }
                // }
                if (userInfo) {
                    let _sersession = userInfo.usersession;
                    if (_sersession) {
                        hd = {
                            authorization: 'Bearer ' + _sersession
                        };
                    }
                }
                _getIsLogin(hd);
            }, () => {
                resolve(false);
            });
            /**
             * 验证登录
             *
             * @param {*} hd header头
             */
            function _getIsLogin(hd: IKeyValue) {
                _that._getData(USER_DATA_TYPE.IS_LOGIN, {}, hd)
                    .then(res => {
                        let _loginState = res && (res.result + '' === '1');
                        resolve(_loginState);
                    }, () => {
                        resolve(false);
                    });
            }
        });
        _that._getIsLoginPromise.then(() => {
            _that._getIsLoginPromise = null;
        });
        return _that._getIsLoginPromise;
    }

    /**
     * 设置用户登录Cookie
     * @param {String} usersession 用户session信息
     * @returns {Promise} 用户设置cookie的promise
     * @memberof UserCore
     */
    protected _setLoginCookie (usersession: string) {
        const _that = this;
        return _that._getData(USER_DATA_TYPE.SET_LOGIN_COOKIE, {}, {
            authorization: 'Bearer ' + usersession
        });
    }

    /**
     * 获取本地用户信息
     *
     * @returns {Promise}
     */
    protected _getLocalData(): Promise<IUserInfo|null> {
        const _that = this;
        const _options = _that.currentOption;
        const _device = _options.device || <IDevice>{};
        return new Promise((resolve, reject) => {
            if (_that._isToNative(APP_API_TYPE.GET_USER_INFO)) { // 获取app的用户信息
                _that._toNative(APP_API_TYPE.GET_USER_INFO, {
                    force: false
                }).then(userInfo => {
                    _that._userInfoFilter(userInfo).then(res => {
                        if (res) {
                            res._localDataType = _device.isLightApp ? 'lightApp' : 'app';
                        }
                        resolve((res && res.islogin && res) || null);
                    }, resolve);
                });
            } else { // 获取h5本地的用户信息
                getBackData(_options.storage && _options.storage.getLocal, STORAGE_KEY.USER_STORAGE_KEY).then(res => { // 从本地获取
                    if (res) {
                        res._localDataType = 'h5';
                    }
                    let _usersession = getCookie(STORAGE_KEY.USER_COOKIE_KEY);
                    if (_usersession && (!res || res.usersession !== _usersession)) {
                        res = {
                            islogin: true,
                            usersession: _usersession
                        };
                        // _that._logout().then(() => {
                        getBackData(_options.storage && _options.storage.setLocal, [STORAGE_KEY.USER_STORAGE_KEY, res]).then(() => {
                            res._localDataType = 'h5';
                            res.usersession && setCookie(STORAGE_KEY.USER_COOKIE_KEY, res.usersession, {
                                time: 1000 * 60 * 60 * 24 * 30,
                                domain: _options.domain || ''
                            });
                            resolve((res && res.islogin && res) || null);
                        });
                        // });
                    } else {
                        resolve((res && res.islogin && res) || null);
                    }
                }, () => {
                    resolve(null);
                });
            }
        });
    }
    /**
     * 获取用户信息结束
     * @param {Object} userInfo 用户信息
     * @param {Boolean} isApp 是否从app取的用户信息
     * @returns {Promise}
     * @memberof UserCore
     */
    protected _getUserInfoEnd(userInfo?: IKeyValue|null, isApp?: boolean): Promise<IUserInfo> {
        const _that = this;
        return new Promise((resolve, reject) => {
            let _options = _that.currentOption;
            _that._userInfoFilter(userInfo).then(res => {
                if (res.islogin && !res.headUrl) {
                    let hd;
                    if (res && res.usersession) {
                        hd = {
                            authorization: 'Bearer ' + res.usersession
                        };
                    }
                    _that._getData(USER_DATA_TYPE.GET_MEMBER_MALL_USER_INFO, { // 获取用户头像
                        UserId: res.userid
                    }, hd).then(data => {
                        const _userInfo = data && data.UserInfo;
                        const _headUrl = _userInfo && _userInfo.HeadUrl;
                        // const _code = ((data && data.Code) || '') + '';
                        res.headUrl = _headUrl ? _headUrl : _options.userHeadImg;
                        _resolve(res);
                    }, () => {
                        res.headUrl = _options.userHeadImg;
                        _resolve(res);
                    });
                } else {
                    _resolve(res);
                }
                /**
                 * 获取信息完成
                 *
                 * @param {*} userInfo 用户信息
                 */
                function _resolve (userInfo: IUserInfo) {
                    _that.$emit(EVENT_TYPE.GET_USER_INFO, extend(true, userInfo));
                    _that._setUserInfo(userInfo, isApp).then(resolve, reject);
                }
            }, reject);
        });
    }
    /**
     * 获取用户信息
     * @param {Object} options 选项
     * @param {IUserInfo} userInfo 用户信息
     * @memberof UserCore
     * @returns {Promise} 用户信息promise
     */
    protected _getUserInfo (options?: IUserConfig, userInfo?: {
        usersession?: string
    }|null) {
        const _that = this;
        const _options = _that._resetInit(options);
        let userPromise: Promise<IUserInfo|null>|null = null;
        let _header: any;
        if (userInfo && userInfo.usersession) {
            _header = {
                authorization: 'Bearer ' + userInfo.usersession
            };
        }
        _that._getUserInfoPromise = _that._getUserInfoPromise || new Promise((resolve, reject) => {
            let isApp = false;
            if (_that._isToNative(APP_API_TYPE.GET_USER_INFO)) {
                isApp = true;
                userPromise = _that._toNative(APP_API_TYPE.GET_USER_INFO, {
                    force: options && options.force || false
                });
            } else {
                if (_options.isReset) { // 重新获取用户信息
                    userPromise = _that._getData(USER_DATA_TYPE.GET_USER_INFO, {}, _header);
                } else { // 先从本地取，如果本地没有的话从接口取
                    userPromise = getLocalData();
                }
            }
            userPromise.then(res => {
                if (userInfo && userInfo.usersession && res && !res.usersession) {
                    res.usersession = userInfo.usersession;
                }
                _that._getUserInfoEnd(res, isApp).then(resolve, reject);
            }, reject);
        });
        _that._getUserInfoPromise.then(() => {
            _that._getUserInfoPromise = null;
        });
        return _that._getUserInfoPromise;

        /**
         * 获取本地用户信息
         *
         * @returns {Promise}
         */
        function getLocalData(): Promise<IUserInfo|null> {
            return new Promise((resolve, reject) => {
                _that._getLocalData().then(res => {
                    if (res && res.userid) {
                        resolve(res);
                    } else {
                        let _usersession = '';
                        if (!_header && res && res.usersession) {
                            _usersession = res.usersession;
                            _header = {
                                authorization: 'Bearer ' + _usersession
                            };
                        }
                        _that._getData(USER_DATA_TYPE.GET_USER_INFO, {}, _header).then((back) => {
                            if (_usersession) {
                                back.usersession = _usersession;
                            }
                            resolve(back);
                        }, reject);
                    }
                });
            });
        }
    }
    /**
     * 去原生登录页
     *
     * @returns {Promise} 登录页promise
     */
    protected _toNativeLogin(): Promise<IUserInfo|null> {
        const _that = this;
        const _options = _that.currentOption;
        const _device = _options.device || <IDevice>{};
        return new Promise((resolve, reject) => {
            _that._toNative(APP_API_TYPE.FORCE_LOGIN).then(userInfo => {
                let _loginType: 'lightApp'|'app' = _device.isLightApp ? 'lightApp' : 'app';
                if (userInfo && userInfo._handleType !== 'cancel') {
                    _that._getUserInfoEnd(userInfo).then(_userinfo => {
                        _userinfo = _userinfo || <IUserInfo>{};
                        _userinfo._loginType = _loginType;
                        if (_userinfo.islogin) {
                            _that.setLoginState(true);
                            resolve(_userinfo);
                            _that.$emit(EVENT_TYPE.USER_LOGIN, extend(true, _userinfo)); // 用户登录完成事件
                        } else {
                            _that.setLoginState(false);
                            _userinfo._handleType = 'cancel';
                            resolve(_userinfo);
                        }
                    }, reject);
                } else {
                    resolve({
                        _loginType: _loginType,
                        islogin: false,
                        _handleType: 'cancel'
                    });
                }
            }, reject);
        });
    }
    /**
     * 唤起h5登录页
     * @returns {Promise} 登录页promise
     */
    protected _toH5Login (options?: ILoginConfig) {
        const _that = this;
        let res = _that._loginPromise || new Promise((resolve, reject) => {
            pageWatch();
            _that.getBlackBox(); // 预加载blackbox
            _that.$emit(EVENT_TYPE.REDIRECT, {
                type: PAGE_TYPE.LOGIN,
                options: options
            });

            /**
             * 跳转登录页面监听数据
             */
            function pageWatch () {
                /**
                 * 点击登录的事件回调
                 * @param {Object} data 用户登录信息
                 */
                function userLogin (data: any) {
                    _reset();
                    resolve(data);
                }

                /**
                 * 取消登录的事件回调
                 * @param {Object} data 用户取消信息
                 */
                function userCancel (data: any) {
                    _reset();
                    resolve(data);
                }

                /**
                 * 重置用户登录回调promise
                 */
                function _reset () {
                    _that._loginPromise = null;
                    _that.$off(EVENT_TYPE.USER_LOGIN, userLogin);
                    _that.$off(EVENT_TYPE.USER_CANCEL, userCancel);
                }

                _that.$on(EVENT_TYPE.USER_LOGIN, userLogin);
                _that.$on(EVENT_TYPE.USER_CANCEL, userCancel);
            }
        });
        _that._loginPromise = res;
        return res;
    }
    /**
     * 登录
     * @param {Object} params 登录参数
     * @param {String} params.phone 手机号
     * @param {String} params.channel 设备标识
     * @param {String} params.code 验证码
     * @return {Promise<IUserInfo>}
     * @memberof User
     */
    signIn (params: {
        phone: string; // 手机号
        code: string; // 验证码
        event_id?: string; // 事件id
    }): Promise<IUserInfo> {
        const _that = this;
        let _options = _that.currentOption;
        return new Promise((resolve, reject) => {
            _that.getBlackBox().then((blackBox) => { // 调用同盾
                params.event_id = 'Login_web_20171031';
                _that._getData(USER_DATA_TYPE.SIGN_IN, params, {
                    'TongDun-TokenId': _that.getUUID(),
                    'black_box': blackBox,
                    channel: _options.channel
                })
                    .then(data => {
                        let userinfo = data && data.userinfo;
                        if (userinfo && userinfo.usersession || data && data.usersession) {
                            _that._h5LoginSuccess(data).then(resolve, reject);
                        } else {
                            reject(new MyError('登录失败', {
                                data: data
                            }));
                        }
                    }, reject);
            });
        });
    }
    /**
     * 登录成功
     * @param {IKeyValue} userinfo 登录后接口返回的用户信息
     * @return {Promise<IUserInfo>}
     */
    protected _h5LoginSuccess(userinfo: IKeyValue): Promise<IUserInfo> {
        const _that = this;
        return new Promise((resolve, reject) => {
            let _tempUserinfo = Object.assign({}, userinfo && userinfo.userinfo || {}, userinfo) || {};
            let usersession = _tempUserinfo.UserSession || _tempUserinfo.usersession;
            _that._setLoginCookie(usersession).then(loginInfo => {
                _that._getUserInfoEnd(userinfo).then(_userinfo => {
                    if (_userinfo) {
                        _userinfo._loginType = 'h5';
                    }
                    _that.setLoginState(_userinfo && _userinfo.islogin);
                    resolve(extend(true, _userinfo));
                    _that.$emit(EVENT_TYPE.USER_LOGIN, extend(true, _userinfo)); // 用户登录完成事件
                }, reject);
            }, reject);
        });
    }
    /**
     * 获取登录验证码
     *
     * @param {Object} params 请求参数
     * @param {String} params.phone 手机号
     * @return {Promise}
     * @memberof UserCore
     */
    getCode(params: {
        phone: string // 手机号
    }) {
        let _that = this;
        return new Promise((resolve, reject) => {
            _that.getBlackBox().then((blackBox) => { // 调用同盾
                _that._getData(USER_DATA_TYPE.GET_IDENTITY_CODE, params, {
                    'TongDun-TokenId': _that.getUUID(),
                    'black_box': blackBox
                }).then(resolve, reject);
            }, reject);
        });
    }

    /**
     * 唤起登录页
     * @returns {Promise} 唤起登录页的promise
     * @memberof User
     */
    login(options?: ILoginConfig): Promise<IUserInfo|null> {
        const _that = this;
        return new Promise((resolve, reject) => {
            let loginPromise = null;
            if (_that._isToNative(APP_API_TYPE.FORCE_LOGIN)) { // 唤起app的登录页
                loginPromise = _that._toNativeLogin();
            } else { // 唤起h5登录页
                loginPromise = _that._toH5Login(options);
            }
            loginPromise.then(resolve, reject);
        });
    }
    /**
     * 退出app登录
     */
    _appLogout() {
        // app退出登录
        const _that = this;
        return new Promise((resolve, reject) => {
            if (_that._isToNative(APP_API_TYPE.VALIDATE_LOGIN)) {
                _that._getData(USER_DATA_TYPE.SIGN_OUT).then(() => {
                    _that._toNative(APP_API_TYPE.VALIDATE_LOGIN, [{
                        presentLogin: false
                    }]).then(res => {
                        resolve(res);
                    });
                }, () => {
                    _that._toNative(APP_API_TYPE.VALIDATE_LOGIN, [{
                        presentLogin: false
                    }]).then(res => {
                        resolve(res);
                    });
                });
            } else {
                resolve(true);
            }
        });
    }
    /**
     * 退出登录
     * @param {Object} options 配置参数
     * @param {Boolean} options.forceClearCookie 是否强制使cookie信息失效
     * @returns {Promise} 退出登录promise对象
     */
    _logout(options?: IUserConfig) {
        const _that = this;
        const _options = _that._resetInit(options);
        // 清除登录状态
        _that._resetLogin();
        return new Promise((resolve, reject) => {
            _that._appLogout().then(() => {
                // 清除localStorage
                setCookie(STORAGE_KEY.USER_COOKIE_KEY, '', {
                    time: -(1000 * 60 * 60 * 24 * 30),
                    domain: _options.domain || ''
                });
                getBackData(_options.storage && _options.storage.removeLocal, STORAGE_KEY.USER_STORAGE_KEY).then(() => {
                    _logout();
                }, () => {
                    _logout();
                });
            });

            /**
             * 清理登录的cookie
             */
            function _logout() {
                // 退出时失效Cookie
                _that._getData(USER_DATA_TYPE.LOGOUT_COOKIE).then(() => {
                    // 抛出登出信息用以更新store
                    _that._setUserInfo(null);
                    _that.$emit(EVENT_TYPE.LOGOUT);
                    resolve(true);
                }, () => {
                    // 抛出登出信息用以更新store
                    _that._setUserInfo(null);
                    _that.$emit(EVENT_TYPE.LOGOUT);
                    resolve(true);
                });
            }
        });

    }
}
export {
    UserBase,
    IUserConfig,
    IUserInfo,
    IUserDataType,
    IValidDataType,
    IAppApiType
// tslint:disable-next-line:max-file-line-count
};
