import { IValidDataType } from '../interface/userConfig';

/**
 * 验证数据是否符合正则匹配
 * @param {IValidDataType} type 验证的类型
 * @param {String|Number|Undefined|Null} data 需要验证的数据
 * @returns {Boolean} 匹配结果
 * @memberof UserBase
 */
export default function validData(type: IValidDataType, data?: string|number|null): string {
    let res = '';
    let _data = (data || data === 0 ? data : '') + '';
    switch (type) {
        case 'vPhone':
            if (!_data) {
                res = '手机号码不能为空';
            } else {
                let _res = /^\d{11}$/.test(_data);
                if (!_res) {
                    res = '手机号码格式不正确';
                }
            }
            break;
        case 'vCode':
            if (!_data) {
                res = '验证码不能为空';
            } else {
                let _res = /\d{4}/.test(_data);
                if (!_res) {
                    res = '验证码格式不正确';
                }
            }
            break;

        default:
            break;
    }
    return res;
}
