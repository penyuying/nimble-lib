import deviceInfo from '../../library/util/browser/deviceInfo';
import { BaseAbstract } from '../../library/basic/base.abstrat';
// import isType from '../../library/util/isType';
import callFn from '../../library/util/callFn';
import getBackData from '../utils/getBackData';
export class WeChatCore extends BaseAbstract {
    /**
     * constructor
     *
     * @param {IWeChatInitConfig} options
     * @memberof WeChatCore
     */
    constructor(config) {
        super(config);
        /**
         * 默认参数
         *
         * @protected
         * @type {IWeChatInitConfig}
         * @memberof WeChatCore
         */
        this.defaultOption = {
            attemptTimeout: 500,
            attempt: 6,
            attemptFilter() {
                return true;
            },
            jsApiList: [
                'checkJsApi',
                'hideOptionMenu',
                'showMenuItems',
                'hideAllNonBaseMenuItem'
            ],
            shareJsApiList: [
                'onMenuShareTimeline',
                'onMenuShareAppMessage',
                'updateAppMessageShareData',
                'updateTimelineShareData' // 自定义“分享到朋友圈”及“分享到QQ空间”按钮的分享内容（1.4.0）
            ]
        };
        this.setDefaultOptions(config);
    }
    /**
     * init wx config
     *
     * @returns {Promise<ICheckJsApiBackParams>}
     * @memberof WeChatCore
     */
    wxConfig(options, attempt) {
        let self = this;
        let config = self.getOptions(options || {});
        let _attempt = attempt || 0;
        let _jsApiList = config.jsApiList || [];
        let _shareJsApiList = config.shareJsApiList || [];
        let jsApiList = Array.from(new Set(_jsApiList.concat(_shareJsApiList)));
        let isSuccess = false;
        return new Promise((resolve, reject) => {
            self._initUrl = location.href;
            if (jsApiList && jsApiList.length > 0) {
                self.getSign(config)
                    .then((data) => {
                    if (typeof wx !== 'undefined') {
                        wx.config({
                            debug: config.debug || false,
                            appId: data.appid,
                            timestamp: data.timestamp,
                            nonceStr: data.nonceStr,
                            signature: data.signature,
                            jsApiList: jsApiList // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
                        });
                        wx.ready(() => {
                            wx.checkJsApi({
                                jsApiList: config.jsApiList || [],
                                success: function (res) {
                                    isSuccess = true;
                                    resolve(res);
                                }
                            });
                        });
                        wx.error((back) => {
                            let attempt = config && config.attempt || 0;
                            if (!isSuccess && (attempt && (attempt - 1) > _attempt)) {
                                getBackData(config.attemptFilter, [{
                                        config: config,
                                        count: _attempt + 1,
                                        err: back
                                    }], false, config).then(res => {
                                    if (res === false) {
                                        reject(back);
                                    }
                                    else {
                                        setTimeout(() => {
                                            let _res = {};
                                            if (res instanceof Object) {
                                                _res = res;
                                            }
                                            self.wxConfig(_res, _attempt + 1).then(resolve, reject);
                                        }, config.attemptTimeout || 0);
                                    }
                                });
                            }
                            else {
                                reject(back);
                            }
                        });
                    }
                    else {
                        console.error('请先引入jweixin.js文件');
                    }
                }, reject);
            }
        });
    }
    /**
     * get sign
     *
     * @param {ISignDataConfig} config
     * @returns {Promise<ISign>}
     * @memberof WeChatCore
     */
    getSign(config) {
        return new Promise((resolve, reject) => {
            getBackData(config && config.signData).then((data) => {
                if (data && typeof data === 'string') {
                    try {
                        data = JSON.parse(data);
                    }
                    catch (error) {
                    }
                }
                resolve(data);
            }, reject);
        });
    }
    /**
     * init config
     *
     * @private
     * @param {IWxConfig} [options]
     * @param {boolean} [isReset=false]
     * @memberof WeChatCore
     */
    initConfig(isReset = false) {
        let self = this;
        let res = self._tempConfigPromise || (!isReset && self._initUrl === location.href && self.wxConfigData);
        res = res || new Promise((resolve, reject) => {
            if (deviceInfo.weixin) {
                self.wxConfig().then(resolve, reject);
            }
            else {
                reject(new Error('not is weixin'));
            }
        });
        self.wxConfigData = res;
        self._tempConfigPromise = res;
        res.then(() => {
            self._tempConfigPromise = null;
        });
        return res;
    }
    /**
     * set wx share
     *
     * @param {IShareOptions} options 分享选项
     * @param {jsApi|jsApi[]} type jsApi 分享的jsapi
     * @param {IShareBack} resolve resolve 分享完成的回调
     */
    setShare(options, type, resolve) {
        // const link = (options.link || '').replace(/\{/g, '%7B').replace(/\}/g, '%7D');
        const link = (options.link || '').replace(/(\{)|(\})/g, ($0, $1, $2) => {
            let res = '';
            if ($1) {
                res = '%7B';
            }
            else if ($2) {
                res = '%7D';
            }
            return res;
        });
        this.handlerWxApi(type, _type => {
            return {
                title: options.title,
                desc: options.desc,
                link: link,
                imgUrl: options.imgUrl,
                success: function () {
                    callFn(resolve, [{
                            type: _type
                        }]);
                },
                cancel: function () {
                    callFn(resolve, [{
                            type: 'cancel'
                        }]);
                },
                fail: function () {
                    callFn(resolve, [{
                            type: 'fail'
                        }]);
                },
            };
        }, options && options.isReset);
    }
    /**
     * 调用微信api
     *
     * @param {(jsApi| jsApi[])} apis 微信api或微信api列表
     * @param {object} [wxApiParam] api需要的参数
     * @param {boolean} [isReset=false] 是否重新初始化config
     * @memberof WeChatCore
     */
    handlerWxApi(apis, wxApiParam, isReset = false) {
        let self = this;
        return self.initConfig(isReset)
            .then(() => {
            eachApi(apis, (type) => {
                if (type && wx[type] instanceof Function) {
                    let res = callFn(wxApiParam, [type]) || {};
                    if (!(res instanceof Object)) {
                        res = {};
                    }
                    let success = res.success;
                    let cancel = res.cancel;
                    let fail = res.fail;
                    res.success = function (...args) {
                        callFn(success, args, res);
                        self.$emit('success', { type: type });
                    };
                    res.cancel = function (...args) {
                        callFn(cancel, args, res);
                        self.$emit('cancel', { type: type });
                    };
                    res.fail = function (...args) {
                        callFn(fail, args, res);
                        self.$emit('fail', { type: type });
                    };
                    callFn(wx[type], [res], wx);
                }
            });
        });
        /**
         * 遍历api
         *
         * @param {(jsApi|jsApi[])} api 微信api或微信api列表
         * @param {Function} cb 回调
         */
        function eachApi(api, cb) {
            if (api && typeof api === 'string') {
                api = [api];
            }
            if (api instanceof Array) {
                api.forEach(item => {
                    item && callFn(cb, [item]);
                });
            }
        }
    }
}
