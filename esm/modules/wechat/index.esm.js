import { ClassProxy } from '../../library/util/ClassProxy';
import deviceInfo from '../../library/util/browser/deviceInfo';
import mountService from '../helpers/mountService';
import callFn from '../../library/util/callFn';
/**
 * 实例化工厂方法
 *
 * @export
 * @param {IAeAnimationConfig} options 配置选项
 * @returns {AeAnimation}
 */
export default function (options) {
    let _proxy = new ClassProxy();
    let res = {
        name: 'Wechat',
        preload() {
            let _that = this;
            let initRes = _proxy.initProxy(() => {
                return new Promise((resolve, reject) => {
                    if (deviceInfo.isWeixin) {
                        import(/* webpackChunkName: "_wechat_" */ './wechat.imp').then(back => {
                            let weChatFactory = back && back.default;
                            let _res = weChatFactory(options);
                            callFn(_that._callInstall, [(...args) => {
                                    _res._getParent = () => {
                                        return _that._getParent();
                                    };
                                    callFn(_res.install, args, _res);
                                }], _that);
                            // if (_that._callInstall instanceof Function) {
                            //     _that._callInstall((...args: any[]) => {
                            //         _res._getParent = () => {
                            //             return _that._getParent();
                            //         };
                            //         callFn(_res.install, args, _res);
                            //         // _res.install(...args);
                            //     });
                            // }
                            resolve(_res);
                        }, reject);
                    }
                    else {
                        resolve({});
                    }
                });
            }, 'preload')();
            _that.preload = () => {
                return initRes;
            };
            return initRes;
        },
        install(vue, opts) {
            mountService(vue, this);
            this._callInstall = function (cb) {
                callFn(cb, [vue, opts]);
            };
        }
    };
    _proxy.proxyHook(res, () => {
        return res.preload();
    }, ['share', 'handlerWxApi'], ['$emit', '$on', '$off', '$once']);
    return res;
}
