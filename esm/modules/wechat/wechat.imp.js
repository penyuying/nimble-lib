import { WeChatCore } from './wechat.core';
/**
 * 微信分享类
 *
 * @export
 * @class WeChat
 * @extends {WeChatCore}
 */
export class WeChat extends WeChatCore {
    /**
     * 配置参数
     *
     * @param {IWeChatInitConfig} options 初始化默认参数
     * @memberof WeixinCore
     */
    constructor(config) {
        super(config);
        this.name = 'WeChat';
    }
    /**
     * 分享
     *
     * @param {IShareOptions} options 分享参数
     * @param {IShareBack} resolve 分享完成后的回调
     * @returns
     * @memberof Weixin
     */
    share(options, resolve) {
        let self = this;
        let config = self.getOptions();
        let shareType = options.shareType;
        if (shareType && typeof shareType === 'string') {
            shareType = [shareType];
        }
        if (!(shareType instanceof Array) && config.shareJsApiList instanceof Array) {
            shareType = config.shareJsApiList;
        }
        shareType && self.setShare(options, shareType, resolve);
        // if (shareType instanceof Array) {
        //     shareType.forEach(item => {
        //         if (item) {
        //             self.setShare(options, item, resolve);
        //         }
        //     });
        // }
        // this.initConfig(options && options.isReset)
        //     .then(() => {
        //         let config = self.getOptions();
        //         let shareType = options.shareType;
        //         if (shareType && typeof shareType === 'string') {
        //             shareType = [shareType];
        //         }
        //         if (!(shareType instanceof Array) && config.shareJsApiList instanceof Array) {
        //             shareType = config.shareJsApiList;
        //         }
        //         if (shareType instanceof Array) {
        //             shareType.forEach(item => {
        //                 if (item) {
        //                     self.setShare(options, item, resolve);
        //                 }
        //             });
        //         }
        //     });
    }
}
/**
 * 实例化工厂方法
 *
 * @export
 * @param {IWeChatInitConfig} options 配置选项
 * @returns {WeChat}
 */
export default function (options) {
    return WeChat.instance(options);
}
