/**
 * Intersection事件类型
 */
export var INDEX_LIST_EVENT_TYPE;
(function (INDEX_LIST_EVENT_TYPE) {
    /**
     * item项移动事件
     */
    INDEX_LIST_EVENT_TYPE["ITEM_MOVE"] = "itemmove";
})(INDEX_LIST_EVENT_TYPE || (INDEX_LIST_EVENT_TYPE = {}));
