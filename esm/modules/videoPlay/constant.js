import hasIntersectionObserver from '../../library/util/hasIntersectionObserver';
import { INTERSECTION_DEFAULT_CONFIG } from '../../library/helpers/intersection/constant';
import deviceInfo from '../../library/util/browser/deviceInfo';
/**
 * VideoListener默认配置
 * @type {IVideoListenerConfig}
 */
export const VIDEO_LISTENER_DEFAULT_CONFIG = Object.assign({}, INTERSECTION_DEFAULT_CONFIG || {}, {
    preLoad: 1,
    device: deviceInfo,
    videoAttr: {
        'x-webkit-airplay': true,
        'x5-playsinline': '',
        'playsinline': '',
        'webkit-playsinline': '',
        'x5-video-player-type': '',
        'x5-video-player-fullscreen': true
    }
});
/**
 * videoPlay默认配置
 * @type {IVideoPlayConfig}
 */
export const DEFAULT_CONFIG = Object.assign({}, VIDEO_LISTENER_DEFAULT_CONFIG || {}, {
    /**
     * IntersectionObserver模式移出视图是否执行回调
     */
    isShiftOut: true,
    /**
     * 节流时间
     */
    throttleWait: 50,
    /**
     * IntersectionObserver模式
     */
    intersectionPattern: hasIntersectionObserver
});
/**
 * status事件类型
 * @type {IListenerQueueState}
 */
export const VIDEO_STATE = {
    /**
     * 初始化
     */
    INIT: 'init',
    // UPDATE: 'update', // 更新
    /**
     * 播放事件
     */
    PLAY: 'play',
    /**
     * 暂停事件
     */
    PAUSE: 'pause',
    /**
     * 刷新数据
     */
    REFRESH: 'refresh',
    /**
     * 消毁
     */
    DESTROYED: 'destroyed',
    /**
     * 消毁
     */
    DISPLAY: 'display',
    /**
     * 移出视图
     */
    SHIFT_OUT: 'shiftOut'
};
/**
 * ImgListener 事件
 *
 * @export
 * @enum {VIDEO_LISTENER_EVENT_TYPE}
 */
export var VIDEO_LISTENER_EVENT_TYPE;
(function (VIDEO_LISTENER_EVENT_TYPE) {
    /**
     * 状态事件
     */
    VIDEO_LISTENER_EVENT_TYPE["STATUS"] = "status";
})(VIDEO_LISTENER_EVENT_TYPE || (VIDEO_LISTENER_EVENT_TYPE = {}));
