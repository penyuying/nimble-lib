import { ClassProxy } from '../../library/util/ClassProxy';
import callFn from '../utils/callFn';
import mountService from '../helpers/mountService';
/**
 * 实例化工厂方法
 *
 * @export
 * @param {IAeAnimationConfig} options 配置选项
 * @returns {AeAnimation}
 */
let fn = (options) => {
    let _proxy = new ClassProxy();
    let res = {
        name: 'AeAnimation',
        preload() {
            let _that = this;
            let initRes = _proxy.initProxy(() => {
                return new Promise((resolve, reject) => {
                    import(/* webpackChunkName: "_aeAnimation_" */ './aeAnimation.imp').then(back => {
                        let aeFactory = back && back.default;
                        let _useList = fn._useArgs || [];
                        _useList.forEach((item) => {
                            aeFactory.use && aeFactory.use.apply(aeFactory, item);
                        });
                        let _res = aeFactory(options);
                        callFn(_that._callInstall, [(...args) => {
                                _res._getParent = () => {
                                    return _that._getParent();
                                };
                                callFn(_res.install, args, _res);
                            }], _that);
                        resolve(_res);
                    }, reject);
                });
            }, 'preload')();
            _that.preload = () => {
                return initRes;
            };
            return initRes;
        },
        install(vue, opts) {
            mountService(vue, this);
            this._callInstall = function (cb) {
                callFn(cb, [vue, opts]);
            };
        }
    };
    _proxy.proxyHook(res, () => {
        return res.preload();
    }, ['load', 'aeJson']);
    return res;
};
fn.use = function (preset) {
    fn._useArgs = fn._useArgs || [];
    fn._useArgs.push([preset]);
};
export default fn;
