/**
 * 默认配置
 */
export const DEFAULT_CONFIG = {
    /**
     * AE动画插件的配置
     */
    lottieConfig: {
        /**
         * svg模式渲染
         */
        renderer: 'svg',
        /**
         * 循环播放
         */
        loop: true,
        /**
         * 自动播放
         */
        autoplay: true,
        /**
         * 文件扩展名
         */
        ext: '',
        /**
         * 重置
         */
        isReset: false,
        /**
         * 解压图片的类型
         */
        imgType: 'blob'
    }
};
