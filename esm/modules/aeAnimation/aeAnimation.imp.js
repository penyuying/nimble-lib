///<reference path="./interface/ae.d.ts" />
import lottie from 'lottie-web';
import extend from '../../library/util/extend';
import { DEFAULT_CONFIG } from './constant';
import getBackData from '../../library/util/getBackData';
import getFileExt from '../../library/util/getFileExt';
import { AeAnimationPreset } from './aeAnimation.preset';
let zipFilesMap;
let promiseCache;
/**
 * AE动画
 * @class AeAnimation
 */
export class AeAnimation extends AeAnimationPreset {
    // private _files = {};
    constructor(options) {
        super(options);
        this.name = 'AeAnimation';
        this.defaultOption = DEFAULT_CONFIG;
        this.setDefaultOptions(options);
    }
    /**
     * 初始化ae动画
     * @param {string} src 动画文件地址
     * @param {HTMLElement|ILottieConfig} options 选项或el
     * @returns {Promise}
     * @memberof AeAnimation
     */
    load(src, options) {
        let _that = this;
        return new Promise((resolve, reject) => {
            _that._filterData(src, options).then((res) => {
                res.forEach(item => {
                    _that.aeJson(item).then(resolve, reject);
                });
            });
        });
    }
    /**
     * 渲染AE动画到dom元素中
     * @param {Object} options 动画数据
     * @return {Promise}
     */
    aeJson(options) {
        return new Promise((resolve, reject) => {
            try {
                lottie.loadAnimation(options);
                resolve(true);
            }
            catch (error) {
                reject(error);
            }
        });
    }
    /**
     * 过滤参数
     * @param {string} src 动画文件地址
     * @param {HTMLElement|ILottieConfig} options 选项或el
     * @return {Promise}
     */
    _filterData(src, options) {
        let _that = this;
        let _opts = _that.defaultOption || {};
        let _config = _that._parseOptions(src, options);
        let _options = extend(true, _opts.lottieConfig, _config || {});
        return new Promise((resolve, reject) => {
            switch (_options.ext) {
                case 'json':
                    resolve([_options]);
                    break;
                default:
                    _that._cacheData(src, () => {
                        return new Promise((resolve, reject) => {
                            AeAnimation.preset(_options.path || '', _options, _options.ext || '').then(res => {
                                resolve(extend(false, {
                                    json: filterJsonList(res.json, res.files)
                                }));
                            }, reject);
                        });
                    }, _options.isReset).then((data) => {
                        let _jsons = data && data.json || [];
                        delete _options.path;
                        let _optionsList = [];
                        if (_jsons.length) {
                            _jsons.forEach(_json => {
                                _optionsList.push(extend(false, {}, _options, {
                                    animationData: _json && _json.url || ''
                                }));
                            });
                        }
                        resolve(_optionsList);
                    }, reject);
                    break;
            }
        });
        /**
         * 过滤json
         * @param {ILottieFileJsons[]} jsons 列表
         * @param {{[key: string]: ILottieOtherFile}} files 文件集
         * @returns {ILottieFileJsons[]}
         */
        function filterJsonList(jsons, files) {
            let _jsons = jsons || [];
            let _jsonList = [];
            if (_jsons && _jsons.length) {
                _jsons.forEach((_json) => {
                    _jsonList.push(extend(false, _json, {
                        url: _that._filtersJson(_json.url, files)
                    }));
                });
            }
            return _jsonList;
        }
    }
    /**
     * 过滤json数据
     * @param {String} jsonData json字符串
     * @param {Object} files 文件列表
     * @returns {Promise}
     * @memberof AeAnimation
     */
    _filtersJson(jsonData, files) {
        let _jsonData = {};
        if (typeof jsonData === 'string') {
            _jsonData = JSON.parse(jsonData);
        }
        else {
            _jsonData = jsonData;
        }
        let assets = _jsonData && _jsonData.assets || [];
        if (assets.length && files instanceof Object) {
            assets.forEach((item) => {
                item = item || {};
                let _path = (item.u || '') + (item.p || '');
                let _file = files[_path];
                if (_file && _file.url) {
                    let pathArr = _file.url.split('/');
                    item.p = pathArr.pop() || '';
                    item.u = pathArr && pathArr.length && pathArr.join('/') + '/' || '';
                }
            });
        }
        return _jsonData;
    }
    /**
     * 缓存数据
     * @param {*} key 缓存的key
     * @param {*} cb 数据的回调
     * @param {*} isReset 是否重置
     * @returns {Promise}
     * @memberof AeAnimation
     */
    _cacheData(key, cb, isReset) {
        promiseCache = promiseCache || {};
        zipFilesMap = zipFilesMap || {};
        if (isReset) {
            delete zipFilesMap[key];
        }
        let res = promiseCache[key] || new Promise((resolve, reject) => {
            if (zipFilesMap[key]) {
                delete promiseCache[key];
                resolve(zipFilesMap[key]);
            }
            else {
                getBackData(cb).then((data) => {
                    delete promiseCache[key];
                    zipFilesMap[key] = data;
                    resolve(data);
                }, (err) => {
                    delete promiseCache[key];
                    reject(err);
                });
            }
        });
        promiseCache[key] = res;
        return res;
    }
    /**
     * 解析参数
     * @param {string} src 动画文件地址
     * @param {HTMLElement|ILottieConfig} options 选项或el
     * @returns {Object}
     * @memberof AeAnimation
     */
    _parseOptions(src, options) {
        let _options = options || {};
        let el;
        if (options instanceof HTMLElement) {
            _options = {};
            el = options;
        }
        else {
            el = options.container;
        }
        src = (src || _options.path || '') + '';
        if ((!src && !_options.animationData) || !el) {
            return;
        }
        if (src) {
            _options.path = src;
            let ext = ((_options.ext || getFileExt(src) || '') + '').toLowerCase();
            if (ext) {
                _options.ext = ext;
            }
        }
        _options.container = el;
        return _options;
    }
}
/**
 * 实例化工厂方法
 * @export
 * @param {IAeAnimationConfig} options 配置选项
 * @returns {AeAnimation}
 */
let aeAnimationFactory = (options) => {
    return AeAnimation.instance(options);
};
aeAnimationFactory.use = AeAnimation.use.bind(AeAnimation);
export default aeAnimationFactory;
