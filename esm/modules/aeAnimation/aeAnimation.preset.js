///<reference path="./interface/ae.d.ts" />
import callFn from '../../library/util/callFn';
import { BaseAbstract } from '../../library/basic/base.abstrat';
/**
 * AE动画Preset解析器
 * @class AeAnimationPreset
 */
export class AeAnimationPreset extends BaseAbstract {
    constructor(options) {
        super(options);
        this.setDefaultOptions(options);
    }
    /**
     * 扩展preset
     *
     * @static
     * @param {IPreset} preset 文件解析器
     * @returns
     * @memberof AeAnimationPreset
     */
    static use(preset) {
        let _that = this;
        preset = callFn(preset);
        if (preset instanceof Object) {
            let _preset = _that._presets;
            if (!preset.ext) {
                console.warn('preset需要提供ext字段（文件后缀名：如zip;多个后缀以英文逗号分隔或数组）');
                return;
            }
            if (!(preset.preset instanceof Function)) {
                console.warn('preset需要提供preset方法');
                return;
            }
            for (let i = 0; i < _preset.length; i++) {
                const item = _preset[i];
                if (item === preset || item.ext === preset.ext) {
                    return;
                }
            }
            _that._presets.push(preset);
        }
    }
    /**
     * 解析其它文件
     * @static
     * @param {string} path 文件路径
     * @param {string} ext 文件后缀名
     * @returns {Promise<ILottieFile>}
     * @memberof AeAnimation
     */
    static preset(path, options, ext) {
        return new Promise((resolve, reject) => {
            if (path) {
                let _presetList = this._presets || [];
                let _preset;
                for (let index = 0; index < _presetList.length; index++) {
                    const item = _presetList[index];
                    if (isExt(item.ext, ext)) {
                        _preset = item;
                        break;
                    }
                }
                if (_preset) {
                    _preset.preset(path, options, ext).then(resolve, reject);
                }
                else {
                    reject(new Error(path + '文件格式无法解析'));
                }
            }
            else {
                reject(new Error('请传入文件路径'));
            }
        });
        /**
         * 判断是否支持类型
         *
         * @param {(string|Array<string>)} extList
         * @param {string} ext
         * @returns
         */
        function isExt(extList, ext) {
            let exts = [];
            if (typeof extList === 'string') {
                exts = extList.split(',');
            }
            else {
                exts = extList;
            }
            for (let index = 0; index < exts.length; index++) {
                const item = exts[index];
                if ((item && (item + '').toLowerCase()) === (ext + '').toLowerCase()) {
                    return true;
                }
            }
            return false;
        }
    }
}
AeAnimationPreset._presets = [];
