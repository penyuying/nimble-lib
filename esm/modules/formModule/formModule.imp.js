import { FormGroup } from './formGroup';
import { FormControl } from './formControl';
import { BaseAbstract } from '../../library/basic/base.abstrat';
import { FormArray } from './formArray';
import { FormDataCache } from './FormDataCache';
import callFn from '../utils/callFn';
/**
 * 表单处理
 *
 * @export
 * @class FormModule
 * @extends {SValidService}
 */
export class FormModule extends BaseAbstract {
    constructor(validate) {
        super(null);
        this.validate = validate;
        this.name = 'Form';
        this.setDefaultOptions(null);
    }
    /**
     * 初始化表单列表
     *
     * @param {(IInitControlData[][]|IFormControlDataMap[])} formList 表单基础数据列表
     * @param {IKeyValue[]} [defaultData] 表单默认数据
     * @param {IFormFilter} [callback] 设置每项值前的回调
     * @returns {FormArray}
     * @memberof FormModule
     */
    initFormArray(formList, defaultData, callback) {
        let _that = this;
        let _groupList = [];
        if (formList instanceof Array && formList.length > 0) {
            for (let index = 0; index < formList.length; index++) {
                const item = formList[index];
                if (item instanceof Array && item.length) {
                    _groupList.push(_that.initFormData(item));
                }
                else if (item instanceof Object) {
                    _groupList.push(_that.initFormGroup(item));
                }
            }
        }
        let res = new FormArray(_groupList);
        res.setValue(defaultData || [], callback);
        return res;
    }
    /**
     * 列表数据生成对象表单
     *
     * @param {IInitControlData[]} formList 列表
     * @param {IFormFilter} callback 过滤回调函数
     * @memberof FormModule
     */
    initFormData(formList, defaultData, callback) {
        let _that = this;
        let _conMap = {};
        let _formList = formList || [];
        flattenedArray(_conMap, _formList);
        if (defaultData) { // 设置默认数据
            for (const key in defaultData) {
                if (defaultData.hasOwnProperty(key)) {
                    if (_conMap[key]) {
                        _conMap[key].defaultValue = defaultData[key];
                    }
                    else {
                        _conMap[key] = {
                            controlName: key,
                            defaultValue: defaultData[key]
                        };
                    }
                }
            }
        }
        return _that.initFormGroup(_conMap, defaultData, callback);
        /**
         * nage
         *
         * @param {IFormControlDataMap} _conMap
         * @param {any[]} list
         */
        function flattenedArray(conMap, list) {
            list.forEach(item => {
                if (item && item.formControl && item.formControl.length > 0) {
                    flattenedArray(conMap, item.formControl);
                }
                else if (item && item.controlName) {
                    conMap[item.controlName] = item;
                }
            });
        }
    }
    /**
     * 初始化表单数据
     *
     * @param {IQueryMemberInfoData} data 控件map生成表单
     * @memberof PersonalInfoComponent
     */
    initFormGroup(formData, defaultData, callback) {
        let _that = this;
        let _conMap = formData;
        let _data = {};
        if (formData instanceof Array) {
            _data = [];
        }
        for (let key in _conMap) {
            if (_conMap.hasOwnProperty(key)) {
                let _conItem = _conMap[key];
                let _groupValid = _that.setGroupValid(_conItem);
                let _val = _conItem.defaultValue;
                if (callback instanceof Function) {
                    _val = callback(_conItem, _val);
                }
                if (_conItem instanceof FormControl || _conItem instanceof FormGroup) {
                    if (_conItem instanceof FormGroup && _groupValid && _groupValid.length) {
                        _conItem._setValidOption(_groupValid);
                    }
                    _data[key] = _conItem;
                }
                else if (_val instanceof FormControl || _val instanceof FormGroup) { // 表单对象
                    if (_val instanceof FormGroup && _groupValid && _groupValid.length) {
                        _val._setValidOption(_groupValid);
                    }
                    _data[key] = _val;
                }
                else {
                    _data[key] = _that.initFormControl(_conItem);
                }
            }
        }
        let res = new FormGroup(_data);
        defaultData && res.setValue(defaultData, callback);
        return res;
    }
    /**
     * 初始化表单控件
     *
     * @param {IFormControlOptions} controlsData 控件选项数据
     * @returns {FormControl}
     * @memberof FormModule
     */
    initFormControl(controlsData) {
        let _contOptions = {};
        let _valid = this.setValid(controlsData);
        if (typeof controlsData.defaultValue !== 'undefined') {
            _contOptions.defaultValue = controlsData.defaultValue;
        }
        if (_valid && _valid.length) {
            _contOptions.valid = _valid;
        }
        if (typeof controlsData.validFlowTime === 'number') {
            _contOptions.validFlowTime = controlsData.validFlowTime;
        }
        return new FormControl(_contOptions);
    }
    /**
     * 验证列表
     *
     * @param {FormGroup|FormArray} formMd 表单模型
     * @param {IFormValidErrorBack} errorBack 取得第一项错误数据的回调
     * @returns {boolean} 验证通过返回false
     * @memberof FormModule
     */
    validForm(formMd, errorBack) {
        let _that = this;
        let controls = formMd.getValid();
        return _that.validControls(controls, errorBack);
    }
    /**
     * 初始化数据缓存
     *
     * @returns
     * @memberof FormModule
     */
    initFormCache() {
        return new FormDataCache();
    }
    /**
     * 设置校验方法
     *
     * @private
     * @param {IFormControlOptions} _conItem 表单项
     * @returns {IValidFunction[]}
     * @memberof PersonalInfoComponent
     */
    setGroupValid(_conItem) {
        let _that = this;
        let _valid = [];
        eachValid(_conItem && _conItem.groupValid, (validItem) => {
            if (validItem instanceof Function) {
                _valid.push(validItem);
            }
            else {
                let _validParams = Object.assign({
                    name: _conItem.nameText || ''
                }, _conItem.valid || {});
                _valid.push(_that.validate.generateArrayValidetor(_validParams));
            }
        });
        return _valid;
    }
    /**
     * 设置校验方法
     *
     * @private
     * @param {IFormControlOptions} _conItem 表单项
     * @returns {IValidFunction[]}
     * @memberof PersonalInfoComponent
     */
    setValid(_conItem) {
        let _that = this;
        let _valid = [];
        eachValid(_conItem && _conItem.valid, (validItem) => {
            if (validItem instanceof Function) {
                _valid.push(validItem);
            }
            else {
                let _validParams = Object.assign({
                    name: _conItem.nameText || ''
                }, validItem || {});
                _valid.push(_that.validate.generateValidetor(_validParams));
            }
        });
        return _valid;
    }
    /**
     * 验证表单项
     *
     * @private
     * @param {Array<any>|Object}} controls 表单控件错误对象
     * @param {IFormValidErrorBack} errorBack 取得第一项错误数据的回调
     * @returns {boolean}  取得第一项错误数据的回调
     * @memberof FormModule
     */
    validControls(controls, errorBack) {
        let _that = this;
        let res = false;
        if (controls) {
            res = true;
            for (let key in controls) {
                if (controls.hasOwnProperty(key)) {
                    let controlItem = controls[key];
                    let errInfo = controlItem && controlItem.errInfo;
                    if (errInfo) {
                        if (errInfo && errInfo.desc) {
                            if (errorBack) {
                                errorBack(errInfo.desc);
                            }
                        }
                        return true;
                    }
                    if (controlItem instanceof Array) {
                        if (_that.validControls(controlItem, errorBack)) {
                            return true;
                        }
                    }
                    if (controlItem && controlItem.controls) {
                        if (_that.validControls(controlItem.controls, errorBack)) {
                            return true;
                        }
                    }
                    if (controlItem instanceof Object) {
                        if (_that.validControls(controlItem, errorBack)) {
                            return true;
                        }
                    }
                }
            }
        }
        if (res) {
            console.error('未定义错误！');
        }
        return res;
    }
}
/**
 * 展开验证选项列表
 *
 * @param {(false|IValidOptions|IValidFunction|Array<IValidOptions|IValidFunction>)} valid 验选项
 * @param {((p: IValidOptions|IValidFunction) => void)} cb 回调
 */
function eachValid(valid, cb) {
    if (valid) {
        if (valid instanceof Array) {
            valid.forEach(validItem => {
                callFn(cb, [validItem]);
            });
        }
        else {
            callFn(cb, [valid]);
        }
    }
    // if (valid && cb instanceof Function) {
    //     if (valid instanceof Array) {
    //         valid.forEach(validItem => {
    //             cb(validItem);
    //         });
    //     } else {
    //         cb(valid);
    //     }
    // }
}
/**
 * factory方法
 *
 * @export
 * @param {Object} env 全局参数
 * @param {Http} http Http服务
 * @returns {SHttpService} 返回服务的实例
 */
export default function formFactory(validate) {
    return new FormModule(validate);
}
export { FormGroup, FormControl, FormArray, FormDataCache };
