var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { FormGroup } from './formGroup';
import { FormGroupCore } from './formGroup.core';
import VmBind from '../../library/util/vmBind';
export class FormArray extends FormGroupCore {
    constructor(groupData) {
        super(groupData);
        this.parent = null;
        this.formGroup = null;
        /**
         * 最后的验证结果是否有错误
         */
        this.isValid = false;
        /**
         * 表单的内容数据
         *
         * @type {(T|null)}
         * @memberof FormArray
         */
        this.value = null;
        /**
         * 是否点过保存
         *
         * @type {boolean}
         * @memberof FormGroupCore
         */
        this.isSave = false;
        /**
         * 表单列表
         *
         * @type {FormGroup[]}
         * @memberof FormArray
         */
        this.groups = [];
        this.setDefaultOptions(null);
        let _that = this;
        // _that._group = groupData;
        _that.groups = groupData;
    }
    /**
     * 设置FormGroup的isSave的值
     */
    setGroupsIsSave(groups, isSave) {
        groups = groups || [];
        return eachGronps(groups, isSave);
        /**
         * 遍历集合设置isSave
         *
         * @param {*} _groups 需要遍历的集合
         * @param {*} _isSave 保存的值
         */
        function eachGronps(_groups, _isSave) {
            for (const key in _groups) {
                if (_groups.hasOwnProperty(key)) {
                    const item = _groups[key];
                    if (item instanceof FormGroup) {
                        item.isSave = _isSave;
                    }
                }
            }
            return true;
        }
    }
    /**
     * 获取FormControl
     *
     * @param {number} index 列表的索引
     * @returns
     * @memberof FormArray
     */
    get(index) {
        index = index || 0;
        return this.groups[index] || null;
    }
    /**
     * 验证数据
     *
     * @memberof FormArray
     */
    getValid() {
        let _that = this;
        this.valid = null;
        return _that.getThisValid() || _that._getValid(this.groups);
        // function getValid(data: any) {
        //     let res;
        //     if (data instanceof FormArray) {
        //         res = _that._getValid(data.groups, (item: FormArray|FormGroup) => {
        //             return getValid(item);
        //         });
        //     } else if (data instanceof FormGroup) {
        //         res = _that._getValid(data.controls, (item: FormArray|FormGroup) => {
        //             return getValid(item);
        //         });
        //     }
        //     return res;
        // }
    }
    /**
     * 获取值
     *
     * @memberof FormArray
     */
    getValue() {
        let _that = this;
        return _that._getValue(this.groups);
    }
    /**
     * 设置表单列表数据
     *
     * @param {IKeyValue[]} defaultData 数据
     * @param {IFormFilter} [callback] 数据过滤的回调
     * @returns {*}
     * @memberof FormArray
     */
    setValue(defaultData, callback) {
        let res;
        defaultData && defaultData.length > 0 && this.groups && this.groups.forEach((group, index) => {
            if (group && typeof defaultData[index] !== 'undefined') {
                res = res || [];
                res.push(group.setValue(defaultData[index]));
            }
        });
        return res;
    }
    /**
     * 添加/设置项
     *
     * @param {(FormControl|FormGroupCore<any, any>)} item
     * @param {(string|number)} [key]
     * @returns {boolean} 返回false为修改失败
     * @memberof FormArray
     */
    setItem(item, key) {
        let _that = this;
        let res = _that._setItem(_that.groups, item, key, info => {
            if (info && info.flag === 'itemTypeErr') {
                console.error('传入的item类型不正确, item只能为FormGroup');
            }
        }, group => {
            group.formArray = _that;
            group.parent = _that;
            group.formGroup = null;
        });
        res && _that.getValid();
        res && _that.getValue();
        return res;
    }
    /**
     * 移除项
     *
     * @param {(number)} key 移除项的key
     * @returns {boolean} 返回false为移除失败
     * @memberof FormArray
     */
    removeItem(key) {
        let res = this._removeItem(this.groups, key);
        res && this.getValid();
        res && this.getValue();
        return res;
    }
    /**
     * 合并FormArray
     *
     * @param {FormGroup} formGroup 需要合并的FormGroup
     * @memberof FormArray
     */
    merge(formArray) {
        let _that = this;
        if (formArray instanceof FormArray) {
            let groups = _that.groups;
            let mergeGroups = formArray.groups;
            if (groups instanceof Array && mergeGroups instanceof Array) {
                mergeGroups.forEach((group) => {
                    let isMerge = groups.every(item => {
                        if (item === group) {
                            return false;
                        }
                        return true;
                    });
                    if (isMerge) {
                        group.formArray = _that;
                        group.parent = _that;
                        group.formGroup = null;
                        groups.push(group);
                    }
                });
                _that.groups = groups;
            }
            else {
                console.error('FormArray的数据格式不正确不能合并');
            }
        }
        return this;
    }
    /**
     * 清空内空
     *
     * @memberof FormArray
     */
    clear() {
        let _that = this;
        if (_that.groups instanceof Array) {
            _that.groups = [];
        }
    }
}
__decorate([
    VmBind({
        set(val) {
            let _that = this;
            _that.isValid = !!val;
        },
        get(val) {
            let _that = this;
            return _that.getValid();
        }
    })
], FormArray.prototype, "valid", void 0);
__decorate([
    VmBind({
        get(val) {
            let _that = this;
            return _that.getValue();
        }
    })
], FormArray.prototype, "value", void 0);
__decorate([
    VmBind({
        set(val) {
            let _that = this;
            _that.setGroupsIsSave(_that.groups, val);
        }
    })
], FormArray.prototype, "isSave", void 0);
