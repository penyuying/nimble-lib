import { FormControl } from './formControl';
import { BaseAbstract } from '../../library/basic/base.abstrat';
import { verifyEmpty, verifyLength, verifySize, validFormat } from './validate.util';
/**
 * 验证表单数据
 *
 * @export
 * @class Validate
 * @extends {BaseAbstract<IValidateInitConfig>}
 */
export class Validate extends BaseAbstract {
    constructor(config) {
        super(config);
        /**
         * 验证的默认选项
         *
         * @protected
         * @type {IValidateInitConfig}
         * @memberof Validate
         */
        this.defaultOption = {
            rules: {
                vPassword: {
                    reg: /(?!^\d+$)(?!^[a-zA-Z]+$)[0-9a-zA-Z]{6,20}$/,
                    infoTxt: '由6-20位数字+字母组成'
                },
                vMobile: {
                    reg: /^1\d{10}$/,
                    infoTxt: '由11位数字组成且第一位为1'
                },
                vEmail: {
                    reg: /^([.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/,
                    infoTxt: '格式不正确'
                }
            }
        };
        this.setDefaultOptions(config);
    }
    /**
     * 获取正则
     *
     * @param {string} key 正则key(如果为空则返回所有)
     * @returns {any} 返回正则
     * @memberof SValidService
     */
    getRegExp(key, options) {
        let _that = this;
        let res;
        let _options = _that.getOptions(options || {});
        let rules = _options && _options.rules;
        if (key) {
            res = rules && rules[key];
            if (!res) {
                console.error(key + ':规则不存在');
            }
        }
        else {
            res = rules;
        }
        return res;
    }
    /**
     * 生成校验方法
     *
     * @param {IValidetorParams} options 规则选项
     * @returns {IValidFunction} 校验方法
     * @memberof SValidService
     */
    generateValidetor(options) {
        let _that = this;
        options = _that.getOptions(options);
        return function (formControl, key) {
            let errType = '';
            if (typeof key === 'undefined') {
                key = 'value';
            }
            let _val = (formControl[key] || '') + '';
            if (formControl instanceof FormControl) {
                formControl.validate = _that;
            }
            if (typeof formControl[key] !== 'undefined' && formControl[key] !== null) {
                _val = formControl[key] + '';
            }
            let _res = false;
            if (!_val && !options.required && !options.isChecked) { // 不校验空和选择
                return false;
            }
            _res = _res || verifyEmpty(options, _val || formControl[key]); // 验证空
            errType = _res && 'emptyErr' || '';
            if (!_res && options.format) { // 校验格式
                _res = _that._validFormat(options.format, _val, options);
                if (_res) {
                    _res = options.formatErr || (options.name || '') + _res;
                }
                errType = _res && 'formatErr' || '';
            }
            if (!_res) {
                _res = verifyLength(options, _val); // 验证长度
                errType = _res && 'lengthErr' || '';
            }
            if (!_res) {
                _res = verifySize(options, _val); // 验证大小
                errType = _res && 'SizeErr' || '';
            }
            return _res ? {
                errInfo: {
                    desc: _res + '',
                    type: errType
                }
            } : false;
        };
    }
    /**
     * 验证列表数据
     *
     * @param {IValidetorArrayParams} options 规则选项
     * @returns {IValidFunction}
     * @memberof Validate
     */
    generateArrayValidetor(options) {
        let _that = this;
        options = _that.getOptions(options);
        return function (formArray) {
            let errType = '';
            let _vals = formArray && formArray.getValue();
            let _res = false;
            let _count = 0;
            if (_vals && _vals.length > 0) {
                _vals.forEach((item) => {
                    if (options.minRequiredName &&
                        typeof item[options.minRequiredName + ''] !== 'undefined' &&
                        (item[options.minRequiredName + ''] + '').replace(/^\s+|\s+$/ig, '') !== '') {
                        ++_count;
                    }
                });
                if (!_res && _vals && options.minRequiredAmount === -1 && _count >= _vals.length) {
                    _res = options.minRequiredAmountErr || (options.name || '') + '必须全部输入';
                    errType = _res && 'requiredAllErr' || '';
                }
                if (!_res && _count < (options.minRequiredAmount || 0)) {
                    _res = options.minRequiredAmountErr || (options.name || '') + '最少需要输入' + options.minRequiredAmount + '项';
                    errType = _res && 'minRequiredErr' || '';
                }
            }
            return _res ? {
                errInfo: {
                    desc: _res + '',
                    type: errType
                }
            } : false;
        };
    }
    /**
     * 校验正则格式
     *
     * @private
     * @param {(string|Array<string>)} param 格式名称(_reg对象里的key名)
     * @param {(number|string)} text 需要校验的字符串
     * @returns {(boolean | string)} 返回flase为校验通过；其它为错误信息
     * @memberof SValidService
     */
    _validFormat(param, text, options) {
        let _that = this;
        let res = false;
        let formatData;
        if (!param || !text) {
            return res;
        }
        if (typeof param === 'string') {
            formatData = _that.getRegExp(param, options);
            if (formatData) {
                res = validFormat(formatData, text);
            }
        }
        else if (param instanceof Array) {
            for (let i = 0; i < param.length; i++) {
                let keyName = param[i];
                formatData = _that.getRegExp(keyName, options);
                if (formatData && !res) {
                    res = validFormat(formatData, text);
                    if (res) {
                        break;
                    }
                }
            }
        }
        return res;
    }
}
/**
 * factory方法
 *
 * @export
 * @returns {Valid} 返回服务的实例
 */
export default function validFactory(options) {
    return new Validate(options);
}
