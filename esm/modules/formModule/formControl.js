var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import VmBind from '../../library/util/vmBind';
import { FormGroup } from './formGroup';
import { BaseAbstract } from '../../library/basic/base.abstrat';
import callFn from '../../library/util/callFn';
/**
 * 表单项
 *
 * @export
 * @class FormControl
 */
export class FormControl extends BaseAbstract {
    constructor(options) {
        super(options);
        this.options = options;
        this.parent = null;
        this.formGroup = null;
        this.defaultOption = {
            /**
             * 导步验证等待中的信息
             */
            asyncWaitInfo: {
                errInfo: {
                    desc: '验证中，请验证无误后再次保存~',
                    type: 'asyncWaitErr'
                }
            },
            validFlowTime: 20,
            /**
             * 异步出错信息
             */
            asyncErrInfo: null
        };
        /**
         * 是否更新数据（0否1是）
         *
         * @private
         * @type {(0|1)}
         * @memberof FormControl
         */
        this.isValidStatus = 1;
        /**
         * 验证是否是否等待中（0否1是）
         *
         * @private
         * @type {(0|1)}
         * @memberof FormControl
         */
        this.isWaitStatus = 0;
        /**
         * 是否失去过焦点
         *
         * @type {boolean}
         * @memberof FormControl
         */
        this.isBlur = false;
        /**
         * 是否获取过焦点
         *
         * @type {boolean}
         * @memberof FormControl
         */
        this.isFocus = false;
        /**
         * 值是否发生过改变
         *
         * @type {boolean}
         * @memberof FormControl
         */
        this.isChange = false;
        /**
         * 表单项的值
         *
         * @type {*}
         * @memberof FormControl
         */
        this.value = null;
        /**
         * 表单项的验证结果
         *
         * @type {(IValidetorData|null|boolean)}
         * @memberof FormControl
         */
        this.valid = null;
        /**
         * 最后的验证结果是否有错误
         */
        this.isValid = false;
        this.setDefaultOptions(options);
        let _that = this;
        if (this.defaultOption && typeof this.defaultOption.defaultValue !== 'undefined') {
            _that.setValue(this.defaultOption.defaultValue);
        }
        else {
            _that.getValid();
        }
    }
    /**
     * 停止设置数据时验证节流的setTimeout
     *
     * @memberof FormControl
     */
    _stopTimeout() {
        let _that = this;
        if (typeof _that._timeout !== 'undefined') {
            clearTimeout(_that._timeout);
            _that._timeout = undefined;
        }
    }
    /**
     * 设置值
     *
     * @param {*} val 值
     * @memberof Controls
     */
    setValue(val) {
        let _that = this;
        if (_that.value !== val) {
            _that.value = val;
        }
    }
    /**
     * 获取验证结果
     *
     * @returns {(IValidetorData|null|boolean)}
     * @memberof FormControl
     */
    getValid() {
        let _that = this;
        let options = _that.getOptions();
        if (_that.isValidStatus === 0 || _that.isWaitStatus === 1) {
            return _that.valid;
        }
        _that._stopTimeout();
        _that.isValidStatus = 0;
        return eachValid(options && options.valid, 0) || null;
        function eachValid(valids, index = 0, len = 0) {
            let res = null;
            let _valid = null;
            if (valids) {
                if (valids instanceof Array) {
                    len = valids.length;
                    for (let i = index; i < len; i++) {
                        const fn = valids[i];
                        _valid = _callFn(fn);
                        index = index + 1;
                        if (_valid) {
                            break;
                        }
                    }
                }
                else {
                    _valid = _callFn(valids);
                }
            }
            if (_valid instanceof Promise) { // 处理异步
                _that.isWaitStatus = 1;
                res = options.asyncWaitInfo || null;
                _valid.then(function (info) {
                    if (info || index >= len) { // 结束验证
                        _that.valid = info || null;
                        _that.isWaitStatus = 0;
                        _that.getValid();
                    }
                    else {
                        res = eachValid(valids, index, len);
                    }
                }, () => {
                    _that.valid = options.asyncErrInfo || null;
                    _that.isWaitStatus = 0;
                    // _that.getValid();
                    _that.isValidStatus = 1; // 出错的时候把
                });
            }
            else {
                res = _valid || null;
                _that.isWaitStatus = 0;
            }
            _that.valid = res;
            return res;
        }
        /**
         * 调用验证方法
         *
         * @param {*} fn 验证方法
         */
        function _callFn(fn) {
            return callFn(fn || null, [_that.value instanceof FormGroup ? _that.value : _that]);
            // if (fn instanceof Function) {
            //     __valid = fn(_that.value instanceof FormGroup ? _that.value : _that);
            // }
            // return __valid || null;
        }
    }
    /**
     * 获取当前FormControl
     *
     * @returns {this}
     * @memberof FormControl
     */
    get() {
        return this;
    }
    /**
     * 获取值
     *
     * @returns
     * @memberof FormControl
     */
    getValue() {
        let _that = this;
        return _that.value;
    }
}
__decorate([
    VmBind({
        set(val) {
            let _that = this;
            let options = _that.getOptions();
            _that.isValidStatus = 1;
            _that.isChange = true;
            _that._stopTimeout();
            _that._timeout = setTimeout(() => {
                _that.getValid();
            }, options.validFlowTime);
        }
    })
], FormControl.prototype, "value", void 0);
__decorate([
    VmBind({
        set(val) {
            let _that = this;
            _that.isValid = !!val;
        },
        get(val) {
            let _that = this;
            let _res;
            if (_that.isValidStatus !== 0 && _that.isWaitStatus !== 1) {
                _res = _that.getValid();
            }
            return _res;
        }
    })
], FormControl.prototype, "valid", void 0);
