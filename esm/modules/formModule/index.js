import formFactory, { FormGroup, FormControl, FormArray, FormDataCache } from './formModule.imp';
import validFactory from './validate';
export default function FormFactory(config) {
    return formFactory(validFactory(config && config.validate || {}));
}
export { FormGroup, FormControl, FormArray, FormDataCache };
