/**
 * 缓存FormModule数据
 *
 * @class FormDataCache
 */
export class FormDataCache {
    /**
     * 当前数据是否为数组
     *
     * @param {(object|object[])} data
     * @returns {(boolean|undefined)}
     * @memberof FormDataCache
     */
    _isArray(data) {
        let _isArray;
        if (data) {
            _isArray = false;
            if (data instanceof Array) {
                _isArray = true;
            }
        }
        this._isArray = function () {
            return _isArray;
        };
        return _isArray;
    }
    // constructor() {}
    /**
     * 设置map列表
     * @param {Object[]} dataItem 当前缓存里的数据
     * @param {Object[]} list formArray
     * @param {IFormCacheKey} listItemKey 列表项取key的字段名或字段名列表
     * @returns {object}
     */
    _setList(dataItem, list, listItemKey) {
        let _that = this;
        let _data = dataItem || [];
        (list || []).forEach((listItem, index) => {
            let _temp = _that._setItem(_data[index], listItem, _that._getKeyItem(listItemKey, index));
            if (typeof _temp !== 'undefined') {
                _data[index] = _temp;
            }
        });
        return _data;
    }
    /**
     * 设置map项
     * @param {Object} dataItem 当前缓存里的数据项
     * @param {Object} groupData formgroup
     * @param {IFormCacheItem} listItemKey 列表项取key的字段名或字段名列表
     * @returns {Array<Object>}
     */
    _setItem(dataItem, groupData, listItemKey) {
        let _that = this;
        let key = _that._getKey(groupData, listItemKey);
        if (key) {
            dataItem = dataItem || {};
            dataItem[key] = Object.assign({}, groupData);
        }
        return dataItem;
    }
    /**
     * 获取列表map
     * @param {Object[]} dataItem 当前缓存里的数据
     * @param {Object[]} list formArray
     * @param {IFormCacheKey} listItemKey 列表项取key的字段名或字段名列表
     * @returns {Array<Object>}
     */
    _getList(dataItem, list, listItemKey) {
        let _that = this;
        let res = [];
        let _data = dataItem;
        _data && (list || []).forEach((listItem, index) => {
            let _temp = _that._getItem(_data[index], listItem, _that._getKeyItem(listItemKey, index));
            if (typeof _temp !== 'undefined') {
                res[index] = _temp;
            }
        });
        return res;
    }
    /**
     * 获取列表map
     * @param {Object} dataItem 当前缓存里的数据项
     * @param {Object} groupData formGroup的value
     * @param {IFormCacheKey} listItemKey 列表项取key的字段名或字段名列表
     * @returns {Array<Object>}
     */
    _getItem(dataItem, groupData, listItemKey) {
        let _that = this;
        let key = _that._getKey(groupData, listItemKey);
        let item = dataItem;
        let res;
        if (key && item && item[key]) {
            res = item[key];
        }
        return res;
    }
    /**
     * 获取formArray对应的key
     *
     * @param {IFormCacheKey} listItemKey
     * @param {number} index
     * @returns
     * @memberof FormDataCache
     */
    _getKeyItem(listItemKey, index) {
        let res = listItemKey;
        if (listItemKey instanceof Array) {
            res = listItemKey[index];
        }
        return res;
    }
    /**
     * 获取列表map
     * @param {Object} listItem 列表项
     * @param {Object|string} keyItem 列表项取key的字段名(string的话为keyName))
     * @returns {string[]}
     */
    _getKey(listItem, keyItem) {
        let key = '';
        let _keyItem = filterKey(keyItem);
        if (typeof _keyItem === 'string') {
            key = _keyItem;
        }
        else if (_keyItem instanceof Array) {
            _keyItem.forEach(item => {
                key = key + ((listItem && listItem[item]) || '');
            });
        }
        return key;
        /**
         * 序列化参数
         *
         * @param {*} params 参数
         * @returns {string|string[]}
         */
        function filterKey(params) {
            let res = '';
            if (params && typeof params === 'string') {
                res = [params];
            }
            else if (params instanceof Array) {
                res = params;
            }
            else if (params instanceof Object) {
                let _params = params;
                if (_params.key) {
                    res = _params.key;
                }
                else if (_params.keyName) {
                    if (typeof _params.keyName === 'string') {
                        res = [_params.keyName];
                    }
                    else if (_params.keyName instanceof Array) {
                        res = _params.keyName;
                    }
                }
            }
            return res;
        }
    }
    /**
     * 设置缓存
     *
     * @param {(object|object[])} data formGroup或formArray的value
     * @param {IFormCacheKey} listItemKey 列表项取key的字段名或字段名列表
     * @memberof FormDataCache
     */
    setCache(data, listItemKey) {
        let _that = this;
        let _isArray = _that._isArray(data);
        if (_isArray) {
            _that.__data = _that._setList(_that.__data, data, listItemKey);
        }
        else if (_isArray === false) {
            _that.__data = _that._setItem(_that.__data, data, listItemKey);
        }
    }
    /**
     * 获取缓存的数据
     *
     * @param {(object|object[])} data formGroup或formArray的value
     * @param {(IFormCacheKey|IFormCacheKey[])} listItemKey 列表项取key的字段名或字段名列表
     * @returns {(object|object[]|undefined)}
     * @memberof FormDataCache
     */
    getCache(data, listItemKey) {
        let _that = this;
        let _isArray = _that._isArray(data);
        let res;
        if (_isArray) {
            res = _that._getList(_that.__data, data, listItemKey);
        }
        else if (_isArray === false) {
            res = _that._getItem(_that.__data, data, listItemKey);
        }
        return res;
    }
    /**
     * 增加项
     * @param {number} index 增加项对应的索引号
     * @param {object} dataItem 增加项对应的索引号
     */
    addItem(index, dataItem) {
        let _that = this;
        let res;
        if (_that.__data instanceof Array) {
            res = _that.__data.splice(index + 1, 0, dataItem || {});
        }
        return res;
    }
    /**
     * 移除项
     * @param {number} index 移除项对应的索引号
     * @returns {Object} 返回整个项的map
     */
    removeItem(index) {
        let _that = this;
        let res;
        if (_that.__data instanceof Array && _that.__data[index]) {
            res = _that.__data.splice(index, 1);
        }
        return res;
    }
    /**
     * 清除缓存
     *
     * @memberof FormDataCache
     */
    clear() {
        this.__data = undefined;
    }
}
