import { FormControl } from './formControl';
import { BaseAbstract } from '../../library/basic/base.abstrat';
import callFn from '../../library/util/callFn';
export class FormGroupCore extends BaseAbstract {
    constructor(config) {
        super(config);
        this.isSend = false; // 是否在发送中
        this.setDefaultOptions(config);
    }
    /**
     * 设置验证方法
     *
     * @param {Function[]} validOption 验证方法列表
     * @memberof FormGroup
     */
    _setValidOption(validOption) {
        this._validOption = validOption;
    }
    /**
     * 验证当前FormGroup对象
     */
    getThisValid() {
        let _that = this;
        let validFn = _that._validOption;
        let _valid = null;
        if (validFn) {
            if (validFn instanceof Array) {
                validFn.forEach(fn => {
                    if (!_valid) {
                        _valid = callFn(fn, [_that]) || null;
                    }
                });
            }
            else {
                _valid = callFn(validFn, [_that]) || null;
            }
        }
        return _valid || null;
        // /**
        //  * 调用验证方法
        //  *
        //  * @param {*} fn 验证方法
        //  */
        // function callFn(fn: any) {
        //     let __valid = null;
        //     if (fn instanceof Function) {
        //         __valid = fn(_that);
        //     }
        //     return __valid || null;
        // }
    }
    /**
     * 验证数据
     * @param {IGroupOptions<FormControl>｜IGroupOptions<FormGroup>} datas FormControl或FormGroup集合
     * @memberof FormGroup
     */
    _getValid(datas) {
        // protected _getValid(datas: IGroupOptions<FormControl>|IGroupOptions<FormControl>[]|FormGroupCore<O, T>[], cb: Function) {
        let _that = this;
        return _each(datas);
        function _each(list) {
            let data;
            groupEach(list, (key, control, len) => {
                if (control) {
                    let _valid;
                    if (control instanceof FormGroupCore) {
                        _valid = control.valid;
                        if (!_valid) {
                            _valid = control.valid;
                        }
                        // if (!_valid && cb instanceof Function) {
                        //     _valid = cb(control);
                        // }
                    }
                    else if (control.value instanceof FormGroupCore) {
                        _valid = control.valid;
                        if (!_valid) {
                            _valid = control.value.valid;
                        }
                        // if (!_valid && cb instanceof Function) {
                        //     _valid = cb(control.value);
                        // }
                    }
                    else {
                        if (control instanceof FormControl && control.valid) {
                            _valid = control.valid;
                        }
                    }
                    if (_valid) {
                        data = data || ((len > 0 && []) || {});
                        data[key] = _valid;
                    }
                }
            });
            return data;
        }
    }
    /**
     * 获取值
     *
     * @protected
     * @param {(IGroupOptions<FormControl>IGroupOptions<FormGroup>)} datas FormControl或FormGroup集合
     * @returns {T}
     * @memberof FormGroupCore
     */
    _getValue(datas) {
        return _each(datas);
        function _each(_group) {
            let data = null;
            groupEach(_group, (key, control, len) => {
                if (control) {
                    data = data || ((len > 0 && []) || {});
                    if (control instanceof FormGroupCore) {
                        data[key] = control.value; // _each(control.controls);
                    }
                    else if (control.value instanceof FormGroupCore) {
                        data[key] = control.value.value; // _each(control.value.controls);
                    }
                    else {
                        data[key] = control.value;
                    }
                }
            });
            return data;
        }
    }
    /**
     * 增加或修改项
     *
     * @protected
     * @param {(IGroupOptions<FormControl>|IGroupOptions<FormControl>[]|FormControl[]|FormGroupCore<O, T>[]|any)} datas 当前的列表
     * @param {(FormControl|FormGroupCore<O, T>)} item 增加或修改项的内空
     * @param {(string|number)} [key] 增加或修改项的key
     * @param {(info: {flag: 'itemTypeErr'}) => void} [errCb] 出错的回调
     * @param {(item: any) => any} [filterItem] 是否修改成功
     * @returns {boolean}
     * @memberof FormGroupCore
     */
    _setItem(datas, item, key, errCb, filterItem) {
        let res = false;
        if (datas && (item instanceof FormGroupCore || item instanceof FormControl)) {
            let _item = callFn(filterItem, [item]);
            if (typeof _item !== 'undefined') {
                item = _item;
            }
            // if (filterItem instanceof Function) {
            //     let _item = filterItem(item);
            //     if (typeof _item !== 'undefined') {
            //         item = _item;
            //     }
            // }
            if (datas instanceof Array) {
                if (!key && key + '' !== '0') {
                    datas.push(item);
                    res = true;
                }
                else {
                    datas.splice(parseInt(key + '', 10) || 0, 0, item);
                    res = true;
                }
            }
            else if (datas instanceof Object) {
                if (typeof key !== 'undefined') {
                    datas[key] = item;
                    res = true;
                }
                else {
                    console.error('请传入key');
                }
            }
            else {
                console.error('表单对象类型不正确');
            }
        }
        else {
            if (!datas) {
                console.error('表单对象不存在');
            }
            else {
                callFn(errCb, [{
                        flag: 'itemTypeErr'
                    }]);
                // if (errCb instanceof Function) {
                //     errCb({
                //         flag: 'itemTypeErr'
                //     });
                // }
            }
        }
        return res;
    }
    /**
     * 移除项
     *
     * @protected
     * @param {IGroupOptions<FormControl>|IGroupOptions<FormControl>[]|FormGroup[]} datas 数据集
     * @param {(string|number)} key 移除项的key
     * @returns {boolean} 是否删除成功
     * @memberof FormGroupCore
     */
    _removeItem(datas, key) {
        let res = false;
        if (datas && typeof key !== 'undefined') {
            if (datas instanceof Array) {
                key = parseInt(key + '', 10) || 0;
                if (datas.length > key) {
                    datas.splice(key, 1);
                    res = true;
                }
            }
            else if (datas instanceof Object) {
                if (datas[key]) {
                    delete datas[key];
                    res = true;
                }
            }
        }
        return res;
    }
}
/**
 * 遍历表单数据
 *
 * @private
 * @param {(key: string, control: FormControl) => {}} cb
 * @memberof FormGroup
 */
function groupEach(_group, cb) {
    if (_group) {
        for (const key in _group) {
            if (_group.hasOwnProperty(key)) {
                const control = _group[key];
                if (control && (control instanceof FormControl || control instanceof FormGroupCore)) {
                    callFn(cb, [key, control, (_group instanceof Array) && _group.length || false]);
                    // if (cb instanceof Function) {
                    //     cb(key, control, (_group instanceof Array) && _group.length || false);
                    // }
                }
            }
        }
    }
}
