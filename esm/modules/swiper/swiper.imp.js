///<reference path="./interface/swiper.d.ts" />
import Swiper from 'swiper/dist/js/swiper.js';
// import { Swiper, Navigation, Pagination, Scrollbar } from 'swiper/dist/js/swiper.esm.js';
// Swiper.use([Swiper, Navigation, Pagination, Scrollbar]);
export default function (el, options) {
    return new Swiper(el, options);
}
