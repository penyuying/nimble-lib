import swiperProxy from './swiperProxy';
import swiperFactory from './swiper.imp';
/**
 * 实例化工厂方法
 *
 * @export
 * @param {IAeAnimationConfig} options 配置选项
 * @returns {AeAnimation}
 */
export default function (el, options) {
    return swiperProxy(() => {
        return new Promise((resolve, reject) => {
            let _res = swiperFactory(el, options);
            resolve(_res);
        });
    });
}
