import getBackData from '../../../../library/util/getBackData';
import callFn from '../../../../library/util/callFn';
import MyError from '../../../../library/util/MyError';
import { LOCATE_DATA_TYPE } from '../../constant';
import { LocatePart } from './LocatePart.base';
/**
 * api定位
 *
 * @export
 * @class ApiLocate
 * @extends {LocatePart}
 */
export class ApiLocate extends LocatePart {
    constructor(options) {
        super(options);
        this._cacheLocate = {}; // 缓存查询过的数据
        this.setDefaultOptions(options);
    }
    /**
     * 根据经纬度获取地理位置
     *
     * @param {*} options 选项
     * @returns {Promise}
     * @memberof BdLocate
     */
    getLocate(options) {
        let _that = this;
        const _opts = _that.getOptions(options || {});
        let key = _opts.longitude + '_' + _opts.latitude;
        let res = _that._cacheLocate[key] || new Promise((resolve, reject) => {
            _that._fromLocate(options).then((_res) => {
                _that._getLocate(_res).then(resolve, errorCb);
            }, errorCb);
            /**
             *  出错的回调
             * @param {*} err 错误对象
             */
            function errorCb(err) {
                delete _that._cacheLocate[key];
                reject(err);
            }
        });
        _that._cacheLocate[key] = res;
        return res;
    }
    /**
     * 获取数据
     *
     * @param {*} type 类型
     * @param {*} param 参数
     * @returns {Promise}
     * @memberof CarCore
     */
    _getData(type, param) {
        let _that = this;
        let _options = _that.defaultOption;
        return getBackData(_options.getLocateData, [type, param]);
    }
    /**
     * 转换经纬度（百度转高德）
     * @param {Object} options 经纬度
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @returns {Promise} 返回链接
     */
    _fromLocate(options) {
        const _that = this;
        return new Promise((resolve, reject) => {
            options = options || {};
            const _lat = options.latitude || (options.latitude + '') === '0' ? options.latitude : '';
            const _lng = options.longitude || (options.longitude + '') === '0' ? options.longitude : '';
            if (_lat && _lng) {
                _that._getData(LOCATE_DATA_TYPE.CONVERT_BAIDU, {
                    longitude: _lng,
                    latitude: _lat
                }).then((back) => {
                    let _data = (back && back.result && back.result[0]) || back || {};
                    if ((_data.x || _data.x + '' === '0') && (_data.y || _data.y + '' === '0')) {
                        let res = null;
                        res = {
                            longitude: _data.x,
                            latitude: _data.y
                        };
                        resolve(res);
                    }
                    else {
                        reject(new Error('Api fromLocate error'));
                    }
                }, reject);
            }
            else {
                reject(new Error('Api fromLocate error'));
            }
        });
    }
    /**
     * 根据百度经纬度获取地理位置
     * @param {Object} options 经纬度
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @returns {Promise} 返回地理位置
     */
    _getLocate(options) {
        const _that = this;
        const _opts = _that.getOptions(options);
        return new Promise((resolve, reject) => {
            // 经纬度是否存在
            let isLngLat = options && (options.longitude || options.longitude + '' === '0') &&
                (options.latitude || options.latitude + '' === '0');
            if (!isLngLat) {
                reject(new Error('Api getLocate error'));
                return;
            }
            _that._getData(LOCATE_DATA_TYPE.SELECT_MAP, options).then(data => {
                let res = null;
                const _data = data && data.result;
                if (_data) {
                    const _addrComp = _data.address_component || {};
                    const _formatAddr = _data.formatted_addresses;
                    const _Locate = _data.location || {};
                    res = {
                        latitude: _Locate.lat,
                        longitude: _Locate.lng,
                        province: _addrComp.province || _addrComp.ad_level_1,
                        city: _addrComp.city || _addrComp.ad_level_2,
                        district: _addrComp.district || _addrComp.ad_level_3,
                        address: (_formatAddr && _formatAddr.recommend) || _formatAddr,
                        info: (_formatAddr && _formatAddr.recommend) || _formatAddr
                    };
                }
                else if (data) {
                    if (data.message && data.status) {
                        reject(new MyError(data.message, {
                            status: data.status
                        }));
                        return;
                    }
                }
                res = callFn(_opts.filterLocateData, [res]);
                if (res) {
                    resolve(res);
                }
                else {
                    reject(new Error('Api getLocate emtpy'));
                }
            }, reject);
        });
    }
}
let _locate;
/**
 * 实例化api定位
 *
 * @export
 * @param {*} options 选项
 * @returns {ApiLocate}
 */
export default function apiLocateFactory(options) {
    _locate = _locate || new ApiLocate(options);
    return _locate;
}
