import { BaseAbstract } from '../../../../library/basic/base.abstrat';
import { DEFAULT_CONFIG } from '../../constant';
export class LocatePart extends BaseAbstract {
    constructor(options) {
        super(options);
        this.defaultOption = DEFAULT_CONFIG; // 默认选项
        this.setDefaultOptions(options);
    }
    /**
     * 唤起微信导航
     * @param {Object} options 地理位置信息
     * @returns {Promise}
     */
    showMap(options) {
        throw new Error('showMap not implemented.');
    }
    /**
     * 获取跳转原生的参数
     *
     * @param {ILocateConfig} options 选项
     * @returns {(Promise<string|null>)}
     * @memberof ILocatePart
     */
    getCheckParam(options) {
        throw new Error('getCheckParam not implemented.');
    }
    /**
     * 获取定位
     *
     * @param {ILocateConfig} options 选项
     * @returns {Promise<ILocateData>}
     * @memberof ILocatePart
     */
    getLocate(options) {
        throw new Error('getLocate not implemented.');
    }
    /**
     * 根据IP获取定位
     *
     * @param {ILocateConfig} options 选项
     * @returns {Promise<ILocateData>}
     * @memberof ILocatePart
     */
    getIpLocate(options) {
        throw new Error('getIpLocate not implemented.');
    }
    /**
     * 导航
     *
     * @param {ILocateConfig} options 选项
     * @param {boolean} isNative 是否调用原生
     * @returns {Promise<any>}
     * @memberof ILocatePart
     */
    navigation(options, isNative) {
        throw new Error('navigation not implemented.');
    }
}
