import serializeQueryParams from '../../../../library/util/serializeQueryParams';
import jsonp from '../../../../library/util/jsonp';
import { MAP_APP_API, MAP_SDK, EVENT_TYPE } from '../../constant';
import { LocatePart } from './LocatePart.base';
/**
 * 高德定位
 *
 * @export
 * @class GdLocate
 * @extends {LocatePart}
 */
export class GdLocate extends LocatePart {
    constructor(options) {
        super(options);
        this.setDefaultOptions(options);
    }
    /**
     * 获取查询是否安装高德地图app的参数
     *
     * @param {*} options 选项
     * @returns {Promise}
     * @memberof GdLocate
     */
    getCheckParam(options) {
        let _that = this;
        let _opts = _that.getOptions(options);
        let device = _opts.device || {};
        return new Promise((resolve, reject) => {
            let res = null;
            if (device.android) {
                res = 'com.autonavi.minimap';
            }
            else if (device.ios) {
                res = 'iosamap://';
            }
            resolve(res);
        });
    }
    /**
     * 唤起高德导航
     * @param {Object} options 地理位置信息
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @param {String} options.toLatitude 导航去的纬度
     * @param {String} options.toLongitude 当前经度
     * @param {String} options.city 当前市
     * @param {String} options.province 当前省
     * @param {String} options.name 地理位置名
     * @param {String} options.address 地址
     * @param {Boolean} isNative 是否跳转原生
     * @returns {Promise}
     */
    navigation(options, isNative) {
        let _that = this;
        let opts = _that.getOptions(options);
        return new Promise((resolve, reject) => {
            _that._fromLocate({
                latitude: options.toLatitude,
                longitude: options.toLongitude
            }).then(res => {
                let _to = {
                    toLatitude: res.latitude,
                    toLongitude: res.longitude
                };
                _that._fromLocate(_that.getOptions(options || {}, opts)).then(from => {
                    _toLink(_to, from);
                }, () => {
                    _toLink(_to, opts);
                });
                /**
                 * 跳转链接
                 * @param {*} data to
                 * @param {*} from from
                 */
                function _toLink(data, from) {
                    if (data) {
                        // if (from.latitude) {
                        //     data.latitude = from.latitude;
                        // }
                        // if (from.longitude) {
                        //     data.longitude = from.longitude;
                        // }
                        _that._getLink(_that.getOptions(options || {}, data), isNative).then(link => {
                            if (link && typeof link === 'string') {
                                _that.$emit(EVENT_TYPE.NAVIGATION, {
                                    link: link
                                });
                                resolve({
                                    type: 'success'
                                });
                                window.location.href = link;
                            }
                            else {
                                reject(new Error('link error'));
                            }
                        }, reject);
                    }
                    else {
                        reject(new Error('latitude or longitude error'));
                    }
                }
            }, reject);
        });
    }
    /**
     * 转换经纬度（百度转高德）
     * @param {Function} options 配置参数
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @param {String} options.toLatitude 导航去的纬度
     * @param {String} options.toLongitude 当前经度
     * @returns {Promise} 返回链接
     */
    _fromLocate(options) {
        let _that = this;
        return new Promise((resolve, reject) => {
            options = options || {};
            const _lat = options.latitude || (options.latitude + '') === '0' ? options.latitude : '';
            const _lng = options.longitude || (options.longitude + '') === '0' ? options.longitude : '';
            let _opts = _that.getOptions(options || {}, {
                gdFromLocateConfig: {
                    locations: `${_lng},${_lat}`
                }
            });
            let _params = _opts.gdFromLocateConfig || {};
            let _url = MAP_SDK.GD_FROM_LOCATE + serializeQueryParams(_params, '?');
            jsonp(_url).then(_res => {
                const _coverData = _res && _res.locations;
                let _locations = (_coverData || '') + '';
                let [lng, lat] = _locations.split(',');
                let _lng = parseFloat(lng);
                let _lat = parseFloat(lat);
                resolve({
                    longitude: (_lng && parseFloat(_lng.toFixed(6))) || 0,
                    latitude: (_lat && parseFloat(_lat.toFixed(6))) || 0
                });
            }, reject);
        });
    }
    /**
     * 获取链接
     * @param {Function} options 定位信息
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @param {String} options.toLatitude 导航去的纬度
     * @param {String} options.toLongitude 当前经度
     * @param {String} options.city 当前市
     * @param {String} options.province 当前省
     * @param {Boolean} isNative 是否跳转原生
     * @returns {Promise} 返回链接
     */
    _getLink(options, isNative) {
        const _that = this;
        return new Promise((resolve, reject) => {
            let _opts = _that.getOptions(options);
            let device = _opts.device || {};
            let res = '';
            const lat = _opts.latitude || '';
            const lng = _opts.longitude || '';
            const _lat = _opts.toLatitude || '';
            const _lng = _opts.toLongitude || '';
            let _params;
            if (isNative) {
                if (device.android) {
                    res = MAP_APP_API.GD_ANDROID;
                }
                else if (device.ios) {
                    res = MAP_APP_API.GD_IOS;
                }
            }
            let _config = _opts.locateToConfig || {};
            if (res) {
                _params = {
                    sourceApplication: _config.tag || '',
                    lat: _lat,
                    lon: _lng,
                    style: '2',
                    // 线路规划参数
                    sid: 'BGVIS1',
                    slat: lat,
                    slon: lng,
                    sname: _config.curTag,
                    did: 'BGVIS2',
                    dlat: _lat,
                    dlon: _lng,
                    dname: _config.shopTag,
                    dev: 0,
                    t: 0
                };
            }
            else {
                _params = {
                    from: `${lng},${lat},${_config.curTag || ''}`,
                    to: `${_lng},${_lat},${_config.shopTag || ''}`,
                    mode: 'car',
                    policy: '1',
                    src: 'mypage',
                    coordinate: 'gaode',
                    callnative: '0'
                };
                res = MAP_APP_API.GD_H5;
            }
            res = res + serializeQueryParams(_params, '?');
            resolve(res);
        });
    }
}
let _locate;
/**
 * 实例化高德地图
 *
 * @export
 * @param {*} options 选项
 * @returns {GdLocate}
 */
export default function gdLocateFactory(options) {
    _locate = _locate || new GdLocate(options);
    return _locate;
}
