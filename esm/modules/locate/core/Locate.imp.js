import { LocateCore, LocateBase } from './Locate.core';
import { MAP_TYPE } from '../constant';
import extend from '../../utils/extend';
import getBackData from '../../../library/util/getBackData';
import callFn from '../../utils/callFn';
export class Locate extends LocateCore {
    constructor(options) {
        super(options);
        this.name = 'Locate';
        this._locatePromise = {}; // 缓存地理位置的Promise
        this._cacheTimeout = 0; // 缓存获取时的时间
        const _that = this;
        _that.setDefaultOptions(options);
    }
    /**
     * 切换城市
     *
     * @param {*} selectCity 需要切换的城市
     * @param {*} options 选项
     * @returns {Promise}
     * @memberof Locate
     */
    selectCity(selectCity, options) {
        let _that = this;
        return new Promise((resolve, reject) => {
            // 这里需要取实时的
            let res = _that._getLocate(options);
            res.then(back => {
                resolveCb(back);
            }, () => {
                resolveCb(null);
            });
            /**
             *  切换地址
             *
             * @param {(ILocateData|null)} cLocate
             */
            function resolveCb(cLocate) {
                _that._switchLocate(cLocate, selectCity, Object.assign({}, { switchLocate: 1 }, options)).then(res => {
                    _that._successfilter(res, options).then(back => {
                        resolve(back);
                    }, reject);
                }, reject);
            }
        });
    }
    /**
     * 唤起百度地图
     * @param {Object} options 地理位置信息
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @param {String} options.name 地理位置名
     * @param {MAP_TYPE} type 类型
     * @returns {Promise}
     */
    showMap(options, type) {
        const _that = this;
        if (type) {
            return _that._showMap(options, type);
        }
        let _opts = _that.getOptions(options);
        let device = _opts.device || {};
        return new Promise((resolve, reject) => {
            if (device.isWeixin) {
                _that._showMap(options, MAP_TYPE.WX).then(res => {
                    const _type = res && res.type;
                    if (_type === 'cancel' || _type === 'fail') {
                        _that._showMap(options, MAP_TYPE.BAIDU).then(resolve, reject);
                    }
                    else {
                        resolve(res);
                    }
                }, reject);
            }
            else {
                _that.getLocalLocate(options).then(res => {
                    _that._showMap(extend(true, res, options), MAP_TYPE.BAIDU).then(resolve, reject);
                });
            }
        });
    }
    /**
     * 获取本地地理位置
     * @param {Object} options 可选参数
     * @returns {Promise}
     * @memberof Locate
     */
    getLocalLocate(options) {
        const _that = this;
        let cRes = _that._getCurLocate(options);
        return new Promise((resolve, reject) => {
            cRes.then(res => {
                if (res) {
                    _that._successfilter(res, options).then((back) => {
                        resolve(back);
                    });
                }
                else {
                    resolve(res || null);
                }
            }, reject);
        });
    }
    /**
     * 定位（百度和api已有经纬度）
     * @param {Object} options 可选参数
     * @param {Number} options.timeout 获取超时时间
     * @param {LOCATE_TYPE} type 类型
     * @returns {Promise}
     * @memberof Locate
     */
    getCurLocate(options, type) {
        const _that = this;
        let cRes = _that._getCurLocate(options);
        return new Promise((resolve, reject) => {
            cRes.then(res => {
                if (res) {
                    _that._successfilter(res, options).then(back => {
                        resolve(back);
                    });
                }
                else {
                    _that.getLocate(options, type, true).then(resolve, reject);
                }
            }, () => {
                _that.getLocate(options, type, true).then(resolve, reject);
            });
        });
    }
    /**
     * 定位（百度和api已有经纬度）
     * @param {Object} options 可选参数
     * @param {Boolean} options.isReset 是否强制重新获取定位信息
     * @param {Number} options.timeout 获取超时时间
     * @param {LOCATE_TYPE} type 类型
     * @param {LOCATE_TYPE} isNotCLocate 不取本地的数据
     * @returns {Promise}
     * @memberof Locate
     */
    getLocate(options, type, isNotCLocate) {
        const _that = this;
        const _opts = _that.getOptions(options);
        const _time = Date.now();
        const _cTime = _opts.cacheTimeout || 0;
        const _key = '_switchLocate_' + (_opts.switchLocate || 0);
        _that._locatePromise = _that._locatePromise || {};
        if (_opts.isReset || (_cTime && (_time - _that._cacheTimeout > _cTime))) {
            _that._locatePromise[_key] = null;
            _that._initData(options);
        }
        if (!isNotCLocate && !_opts.isReset && _that._cacheLocate) {
            return new Promise((resolve, reject) => {
                resolve(_that._cacheLocate);
            });
        }
        // tslint:disable-next-line:one-variable-per-declaration
        let _res = !isNotCLocate && _that._locatePromise[_key] || new Promise((resolve, reject) => {
            _that._cacheTimeout = _time;
            let _attempt = _opts && _opts.attempt || 0;
            handle(0);
            /**
             * 执行获取地理位置操作
             */
            function handle(count) {
                let _promise = _getlocate();
                _promise.then(res => {
                    if (res === null) {
                        attemptFilter(_switchLocate, count + 1, res);
                    }
                    else {
                        _switchLocate(res);
                    }
                }, err => {
                    attemptFilter(_errorFilter, count + 1, err);
                });
            }
            /**
             * 尝试请求
             *
             * @param {(data: any) => void} cb 回调
             * @param {number} count 当前第几次尝试
             * @param {*} data 当前的数据
             */
            function attemptFilter(cb, count, data) {
                if (_attempt >= count) {
                    getBackData(_opts.attemptFilter, [count, _opts]).then((back) => {
                        if (back) {
                            handle(count);
                        }
                        else {
                            callFn(cb, [data]);
                        }
                    });
                }
                else {
                    callFn(cb, [data]);
                }
            }
            /**
             * 定位失败时的过滤方法
             *
             * @param {*} err 错误信息
             */
            function _errorFilter(err) {
                _that._locatePromise[_key] = null;
                reject(err);
            }
            /**
             * 切换位置信息
             * @param {{locateInfo: ILocateData|null; cLocateInfo ?: ILocateData | null;}} data 定位数据
             */
            function _switchLocate(data) {
                data = data || {};
                let back = data.locateInfo;
                let cBack = data.cLocateInfo;
                if (cBack && back) {
                    _that._switchLocate(back, cBack).then(_successFilter, (err) => {
                        _that._locatePromise[_key] = null;
                        reject(err);
                    });
                }
                else {
                    _successFilter(cBack || back || null);
                }
            }
            /**
             * 获取完定位时的数据过滤
             *
             * @param {(ILocateData|null)} locateInfo 定位信息
             */
            function _successFilter(locateInfo) {
                _that._successfilter(locateInfo, options, !_opts.isCommit).then(_locate => {
                    _that._locatePromise[_key] = null;
                    resolve(_locate);
                }, err => {
                    _that._locatePromise[_key] = null;
                    reject(err);
                });
            }
            /**
             * 获取地理位置
             * @return {Promise<ILocateData|null>}
             */
            function _getlocate() {
                return Promise.race([new Promise((_resolve, reject) => {
                        !isNotCLocate && _opts.timeout && setTimeout(() => {
                            _that._getCurLocate(options).then((res) => {
                                _resolve({
                                    locateInfo: null,
                                    cLocateInfo: res
                                });
                            }, reject);
                        }, _opts.timeout);
                    }), getLocate()]);
            }
        });
        return _res;
        /**
         * 获取地理位置
         *
         * @returns {Promise}
         */
        function getLocate() {
            return new Promise((resolve, reject) => {
                let res = _that._getLocate(options, type);
                res.then(back => {
                    _getCLocate(back);
                }, () => {
                    _getCLocate(null);
                });
                /**
                 * 获取本地地理位置
                 *
                 * @param {*} back 当前地理位置
                 */
                function _getCLocate(back) {
                    if (!isNotCLocate && (_opts.checkLocate || !back)) {
                        let cRes = _that._getCurLocate(options);
                        cRes.then(cBack => {
                            resolve({
                                locateInfo: back,
                                cLocateInfo: cBack
                            });
                        }, () => {
                            resolve({
                                locateInfo: back
                            });
                        });
                    }
                    else {
                        resolve({
                            locateInfo: back
                        });
                    }
                }
            });
        }
    }
    /**
     * 唤起导航和地图（微信、百度、高德）
     * @param {Object} options 地理位置
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @param {String} options.name 地理位置名
     * @param {String} options.address 地址
     * @param {MAP_TYPE} type 类型
     * @returns {Promise}
     */
    locateTo(options, type) {
        const _that = this;
        console.warn('locate的locateTo方法将要废弃，请使用showMap');
        return _that.showMap(options, type);
    }
    /**
     * 显示导航列表
     *
     * @param {Object} options 地理位置信息
     * @param {String} options.latitude 纬度
     * @param {String} options.longitude 经度
     * @param {String} options.fromLatitude 当前纬度
     * @param {String} options.fromLongitude 当前经度
     * @param {String} options.fromCity 当前经度
     * @param {String} options.fromProvince 当前省
     * @param {Boolean} isNative 是否跳转原生
     * @returns {Promise}
     * @memberof Locate
     */
    nativeMap(options, isNative) {
        console.warn('nativeMap将废弃，请使用showNavigation');
        return this.showNavigation(options, isNative);
    }
}
/**
 * 实例化工厂方法
 *
 * @export
 * @param {ILocateConfig} options 配置选项
 * @returns {Locate}
 */
export default function (options) {
    return Locate.instance(options);
}
export { LocateCore, LocateBase };
