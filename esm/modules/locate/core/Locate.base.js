import getBackData from '../../../library/util/getBackData';
import callFn from '../../../library/util/callFn';
import { BaseAbstract } from '../../../library/basic/base.abstrat';
import { DEFAULT_CONFIG, NAVIGATION_TYPE } from '../constant';
export class LocateBase extends BaseAbstract {
    constructor(options) {
        super(options);
        /**
         * 默认配置信息
         * @memberof LocateBase
         */
        this.defaultOption = DEFAULT_CONFIG;
        this.isCheckLocate = null; // 记录上次是否要切换地理位置
        this._nowLocate = null; // 当前的已存在的信息
        this._cacheLocate = null; // 缓存的信息
        const _that = this;
        _that.setDefaultOptions(options);
    }
    /**
     * 初始化数据
     *
     * @param {*} options 选项
     * @memberof LocateBase
     */
    _initData(options) {
        let _that = this;
        if (options && typeof options.checkLocate === 'undefined') {
            _that.isCheckLocate = null;
        }
        _that._cacheLocate = null;
    }
    /**
     * 地图事件
     *
     * @param {*} type 事件类型
     * @param {*} evt 事件对象
     * @memberof LocateCore
     */
    mapEvent(type, evt) {
        this.$emit(type, evt);
    }
    /**
     * 提示消息
     *
     * @param {*} msgCont 消息内容
     * @param {*} param 类型|参数
     * @returns {Promise}
     * @memberof CarBase
     */
    message(msgCont, param) {
        let _that = this;
        let _options = _that.defaultOption;
        return getBackData(_options.message, [msgCont, param]);
    }
    /**
     * 调用原生
     * @param {String} type 原生接口名称
     * @param {Object} params 接口请求参数
     * @returns {Promise<Object|String>} 结果数据
     * @memberof LocateBase
     */
    _toNative(type, params) {
        const _that = this;
        const _options = _that.getOptions();
        return getBackData(_options.actionWithNative, [type, params]);
    }
    /**
     * 是否调用原生
     *
     * @param {String} type 调用类型
     * @return {Boolean}
     * @memberof LocateBase
     */
    _isToNative(type) {
        const _that = this;
        const _options = _that.getOptions();
        let res = callFn(_options && _options.isToNativeFilter || false, [_options, type]);
        return res;
    }
    /**
     * 获取跳转原生导航列表
     *
     * @returns {Promise}
     * @memberof LocateBase
     */
    _getNavigationList() {
        const _that = this;
        const _options = _that.getOptions();
        return new Promise((resolve, reject) => {
            let list = [[{
                        text: '高德地图',
                        className: 'th_modal-button th_bold',
                        type: NAVIGATION_TYPE.GD
                    }, {
                        text: '百度地图',
                        className: 'th_modal-button th_bold',
                        type: NAVIGATION_TYPE.BAIDU
                    }], [{
                        className: 'th_modal-button th_bold',
                        text: '取消'
                    }]];
            getBackData(_options.navigationList || list, [list]).then(resolve, reject);
        });
    }
}
