import { ClassProxy } from '../../library/util/ClassProxy';
import callFn from '../utils/callFn';
import mountService from '../helpers/mountService';
/**
 * 实例化获取地理位置
 *
 * @export
 * @param {*} options 配置参数
 * @returns {Locate}
 */
export default function locateFactory(options) {
    let _proxy = new ClassProxy();
    let res = {
        name: 'Locate',
        preload() {
            let _that = this;
            let initRes = _proxy.initProxy(() => {
                return new Promise((resolve, reject) => {
                    import(/* webpackChunkName: "_locate_" */ './core/Locate.imp').then((back) => {
                        let locate = back && back.default;
                        let _res = locate(options);
                        callFn(_that._callInstall, [(...args) => {
                                _res._getParent = () => {
                                    return _that._getParent();
                                };
                                callFn(_res.install, args, _res);
                            }], _that);
                        resolve(_res || {});
                    }, reject);
                });
            }, 'preload')();
            _that.preload = () => {
                return initRes;
            };
            return initRes;
        },
        install(vue, opts) {
            mountService(vue, this);
            this._callInstall = function (cb) {
                callFn(cb, [vue, opts]);
            };
        }
    };
    _proxy.proxyHook(res, () => {
        return res.preload();
    }, [
        'showNavigation',
        'selectCity',
        'showMap',
        'getLocalLocate',
        'getCurLocate',
        'getLocate',
        'locateTo',
        'nativeMap',
        'setDefaultOptions',
        'handlerLocate',
        'mapEvent'
    ], [
        '$on',
        '$off',
        '$emit',
        '$once'
    ]);
    return res;
}
