import jsonp from '../../library/util/jsonp';
import deviceInfo from '../../library/util/browser/deviceInfo';
import lStorage from '../../library/util/lStorage';
import nativeDeviceFilter from '../../library/util/nativeDeviceFilter';
import callFn from '../../library/util/callFn';
import filterLocateData from './helpers/filterLocateData';
import diffData from '../../library/util/diffData';
/**
 * 导航类型
 */
export const NAVIGATION_TYPE = {
    BAIDU: 'baidu',
    GD: 'gd' // 高德
};
/**
 * 默认配置信息
 * @exports
 */
export const DEFAULT_CONFIG = {
    isNativeNavigation: true,
    // latitude: null, // 导航的纬度
    // longitude: null, // 导航的经度
    // fromLatitude: null, // 当前纬度
    // fromLongitude: null, // 当前经度
    // fromCity: null, // 当前经度
    // fromProvince: null, // 当前省
    // name: null, // 地理位置名
    // address: null, // 地址
    switchLocate: 0,
    cacheTimeout: 1000 * 60 * 5,
    timeout: 1000 * 10,
    attempt: 1,
    attemptFilter: true,
    isCommit: true,
    checkLocate: false,
    isReset: false,
    device: deviceInfo,
    isNative: true,
    actionWithNative: undefined,
    storage: lStorage,
    diffData: diffData,
    // handlerWxApi: null, // 调用微信api
    filterLocateData: filterLocateData,
    jsonp: jsonp,
    successfilter: (locate, _options) => {
        let res = locate;
        if (_options && !callFn(_options.filterLocateData, [locate])) {
            res = callFn(_options.filterLocateData, [_options]);
        }
        return res;
    },
    diffLocateKeys: ['province', 'latitude', 'longitude', 'city', 'district', 'info', 'address'],
    // defaultLocate: { // 获取定位失败时的默认定位信息
    // },
    wxConfig: {
        type: '3',
        output: 'jsonp',
        key: 'FQBBZ-RC5K4-E4CU3-XMZ43-4QEHZ-EZB5X'
    },
    baiduConfig: {
        v: '2.0',
        ak: 'w742K60CZ4fOaGEQ3n6HeB05'
    },
    gdFromLocateConfig: {
        coordsys: 'baidu',
        output: 'JSON',
        key: 'e1a4e5f544003203fdb8ddb9f0f8062f'
    },
    isToNativeFilter: (options, type) => {
        let res = nativeDeviceFilter(options, options && options.device, type);
        return res;
    },
    locateToConfig: {
        curTag: '我的位置',
        shopTag: '途虎门店',
        tag: '途虎养车' // 标签名称
    }
    // navigationList: null // 过滤跳转第三方导航app列表
};
/**
 * 第三方地图SDK
 */
export var MAP_SDK;
(function (MAP_SDK) {
    MAP_SDK["BAIDU_API"] = "https://api.map.baidu.com/api";
    MAP_SDK["WX_API"] = "https://apis.map.qq.com/ws/coord/v1/translate";
    MAP_SDK["GD_FROM_LOCATE"] = "https://restapi.amap.com/v3/assistant/coordinate/convert"; // 高德转绝经纬度
})(MAP_SDK || (MAP_SDK = {}));
// 调用第三方地图app
export var MAP_APP_API;
(function (MAP_APP_API) {
    MAP_APP_API["BAIDU_ANDROID"] = "baidumap://map/direction";
    MAP_APP_API["BAIDU_IOS"] = "androidamap://navi";
    MAP_APP_API["BAIDU_H5"] = "https://api.map.baidu.com/direction";
    // GD_ANDROID: 'androidamap://navi', // 高德安卓导航
    MAP_APP_API["GD_ANDROID"] = "amapuri://route/plan/";
    // GD_IOS: 'iosamap://navi', // 高德IOS导航
    MAP_APP_API["GD_IOS"] = "iosamap://path";
    MAP_APP_API["GD_H5"] = "https://uri.amap.com/navigation"; // 高德H5
})(MAP_APP_API || (MAP_APP_API = {}));
/**
 * 地图地理类型
 */
export const MAP_TYPE = {
    BAIDU: 'baidu',
    WX: 'wx' // 微信
};
/**
 * 地图地理位置类型
 */
export const LOCATE_TYPE = {
    BAIDU: 'baidu',
    API: 'api',
    WX: 'wx' // api
};
/**
 * 获取接口类型
 * @exports
 */
export const LOCATE_DATA_TYPE = {
    CONVERT_BAIDU: 'convertBaidu',
    SELECT_MAP: 'selectMap' // 根据经纬度获取地理位置
};
/**
 * 调用app的api
 */
export const APP_API_TYPE = {
    LOCATE: 'locate',
    LOCAL_LOCATE: 'localLocate',
    UPDATE_LOCATE: 'updateLocate',
    CHECK_THIRD_APP: 'checkThirdApp' // 检查第三方app x
};
/**
 * 事件类型
 */
export const EVENT_TYPE = {
    CLICK_NAVIGATION: 'clickNavigation',
    SUCCESS: 'success',
    TO_MAP: 'toMap',
    SHOW_NAVIGATION: 'showNavigation',
    CHECK_NAVIGATION: 'checkNavigation',
    CLOSE_MAP: 'closeMap',
    ERROR: 'error',
    NAVIGATION: 'navigation' // 跳转导航
};
/**
 * 本地存储
 * @exports
 */
export var STORAGE_KEY;
(function (STORAGE_KEY) {
    /**
     * 存储用户保存的地址信息
     */
    STORAGE_KEY["LOCATE_USER_SELECTED"] = "_locate_user_selected";
})(STORAGE_KEY || (STORAGE_KEY = {}));
