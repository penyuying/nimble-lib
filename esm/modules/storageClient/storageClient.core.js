import { MemoryStorage } from './memoryStorage';
import { storageJudge } from '../../library/util/storageJudge/storageJudge';
import { BaseAbstract } from '../../library/basic/base.abstrat';
import getBLen from '../../library/util/getBLen';
/**
 * 本地存储核心类
 *
 * @export
 * @abstract
 * @class StorageCore
 * @extends {BaseAbstract<any>}
 * @implements {IStorageInterface}
 */
export class StorageCore extends BaseAbstract {
    /**
     * 本地存储核心类
     * @param {IStorageInitConfig} config 默认参数
     * @memberof StorageCore
     */
    constructor(config) {
        super(config);
        /**
         * 判断浏览是否支持storage
         *
         * @private
         * @type {StorageJudge}
         * @memberof StorageCore
         */
        this.storageJudge = storageJudge;
        /**
         * 默认选项
         *
         * @protected
         * @type {IStorageInitConfig}
         * @memberof StorageCore
         */
        this.defaultOption = {};
        this.setDefaultOptions(config);
    }
    /**
     * 获取Storage对象(对Storage做兼容和缓存处理)
     *
     * @private
     * @param {string} key 存储类型的KEY
     * @param {string} isStorageType 存放的类型（s存session；l存local；其它为暂存）
     * @returns {SStorage}
     * @memberof SStorageService 返回HttpStorage对象
     */
    getStorage(key, isStorageType) {
        let res;
        let _that = this;
        let _cacheKey = key;
        let _objKey = _cacheKey + '_' + (isStorageType || 'd');
        let _cacheStorage = StorageCore._cacheStorage = StorageCore._cacheStorage || new MemoryStorage('_get-new-storage_');
        res = _cacheStorage.getItem(_objKey);
        if (!res) {
            if (isStorageType === 's') { // 存session
                StorageCore.isSess = _that.storageJudge.isSessionStorage();
                if (StorageCore.isSess) {
                    res = window.sessionStorage;
                }
            }
            else if (isStorageType === 'l') { // 存local
                StorageCore.isLocal = _that.storageJudge.isLocalStorage();
                if (StorageCore.isLocal) {
                    res = window.localStorage;
                }
            }
            else { // 暂存
                res = new MemoryStorage(_cacheKey);
            }
            if (!res) {
                res = new MemoryStorage(_cacheKey);
            }
            _cacheStorage.setItem(_objKey, res);
        }
        return res;
    }
    /**
     * 更新数据
     *
     * @private
     * @param {(MemoryStorage|Storage)} storage SStorage|Storage类型
     * @param {string} key 缓存的Key名
     * @param {*} value 存储的值
     * @param {string} [isStorageType] 存放的类型（s存session；l存local；其它为暂存）
     * @memberof StorageCore
     */
    updateStorageItem(storage, key, value, isStorageType) {
        if (value instanceof Object) {
            if (storage instanceof MemoryStorage) {
                storage.setItem(key, value);
            }
            else if (isStorageType === 's' && StorageCore.isSess || isStorageType === 'l' && StorageCore.isLocal) {
                storage.setItem(key, JSON.stringify(value));
            }
        }
    }
    /**
     * 保存数据
     *
     * @protected
     * @template T
     * @param {T} storage SStorage|Storage类型
     * @param {string} key 缓存的Key名
     * @param {*} value 存储的值
     * @param {string} [isStorageType] 存放的类型（s存session；l存local；其它为暂存）
     * @memberof SStorageCore
     */
    setStorageItem(storage, key, value, isStorageType, options) {
        let _that = this;
        let config = _that.getOptions(options);
        if (!key || !storage) {
            return;
        }
        let obj = {
            size: 0
        };
        if (typeof value !== 'undefined') {
            obj['_key_'] = value;
            if (config.isSetTime || typeof config.timeout === 'number') {
                obj.ctime = (new Date()).getTime();
                obj.atime = (new Date()).getTime();
                if (typeof config.timeout === 'number' && config.timeout > -1) {
                    obj.timeout = config.timeout;
                }
            }
            try {
                obj.size = getBLen(JSON.stringify(value)); // 记录字符串长度
            }
            catch (error) {
            }
        }
        _that.updateStorageItem(storage, key, obj, isStorageType);
    }
    /**
     * 读取StorageItem的值
     *
     * @private
     * @param {(Storage)} storage
     * @param {string} key
     * @param {string} [isStorageType]
     * @returns {(IStorageItem | undefined)}
     * @memberof StorageCore
     */
    readStorageItem(storage, key, isStorageType) {
        let _that = this;
        if (!storage) {
            return undefined;
        }
        let _data;
        // 存储的类型
        if (isStorageType === 's' && StorageCore.isSess || isStorageType === 'l' && StorageCore.isLocal) {
            _data = storage.getItem(key);
            if (_data) {
                try {
                    _data = JSON.parse(_data);
                }
                catch (error) {
                    _that.removeStorageItem(storage, key);
                    _data = undefined;
                }
            }
        }
        else {
            _data = storage.getItem(key);
        }
        return _data;
    }
    /**
     * 获取数据
     *
     * @protected
     * @param {(Storage)} storage Storage对象
     * @param {string} key 获的Key名
     * @param {boolean} isDel 获取后是否删除
     * @param {string} isStorageType 获取的类型（s取session；l取local；其它为存临时）
     * @returns {any} 返回获取的值
     * @memberof SStorageService
     */
    getStorageItem(storage, key, isDel, isStorageType) {
        let _that = this;
        let _data = _that.readStorageItem(storage, key, isStorageType);
        // 取完后删除
        if (isDel && _data) {
            _that.removeStorageItem(storage, key);
            // 设置时间
        }
        else if (_data && _data.ctime) {
            let nowTime = (new Date()).getTime();
            // 超时移除
            if (typeof _data.timeout === 'number' && (_data.timeout < (nowTime - _data.ctime))) {
                _data = undefined;
                _that.removeStorageItem(storage, key);
                // 更新读取时间
            }
            else {
                _data.atime = (new Date()).getTime();
                _that.updateStorageItem(storage, key, _data, isStorageType);
            }
        }
        return _data && _data['_key_'] || undefined;
    }
    /**
     * 移除对应Key的数据
     *
     * @protected
     * @param {*} storage storage Storage对象
     * @param {string} key 删除的Key名
     * @memberof SStorageService
     */
    removeStorageItem(storage, key) {
        if (!key) {
            return;
        }
        storage.removeItem(key);
    }
    /**
     * 清空Storage对象
     *
     * @protected
     * @param {*} storage storage Storage对象
     * @memberof SStorageCore
     */
    clearStorage(storage) {
        try {
            storage && storage.clear();
        }
        catch (error) {
        }
    }
    /**
     * 获取sessionStorage对象
     *
     * @protected
     * @returns {sessionStorage} 返回sessionStorage对象
     * @memberof SStorageService
     */
    getSessionStorage() {
        let _session = this.getStorage('_session_', 's');
        this.getSessionStorage = () => {
            return _session;
        };
        return _session;
    }
    /**
     * 获取localStorage对象
     *
     * @protected
     * @returns {localStorage} 返回localStorage对象
     * @memberof SStorageService
     */
    getLocalStorage() {
        let _local = this.getStorage('_local_', 'l');
        this.getLocalStorage = () => {
            return _local;
        };
        return _local;
    }
    /**
     * 获取MemoryStorage对象
     *
     * @protected
     * @returns {localStorage} 返回localStorage对象
     * @memberof SStorageService
     */
    getMemoryStorage(options) {
        let storageKey = options && options.storageKey || '';
        let _local = this.getStorage('_data_' + storageKey, 'd');
        return _local;
    }
}
export { MemoryStorage };
