import storageClientFactory, { StorageClient, MemoryStorage, StorageCore } from './storageClient.imp';
export default storageClientFactory;
export { StorageClient, MemoryStorage, StorageCore };
