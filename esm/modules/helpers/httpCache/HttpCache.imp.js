import 'rxjs/add/operator/publishReplay';
import { Observable } from 'rxjs/Observable';
import { BaseAbstract } from '../../../library/basic/base.abstrat';
import callFn from '../../../library/util/callFn';
import { HTTP_CACHE_DEFAULT_CONFIG } from './constant';
export class HttpCache extends BaseAbstract {
    constructor(options) {
        super(options);
        this.defaultOption = HTTP_CACHE_DEFAULT_CONFIG;
        this.setDefaultOptions(options);
    }
    /**
     * httpClient执行
     * @param {*} cb 数据或回调方法
     * @param {*} options 参数选项
     * @memberof HttpCache
     */
    handler(cb, url, options) {
        let _that = this;
        let _options = _that._getCacheOption(url, options);
        return _that._cacheData(cb, _options);
    }
    /**
     * 获取缓存参数
     * @param {String} url 请求地址
     * @param {Object} options 请求选项
     * @returns {Object|undefined}
     * @memberof HttpClient
     */
    _getCacheOption(url, options) {
        let res;
        // 设置缓存
        let cacheData = options && options.cacheData;
        if (url && cacheData) {
            res = {
                cacheKey: url
            };
            if (cacheData instanceof Object) {
                res = Object.assign(res, cacheData);
            }
        }
        return res;
    }
    /**
     * 缓存请求数据
     * @param {*} cb 请求的回调
     * @param {object} options 缓存选项
     * @param {Function} filtersCacheData 过滤缓存数据
     * @returns {Observable<object>}
     * @memberof HttpCache
     */
    _cacheData(cb, options) {
        let _that = this;
        let res;
        let _options = _that.getOptions(options) || {};
        let $storage = _options.storage;
        let cacheKey = _options.cacheKey;
        if (cacheKey && $storage instanceof Object) {
            if (cacheKey) {
                res = $storage.getMemory(cacheKey, false, {
                    storageKey: 'temp_http'
                });
            }
            if (!res) {
                res = Observable.create((observer) => {
                    let memoryCacheData;
                    if (_options.needSend) { // 获取本地数据后还需要发请求的，查看内存是否有数据
                        memoryCacheData = _that._getNotNeedSendData(_options);
                    }
                    let cacheData = memoryCacheData || _that._getCacheData(_options);
                    // 返回缓存数据
                    if (cacheKey && cacheData) {
                        if (cacheData instanceof Object) {
                            cacheData._status = 304;
                        }
                        observer.next({
                            data: cacheData,
                            status: 304,
                            statusText: 'OK'
                        });
                    }
                    if (!cacheData || (!memoryCacheData && _options.needSend)) {
                        // 缓存接口请求
                        cacheKey && $storage && $storage.setMemory(cacheKey, res, {
                            storageKey: 'temp_http'
                        });
                        _that._getData(cb, _options, observer);
                    }
                    else {
                        observer.complete();
                    }
                }).publishReplay().refCount();
            }
        }
        else {
            res = callFn(cb);
        }
        return res;
    }
    /**
     * 获取接口数据
     * @private
     * @param {() => any} cb 获取数据的回调
     * @param {IHttpCacheParams} options 选项
     * @param {Observer<any>} observer Observer
     * @memberof HttpCache
     */
    _getData(cb, options, observer) {
        let _that = this;
        let _options = _that.getOptions(options) || {};
        let $storage = _options.storage;
        let cacheKey = _options.cacheKey;
        // 发送请求
        let res = callFn(cb);
        if (res && res.subscribe instanceof Function) {
            res.subscribe((back) => {
                observer.next(back);
                let _tempCacheData = callFn(_options.cacheFilter, [back]); // 过滤数据
                if (typeof _tempCacheData !== 'undefined') {
                    // 缓存接口数据
                    _that._setCacheData(_options, _tempCacheData);
                }
            }, (err) => {
                observer.error(err);
            }, () => {
                // 如果缓存接口请求则删除
                cacheKey && $storage && $storage.removeMemory(cacheKey, {
                    storageKey: 'temp_http'
                });
                observer.complete();
            });
        }
        else {
            // 如果缓存接口请求则删除
            cacheKey && $storage && $storage.removeMemory(cacheKey, {
                storageKey: 'temp_http'
            });
            observer.next(res);
            observer.complete();
        }
    }
    /**
     * 获取内存+本地份数据的内存数据
     * @param {*} options 缓存选项
     * @returns {object}
     * @memberof HttpCache
     */
    _getNotNeedSendData(options) {
        let _options = this.getOptions(options) || {};
        let $storage = _options.storage;
        let res;
        let cacheKey = _options.cacheKey;
        $storage && cacheKey && filterCacheType(_options.cacheType, (back) => {
            if (back && back.type === 'gm' && $storage) {
                res = $storage.getMemory(cacheKey, false);
            }
        });
        return res;
    }
    /**
     * 获取缓存数据
     * @param {object} options 缓存选项
     * @returns {object}
     * @memberof HttpCache
     */
    _getCacheData(options) {
        let _options = this.getOptions(options) || {};
        let $storage = _options.storage;
        let res;
        let cacheKey = _options.cacheKey;
        $storage && cacheKey && filterCacheType(_options.cacheType, (back) => {
            if ($storage) {
                switch (back && back.type) {
                    case 'cl':
                        $storage.removeLocal(cacheKey);
                        break;
                    case 'cs':
                        $storage.removeSession(cacheKey);
                        break;
                    case 'cm':
                        $storage.removeMemory(cacheKey);
                        break;
                    case 'l':
                        res = $storage.getLocal(cacheKey, false);
                        break;
                    case 's':
                        res = $storage.getSession(cacheKey, false);
                        break;
                    case 'm':
                        res = $storage.getMemory(cacheKey, false);
                        break;
                }
            }
        });
        return res;
    }
    /**
     * 设置缓存数据
     * @param {object} options 缓存选项
     * @param {any} data 缓存的数据
     * @memberof HttpCache
     */
    _setCacheData(options, data) {
        let _options = this.getOptions(options) || {};
        let $storage = _options.storage;
        let cacheKey = _options.cacheKey;
        $storage && cacheKey && filterCacheType(_options.cacheType, (back) => {
            if ($storage) {
                switch (back && back.type) {
                    case 'l':
                        $storage.setLocal(cacheKey, data, Object.assign({
                            isSetTime: true
                        }, options));
                        break;
                    case 's':
                        $storage.setSession(cacheKey, data, Object.assign({
                            isSetTime: true
                        }, options));
                        break;
                    case 'm':
                    case 'gm':
                        $storage.setMemory(cacheKey, data, Object.assign({
                            isSetTime: true
                        }, options));
                        break;
                }
            }
        });
    }
}
/**
 * 过滤类型
 * @param {string} cacheType 缓存类型
 * @param {Function} cb 类型回调，参数options.type:l为localStorage；s为sessionStorage；m为memoryStorage；gm为group memoryStorage
 */
function filterCacheType(cacheType, cb) {
    cacheType = ((cacheType || 'm') + '').toLowerCase();
    if (cb instanceof Function) {
        switch (cacheType) { // 清除类型
            case 'cl':
            case 'cm':
            case 'cs':
                cb({ type: cacheType });
                break;
            case 'cleanlocal':
                cb({ type: 'cl' });
                break;
            case 'cleansession':
                cb({ type: 'cs' });
                break;
            case 'cleanmemory':
                cb({ type: 'cm' });
                break;
            default:
                break;
        }
        switch (cacheType) {
            case 'cl':
            case 'l':
            case 'ml':
            case 'memorylocal':
            case 'local':
                cb({ type: 'l' });
                break;
            case 'cs':
            case 's':
            case 'ms':
            case 'memorysession':
            case 'session':
                cb({ type: 's' });
                break;
            case 'cm':
            case 'm':
            case 'memory':
                cb({ type: 'm' });
                break;
        }
        switch (cacheType) { // 因为获取数据的时候要分开，所以不能和m\memory放一起
            case 'ml':
            case 'memorylocal':
            case 'ms':
            case 'memorysession':
                cb({ type: 'gm' }); // 本地取后需要继续发请求，如果从内存中取数据则不需要发请求
                break;
        }
    }
}
/**
 * 实例化工厂方法
 * @export
 * @param {any} options 配置选项
 * @returns {HttpCache}
 */
export default function httpCacheFactory(options) {
    return HttpCache.instance(options);
}
