import { ClassProxy } from '../../../library/util/ClassProxy';
/**
 * 实例化工厂方法
 *
 * @export
 * @param {IAeAnimationConfig} options 配置选项
 * @returns {AeAnimation}
 */
export default function (options) {
    let _proxy = new ClassProxy();
    let res = {
        ext: 'zip',
        preload() {
            let _that = this;
            let initRes = _proxy.initProxy(() => {
                return new Promise((resolve, reject) => {
                    import('./aeZipPreset.imp').then(back => {
                        let aeFactory = back && back.default;
                        let _res = aeFactory(options);
                        resolve(_res);
                    }, reject);
                });
            }, 'preload')();
            _that.preload = () => {
                return initRes;
            };
            return initRes;
        }
    };
    _proxy.proxyHook(res, () => {
        return res.preload();
    }, ['preset']);
    return res;
}
