import { BaseAbstract } from '../../library/basic/base.abstrat';
import prefixInt from '../../library/util/prefixInt';
import formatDate from '../../library/util/formatDate';
import pastTimeCount from '../../library/util/pastTimeCount';
import callFn from '../../library/util/callFn';
export class CountDown extends BaseAbstract {
    constructor(_options) {
        super(_options);
        this.name = 'CountDown';
        // private options: ICountDownInitConfig|undefined;
        this.defaultOption = {
            /**
             * 开始时间
             */
            startTime: 0,
            /**
             * 当前时间
             */
            endTime: (new Date()).getTime(),
            /**
             * 计数时间（毫秒）
             */
            timeCount: 0,
            /**
             * 间隔时间计数（毫秒）
             */
            interval: 1000,
            /**
             * 时间格式类型(ms:毫秒；s:秒；m：分；h:时；d:天；M:月；y年)
             */
            formatType: 'ms',
            /**
             * 最小值
             */
            minNum: 0
            // minText: ''// 小于最小值的时候返回值的内容
            // callback: ITimeCountCallback, // 时间变动的回调(接收一个参数为：计算好的时间毫秒数);
        };
        let _that = this;
        this.setDefaultOptions(_options);
        let options = _that.getOptions();
        _that.timeCount(formatDate(options.startTime, 'yyyy/MM/dd hh:mm:ss'), formatDate(options.endTime, 'yyyy/MM/dd hh:mm:ss'), options.timeCount, options.interval, options.formatType, options.callback);
    }
    /**
     * 计算到关闭时间的剩余时间
     *
     * @export
     * @param {String} startTime 开始时间
     * @param {String} currentTime 当前时间
     * @param {Number} timeCount 计数时间（毫秒）
     * @param {Number} interval 间隔时间计数（毫秒）
     * @param {String} formatType 时间格式类型(ms:毫秒；s:秒；m：分；h:时；d:天；M:月；y年)
     * @param {Function} callback 时间变动的回调(接收一个参数为：计算好的时间毫秒数)
     * @param {String|Number} key 当前时间的插件的
     */
    timeCount(startTime, currentTime, timeCount, interval, formatType, callback) {
        let _that = this;
        let _options = _that.getOptions();
        // _that._interval && clearInterval(_that._interval);
        _that.stop();
        interval = (interval > 0 && interval) || 1000;
        if (!timeCount && (!startTime || !currentTime)) {
            _callback(timeCount);
            return;
        }
        let _pastTime = pastTimeCount(startTime, currentTime);
        let surplus = timeCount > 0 && timeCount - _pastTime || _pastTime;
        _callback(surplus);
        if (surplus > ((_options && _options.minNum) || 0)) {
            _that._interval = setInterval(() => {
                surplus = surplus - interval;
                if (surplus <= (_options && _options.minNum || 0)) {
                    _that.stop();
                    // _that._interval && clearInterval(_that._interval);
                    _callback(_options && _options.minNum || 0);
                }
                else {
                    _callback(surplus);
                }
            }, interval);
        }
        /**
         * 时间更改的回调
         *
         * @param {Number} _surplus 时间（毫秒）
         */
        function _callback(_surplus) {
            if (_surplus > ((_options && _options.minNum) || 0)) {
                _surplus = _that._formatDate(parseFloat(_surplus + ''), formatType);
            }
            else {
                if (_options && typeof _options.minText !== 'undefined') {
                    _surplus = _options && _options.minText;
                }
            }
            callFn(callback, [_surplus]);
            // if (callback instanceof Function) {
            //     callback(_surplus);
            // }
        }
    }
    /**
     * 格式化时间
     *
     * @param {Number} _time 毫秒
     * @param {String} formatType 时间格式类型(ms:毫秒；s:秒；m：分；h:时；d:天；M:月；y年)
     * @return {String} 返回格式化好的时间
     */
    _formatDate(_time, formatType) {
        let preFix = '';
        let fmt = '';
        // let initDate = new Date('1900/01/01 00:00:00');
        let endDate = new Date('1900/01/01 00:00:00');
        endDate.setTime(endDate.getTime() + _time);
        switch (formatType) {
            case 'ms':
                return (_time || '') || '';
            case 's':
                preFix = prefixInt(parseInt((_time / 1000) + '', 10), 2);
                break;
            case 'm':
                preFix = prefixInt(parseInt((_time / (1000 * 60)) + '', 10), 2) + ':'; // 计算剩余的分钟
                fmt = 'ss';
                break;
            case 'h':
                preFix = prefixInt(parseInt((_time / (1000 * 60 * 60)) + '', 10), 2) + ':'; // 计算剩余的分钟
                fmt = 'mm:ss';
                break;
            case 'd':
                let d = parseInt((_time / (1000 * 60 * 60 * 24)) + '', 10);
                preFix = d > 0 && d + '天' || '00天'; // 计算剩余的分钟
                fmt = 'hh时mm分ss秒';
                break;
            default:
                if (formatType) {
                    fmt = formatType;
                }
                break;
        }
        return preFix + (fmt && formatDate(endDate, fmt)); // 计算剩余的秒数
    }
    /**
     * 停止定时
     *
     * @memberof CountDown
     */
    stop() {
        let _that = this;
        _that._interval && clearInterval(_that._interval);
    }
}
/**
 * 实例化工厂方法
 *
 * @export
 * @param {IWeChatInitConfig} options 配置选项
 * @returns {WeChat}
 */
export default function (options) {
    return CountDown.instance(options);
}
