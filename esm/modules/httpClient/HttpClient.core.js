import axios from 'axios';
import 'rxjs/add/operator/publishReplay';
import { Observable } from 'rxjs/Observable';
import getBackData from '../../library/util/getBackData';
import serializeQueryParams from '../../library/util/serializeQueryParams';
import extend from '../../library/util/extend';
import callFn from '../../library/util/callFn';
import { HTTP_CLIENT_DEFAULT_CONFIG } from './constant';
import { HttpClientPlugin } from './HttpClient.plugin';
export class HttpClientCore extends HttpClientPlugin {
    constructor(options) {
        super(options);
        this.defaultOption = HTTP_CLIENT_DEFAULT_CONFIG;
        this.axios = axios;
        let _that = this;
        _that.setDefaultOptions(options);
        // _that.config = config;
        // _that.$httpCache = new HttpCache($storage);
        _that._axios = axios.create(_that.defaultOption.requestConfig);
        // this.axios = fetch;
        // this._axios = fetch;
        // _that._jsonp = fetchJsonp;
        // _that.$env = $env;
    }
    /**
     * 打包请求参数
     * @param {*} service api url
     * @param {Object} options 请求选项
     * @param {Object} extOpts 追加人请求选项
     * @return {Promise}
     */
    _packOptions(service, options, extOpts) {
        let _that = this;
        let config = _that.defaultOption || {};
        options = extend(true, config.requestConfig || {}, options, extOpts);
        return new Promise((resolve, reject) => {
            // 包装header
            options.headers = _that._packHeader(options) || {};
            // 包装body
            let _body = _that._packBody(options);
            delete options.body;
            if (_body) {
                options.body = _body;
            }
            /**
             * 调用过滤勾子
             */
            getBackData(config.optionsFilter || options, [{
                    service, options, config
                }]).then(tempOption => {
                if (typeof tempOption !== 'undefined') {
                    options = tempOption;
                }
                resolve(options);
            }, reject);
        });
    }
    /**
     * 包装请求header头信息
     * @param {Object} options 选项
     * @return {Object|undefined}
     */
    _packHeader(options) {
        let _that = this;
        let config = _that.defaultOption || {};
        let _options = extend(true, config.requestConfig || {}, options);
        return _options.headers;
    }
    /**
     * 包装请求body体信息
     * @param {Object} options 选项
     * @return {Object|undefined}
     */
    _packBody(options) {
        let config = this.defaultOption || {};
        let _options = extend(true, config.requestConfig || {}, options);
        if (_options && _options.data) {
            if (_options.data instanceof FormData) {
                _options.body = options.data;
            }
            else {
                if (_options.data instanceof Object) {
                    _options.body = JSON.stringify(_options.data);
                }
                else if (_options.data) {
                    _options.body = _options.data;
                }
            }
        }
        return _options.body;
    }
    /**
     * 解包返回的数据
     *
     * @param {*} back 返回的数据
     * @returns {Promise}
     * @memberof HttpClientCore
     */
    _unPackData(back) {
        return new Promise((resolve, reject) => {
            let data = {};
            if (back && back.json instanceof Function) { // 兼容fetch
                data.data = back.json();
            }
            else if (!back.data) { // 兼容fetch
                data.data = back;
            }
            else {
                data = back;
            }
            if (data && data.data instanceof Promise) {
                data.data.then((_data) => {
                    let food = {
                        data: _data
                    };
                    resolve(food);
                }, reject);
            }
            else {
                resolve(data);
            }
        });
    }
    /**
     * 设置数据缓存
     *
     * @private
     * @param {*} obs 请求的Observable
     * @param {string} url 接口名称
     * @param {IRequestParams} options 请求参数
     * @returns {Observable}
     * @memberof HttpClientCore
     */
    _handlerPlugin(obs, url, options) {
        let _that = this;
        let config = _that.defaultOption || {};
        return HttpClientCore.handlerPlugin(obs, url, options, config);
    }
    /**
     * 出错后尝试调用
     *
     * @private
     * @param {number} attempt 请求出错后尝试次数
     * @param {Error} err 错误信息
     * @param {string} service 接口服务地址
     * @param {IRequestParams} options 请求参数
     * @param {(options: IRequestParams) => void} cb 成功的回调
     * @param {(err: any) => void} errCb 出错的回调
     * @memberof HttpClientCore
     */
    _attemptFilter(attempt, err, service, options, cb, errCb) {
        let _that = this;
        let config = _that.defaultOption || {};
        if (attempt > 0) {
            getBackData(config.attemptFilter, [{
                    service: service,
                    options: options,
                    config: config,
                    err: err,
                    count: (config.attempt || 0) - attempt + 1
                }])
                .then(isAttempt => {
                if (isAttempt) {
                    let _opts = {};
                    if (isAttempt instanceof Object) {
                        _opts = isAttempt;
                    }
                    callFn(cb, [_opts]);
                }
                else {
                    callFn(errCb, [err]);
                }
            }, _err => {
                callFn(errCb, [_err]);
            });
        }
        else {
            callFn(errCb, [err]);
        }
    }
    /**
     * 发送请求
     * @param {Object} opts 请求选项
     */
    _send(service, url, options, opts, cb, errCb) {
        let _that = this;
        let config = _that.defaultOption;
        _that._packOptions(service, options, opts).then(_options => {
            let q;
            let _tempUrl = _options.url || url;
            if (_options.dataType === 'jsonp') {
                let _query = _options.params && serializeQueryParams(_options.params);
                q = _that._jsonp(_tempUrl + (_query ? '?' + _query : ''));
            }
            else {
                q = _that._axios(_tempUrl, _options, {
                    session: true,
                    query: _options.params,
                    status: _options.isLoading,
                    timeout: 60 * 60 * 5
                });
            }
            q.then((back) => {
                _that._unPackData(back).then(_res => {
                    getBackData(config.afterSend || _res, [{
                            service: service,
                            options: _options,
                            config: config,
                            data: _res
                        }]).then(_data => {
                        if (_data === undefined) {
                            _data = _res;
                        }
                        callFn(cb, [_data]);
                    });
                }, err => {
                    callFn(errCb, [err, _options]); // fetch请求报错
                });
            }, (err) => {
                callFn(errCb, [err, _options]); // fetch请求报错
            });
        });
    }
    /**
     * 发送请求
     * @param {*} url api url
     * @param {*} options 选项
     * @returns {Observable<object>}
     */
    send(url, options) {
        options = options || {};
        let _that = this;
        let config = _that.defaultOption || {};
        let _attempt = config.attempt || 0; // 请求出错后尝试次数
        let _url = _that.getApiUrl(options && options.apiServer || '', url);
        // 发送请求
        let _axios = Observable.create((observer) => {
            options.url = _url;
            getBackData(config.beforeSend || options, [{
                    service: url,
                    options: options,
                    config: config
                }]).then(_res => {
                _send(_res);
            }, err => {
                errBack(err, options); // fetch请求报错
            });
            /**
             * 发送请求
             * @param {Object} opts 请求选项
             */
            function _send(opts) {
                _that._send(url, _url, options, opts, (_data) => {
                    observer.next(_data);
                    observer.complete();
                }, (err, _opts) => {
                    errBack(err, _opts); // fetch请求报错
                });
            }
            /**
             * 错误回调
             * @param {Object} err 错误信息
             * @param {Object} _options 选项
             */
            function errBack(err, _options) {
                _that._attemptFilter(_attempt, err, url, _options, (_opts) => {
                    _attempt -= 1;
                    _send(_opts);
                }, (err) => {
                    afterSend(err, _options);
                });
            }
            /**
             * 出错结束
             * @param {Object} err 错误信息
             * @param {Object} _options 选项
             */
            function afterSend(err, _options) {
                getBackData(config.afterSend || err, [{
                        service: url,
                        options: _options,
                        config: config,
                        err: err
                    }]).then(back => {
                    if (back === undefined) {
                        back = err;
                    }
                    observer.error(back || err);
                    observer.complete();
                });
            }
        }).publishReplay().refCount();
        return _that._handlerPlugin(_axios, url, options);
    }
    /**
     * 获取api链接
     * @param {*} key api服务器key
     * @param {*} service 服务名称
     * @returns {string}
     */
    getApiUrl(key, service) {
        let server = '';
        let res = service || '';
        let config = this.defaultOption || {};
        let env = config.env || {};
        if (!/^\s*(https?:|\/\/)/.test(res)) { // 没有加协议的才可以拼接
            key = (key || config.apiServer || 'apiServer') + '';
            server = env[key] || '';
            res = server + res + '';
        }
        res = callFn(config.apiServerFilter || res, [{
                service: res
            }]); // 过滤url的过滤器
        return res;
    }
}
