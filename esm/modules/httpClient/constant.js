export const HTTP_CLIENT_DEFAULT_CONFIG = {
    requestConfig: {
        withCredentials: true,
        headers: {}
    },
    attempt: 0
};
