import callFn from '../../library/util/callFn';
import { BaseAbstract } from '../../library/basic/base.abstrat';
export class HttpClientPlugin extends BaseAbstract {
    constructor(options) {
        super(options);
        this.setDefaultOptions(options);
    }
    /**
     * 挂载插件
     */
    static use(plugin) {
        this._pluginList.push(plugin);
    }
    /**
     * 执行插件操作
     *
     * @static
     * @param {(Function|Observable<any>)} data 数据Observable/回调
     * @param {string} service 当前请求的
     * @param {IKeyValue} [options] 请求参数
     * @param {IHttpClientConfig} [config] 默认配置
     * @returns {Observable}
     * @memberof HttpClientPlugin
     */
    static handlerPlugin(data, service, options, config) {
        let _pluginList = this._pluginList;
        if (_pluginList && _pluginList.length) {
            return handler(data, _pluginList, 0);
        }
        else {
            return callFn(data);
        }
        /**
         * 执行插件操作
         *
         * @param {(Function|Observable<any>)} _data 数据Observable/回调
         * @param {IHttpClientPlugin[]} list 插件列表
         * @param {number} index 索引
         * @returns {(Function|Observable<any>)}
         */
        function handler(_data, list, index) {
            let item = list && list[index];
            if (item && item.handler instanceof Function) {
                let res = item.handler(function () {
                    return callFn(_data);
                }, service, options, config);
                return handler(res, list, index + 1);
            }
            else {
                return callFn(_data);
            }
        }
    }
}
/**
 * 插件列表
 *
 * @static
 * @type {IHttpClientPlugin[]}
 * @memberof HttpClientCore
 */
HttpClientPlugin._pluginList = [];
