import lazyLoadImgFactory, { LazyLoadImg } from './lazyLoadImg.imp';
export default lazyLoadImgFactory;
export { LazyLoadImg };
