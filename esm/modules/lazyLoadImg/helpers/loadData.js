import imgLoad from '../../../library/util/imgLoad';
import getBackData from '../../../library/util/getBackData';
import { IMT_LISTENER_STATE } from '../constant';
/**
 * 加载图片
 *
 * @param {Object} options 选项
 * @param {Listener} setState Listener
 * @param {Function} attemptFilter 尝试的时候的过滤器
 *
 */
export default function loadData(options, setState, attemptFilter) {
    let _options = options;
    let _attempt = 0;
    reload(_options.src);
    /**
     * 重新加载
     *
     * @param {*} src 图片地址
     */
    function reload(src) {
        if (src === _options.loadingSrc || !src) {
            setState(IMT_LISTENER_STATE.LOAD_END, {
                data: {
                    src: src,
                    naturalWidth: 0,
                    naturalHeight: 0
                }
            });
        }
        imgLoad(src || '', (image, src) => {
            /**
             * 开始加载时的回调
             */
            if (src !== _options.loadingSrc) {
                setState(IMT_LISTENER_STATE.START);
                setState(IMT_LISTENER_STATE.LOADED);
            }
        }).then(res => {
            // _listener.isDel = true;
            setState(IMT_LISTENER_STATE.LOAD_END, {
                data: res
            });
        }, (err) => {
            _attempt += 1;
            getBackData(attemptFilter, [src, _attempt]).then(backSrc => {
                if (backSrc) {
                    setState(IMT_LISTENER_STATE.RELOAD, {});
                    reload(backSrc);
                }
                else {
                    setState(IMT_LISTENER_STATE.LOAD_ERROR, {
                        data: err
                    });
                }
            });
        });
    }
}
