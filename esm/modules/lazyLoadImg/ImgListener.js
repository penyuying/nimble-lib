// import extend from '../utils/extend';
// import { LOAD_STATE, DEFAULT_CONFIG } from '../constant';
import { ViewRegion } from '../../library/helpers/viewRegion/ViewRegion';
import { IMG_LISTENER_DEFAULT_CONFIG, IMG_LISTENER_EVENT_TYPE, IMT_LISTENER_STATE } from './constant';
import getBackData from '../../library/util/getBackData';
import callFn from '../../library/util/callFn';
/**
 * 图片加载队列项
 *
 * @export
 * @class Listener
 * @extends {ViewRegion}
 */
export class ImgListener extends ViewRegion {
    /**
     * Creates an instance of ImgListener.
     * @param {HTMLElement} el 元素
     * @param {Object} options 选项
     * @param {Function?} options.loading 图片url
     * @param {String} options.src 图片url
     * @param {String?} options.loadingSrc 加载时的图片地址
     * @param {String?} options.errorSrc 出错后的图片地址
     * @param {Function?} options.loadData 加载数据的回调
     * @param {Function?} [options.attempt=1] 加载出错后尝试加载次数
     * @memberof ImgListener
     */
    constructor(el, options) {
        // let _options = extend({}, DEFAULT_CONFIG, options);
        super(el, options);
        /**
         * 默认数据
         *
         * @protected
         * @memberof ImgListener
         */
        this.defaultOption = IMG_LISTENER_DEFAULT_CONFIG;
        /**
         * 是否删除
         *
         * @memberof ImgListener
         */
        this.isDel = false;
        /**
         * 状态Wait;
         *
         * @memberof ImgListener
         */
        this.state = IMT_LISTENER_STATE.INIT;
        /**
         * 当前对应的滚动条数据
         *
         * @memberof ImgListener
         */
        this._$scroll = null;
        /**
         * 老的src
         *
         * @memberof ImgListener
         */
        this.oldSrc = null;
        let _that = this;
        _that.setDefaultOptions(options);
        _that.content = el;
        /**
         * 默认数据
         */
        // _that._options = _options;
        setTimeout(() => {
            _that._init && _that._init();
        }, 0);
    }
    /**
     * 加载
     *
     * @returns {Listener}
     * @memberof Listener
     */
    display() {
        let _that = this;
        let _options = _that.defaultOption;
        callFn(_options.loadData, [_options, (state, evt) => {
                _that._stateHandler(state, evt);
            }, (src, attempt) => {
                let _attempt = _options && _options.attempt || 0;
                if (_attempt && (_attempt - 1) > attempt) {
                    return getBackData(_options.attemptFilter, [src, attempt, _options]);
                }
                else {
                    return false;
                }
            }], _options);
        // if (_options.loadData instanceof Function) {
        //     _options.loadData();
        // }
        return _that;
    }
    /**
     * 当前在图中的时候加载
     * @param {Object} opts 选项
     *
     * @memberof Listener
     */
    viewDisplay(opts) {
        let _that = this;
        // START: 'loadStart', // 开始
        // LOADED: 'loaded', // 加载中
        // RELOAD: 'reload', // 重新加载
        if (_that.isDel || _that.state === IMT_LISTENER_STATE.START ||
            _that.state === IMT_LISTENER_STATE.LOADED ||
            _that.state === IMT_LISTENER_STATE.RELOAD) {
            return true;
        }
        const catIn = _that.checkInView(opts);
        if (!catIn) {
            return catIn;
        }
        _that.display();
        return catIn;
    }
    /**
     * 更新src
     *
     * @param {*} src 更新的src
     * @param {*} options 配置参数
     * @returns {Boolean} 是否有更新
     * @memberof Listener
     */
    update(el, options) {
        let _that = this;
        options = options || {};
        // let _options = _that.defaultOption;
        if (options.src === _that.oldSrc) {
            return false;
        }
        _that.content = el;
        _that.isDel = false;
        _that.oldSrc = options.src;
        _that.setDefaultOptions(options);
        _that._stateHandler(IMT_LISTENER_STATE.UPDATE);
    }
    /**
     * 销毁
     */
    destroyed() {
        let _that = this;
        _that._$scroll = null; // 当前对应的滚动条数据
        _that.oldSrc = null; // 老的src
        _that._stateHandler(IMT_LISTENER_STATE.DESTROYED);
        _that.content = null;
        _that._events = {};
    }
    /**
     * 设置状态、事件
     *
     * @param {*} state 状态
     * @param {*} evt 事件对像
     * @memberof Listener
     */
    _stateHandler(state, evt) {
        let _that = this;
        let _options = _that.defaultOption;
        _that._init && _that._init();
        _that.state = state;
        switch (state) {
            case IMT_LISTENER_STATE.DESTROYED:
            case IMT_LISTENER_STATE.LOAD_END:
                _that.isDel = true;
                break;
            default:
                break;
        }
        let _res = Object.assign(evt || {}, {
            type: state,
            options: _options,
            el: _that.content
        });
        if (_that.oldSrc) {
            _res.oldSrc = _that.oldSrc;
        }
        _that.$emit(IMG_LISTENER_EVENT_TYPE.LOADING, _res);
        callFn(_options.loading, [_res]);
    }
    /**
     * 初始化事件
     */
    _init() {
        let _that = this;
        _that._init = () => { };
        _that._stateHandler(IMT_LISTENER_STATE.INIT);
    }
}
