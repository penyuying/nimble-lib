import { INTERSECTION_DEFAULT_CONFIG } from './../../library/helpers/intersection/constant';
import hasIntersectionObserver from '../../library/util/hasIntersectionObserver';
import loadData from './helpers/loadData';
import imgRenderer from './helpers/imgRenderer';
/**
 * ImgListener默认配置
 */
export const IMG_LISTENER_DEFAULT_CONFIG = Object.assign({}, INTERSECTION_DEFAULT_CONFIG, {
    onlyCut: false,
    /**
     * 加载时的图片地址
     */
    loadingSrc: null,
    /**
     * 出错后的图片地址
     */
    errorSrc: null,
    /**
     * 加载时的回调
     */
    loading: null,
    /**
     * 加载出错后尝试加载次数
     */
    attempt: 1,
    /**
     * 尝试时的过滤器回调方法接收三个参数(src: 当前的src, count: 第几次尝试, options: 初始化时的选项)=> (Promise|src)
     */
    attemptFilter: null,
    /**
     * 加载时是否加包裹元素
     */
    isWrap: false,
    /**
     * 加载数据的回调
     */
    loadData: loadData,
    /**
     * 包裹的class
     */
    lazyWrapCla: null,
    /**
     * 当前加载项的class
     */
    lazyCla: null,
    /**
     * src属性名称
     */
    srcAttr: 'src',
    /**
     * 是否设置图片style
     */
    isSetStyle: false,
    /**
     * 当传入的高为HTMLElement时是否取元素高度作为图片裁剪
     */
    accessHeight: false
});
// lazyLoadImg默认配置参数
export const DEFAULT_CONFIG = Object.assign({}, IMG_LISTENER_DEFAULT_CONFIG || {}, {
    /**
     * 图片url过滤器
     */
    imgFilter: null,
    /**
     * IntersectionObserver模式
     */
    intersectionPattern: hasIntersectionObserver,
    /**
     * 渲染图片的回调
     */
    imgRenderer: imgRenderer
});
/**
 * ImgListener 事件
 *
 * @export
 * @enum {number}
 */
export var IMG_LISTENER_EVENT_TYPE;
(function (IMG_LISTENER_EVENT_TYPE) {
    IMG_LISTENER_EVENT_TYPE["LOADING"] = "loading"; // 正在加载图片的事件
})(IMG_LISTENER_EVENT_TYPE || (IMG_LISTENER_EVENT_TYPE = {}));
export const IMT_LISTENER_STATE = {
    /**
     * 初始化
     */
    INIT: 'init',
    /**
     * 更新图片
     */
    UPDATE: 'update',
    /**
     * 开始
     */
    START: 'loadStart',
    /**
     * 加载中
     */
    LOADED: 'loaded',
    /**
     * 重新加载
     */
    RELOAD: 'reload',
    /**
     * 加载结束
     */
    LOAD_END: 'loadEnd',
    /**
     * 加载出错
     */
    LOAD_ERROR: 'loadError',
    /**
     * 销毁
     */
    DESTROYED: 'destroyed'
};
