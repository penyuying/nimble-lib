import isPlainObject from '../../../library/util/isPlainObject';
export default isPlainObject;
