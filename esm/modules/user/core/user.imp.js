import extend from '../../../library/util/extend';
import { EVENT_TYPE } from '../constant';
import { UserMiniProgram, UserThirdparty, UserCore, UserBase } from './user.miniProgram';
/**
 * 用户模块
 *
 * @export
 * @class User
 * @extends {UserMiniProgram}
 */
export class User extends UserMiniProgram {
    /**
     * 用户模块
     * @param {IUserConfig} options 默认参数
     * @memberof User
     */
    constructor(options) {
        super(options);
        this.name = 'User';
        let _that = this;
        _that.setDefaultOptions(options);
        _that._resetLogin();
    }
    /**
     * 退出登录
     * @param {Object} options 配置参数
     * @param {Boolean} options.forceClearCookie 是否强制使cookie信息失效
     * @returns {Promise} 退出登录promise对象
     */
    logout(options) {
        const _that = this;
        _that._resetInit(options);
        return new Promise((resolve, reject) => {
            _that.isLogin().then(isLogin => {
                if (!isLogin) {
                    _that._setUserInfo(null);
                    resolve(true);
                    return;
                }
                _that._logout(options).then(resolve, reject);
            });
        });
    }
    /**
     * 判断用户是否已经登录
     * @param {Object} options 配置参数
     * @param {Boolean} options.isReset 是否强制重置用户登录状态信息
     * @returns {Promise} 用户登录状态Promise
     * @memberof UserCore
     */
    isLogin(options) {
        const _that = this;
        const _options = _that._resetInit(options);
        return new Promise((resolve, reject) => {
            if (_options.isReset) {
                _that._resetLogin();
            }
            let elapsed = Date.now() - _that._lastRun;
            let _loginState = _that._loginState;
            if (typeof _loginState !== 'boolean' || _options.isReset ||
                elapsed >= (_options.timeout || 0)) {
                _that._lastRun = Date.now();
                _that._getIsLogin().then((_isLogin) => {
                    _that.setLoginState(_isLogin);
                    resolve(_isLogin);
                }, reject);
            }
            else {
                _that.setLoginState(_loginState);
                resolve(_loginState);
            }
        });
    }
    /**
     * 用户取消登录
     * @memberof User
     * @exports
     */
    loginCancel() {
        const _that = this;
        _that.$emit(EVENT_TYPE.USER_CANCEL, {
            _handleType: 'cancel'
        });
    }
    /**
     * 获取用户信息
     * @param {Boolean} force 是否强制登录
     * @param {Object} options 配置参数
     * @param {Boolean} options.force 是否强制唤起登录页
     * @param {Boolean} options.isReset 是否强制重置登录状态
     * @returns {Promise<Object>} 返回用户信息promise
     * @memberof User
     * @exports
     */
    getUserInfo(force, options) {
        const _that = this;
        let _tempForce = {};
        if (force instanceof Object) {
            _tempForce = force;
        }
        else {
            _tempForce = {
                force: force || false
            };
        }
        const _options = _that._resetInit(extend(true, options, _tempForce));
        const _force = (_options && _options.force) || false;
        let res = new Promise((resolve, reject) => {
            _that._miniProgramLogin(_options).then(mUserInfo => {
                if (mUserInfo && mUserInfo.islogin) {
                    resolveCb(mUserInfo);
                }
                else {
                    _that._getAccess(_options).then((userInfo) => {
                        if (userInfo && userInfo.islogin) {
                            resolveCb(userInfo);
                        }
                        else {
                            _that.isLogin(_options).then(state => {
                                if (state) { // 已经登录的状态
                                    const _userInfo = _that._userInfo;
                                    if (_userInfo && _userInfo.islogin) {
                                        resolveCb(_userInfo);
                                    }
                                    else {
                                        _that._getUserInfo(_options, _userInfo).then(resolveCb);
                                    }
                                }
                                else {
                                    if (_force) { // 未登录状态且是强制登录
                                        _that.logout().then(() => {
                                            _that.login().then(resolveCb);
                                        });
                                    }
                                    else { // 未登录且不是强制登录
                                        this._successfilter(null).then(resolveCb, rejectCb);
                                    }
                                }
                            });
                        }
                    });
                }
            });
            /**
             * 完成的回调
             * @param {Object} data 用户信息
             */
            function resolveCb(data) {
                resolve(data);
            }
            /**
             * 出错的回调
             * @param {Object} err 错误信息
             */
            function rejectCb(err) {
                reject(err);
            }
        });
        res.then(() => {
            _that.currentOption = _that.getOptions();
        });
        return res;
    }
    /**
     * 设置登录状态和设置或清除localstorage(兼容老的)
     * @param {Object|Undefined} userInfo 用户信息（当type＝add时，需传userInfo，否则可以不传）
     * @returns {Promise}
     */
    setUserInfo(userInfo) {
        const _that = this;
        let res;
        console.warn('此方法将来可能会废弃，请使用新的方法setUserInfo和logout');
        if (userInfo && userInfo.islogin) {
            res = _that._setUserInfo(userInfo);
        }
        else {
            res = _that.logout();
        }
        return res;
    }
    /**
     * 绑定和手机号登录
     * @param {Object} params 登录参数
     * @param {String} params.phone 手机号
     * @param {String} params.channel 设备标识
     * @param {String} params.code 验证码
     * @return {Promise<IUserInfo>}
     */
    signInAndBind(params) {
        let _that = this;
        if (_that.isAccessFailure) {
            return _that.weChatBind(params);
        }
        else {
            return _that.signIn(params);
        }
    }
}
/**
 * 实例化User
 *
 * @export
 * @param {*} args 参数
 * @returns {User}
 */
export default function userFactory(options) {
    return new User(options);
}
export { UserMiniProgram, UserThirdparty, UserCore, UserBase };
