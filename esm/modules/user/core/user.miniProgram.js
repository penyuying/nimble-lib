import { USER_DATA_TYPE } from '../constant';
import { UserThirdparty, UserCore, UserBase } from './user.thirdparty';
import getParams from '../../utils/getParams';
import getBackData from '../../utils/getBackData';
/**
 * 小程序用户登录模块
 *
 * @export
 * @class UserMiniProgram
 * @extends {UserThirdparty}
 */
export class UserMiniProgram extends UserThirdparty {
    /**
     * 用户模块
     * @param {IUserConfig} options 默认参数
     * @memberof User
     */
    constructor(options) {
        super(options);
        this.name = 'UserMiniProgram';
        /**
         * 小程序登录参数
         */
        this._miniProgramLoginParams = {};
        let _that = this;
        _that.setDefaultOptions(options);
        const _params = Object.assign({}, getParams());
        let params = {};
        for (const key in _params) {
            if (_params.hasOwnProperty(key)) {
                const element = _params[key];
                params[key.toLowerCase()] = element;
            }
        }
        _that._miniProgramLoginParams = params;
        if (params && params.needlogin + '' === '1') {
            _that._miniProgramLogin(options);
        }
        const _options = this.getOptions(options || {});
        const device = _options.device || {};
        if (device.isMiniProgram && _options.isMiniProgram) {
            getBackData(_options.getWxSdk);
            try {
                _that.$on('success', (userInfo) => {
                    getBackData(_options.getWxSdk).then(() => {
                        if (window.wx && window.wx.miniProgram) {
                            window.wx.miniProgram.postMessage({
                                data: {
                                    invoke: 'SetUserInfo',
                                    message: userInfo || { islogin: false }
                                }
                            });
                        }
                    });
                });
            }
            catch (e) {
            }
        }
    }
    /**
     * 获授权登录数据
     * @param {IUserConfig} options 选项
     */
    _miniProgramLogin(options) {
        const _that = this;
        const _options = this.getOptions(options || {});
        const device = _options.device || {};
        let res = _that._miniProgramLoginPromise || new Promise((resolve, reject) => {
            if (!device.isMiniProgram || !_options.isMiniProgram) {
                resolve(null);
                return;
            }
            const params = _that._miniProgramLoginParams;
            // 静默登录回调
            if (params && params.sign && params.source && params.timestap && params.token) {
                setTimeout(() => {
                    _that._getAuthInfo({
                        needLogin: (params.needlogin + '' === '1') ? 1 : 0,
                        timeStap: params.timestap || '',
                        source: params.source || '',
                        sign: params.sign || '',
                        token: params.token || '',
                    }).then(res => {
                        let _userSession = res && (res.UserSession || res.usersession) || '';
                        if (_userSession) {
                            _that._logout(_options).then(() => {
                                _that._setLoginCookie(_userSession).then(() => {
                                    _that._getUserInfo(options, {
                                        usersession: _userSession
                                    }).then(resolve, () => {
                                        resolve(null);
                                    });
                                });
                            });
                        }
                        else {
                            resolve(null);
                        }
                    }, () => {
                        resolve(null);
                    });
                }, 0);
            }
            else {
                resolve(null);
            }
        });
        _that._miniProgramLoginPromise = res;
        return res;
    }
    /**
     * 获取微信小程序登录信息
     * @param {IUserConfig} options 选项
     */
    _getAuthInfo(options) {
        let _that = this;
        return new Promise((resolve, reject) => {
            _that._getData(USER_DATA_TYPE.GET_AUTH_INFO, options).then(resolve, reject);
        });
    }
}
export { UserThirdparty, UserCore, UserBase };
