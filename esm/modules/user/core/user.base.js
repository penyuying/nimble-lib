import { BaseAbstract } from '../../../library/basic/base.abstrat';
import callFn from '../../../library/util/callFn';
import getBackData from '../../../library/util/getBackData';
import { DEFAULT_CONFIG, STORAGE_KEY, EVENT_TYPE, USER_DATA_TYPE } from '../constant';
import extend from '../../utils/extend';
import setCookie from '../../utils/setCookie';
/**
 * 用户模块基础类
 *
 * @export
 * @class UserBase
 * @extends {DefaultOption}
 */
export class UserBase extends BaseAbstract {
    /**
     * 用户模块基础类
     * @param {IUserConfig} options 默认参数
     * @memberof UserBase
     */
    constructor(options) {
        super(options);
        /**
         * 用户信息
         *
         * @protected
         * @type {(IUserInfo|null)}
         * @memberof UserBase
         */
        this._userInfo = null;
        /**
         * 当前用户信息(拿来作比较用的)
         *
         * @private
         * @memberof UserBase
         */
        this._currentUserInfo = null;
        /**
         * 登录状态
         *
         * @protected
         * @type {(boolean|null)}
         * @memberof UserBase
         */
        this._loginState = null;
        /**
         * 当前选项
         *
         * @protected
         * @memberof UserBase
         */
        this.currentOption = DEFAULT_CONFIG;
        /**
         * 默认选项
         *
         * @protected
         * @memberof UserBase
         */
        this.defaultOption = DEFAULT_CONFIG;
        let _that = this;
        _that.setDefaultOptions(options);
        _that.currentOption = _that.getOptions();
        _that._resetInit(_that.defaultOption);
        _that._resetLogin();
    }
    /**
     * 重设置初始化配置
     *
     * @protected
     * @param {IUserConfig} [options]
     * @memberof UserBase
     */
    _resetInit(options) {
        const _that = this;
        let currentOption = _that.currentOption;
        _that.currentOption = options && _that.getOptions(_that.defaultOption, options || {}) || _that.currentOption;
        if (currentOption.force) {
            _that.currentOption.force = currentOption.force;
        }
        return _that.currentOption;
    }
    /**
     * 重置数据
     * @memberof UserBase
     */
    _resetLogin() {
        const _that = this;
        _that.currentOption = _that.defaultOption;
        _that._userInfo = {
            islogin: false
        };
        _that._loginState = null;
    }
    /**
     * 获取数据
     *
     * @param {*} type 类型
     * @param {*} param 参数
     * @param {*} headers 请求头
     * @returns {Promise}
     * @memberof CarCore
     */
    _getData(type, param, headers) {
        let _that = this;
        let _options = _that.currentOption;
        let getUserData = _options.getUserData;
        let args = [type, param, headers];
        switch (type) {
            case USER_DATA_TYPE.GET_BLACK_BOX: // 获取blackbox
                getUserData = _options.getBlackBox;
                args = [param, headers];
                break;
        }
        return getBackData(getUserData, args);
    }
    /**
     * 过滤传入的用户信息
     *
     * @param {*} userInfo 用户信息
     * @returns {Promise}
     * @memberof UserBase
     */
    _userInfoFilter(userInfo) {
        let _that = this;
        let _options = _that.currentOption;
        return getBackData(_options.filterUserInfoData || userInfo, userInfo);
    }
    /**
     * 用户信息获取完成
     *
     * @param {*} userInfo 车型数据
     * @returns {Promise}
     * @memberof UserCore
     */
    _successfilter(userInfo) {
        let _that = this;
        let _options = _that.currentOption;
        return new Promise((resolve, reject) => {
            getBackData(_options.successfilter || userInfo, userInfo).then(res => {
                let _userInfo = res || {
                    islogin: false
                };
                let isEqually = callFn(_options.diffData, [_that._currentUserInfo, _userInfo, _options.diffLoginKeys]);
                resolve(extend(true, _userInfo)); // 如果放在success事件后面，数据发生变化接口重新接口重新请求，这里候这里还没返回，会造再次请求的接口拿到的还是没登录
                if (!isEqually) {
                    _that._currentUserInfo = _userInfo;
                    _that.$emit(EVENT_TYPE.SUCCESS, extend(true, res));
                }
            }, reject);
        });
    }
    /**
     * 设置用户信息
     *
     * @param {*} userInfo 用户信息
     * @param {*} isNotLocal 是否不存储本地
     * @returns {Promise}
     * @memberof UserCore
     */
    _setUserInfo(userInfo, isNotLocal) {
        const _that = this;
        const _options = _that.currentOption;
        return new Promise((resolve, reject) => {
            _that._successfilter(userInfo).then(res => {
                _that._userInfo = res; // 设置用户信息
                // _that._currentUserInfo = res; // 设置用户信息
                _that.setLoginState(res && res.islogin);
                let _setStorage;
                if (!isNotLocal && res && res.islogin) {
                    res.usersession && setCookie(STORAGE_KEY.USER_COOKIE_KEY, res.usersession, {
                        time: 1000 * 60 * 60 * 24 * 30,
                        domain: _options.domain || ''
                    });
                    _setStorage = getBackData(_options.storage && _options.storage.setLocal, [STORAGE_KEY.USER_STORAGE_KEY, res]);
                }
                else {
                    _setStorage = getBackData(_options.storage && _options.storage.removeLocal, STORAGE_KEY.USER_STORAGE_KEY);
                }
                _setStorage.then(() => {
                    resolve(res);
                }, reject);
            }, reject);
        });
    }
    /**
     * 调用原生
     * @param {String} type 原生接口名称
     * @param {Object} params 接口请求参数
     * @returns {Promise<Object|String>} 结果数据
     */
    _toNative(type, params) {
        const _options = this.currentOption;
        return getBackData(_options.actionWithNative, [type, params]);
    }
    /**
     * 是否调用原生
     *
     * @param {String} type 调用类型
     * @return {Boolean}
     * @memberof UserBase
     */
    _isToNative(type) {
        const _options = this.currentOption;
        return callFn(_options.isToNativeFilter || false, [_options, type]);
    }
    /**
     * 验证数据是否符合正则匹配
     * @param {String|Number|Undefined|Null} data 需要验证的数据
     * @param {String} type 验证的类型
     * @returns {Boolean} 匹配结果
     * @memberof UserBase
     */
    validData(type, data) {
        const _options = this.currentOption;
        return callFn(_options.validData || false, [type, data]);
    }
    /**
     * 设置登录状态
     * @param {Boolean} state 登录状态
     */
    setLoginState(state) {
        const _state = state || false;
        this._loginState = _state;
    }
    /**
     * 提示消息
     *
     * @param {*} msgCont 消息内容
     * @param {*} param 类型|参数
     * @param {toast|alert} param.type 类型
     * @returns {Promise}
     * @memberof CarBase
     */
    message(msgCont, param) {
        let _that = this;
        let _options = _that.currentOption;
        return getBackData(_options.message, [msgCont, param]);
    }
    /**
     * 获取登录uuid
     *
     * @memberof UserBase
     */
    getUUID() {
        let _that = this;
        let _options = _that.currentOption || {};
        let _storage = _options && _options.storage;
        let uuid = callFn(_storage && _storage.getLocal, [STORAGE_KEY.LOGIN_UUID]);
        if (!uuid) {
            uuid = callFn(_options.generateUUID);
            uuid && callFn(_storage && _storage.setLocal, [STORAGE_KEY.LOGIN_UUID, uuid]);
        }
        _that.getUUID = () => {
            uuid && callFn(_storage && _storage.setLocal, [STORAGE_KEY.LOGIN_UUID, uuid]);
            return uuid;
        };
        return uuid;
    }
    /**
     * 加载同盾apiJs
     * @return {Promise}
     */
    getBlackBox() {
        let _that = this;
        let _options = _that.currentOption;
        return _that._getData(USER_DATA_TYPE.GET_BLACK_BOX, {
            uuid: _that.getUUID(),
            partner: _options.partner,
            appName: _options.appName
        });
    }
}
