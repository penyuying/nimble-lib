/**
 * 过滤用户信息数据
 *
 * @export
 * @param {*} userInfo 用户信息数据
 * @returns {Object}
 */
export default function filterUserInfoData(userInfo) {
    const data = userInfo || {};
    const userid = (data.UserID || userInfo.userid || data.userID || data.userId || data.UserId || '').replace(/[{}]/g, '');
    const result = {
        headUrl: userInfo.headUrl || userInfo.AvatarUrl || '',
        islogin: !!userid,
        userid: userid,
        nickname: data.NickName || data.Nickname || data.name || data.username || data.RealName || '',
        phone: data.phone || data.Phone || data.MobileNumber || data.TelNumber || '',
        name: data.name || data.username || data.RealName || data.NickName || '',
        usersession: data.usersession || data.UserSession || (data.userinfo && data.userinfo.usersession) || '',
        _newUser: true
    };
    return result;
}
