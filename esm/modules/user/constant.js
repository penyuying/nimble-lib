import generateUUID from '../../library/util/generateUUID';
import lStorage from '../../library/util/lStorage';
import deviceInfo from '../../library/util/browser/deviceInfo';
import diffData from '../../library/util/diffData';
import nativeDeviceFilter from '../../library/util/nativeDeviceFilter';
import filterUserInfoData from './helpers/filterUserInfoData';
import getBlackBox from '../../library/util/getBlackBox';
import validData from './helpers/validData';
// 比较的key列表
export const DIFF_KEYS = ['islogin', 'userid', 'phone', 'name', 'usersession'];
/**
 * 默认配置参数
 * @type {IUserConfig}
 */
export const DEFAULT_CONFIG = {
    domain: !/172\.|192\.|localhost/.test(location.host) && location.hostname.split('.').slice(-2).join('.') || '',
    /**
     * 是否启用获取微信小程序登录信息
     */
    isMiniProgram: false,
    appid: 'wx8bbeb62520772e01',
    // popup: {}, // 弹窗选项
    // message: null, // 消息提示
    /**
     * 同盾的partnerCode
     */
    partner: 'tuhu',
    /**
     * 同盾的app名称
     */
    appName: 'tuhu_yidongzhan',
    /**
     * 标识
     */
    channel: 'TongDunWx',
    /**
     * 登录事件ID
     */
    event_id: 'Login_web_20171031',
    /**
     * 登录的唯一标识
     */
    generateUUID: generateUUID,
    /**
     * 本地存储
     */
    storage: lStorage,
    // getUserData: null, // 获取接口数据的方法
    /**
     * 硬件信息
     */
    device: deviceInfo,
    /**
     * 如果用户没有登录是否强制唤起登录页
     */
    force: false,
    // successfilter: null, // 完成时过滤用户信息
    /**
     * 过滤用户信息
     */
    filterUserInfoData: filterUserInfoData,
    /**
     * 默认头像
     */
    userHeadImg: '',
    /**
     * 是否强制重置
     */
    isReset: false,
    /**
     * 是否和原生交互
     */
    isNative: true,
    // /**
    //  * 调用app的方法
    //  */
    // actionWithNative: undefined,
    /**
     * 缓存登录状态时间
     */
    timeout: 5 * 60 * 1000,
    /**
     * 是否强制使cookie信息失效
     */
    forceClearCookie: false,
    /**
     * 比较数据的方法
     */
    diffData: diffData,
    /**
     * 需要比较的key
     */
    diffLoginKeys: DIFF_KEYS,
    /**
     * 验证输入的方法
     */
    validData: validData,
    /**
     * 获取同盾blackbox
     */
    getBlackBox: getBlackBox,
    /**
     * 是否调用app
     */
    isToNativeFilter: (options, type) => {
        let res = nativeDeviceFilter(options, options && options.device);
        return res;
    }
};
/**
 * 调用app的api
 */
export const APP_API_TYPE = {
    /**
     * 获取用户信息
     */
    GET_USER_INFO: 'getUserInfo',
    /**
     * 唤起登录页
     */
    FORCE_LOGIN: 'forceLogin',
    /**
     * 验证登录
     */
    VALIDATE_LOGIN: 'validateLogin'
};
/**
 * 获取接口数据的类型
 */
export const USER_DATA_TYPE = {
    /**
     * 获取会员信息(用户头像)
     */
    GET_MEMBER_MALL_USER_INFO: 'GetMemberMallUserInfo',
    /**
     * 用户是否登录
     */
    IS_LOGIN: 'IsLogin',
    /**
     * 获取会员等级
     */
    GET_USER_INFO: 'GetUserInfo',
    /**
     * 退出时失效Cookie
     */
    LOGOUT_COOKIE: 'LogoutCookie',
    /**
     * 用户登录签名
     */
    SIGN_IN: 'SignIn',
    /**
     * 设置用户登录Cookie
     */
    SET_LOGIN_COOKIE: 'SetLoginCookie',
    /**
     * 获取验证码
     */
    GET_IDENTITY_CODE: 'GetIdentityCode',
    /**
     * 获取同盾BlackBox
     */
    GET_BLACK_BOX: 'GetBlackBox',
    /**
     * 授权code登录
     */
    AUTH_LOGIN: 'authLogin',
    /**
     * 授权登录失败绑定登录
     */
    AUTH_LOGIN_BINGDING: 'AuthLoginBingding',
    /**
     * 获取微信小程序登录信息
     */
    GET_AUTH_INFO: 'getAuthInfo',
    /**
     * 退出登录
     */
    SIGN_OUT: 'SignOut'
};
/**
 * 事件类型
 */
export const EVENT_TYPE = {
    /**
     * 完成时候的事件
     */
    SUCCESS: 'success',
    // CANCEL_CAR: 'cancelCar', // 取消选择车型的事件
    // LOGIN_PAGE: 'loginPage', // 唤起登录页事件
    /**
     * 定向页面, // 唤起登录页事件
     */
    REDIRECT: 'redirect',
    /**
     * 用户登录事件
     */
    USER_LOGIN: 'userLogin',
    /**
     * 用户取消登录事件
     */
    USER_CANCEL: 'userCancel',
    /**
     * 获取完用户信息
     */
    GET_USER_INFO: 'getUserInfo',
    /**
     * 退出登录事件
     */
    LOGOUT: 'logout',
    /**
     * wx绑定登录完成
     */
    WE_BIND_SUCCESS: 'weBindSuccess',
    /**
     * wx绑定登录完成
     */
    WX_AUTH_SUCCESS: 'wxAuthSuccess',
    /**
     * 获取微信小程序登录信息
     */
    MINI_PROGRAM_AUTH_INFO: 'miniProgramAuthInfo'
};
/**
 * 本地存储的key
 */
export var STORAGE_KEY;
(function (STORAGE_KEY) {
    /**
     * 用户登录的uuid
     */
    STORAGE_KEY["LOGIN_UUID"] = "_login_uuid";
    /**
     * 用户信息的key
     */
    STORAGE_KEY["USER_STORAGE_KEY"] = "_tuhu_user_key";
    /**
     * 用户信息的key
     */
    STORAGE_KEY["USER_COOKIE_KEY"] = "__user_cookie_key_";
})(STORAGE_KEY || (STORAGE_KEY = {}));
/**
 * 页面类型
 */
export const PAGE_TYPE = {
    /**
     * 登录页
     */
    LOGIN: 'login',
    /**
     * 途虎用户协议页面
     */
    AGREEMENT: 'agreement'
};
