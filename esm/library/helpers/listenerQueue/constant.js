/**
 * 图片加载状态
 */
export const LISTENER_STATE = {
    /**
     * 等待
     */
    INIT: 'init',
    /**
     * 更新图片
     */
    UPDATE: 'update'
    // START = 'loadStart', // 开始
    // LOADED = 'loaded', // 加载中
    // RELOAD = 'reload', // 重新加载
    // LOAD_END = 'loadEnd', // 加载结束
    // LOAD_ERROR = 'loadError', // 加载出错
    // DESTROYED = 'destroyed' // 销毁
};
/**
 * 事件类型
 *
 * @export
 * @enum {number}
 */
export var EVENT_TYPE;
(function (EVENT_TYPE) {
    /**
     * Listener推入队列事件
     */
    EVENT_TYPE["PUSH"] = "pushListener";
    /**
     * 更新Listener事件
     */
    EVENT_TYPE["UPDATE"] = "updateListener";
    /**
     * 移除Listener事件
     */
    EVENT_TYPE["REMOVE"] = "removeListener";
    /**
     * 清理队列
     */
    EVENT_TYPE["CLEAR"] = "clearQueue";
})(EVENT_TYPE || (EVENT_TYPE = {}));
