// import extend from '../utils/extend';
import { LISTENER_STATE, EVENT_TYPE } from './constant';
import { BaseAbstract } from '../../basic/base.abstrat';
import callFn from '../../util/callFn';
/**
 * 懒加载基础队列
 *
 * @export
 * @class ListenerQueue
 * @extends {DefaultOption}
 */
export class ListenerQueue extends BaseAbstract {
    /**
     * Creates an instance of ListenerQueue.
     * @param {Object} options 选项
     * @memberof ListenerQueue
     */
    constructor(options) {
        super(options);
        this.listenerQueue = []; // 监听队列
    }
    /**
     * 添加加载项到队列
     * @param {Object|Function} listener listener或回调
     * @return {Object}
     */
    addQueue(listener) {
        const _that = this;
        let _listener = callFn(listener, [], listener);
        // if (listener instanceof Function) {
        //     _listener = listener(el, _that.getOptions(_urls));
        // }
        if (listener) {
            _that.listenerQueue.push(_listener);
            // _listener.$on('loading', evt => {
            //     _that.$emit('loading', evt);
            // });
            _that.$emit(EVENT_TYPE.PUSH, _listener);
        }
        // _listener.viewLoad(); // 更新一下数据
        return {
            listener: listener,
            isUpdata: false // 是否为更新数据项
        };
    }
    /**
     * 更新队列加载项
     * @param {HTMLElement} content 当前img元素
     * @param {Object} data 选项
     * @param {Object|Function} genListener 生成listener的回调
     * @return {Object}
     */
    updateQueue(content, data, genListener) {
        const _that = this;
        let _listenerQueue = _that.listenerQueue;
        let _listener = _listenerQueue.find((item) => {
            return item && item.content === content;
        });
        if (_listener) {
            let _oldState = _listener.state;
            callFn(_listener.update, [content, data], _listener);
            if (_oldState !== LISTENER_STATE.INIT && _oldState !== LISTENER_STATE.UPDATE) {
                _that.$emit(EVENT_TYPE.UPDATE, _listener);
            }
            return {
                listener: _listener,
                isUpdata: true // 是否为更新数据项
            };
        }
        else {
            return _that.addQueue(genListener);
        }
    }
    /**
     * 清理队列(已加载完成项)
     *
     * @memberof ImgLazy
     */
    clearQueue() {
        const _that = this;
        let isClear = false;
        let list = _that.listenerQueue.filter((item, index) => {
            if (item.isDel) {
                isClear = true;
                _that.removeItem(index, item);
                // _that.$emit(EVENT_TYPE.REMOVE, item);
                // callFn(item.destroyed, [], item);
            }
            return !item.isDel;
        });
        if (isClear) {
            _that.listenerQueue = list;
            _that.$emit(EVENT_TYPE.CLEAR, list);
        }
    }
    /**
     * 移除单项listener
     *
     * @memberof ImgLazy
     */
    removeItem(index, item) {
        const _that = this;
        if (!item) {
            let items = _that.listenerQueue.splice(index, 1);
            if (items && items.length) {
                item = items[0];
            }
        }
        _that.$emit(EVENT_TYPE.REMOVE, item);
        callFn(item && item.destroyed, [], item);
    }
    /**
     * 获取listener
     *
     * @param {HTMLElement} el 当前元素
     * @returns {Object}
     * @memberof ListenerQueue
     */
    getListener(el) {
        let _that = this;
        let list = _that.listenerQueue;
        if (el && list && list.length) {
            for (let i = 0; i < list.length; i++) {
                const item = list[i];
                if (item && item.content === el) {
                    return {
                        listener: item,
                        index: i
                    };
                }
            }
        }
    }
}
export { LISTENER_STATE, EVENT_TYPE };
