import { VIEW_REGION_DEFAULT_CONFIG } from './constant';
import { BaseAbstract } from '../../basic/base.abstrat';
/**
 * 计算视图区域
 *
 * @export
 * @class ViewRegion
 * @extends {DefaultOption}
 */
export class ViewRegion extends BaseAbstract {
    /**
     * Creates an instance of ViewRegion.
     * @param {*} el 观察的元素
     * @param {Object} options 选项
     * @param {Number} [options.viewWidth=window.innerWidth] 加载屏视口宽度
     * @param {Number} [options.viewHeight=window.innerHeight] 加载屏视口高度
     * @param {Number} [options.preLoad=1] 预加载(屏)
     * @param {Number} [options.preLoadTop=0] 底部距离顶部的最小距离
     * @memberof ViewRegion
     */
    constructor(el, options) {
        super(options);
        /**
         * 默认数据
         *
         * @memberof ViewRegion
         */
        this.defaultOption = VIEW_REGION_DEFAULT_CONFIG;
        /**
         * 当前元素
         *
         * @type {(Element|null)}
         * @memberof ViewRegion
         */
        this.content = null;
        /**
         * 最后getBoundingClientRect的时间
         *
         * @type {number}
         * @memberof ViewRegion
         */
        this._lastRun = 0;
        let _that = this;
        /**
         * 默认数据
         */
        _that.setDefaultOptions(options);
        _that.content = el;
    }
    /**
     * 获取元素的位置
     * @param {Boolean} isReset 是否重新获取
     * @returns {Object}
     * @memberof ViewRegion
     */
    getRect(isReset) {
        let _that = this;
        let el = _that.content;
        if (el) {
            let now = Date.now();
            let elapsed = now - (_that._lastRun || 0);
            if (isReset || !_that._rect || elapsed > 100) {
                _that._lastRun = now;
                _that._rect = (el && el.getBoundingClientRect()) || {};
            }
        }
        return _that._rect;
    }
    /**
     * 判断是否在加载的区域
     *
     * @param {Object} opts 选项
     * @return {Boolean}
     * @memberof ViewRegion
     */
    checkInView(opts) {
        let _that = this;
        if (opts) { // 更新参数
            _that.setDefaultOptions(false, opts);
        }
        let _options = _that.defaultOption || {};
        let _rect = _that.getRect(!!opts);
        let _preLoad = _options.preLoad || 0;
        let _preLoadEnd = _preLoad;
        let _height = _options.viewHeight || 1024; // 视口高度
        let _width = _options.viewWidth || 800; // 视频宽度
        return (_rect.top >= (0 - (_height * _preLoad)) && // 顶部开始位置
            _rect.top < _height * _preLoadEnd && // 底部结束位置
            _rect.bottom > _options.preLoadTop) && // 垂直最小触发加载区域
            (_rect.left >= (0 - (_width * _preLoad)) && // 左边开始位置
                _rect.left < _width * _preLoadEnd && // 右边结束位置
                _rect.right > 0); // 水平最小触发加载区域
    }
}
