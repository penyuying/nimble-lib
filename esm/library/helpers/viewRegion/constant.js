export let VIEW_REGION_DEFAULT_CONFIG = {
    /**
     * 加载屏视口宽度
     */
    viewWidth: (typeof window !== 'undefined' && window.innerWidth) || 0,
    /**
     * 加载屏视口高度
     */
    viewHeight: (typeof window !== 'undefined' && window.innerHeight) || 0,
    /**
     * 底部距离顶部的最小距离（底部超过此距离才可加载）
     */
    preLoadTop: 0,
    /**
     * 预加载(屏)
     */
    preLoad: 1.3
};
// // 防止ios负一屏获取不到宽度的问题
// if (!VIEW_REGION_DEFAULT_CONFIG.viewHeight || !VIEW_REGION_DEFAULT_CONFIG.viewWidth) {
//     window.addEventListener('resize', resetSize);
// }
// /**
//  * 窗口缩放
//  */
// function resetSize() {
//     let w = window.innerWidth;
//     let h = window.innerHeight;
//     if (w > 10 || h > 10) {
//         VIEW_REGION_DEFAULT_CONFIG.viewHeight = h;
//         VIEW_REGION_DEFAULT_CONFIG.viewWidth = w;
//     }
//     if (w > 10 && h > 10) {
//         window.removeEventListener('resize', resetSize);
//     }
// }
