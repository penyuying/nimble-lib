import extend from '../../util/extend';
import { INTERSECTION_DEFAULT_CONFIG, INTERSECTION_EVENT_TYPE } from './constant';
import { ListenerQueue, EVENT_TYPE } from '../listenerQueue/ListenerQueue';
import callFn from '../../util/callFn';
/**
 * IntersectionObserver模式加载
 *
 * @export
 * @class Intersection
 * @extends {Intersection}
 */
export class Intersection extends ListenerQueue {
    /**
     * Creates an instance of Intersection.
     * @param {Object} options 选项
     * @param {Number} [options.viewWidth=window.innerHeight] 加载屏视口宽度
     * @param {Number} [options.viewHeight=window.innerWidth] 加载屏视口高度
     * @param {Number} [options.preLoad=1] 预加载(屏)
     * @param {Boolean} [options.intersectionPattern=hasIntersectionObserver] 是否为IntersectionObserver模式
     * @param {Object} [options.observerOptions={threshold: 0,rootMargin: '${y}px ${x}px ${y}px ${x}px'}] IntersectionObserver的选项
     * @memberof Intersection
     */
    constructor(options) {
        super(options);
        /**
         * 默认数据
         */
        this.defaultOption = INTERSECTION_DEFAULT_CONFIG;
        /**
         * IntersectionObserver对象
         */
        this._observer = null;
        /**
         * 默认数据
         */
        let _options = extend({}, INTERSECTION_DEFAULT_CONFIG, options);
        /**
         * 获取预加载的margin
         *
         * @param {Number} size 大小
         * @returns {Number}
         */
        let getMargin = (size) => {
            return Math.max(0, Math.ceil(size * ((_options.preLoad - 1) || 0)));
        };
        let x = getMargin(_options.viewWidth);
        let y = getMargin(_options.viewHeight);
        _options.observerOptions = extend({
            // 设置默认IntersectionObserver预加载范围
            rootMargin: `${y}px ${x}px ${y}px ${x}px`,
            threshold: 0
        }, _options.observerOptions);
        let _that = this;
        _that.setDefaultOptions(_options);
        _that._init();
    }
    _init() {
        let _that = this;
        let _observer = _that._initIntersectionObserver();
        _that.$on(EVENT_TYPE.PUSH, (item) => {
            if (_observer && item.content) {
                _observer.observe(item.content);
            }
            setTimeout(() => {
                callFn(item.viewDisplay, [null, false], item); // 修补部分安卓机不加载的问题
            }, 50);
        });
        // 更新队列时候的事件
        _that.$on(EVENT_TYPE.UPDATE, (item) => {
            callFn(item.viewDisplay, [], item);
        });
        _that.$on(INTERSECTION_EVENT_TYPE.UNOBSERVE, (el) => {
            el && _observer && _observer.unobserve(el);
        });
        _that.$on(EVENT_TYPE.REMOVE, (item) => {
            item.content && _observer && _observer.unobserve(item.content);
        });
    }
    /**
     * 初始化 IntersectionObserver
     * @return {IntersectionObserver}
     */
    _initIntersectionObserver() {
        let _that = this;
        let _options = _that.defaultOption;
        let res = null;
        try {
            res = new IntersectionObserver((evt) => {
                _that._loadObserver(evt);
            }, _options.observerOptions);
        }
        catch (error) {
            res = null;
        }
        _that._initIntersectionObserver = () => {
            return res;
        };
        return res;
    }
    /**
     * IntersectionObserver事件回调
     * @param {Array} entries 监听的列表
     */
    _loadObserver(entries) {
        let _that = this;
        let _options = _that.defaultOption;
        if (!entries) {
            return;
        }
        entries.forEach((entry) => {
            // alert(entries.length + '-->' + entry.isIntersecting + '-->' + JSON.stringify(entry));
            if (entry.isIntersecting || entry.intersectionRatio > 0 || (_options && _options.isShiftOut)) {
                _that.listenerQueue.forEach(listener => {
                    if (listener.content === entry.target) {
                        if (listener.isDel) {
                            return _that.$emit(INTERSECTION_EVENT_TYPE.UNOBSERVE, listener.content);
                        }
                        else {
                            if (entry.isIntersecting || entry.intersectionRatio > 0) {
                                callFn(listener.display, [null], listener);
                            }
                            else {
                                callFn(listener.shiftOut, [null], listener);
                            }
                        }
                    }
                });
            }
        });
        _that.clearQueue();
    }
}
