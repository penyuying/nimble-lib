import extend from '../util/extend';
import { EventModule } from '../helpers/EventModule';
import mountService from '../helpers/mountService';
export class BaseAbstract extends EventModule {
    /**
     * 构造函数
     */
    constructor(options) {
        super();
        /**
         * 名称
         */
        this.name = '';
        /**
         * 默认选项
         */
        this.defaultOption = {};
        this.setDefaultOptions(true, options);
    }
    /**
     * 单例实例化
     *
     * @static
     * @param {{[key: string]: any}} [options] 实例化参数
     * @returns
     * @memberof BaseAbstract
     */
    static singleton(options) {
        let _that = this;
        let res = _that._singleton || _that.instance(options);
        _that._singleton = res;
        return res;
    }
    /**
     * 实例化
     *
     * @static
     * @param {{[key: string]: any}} [options] 实例化参数
     * @returns
     * @memberof BaseAbstract
     */
    static instance(options) {
        let res = new this(options);
        return res;
    }
    /**
     * 安装
     *
     * @param {Vue} Vue Vue
     * @param {Object} options 选项
     * @memberof Service
     * @returns {Object}
     */
    static install(Vue, options) {
        let res = this.instance(options);
        if (Vue && Vue.use instanceof Function) {
            Vue.use(res, options);
        }
        return res;
    }
    /**
     * 安装
     *
     * @param {Vue} Vue Vue
     * @param {Object} options 选项
     * @memberof Service
     * @returns {Object}
     */
    install(Vue, options) {
        let _that = this;
        let name = mountService(Vue, _that);
        _that.setDefaultOptions((options && name && options[name]) || options);
        return _that;
    }
    setDefaultOptions(target) {
        let _that = this;
        let _options;
        let _target = true;
        if (typeof target === 'boolean') {
            _target = target;
            _options = arguments[1] || {};
        }
        else {
            _options = target;
        }
        if (_options) {
            _that.defaultOption = extend(_target, _that.defaultOption || {}, _options);
        }
    }
    getOptions(...args) {
        let _that = this;
        let _options = [{}, _that.defaultOption || {}];
        let _target = true;
        if (typeof args[0] === 'boolean') {
            _target = args[0];
            args.splice(0, 1);
        }
        if (args && args.length > 0) {
            _options = _options.concat(args);
        }
        return extend(_target, ..._options);
    }
    /**
     * 异步加载的时候预加载
     *
     * @returns {Promise}
     * @memberof BaseAbstract
     */
    preload() {
        let _that = this;
        return new Promise((resolve, reject) => {
            resolve(_that);
        });
    }
}
