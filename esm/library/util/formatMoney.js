import decimal from './decimal';
/**
 * 格式化价格
 * @export
 * @param {number} value 金额
 * @param {string|number} args 保留小数位数
 * @returns {string} 格式化后的价格
 */
export default function formatMoney(value, args, prefix) {
    let _res = '';
    value = value + '' === '0' ? '0' : (value || '') + '';
    if (typeof prefix === 'undefined') {
        prefix = '¥';
    }
    if (value) {
        let _fix = value.slice(0, 1);
        if (_fix === '¥') {
            value = value.slice(1);
        }
        if (args) {
            _res = decimal(value, args);
        }
        else {
            _res = value;
        }
        _res = prefix + _res;
    }
    return _res;
}
