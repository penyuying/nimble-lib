/**
 * 判断类型
 *
 * @export
 * @param {*} obj
 * @param {string} str
 * @returns
 */
export default function isType(obj, str) {
    let res = false;
    try {
        res = Object.prototype.toString.call(obj) === '[object ' + str + ']';
    }
    catch (error) { }
    return res;
}
