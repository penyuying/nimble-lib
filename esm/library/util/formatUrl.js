/**
 * 格式化url
 *
 * @param {string} [url] url
 * @returns {String}
 */
export default function formatUrl(url) {
    url = (url || '') + '';
    url = url || location.href;
    let divideArr = url.split('#');
    let resArr = [];
    let _hostArr = [];
    // 重新格式化url
    for (let i = 0; i < divideArr.length; i++) {
        const tempArr = (divideArr[i] || '').split('?');
        let splitArr = tempArr.splice(0, 1)[0].split('&');
        let _arr = splitArr.splice(0, 1);
        _arr[0] && _hostArr.push(_arr[0]);
        resArr = resArr.concat(splitArr); // ？前面带有&的参数
        resArr = resArr.concat(tempArr); // 里面含有&
    }
    let urlArr = [];
    if (_hostArr.length) {
        urlArr.push(_hostArr.join('#'));
    }
    if (resArr.length) {
        urlArr.push(resArr.join('&'));
    }
    return urlArr.join('?') || '';
}
