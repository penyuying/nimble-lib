import formatUrl from './formatUrl';
/**
 * 从URL中获取参数
 * @param {String} key 参数名（不指定则返回所有参数组成的对象）
 * @param {String} url 链接
 * @param {Boolean} noToText 不转换参数
 * @returns {Object | type} 返回的参数
 */
export default function getParams(key, url, noToText) {
    let res = {};
    let _urlArr = (formatUrl(url) || '').split('?');
    let paramString = _urlArr[1] || '';
    let paramArr = paramString.split('&');
    for (let i = 0; i < paramArr.length; i++) {
        const item = paramArr[i];
        let itemArr = item.split('=');
        if (itemArr) {
            // res[itemArr[0]] = itemArr[1];
            let _key = itemArr[0];
            if (_key) {
                let _val = noToText ? (decodeURIComponent(itemArr[1]) || '') : toText(decodeURIComponent(itemArr[1]) || '');
                if (res[_key]) {
                    if (res[_key] instanceof Array) {
                        res[_key].push(_val);
                    }
                    else {
                        res[_key] = [res[_key], _val];
                    }
                }
                else {
                    res[_key] = _val;
                }
            }
        }
    }
    if (key) {
        res = res[key];
    }
    return res;
}
function toText(text) {
    let docDIV = document.createElement('div');
    docDIV.innerText = text;
    return docDIV.innerHTML;
}
