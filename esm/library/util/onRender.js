import callFn from '../../modules/utils/callFn';
/**
 * 监听元素渲染
 *
 * @param {HTMLElement} el 元素
 * @param {Function} callback 完成后的回调
 * @param {Object} options 完成后的回调
 * @param {Number} options.count 次数
 * @param {Number} options.time 每次的间隔时间
 */
export default function onRender(el, callback, options) {
    options = options || {};
    let total = 0;
    let count = options.count || 5;
    let time = options.time || 50;
    let _parent = el && el.parentNode;
    next();
    /**
     * 调用下一次
     */
    function next() {
        _parent = _parent || (el && el.parentNode);
        if (total >= count || !el || (_parent && (_parent.clientHeight || _parent.clientWidth))) {
            callFn(callback, [el, total + 1]);
            _parent = null;
        }
        else {
            setTimeout(() => {
                callFn(next);
            }, time);
        }
        total += 1;
    }
}
