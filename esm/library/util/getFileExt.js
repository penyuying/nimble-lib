/**
 * 获取文件扩展名
 *
 * @export
 * @param {*} path 文件路径
 */
export default function getFileExt(path) {
    let _srcArr = ((path || '') + '').match(/\.([^.]+)$/);
    let ext = ((_srcArr && _srcArr[1] || '') + '').toLowerCase();
    return ext;
}
