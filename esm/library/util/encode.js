/**
 * 编码
 *
 * @export
 * @param {*} str 编码的字符串
 * @returns {String}
 */
export default function encode(str) {
    return str && encodeURIComponent(str)
        .replace(/%40/g, '@')
        .replace(/%3A/gi, ':')
        .replace(/%24/g, '$')
        .replace(/%2C/gi, ',')
        .replace(/%3B/gi, ';') || '';
}
