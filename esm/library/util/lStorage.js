let _ls = localStorage;
export class LStorage {
    setLocal(key, value) {
        if (!key || typeof value === 'undefined') {
            return;
        }
        _ls.setItem(key, JSON.stringify(value));
    }
    getLocal(key) {
        let res = _ls.getItem(key);
        if (res) {
            try {
                res = JSON.parse(res);
            }
            catch (error) {
                res = null;
                _ls.removeItem(key);
            }
        }
        return res;
    }
    removeLocal(key) {
        return _ls.removeItem(key);
    }
    clearLocal() {
        return _ls.clear();
    }
}
export default new LStorage();
