import toCamel from '../util/toCamel';
import toPascal from '../util/toPascal';
let elementStyle = document.createElement('div').style;
/**
 * 火狐浏览器也有webkit前缀
 */
let transformNames = ['Moz', 'webkit', 'O', 'ms'];
let _styleCache = {};
/**
 * 给css3样式加前缀
 *
 * @export
 * @param {any} style 样式名称
 * @param {Boolean} isFix 是否返回前缀
 * @returns {String} 加过前缀的样式
 */
export default function prefixStyle(style, isFix) {
    style = toCamel(((style || '') + '').replace(/\s+/g, ''));
    if (!isFix && _styleCache[style]) {
        return _styleCache[style];
    }
    if (!isFix && style in elementStyle) {
        return style;
    }
    let _txt = toPascal(style);
    for (let i = 0; i < transformNames.length; i++) {
        const _fix = transformNames[i];
        const _style = _fix + _txt;
        if (_style in elementStyle) {
            transformNames = [_fix];
            _styleCache[style] = _style;
            return isFix ? _fix : _style;
        }
    }
    return isFix ? '' : style;
}
