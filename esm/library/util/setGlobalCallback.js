import callFn from './callFn';
/**
 * 设置全局回调
 *
 * @param {*} fnName 回调的名称
 * @param {*} fn 回调的方法
 * @param {*} flag 当前注册全局方法的维一标识
 * @param {Boolean} [once = true] 是否只调用一次
 * @param {Object} [global = window] 需要挂载的对象
 */
export default function setGlobalCallback(fnName, fn, flag, once = true, global = window) {
    global = global || window;
    let oldFn = global[fnName];
    if (oldFn && flag) {
        oldFn.__flag = oldFn.__flag || {};
        if (oldFn.__flag[flag] === true) {
            return;
        }
    }
    global[fnName] = function (...args) {
        callFn(fn, args);
        callFn(oldFn, args);
        if (global[fnName].once) {
            if (oldFn instanceof Function) {
                global[fnName] = oldFn;
            }
            else {
                delete global[fnName];
            }
        }
    };
    global[fnName]._once = once;
    if (flag || oldFn && oldFn.__flag) {
        global[fnName].__flag = Object.assign({}, oldFn && oldFn.__flag || {});
        global[fnName].__flag[flag] = true;
    }
}
