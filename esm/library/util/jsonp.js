///<reference path="./jsonp.d.ts" />
import fetchJsonp from 'fetch-jsonp';
// declare namespace fetchJsonp {
//     interface Options {
//         timeout?: number;
//         jsonpCallback?: string;
//     }
//   }
// declare function fetchJsonp(url: RequestInfo, options?: fetchJsonp.Options): Promise<Response>;
/**
 * jsonp请求
 *
 * @param {*} args 参数
 * @returns {Promise}
 */
export default function jsonp(url, options) {
    return new Promise((resolve, reject) => {
        fetchJsonp(url, options).then(res => {
            if (res && res.json instanceof Function) {
                let _res = res.json();
                _res.then(resolve, reject);
            }
            else {
                reject(new Error('jsonp err'));
            }
        }, reject);
    });
}
