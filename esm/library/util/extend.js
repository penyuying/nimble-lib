import isType from './isType';
import isPlainObject from './isPlainObject';
function extend(...args) {
    let isDeep = true;
    let target;
    if (args instanceof Array) {
        target = args.slice(0, 1)[0];
    }
    if (typeof target === 'boolean') {
        isDeep = target;
        args.splice(0, 1);
        if (!isDeep) {
            target = args.splice(0, 1)[0];
        }
    }
    if (args && args.length < 1) {
        return typeof target === 'boolean' ? args[1] : target;
    }
    if (isDeep) {
        target = null;
    }
    for (let i = 0; i < args.length; i++) {
        let source = args[i];
        if (source instanceof Object) {
            if (isDeep) {
                if (!target || !(target instanceof Object)) {
                    target = (source instanceof Array) ? [] : {};
                }
                for (const key in source) {
                    if (source.hasOwnProperty(key)) {
                        const sourceItem = source[key];
                        if (isObjectAndArray(sourceItem)) {
                            let children = isObjectAndArray(target[key]) && target[key] || ((sourceItem instanceof Array) ? [] : {});
                            target[key] = extend(children, sourceItem);
                        }
                        else {
                            if (typeof sourceItem !== 'undefined') {
                                target[key] = sourceItem;
                            }
                            else if (typeof target[key] !== 'undefined') {
                                target[key] = sourceItem;
                            }
                        }
                    }
                }
            }
            else {
                if (!target || !(target instanceof Object)) {
                    target = source;
                }
                else {
                    target = Object.assign(target, source);
                }
            }
        }
    }
    return target;
    /**
     * 判断是否为对象
     *
     * @param {*} obj
     * @returns
     */
    function isObjectAndArray(obj) {
        // return obj && (isType(obj, 'Array') || isType(obj, 'Object') && (!obj.constructor || obj.constructor === Object)) || false;
        return obj && (isType(obj, 'Array') || isPlainObject(obj)) || false;
    }
}
export default extend;
