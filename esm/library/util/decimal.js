import prefixInt from './prefixInt';
/**
 * 格式化数字小数位
 * @export
 * @param {number} value 需要格式化的数字
 * @param {(string|number)} [args] 格式(如：1.2-3)
 * @returns {string}
 */
export default function decimal(value, args) {
    let _res = '';
    let _addend = value < 0 ? -5 : 5;
    value = value + '' === '0' ? '0' : (value || '') + '';
    if (value || value + '' === '0') {
        if (args || args + '' === '0') {
            let _decimal = getDecimal(value, args);
            let _isSuffix = _decimal.suffix || _decimal.suffix + '' === '0';
            if (_isSuffix) {
                let _pow = Math.pow(10, (_decimal.suffix || 0) + 1);
                value = (parseInt((parseFloat(value) * _pow + _addend) + '', 10) / _pow) + '';
            }
            let valueArray = value.split('.') || [];
            let _numArr = [];
            if (_decimal.prefix || _decimal.prefix + '' === '0') {
                _numArr.push(prefixInt(valueArray[0] || 0, _decimal.prefix) || 0);
            }
            else {
                _numArr.push(valueArray[0] || 0);
            }
            if (_isSuffix) {
                if (_decimal.suffix + '' !== '0') {
                    let ext = prefixInt(valueArray[1] || 0, _decimal.suffix, true) || 0;
                    _numArr[1] = ext;
                }
            }
            else {
                if (valueArray[1] !== undefined) {
                    _numArr.push(valueArray[1]);
                }
            }
            _res = _numArr.join('.');
        }
        else {
            _res = value;
        }
    }
    return _res;
}
/**
 * 计算小数位数
 *
 * @param {(number|string)} num 需要格式化的数字
 * @param {(number|string)} [count] 格式(如：1.2-3)
 * @returns {object}
 */
function getDecimal(num, count) {
    let _res = {};
    num = (num + '' === '0') ? '0' : num;
    num = (num || '') + '';
    if (count) {
        count = (count || '') + '';
        let numArr = num.split('.') || [];
        let countArr = count.split('.') || [];
        let _count = parseInt(countArr[0], 10) || 0;
        let _intLen = (numArr[0] && numArr[0].length) || 0;
        let _decimalLen = (numArr[1] && numArr[1].length) || 0;
        if (countArr.length === 1) {
            _res.suffix = _count;
        }
        else {
            let decimalArr = (countArr.length > 1 && countArr[1].split('-')) || [];
            if ((_count > 0 && _intLen) || _count > _intLen) {
                _res.prefix = _count;
            }
            if (decimalArr && decimalArr.length > 0) {
                let _decimalMin = parseInt(decimalArr[0], 10) || 0;
                let _decimalMax = parseInt(decimalArr[1], 10) || 0;
                if (!decimalArr[1] || _decimalMin > _decimalLen) {
                    _res.suffix = _decimalMin;
                }
                else if (_decimalLen && _decimalMax && (_decimalMax < _decimalLen)) {
                    _res.suffix = _decimalMax;
                }
            }
        }
    }
    else if (count === 0) {
        _res.suffix = 0;
    }
    return _res;
}
