/**
 * 自定义错误
 */
export default class MyError extends Error {
    constructor(message, options) {
        super(message);
        Object.assign(this, options);
        this.name = 'MyError';
    }
}
