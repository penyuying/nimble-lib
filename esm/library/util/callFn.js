/**
 * 执行回调函数
 *
 * @export
 * @param {Function} fn 回调
 * @param {*} arr 参数列表
 * @param {*} context this上下文
 * @returns {*}
 */
export default function callFn(fn, arr, context) {
    let res = fn;
    if (fn instanceof Function) {
        res = fn.apply(context, arr);
    }
    return res;
}
