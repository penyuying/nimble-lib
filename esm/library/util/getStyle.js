/**
 * 获取样式
 *
 * @export
 * @param {HTMLElement} el 元素
 * @param {String} prop 样式名称
 * @returns {String}
 */
export default function getStyle(el, prop) {
    let n = null;
    return (typeof getComputedStyle !== 'undefined'
        ? getComputedStyle(el, n).getPropertyValue(prop)
        : el.style[prop]) || '';
}
