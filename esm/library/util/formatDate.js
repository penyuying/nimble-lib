/**
 * 格式化时间
 *
 * @param {Date} _date 时间对象
 * @param {string} [fmt='yyyy-MM-dd hh:mm:ss'] 时间的格式
 * @returns {string}
 */
export default function formatDate(_date, fmt) {
    _date = toDate(_date);
    if (!_date) {
        return '';
    }
    fmt = (fmt || '') + '' || 'yyyy-MM-dd hh:mm:ss';
    let o = {
        'M+': _date.getMonth() + 1,
        'd+': _date.getDate(),
        'h+': _date.getHours(),
        'm+': _date.getMinutes(),
        's+': _date.getSeconds(),
        'q+': Math.floor((_date.getMonth() + 3) / 3),
        'S': _date.getMilliseconds() // 毫秒
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (_date.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (let k in o) {
        if (new RegExp('(' + k + ')').test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)));
        }
    }
    return fmt;
}
/**
 * 转换时间为Date对象
 *
 * @param {*} dateStr
 * @returns {Date}
 */
function toDate(dateStr) {
    let res = dateStr;
    let str = '';
    if (typeof dateStr === 'string') {
        str = dateStr.replace('-', '/');
    }
    else if (typeof dateStr === 'number') {
        str = dateStr;
    }
    if (str) {
        res = new Date(str);
    }
    return res;
}
