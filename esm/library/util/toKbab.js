import toCamel from './toCamel';
/**
 * 将驼峰转成用"-"分隔
 *
 * @export
 * @param {string} text 需要转换的文本
 * @param {string} splitStr 分隔符
 * @returns {string}
 */
export default function toKbab(text, splitStr) {
    let _splitStr = splitStr || '-|_';
    let _split = _splitStr.slice(0, 1);
    text = toCamel(text + '', splitStr);
    text = text.replace(/[A-Z]/g, $1 => {
        return (_split) + $1.toLowerCase();
    });
    return text;
}
