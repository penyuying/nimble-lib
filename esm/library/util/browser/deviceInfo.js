import browserInfo from './browserInfo';
/**
 * 硬件设备
 *
 * @returns {IBrowserDevice} IBrowserDevice
 * @memberof BrowserService
 */
const deviceInfo = (function () {
    let os = browserInfo;
    let ret = {
        mobile: false,
        pc: false,
        ios: os.ios || os.iPhone || os.iPad || false,
        weixin: os.weixin || false,
        isWeixin: os.weixin || false,
        iPad: os.iPad || false,
        isApp: false,
        appVersion: os.appVersion,
        browserVersion: os.browserVersion,
        isLightApp: false,
        kernel: os.kernel,
        isIphoneX: os.iPhone && (screen.height === 812 && screen.width === 375),
        android: os.android,
        isQqnews: os.qqnews,
        isMiniProgram: os.isMiniProgram // 微信小程序
    };
    if (os.mobile || os.ios || os.android || os.iPhone || os.iPad ||
        os.symbian || os.windowPhone || os.iPod || ret.isApp) { // 判断移动端
        ret.mobile = true;
    }
    else {
        ret.pc = true;
    }
    return ret;
})();
export default deviceInfo;
