/**
 * 浏览器内核版本
 *
 * @returns {IBrowserVersions} IBrowserVersions
 * @memberof BrowserService
 */
const browserInfo = (function () {
    const u = navigator.userAgent;
    return {
        trident: u.indexOf('Trident') > -1,
        presto: u.indexOf('Presto') > -1,
        webKit: u.indexOf('AppleWebKit') > -1,
        gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') === -1,
        mobile: !!u.match(/AppleWebKit.*Mobile.*/),
        ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
        // android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, // android终端或者uc浏览器
        android: u.indexOf('Android') > -1 || u.indexOf('Adr') > -1,
        iPhone: u.indexOf('iPhone') > -1,
        iPad: u.indexOf('iPad') > -1,
        symbian: u.indexOf('SymbianOS') > -1,
        windowPhone: u.indexOf('Windows Phone') > -1,
        iPod: u.indexOf('iPod') > -1,
        weixin: /MicroMessenger/i.test(u),
        browserVersion: (u.match(/version\/(.+?)\s/i) || [])[1],
        appVersion: '',
        kernel: (/tencenttraveler|qqbrowse/i.test(u) && 'x5') || '',
        qqnews: /qqnews/i.test(u),
        isMiniProgram: window.__wxjs_environment === 'miniprogram' || /miniProgram/i.test(u) // 微信小程序
    };
})();
export default browserInfo;
