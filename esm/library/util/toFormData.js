import isPlainObject from './isPlainObject';
import isType from './isType';
/**
 * 给数字加前缀
 *
 * @export
 * @param {(number|string)} value 内容
 * @param {number} [leng=1] 长度
 * @param {number} isExt 是否为后缀加0
 * @returns
 */
export default function toFormData(data) {
    let formData;
    if (data instanceof FormData) {
        formData = data;
    }
    else if (isPlainObject(data)) {
        for (const key in data) {
            if (data.hasOwnProperty(key)) {
                formData = formData || new FormData();
                let item = data[key];
                if (isPlainObject(item) || isType(item, 'Array')) {
                    item = JSON.stringify(item);
                }
                formData.append(key, item);
            }
        }
    }
    return formData;
}
