import {
    formatDate,
    pastTimeCount,
    CountDown
} from '../src/modules/index';

console.log(formatDate('2018-09-06 12:18'));
console.log(formatDate(new Date()));
console.log(pastTimeCount('2018-09-06 12:18', '2018-09-06 12:20'));
CountDown({
    startTime: new Date(),
    endTime: '2018-09-06 12:18',
    formatType: 'd',
    callback(data) {
        console.log(data);
    }
});
CountDown({
    startTime: '2018-08-06 12:18',
    endTime: '2018-09-06 12:18',
    formatType: 'h',
    callback(data) {
        console.log(data);
    }
});
CountDown({
    startTime: '2018-10-06 12:18',
    endTime: '2018-09-06 12:18',
    formatType: 'h',
    callback(data) {
        console.log(data);
    }
});
console.log(pastTimeCount('2018-09-06 12:18', '2018-09-06 12:20'));
